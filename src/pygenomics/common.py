"""Common routines for pygenomics package."""

import collections
import functools
from typing import Callable, Iterable, List, NewType, Optional, Tuple, TypeVar

from pygenomics import error

_T = TypeVar("_T")
_S = TypeVar("_S")


def partition(
    elements: Iterable[_T], predicate: Callable[[_T], bool]
) -> Tuple[List[_T], List[_T]]:
    """Partition items according to the specified predicate.

    :param elements: Elements to be partitioned.

    :param predicate: Predicate used to partition `elements`.

    :return: Tuple of two lists: the first list contains elements that
      passed `predicate`, the second one contains elements that failed
      `predicate`.

    """

    Acc = NewType("Acc", Tuple[List[_T], List[_T]])

    def process_element(acc: Acc, element: _T) -> Acc:
        passed, failed = acc
        list.append(passed if predicate(element) else failed, element)
        return Acc((passed, failed))

    init: Acc = Acc(([], []))
    return functools.reduce(process_element, elements, init)


def assoc_all(alist: List[Tuple[_T, _S]], key: _T) -> List[_S]:
    """Get all values from an association list for the specified key.

    :param alist: Association list.

    :param key: Key to get the values for.

    :return: Values from `alist` associated with `key`.

    """
    return [value for k, value in alist if k == key]


def assoc_single(alist: List[Tuple[_T, _S]], key: _T) -> Optional[_S]:
    """Get a single value from an association list for the specified key.

    :param alist: Association list.

    :param key: Key to get the value for.

    :return: Value from `alist` associated with `key` if there is a
      single associated value, `None` otherwise.
    """
    values = assoc_all(alist, key)
    return values[0] if len(values) == 1 else None


def assoc_unique(alist: List[Tuple[_T, _S]]) -> bool:
    """Check if keys of an association list are unique.

    :param alist: Association list.

    :return: Are keys of `alist` unique?

    """
    key_counts = collections.Counter(k for k, _ in alist)
    return all(k == 1 for k in dict.values(key_counts))


def assoc_extract(
    alist: List[Tuple[_T, _S]], key: _T
) -> Tuple[List[_S], List[Tuple[_T, _S]]]:
    """Extract values for a key from an association list.

    :param alist: Association list.

    :param key: Key to get the values for.

    :return: Tuple which first element is the list of values for `key`
      (it may be empty if there are no such values in `alist`) and the
      second element is `alist` without the extracted values.

    """
    key_values, others = partition(alist, lambda x: x[0] == key)
    return ([value for _, value in key_values], others)


def split2(line: str, sep: str) -> Optional[Tuple[str, str]]:
    """Split a line at the specified separator.

    :param line: Line to be split.

    :param sep: Separator to split the line at.

    :return: Tuple with left and right parts of the split line if the
      separator is present in the line, `None` otherwise.

    """
    parts = str.split(line, sep, 1)
    return (parts[0], parts[1]) if len(parts) == 2 else None


def str_to_int(line: str) -> Optional[int]:
    """Convert a string to an integer.

    :param line: Line possibly encoding an integer.

    :return: Integer represented by `line` if it encodes it, `None`
      otherwise.

    """
    try:
        return int(line)
    except ValueError:
        return None


def str_to_float(line: str) -> Optional[float]:
    """Convert a string to a floating point number.

    :param line: Line possibly encoding a floating point number.

    :return: Number represented by `line` if it encodes it, `None`
      otherwise.

    """
    try:
        return float(line)
    except ValueError:
        return None


def str_to_int_list(line: str, sep: str) -> Optional[List[int]]:
    """Convert a string to a list of integers.

    :param line: Line possibly encoding a series of integers.

    :param sep: Separator of numbers in `line`.

    :return: Integers represented by `line` if it encodes it, `None`
      otherwise.

    """
    result = [str_to_int(k) for k in str.split(line, sep)]
    filtered = [k for k in result if k is not None]
    return filtered if len(filtered) == len(result) else None


def is_proper_integer(line: str) -> bool:
    """Check if the line encodes a proper integer.

    :param line: Line possibly encoding an integer.

    :return: Does `line` encode a proper integer?

    """
    return str_to_int(line) is not None


def assoc_contains_none(alist: List[Tuple[_T, _S]], keys: List[_T]) -> bool:
    """Check that an association list contains none of specified keys.

    :param alist: Association list.

    :param keys: Keys which absence in `alist` is checked.

    :return: Does `alist` contain no specified keys?

    """
    key_set = frozenset(keys)
    return all(k not in key_set for k, _ in alist)


def optional_to_string(value: _T, default: str) -> str:
    """Convert a possibly missing value to a string.

    :param value: Value to be converted to a string.

    :param default: String to be returned if `value` is `None`.

    :return: String representation of `value` if `value` is not
      `None`, otherwise `default`.

    """
    return str(value) if value is not None else default


def optional_from_string(
    parser_fn: Callable[[str], Optional[_T]], line: str, na_value: str
) -> Tuple[bool, Optional[_T]]:
    """Parse an optional value from a string.

    :param parser_fn: Function that parses `line` if its value differs
      from `na_value`; the function should return `None` if the
      parsing fails.

    :param line: Line to be parsed.

    :param na_value: String that indicates a missing value.

    :return: Tuple that contains a flag indicating the parsing status
      (success or failure) and the parsing result.

    """
    if line == na_value:
        return (True, None)

    parsed_result = parser_fn(line)
    return (False, None) if parsed_result is None else (True, parsed_result)


class RangeError(error.Error):
    """Base class for range violation invariants."""


class Range:
    """Base classes for ranges in different formats."""

    _start: int
    _end: int

    def __init__(self, start: int, end: int):
        self._start = start
        self._end = end

    @property
    def start(self) -> int:
        """Start coordinate."""
        return self._start

    @property
    def end(self) -> int:
        """End coordinate."""
        return self._end

    def __repr__(self) -> str:
        return str.format(
            "{:s}(start={:d}, end={:d})", self.__class__.__name__, self.start, self.end
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Range):
            return NotImplemented
        return self._start == other._start and self._end == other._end


def strip_prefix_suffix(line: str, prefix: str, suffix: str) -> Optional[str]:
    """Remove the prefix and the suffix from a line.

    :param line: Line to remove the prefix and the suffix from.

    :param prefix: Prefix to remove from the line.

    :param suffix: Suffix to remove from the line.

    :return: Line without the specified prefix and suffix if it
      contains them, `None` if the prefix and the suffix overlap in
      the line or are missing from it.

    """

    # note the third part of the condition statement: it ensures that
    # the prefix and the suffix do not overlap
    if (
        str.startswith(line, prefix)
        and str.endswith(line, suffix)
        and len(line) >= len(prefix) + len(suffix)
    ):
        return line[len(prefix) : len(line) - len(suffix)]
    return None


def filter_list(values: List[Optional[_T]]) -> Optional[List[_T]]:
    """Check that the specified list does not contain `None` values.

    :param values: List that can contain `None` values.

    :return: Original list if it does not contain `None` values,
      `None` otherwise.

    """
    filtered_values = [k for k in values if k is not None]
    return filtered_values if len(values) == len(filtered_values) else None


def are_unique(values: Iterable[_T]) -> bool:
    """Check that values are unique."""
    return all(k == 1 for k in dict.values(collections.Counter(values)))
