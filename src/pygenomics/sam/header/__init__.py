"""SAM header class and routines."""

import itertools
import operator
from typing import Callable, Dict, List, NamedTuple, Optional, Set, Tuple, TypeVar

from pygenomics.sam.header import coline, error, hdline, pgline, rgline, sqline

_LINE_TAGS = frozenset(["@HD", "@SQ", "@RG", "@PG", "@CO"])


class Error(error.Error):
    """Base class for errors related to parsing a SAM file header."""


class IncorrectTags(Error):
    """Incorrect tags in SAM header lines."""

    tags: Set[str]

    def __init__(self, tags: Set[str]):
        super().__init__(tags)
        self.tags = tags


class IncorrectLine(Error):
    """Incorrect SAM header line."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


class MultipleHDLines(Error):
    """Multiple @HD lines are not allowed in a SAM header."""


_T = TypeVar("_T")


class Header(NamedTuple):
    """SAM file header."""

    file_data: Optional[hdline.HDLine]
    sequences: List[sqline.SQLine]
    read_groups: List[rgline.RGLine]
    programs: List[pgline.PGLine]
    comments: List[coline.COLine]

    @staticmethod
    def of_list(lines: List[str]) -> Optional["Header"]:
        """Parse a SAM file header from a list of lines.

        :param lines: Lines possibly encoding a SAM file header.

        :return: Parsed SAM header if `lines` properly encodes it,
          `None` otherwise.

        """

        def get_line_tag(line: str) -> str:
            """Get the SAM header line tag."""
            return str.split(line, "\t", 1)[0]

        grouped_lines = {
            tag: [k for _, k in lines]
            for tag, lines in itertools.groupby(
                sorted(
                    ((get_line_tag(k), k) for k in lines), key=operator.itemgetter(0)
                ),
                key=operator.itemgetter(0),
            )
        }

        if not frozenset.issuperset(_LINE_TAGS, set(dict.keys(grouped_lines))):
            raise IncorrectTags(
                set.difference(set(dict.keys(grouped_lines)), _LINE_TAGS)
            )

        if "HD" in grouped_lines and len(grouped_lines["HD"]) > 1:
            raise MultipleHDLines()

        def parse_lines(
            lines_dict: Dict[str, List[str]],
            tag: str,
            parser_fn: Callable[[str], Optional[_T]],
        ) -> Tuple[bool, List[_T]]:
            """Parse SAM header lines of the same kind."""
            if tag not in lines_dict:
                return (True, [])
            results = [parser_fn(k) for k in lines_dict[tag]]
            filtered_results = [k for k in results if k is not None]
            return (len(filtered_results) == len(results), filtered_results)

        hd_lines = parse_lines(grouped_lines, "@HD", hdline.HDLine.of_string)
        sq_lines = parse_lines(grouped_lines, "@SQ", sqline.SQLine.of_string)
        rg_lines = parse_lines(grouped_lines, "@RG", rgline.RGLine.of_string)
        pg_lines = parse_lines(grouped_lines, "@PG", pgline.PGLine.of_string)
        co_lines = parse_lines(grouped_lines, "@CO", coline.COLine.of_string)

        if not all([hd_lines[0], sq_lines[0], rg_lines[0], pg_lines[0], co_lines[0]]):
            return None

        return Header(
            hd_lines[1][0] if hd_lines[1] else None,
            sq_lines[1],
            rg_lines[1],
            pg_lines[1],
            co_lines[1],
        )
