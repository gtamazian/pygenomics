"""File-level metadata line (@HD) in the SAM header."""

from enum import Enum, auto
from typing import List, NamedTuple, Optional, Tuple

import pygenomics.common
from pygenomics.sam import datafield
from pygenomics.sam.header import common, error


class Error(error.Error):
    """Base class for exceptions related to file-level metadata lines."""


class FormatVersion(NamedTuple):
    """Major and minor version numbers."""

    major: int
    minor: int

    def __str__(self) -> str:
        return str.format("{:d}.{:d}", self.major, self.minor)

    @staticmethod
    def of_string(line: str) -> Optional["FormatVersion"]:
        """Parse the format version from a line.

        :param line: Line possibly encoding the format version.

        :return: Format version if `line` properly encodes it, `None`
          otherwise.

        """
        parts = pygenomics.common.split2(line, ".")
        if parts is not None:
            try:
                return FormatVersion(int(parts[0]), int(parts[1]))
            except ValueError:
                return None
        return None


class SortingOrder(Enum):
    """Sorting order of alignments in a SAM file."""

    UNKNOWN = auto()
    UNSORTED = auto()
    QUERYNAME = auto()
    COORDINATE = auto()

    def __str__(self) -> str:
        if self == SortingOrder.UNKNOWN:
            return "unknown"
        if self == SortingOrder.UNSORTED:
            return "unsorted"
        if self == SortingOrder.QUERYNAME:
            return "queryname"
        assert self == SortingOrder.COORDINATE
        return "coordinate"

    @staticmethod
    def of_string(line: str) -> Optional["SortingOrder"]:
        """Parse the sorting order value from a line.

        :param line: Line possibly encoding the sorting order value.

        :return: Sorting order if `line` properly encodes it, `None`
          otherwise.

        """
        if line == "unknown":
            return SortingOrder.UNKNOWN
        if line == "unsorted":
            return SortingOrder.UNSORTED
        if line == "queryname":
            return SortingOrder.QUERYNAME
        if line == "coordinate":
            return SortingOrder.COORDINATE
        return None


class AlignmentGrouping(Enum):
    """Types of the ways how alignments can be grouped."""

    NONE = auto()
    QUERY = auto()
    REFERENCE = auto()

    def __str__(self) -> str:
        if self == AlignmentGrouping.NONE:
            return "none"
        if self == AlignmentGrouping.QUERY:
            return "query"
        assert self == AlignmentGrouping.REFERENCE
        return "reference"

    @staticmethod
    def of_string(line: str) -> Optional["AlignmentGrouping"]:
        """Parse the type of alignment grouping from a line.

        :param line: Line possibly encoding the alignment grouping type.

        :return: Parsed alignment grouping type if `line` encodes it,
          `None` otherwise.

        """
        if line == "none":
            return AlignmentGrouping.NONE
        if line == "query":
            return AlignmentGrouping.QUERY
        if line == "reference":
            return AlignmentGrouping.REFERENCE
        return None


class EmptySubsortingOrder(Error):
    """Error for specifying an empty sub-sorting order string."""


class SubsortingOrder:
    """Sub-sorting order of alignments in a SAM file.

    The class guarantees that :py:attr:`sub_sort` is a non-empty
      string without spaces or tabs.

    """

    _sort_order: SortingOrder
    _sub_sort: str

    def __init__(self, sort_order: SortingOrder, sub_sort: str):
        if not sub_sort:
            raise EmptySubsortingOrder()
        self._sort_order = sort_order
        self._sub_sort = sub_sort

    @property
    def sort_order(self) -> SortingOrder:
        """Sorting order."""
        return self._sort_order

    @property
    def sub_sort(self) -> str:
        """Sub-sorting order."""
        return self._sub_sort

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SubsortingOrder):
            return NotImplemented
        return self.sort_order == other.sort_order and self.sub_sort == other.sub_sort

    def __repr__(self) -> str:
        return str.format(
            "{:s}(sort_order={:s}, sub_sort='{:s}')",
            self.__class__.__name__,
            repr(self.sort_order),
            self.sub_sort,
        )

    def __str__(self) -> str:
        return str.format("{}:{:s}", self.sort_order, self.sub_sort)

    @staticmethod
    def of_string(line: str) -> Optional["SubsortingOrder"]:
        """Parse the sub-sorting order value from a line.

        :param line: Line possibly encoding the sub-sorting order value.

        :return: Sub-sorting order if `line` properly encodes it,
          `None` otherwise.

        """
        parts = pygenomics.common.split2(line, ":")
        if parts is not None:
            sort_order = SortingOrder.of_string(parts[0])
            if sort_order is not None:
                try:
                    return SubsortingOrder(sort_order, parts[1])
                except EmptySubsortingOrder:
                    return None
        return None


class HDLineError(Error):
    """Base class for violations of invariants of :py:class:`HDLine`."""


class InconsistentSortingOrder(HDLineError):
    """Error indicating indicating sorting and sub-sorting orders."""

    sorting_order: SortingOrder
    subsorting_order: SubsortingOrder

    def __init__(self, sorting_order: SortingOrder, subsorting_order: SubsortingOrder):
        super().__init__(sorting_order, subsorting_order)
        self.sorting_order = sorting_order
        self.subsorting_order = subsorting_order


class HDLineOptionalFields(NamedTuple):
    """Optional fields for an HD header line."""

    sorting_order: Optional[SortingOrder]
    alignment_grouping: Optional[AlignmentGrouping]
    subsorting_order: Optional[SubsortingOrder]

    def __str__(self) -> str:
        return str.join(
            "\t",
            (
                []
                if self.sorting_order is None
                else [str.format("SO:{}", self.sorting_order)]
            )
            + (
                []
                if self.alignment_grouping is None
                else [str.format("GO:{}", self.alignment_grouping)]
            )
            + (
                []
                if self.subsorting_order is None
                else [str.format("SS:{}", self.subsorting_order)]
            ),
        )


class HDLine(common.HeaderLine):
    """File-level metadata line (@HD).

    The class guarantees that the sorting order values in fields
      :py:attr:`sorting_order` and :py:attr:`subsorting_order` are
      consistent with each other, no extra field has a predefined tag,
      and tags of extra fields are unique.

    """

    _format_version: FormatVersion
    _optional_fields: HDLineOptionalFields

    _code = "HD"
    _required_fields = frozenset(["VN"])
    _predefined_fields = frozenset(["VN", "SO", "GO", "SS"])

    def __init__(
        self,
        format_version: FormatVersion,
        optional_fields: HDLineOptionalFields,
        extra: List[datafield.OptionalDataField],
    ):
        # pylint: disable=too-many-arguments
        def check_sorting_orders() -> None:
            """Check that sorting and sub-sorting orders are consistent."""
            if (
                optional_fields.sorting_order is not None
                and optional_fields.subsorting_order is not None
                and optional_fields.sorting_order
                != optional_fields.subsorting_order.sort_order
            ):
                raise InconsistentSortingOrder(
                    optional_fields.sorting_order, optional_fields.subsorting_order
                )

        check_sorting_orders()

        super().__init__(extra)
        self._format_version = format_version
        self._optional_fields = optional_fields

    @property
    def format_version(self) -> FormatVersion:
        """Format version of the line."""
        return self._format_version

    @property
    def optional_fields(self) -> HDLineOptionalFields:
        """Optional fields of the HD header line."""
        return self._optional_fields

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, HDLine):
            return NotImplemented
        return (
            self.format_version == other.format_version
            and self.optional_fields == other.optional_fields
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(format_version={:s}, optional_fields={:s}, extra={:s})",
            self.__class__.__name__,
            repr(self.format_version),
            repr(self.optional_fields),
            repr(self.extra),
        )

    def __str__(self) -> str:
        optional_part = str(self.optional_fields)
        return str.join(
            "\t",
            [str.format("@{:s}\tVN:{}", self._code, self.format_version)]
            + ([] if not optional_part else [optional_part])
            + [str(k) for k in self.extra],
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["HDLine"]:
        """Parse file-level metadata line.

        :param line: Line possibly encoding file-level metadata.

        :return: Parsed file-level metadata if `line` properly
          encodes it, `None` otherwise.

        """

        def parse_predefined_fields(
            parts: List[datafield.DataField],
        ) -> Optional[Tuple[FormatVersion, HDLineOptionalFields]]:
            """Parse predefined fields.

            The function assumes that all predefined fields are present
              and there are no fields with duplicate tags. The
              function returns `None` it it failed to parse the
              fields.

            """
            predefined_dict = {k.tag: k.value for k in parts}

            vn_field = common.get_and_parse(
                predefined_dict, "VN", FormatVersion.of_string
            )
            so_field = common.get_and_parse(
                predefined_dict, "SO", SortingOrder.of_string
            )
            go_field = common.get_and_parse(
                predefined_dict, "GO", AlignmentGrouping.of_string
            )
            ss_field = common.get_and_parse(
                predefined_dict, "SS", SubsortingOrder.of_string
            )

            if not all([vn_field[0], so_field[0], go_field[0], ss_field[0]]):
                return None

            assert vn_field[1] is not None
            return (
                vn_field[1],
                HDLineOptionalFields(so_field[1], go_field[1], ss_field[1]),
            )

        parts = str.split(line, "\t")

        if parts[0] == "@" + cls._code:
            parsed_fields = cls.parse_fields(parts[1:])
            if parsed_fields is not None:
                predefined, extra = parsed_fields
                if cls.check_predefined_fields(predefined):
                    parsed = parse_predefined_fields(predefined)
                    if parsed is not None:
                        try:
                            return HDLine(parsed[0], parsed[1], extra)
                        except HDLineError:
                            return None
        return None
