"""Read group line (@RG) in the SAM header."""

import datetime
from enum import Enum, auto
from typing import Dict, List, NamedTuple, Optional, Tuple

import pygenomics.common
from pygenomics.sam import datafield
from pygenomics.sam.header import common


class Platform(Enum):
    """Types of sequencing platforms."""

    CAPILLARY = auto()
    DNBSEQ = auto()
    HELICOS = auto()
    ILLUMINA = auto()
    IONTORRENT = auto()
    LS454 = auto()
    ONT = auto()
    PACBIO = auto()
    SOLID = auto()

    def __str__(self) -> str:
        # pylint: disable=too-many-return-statements
        if self == Platform.CAPILLARY:
            return "CAPILLARY"
        if self == Platform.DNBSEQ:
            return "DNBSEQ"
        if self == Platform.HELICOS:
            return "HELICOS"
        if self == Platform.ILLUMINA:
            return "ILLUMINA"
        if self == Platform.IONTORRENT:
            return "IONTORRENT"
        if self == Platform.LS454:
            return "LS454"
        if self == Platform.ONT:
            return "ONT"
        if self == Platform.PACBIO:
            return "PACBIO"
        assert self == Platform.SOLID
        return "SOLID"

    @staticmethod
    def of_string(line: str) -> Optional["Platform"]:
        """Parse the sequencing platform value from a line.

        :param line: Line possibly encoding the sequencing platform.

        :return: Sequencing platform if `line` properly encodes it,
          `None` otherwise.

        """
        # pylint: disable=too-many-return-statements
        if line == "CAPILLARY":
            return Platform.CAPILLARY
        if line == "DNBSEQ":
            return Platform.DNBSEQ
        if line == "HELICOS":
            return Platform.HELICOS
        if line == "ILLUMINA":
            return Platform.ILLUMINA
        if line == "IONTORRENT":
            return Platform.IONTORRENT
        if line == "LS454":
            return Platform.LS454
        if line == "ONT":
            return Platform.ONT
        if line == "PACBIO":
            return Platform.PACBIO
        if line == "SOLID":
            return Platform.SOLID
        return None


class LibraryFields(NamedTuple):
    """Fields related to a sequenced library."""

    barcode: Optional[str]
    library: Optional[str]
    median_insert_size: Optional[int]
    sample: Optional[str]
    flow_order: Optional[str]
    key_seq_nucl_bases: Optional[str]

    def __str__(self) -> str:
        return str.join(
            "\t",
            ([] if self.barcode is None else [str.format("BC:{:s}", self.barcode)])
            + ([] if self.library is None else [str.format("LB:{:s}", self.library)])
            + (
                []
                if self.median_insert_size is None
                else [str.format("PI:{:d}", self.median_insert_size)]
            )
            + ([] if self.sample is None else [str.format("SM:{:s}", self.sample)])
            + (
                []
                if self.flow_order is None
                else [str.format("FO:{:s}", self.flow_order)]
            )
            + (
                []
                if self.key_seq_nucl_bases is None
                else [str.format("KS:{:s}", self.key_seq_nucl_bases)]
            ),
        )

    @staticmethod
    def of_dict(fields: Dict[str, str]) -> Optional["LibraryFields"]:
        """Parse library-related data fields from line parts."""

        bc_field = common.get_and_parse(fields, "BC", lambda x: x)
        lb_field = common.get_and_parse(fields, "LB", lambda x: x)
        pi_field = common.get_and_parse(fields, "PI", pygenomics.common.str_to_int)
        sm_field = common.get_and_parse(fields, "SM", lambda x: x)
        fo_field = common.get_and_parse(fields, "FO", lambda x: x)
        ks_field = common.get_and_parse(fields, "KS", lambda x: x)

        if not all(
            [
                bc_field[0],
                lb_field[0],
                pi_field[0],
                sm_field[0],
                fo_field[0],
                ks_field[0],
            ]
        ):
            return None

        return LibraryFields(
            bc_field[1], lb_field[1], pi_field[1], sm_field[1], fo_field[1], ks_field[1]
        )


class PlatformFields(NamedTuple):
    """Fields related to a sequencing platform."""

    platform: Optional[Platform]
    platform_model: Optional[str]
    platform_unit: Optional[str]

    def __str__(self) -> str:
        return str.join(
            "\t",
            ([] if self.platform is None else [str.format("PL:{:s}", self.platform)])
            + (
                []
                if self.platform_model is None
                else [str.format("PM:{:s}", self.platform_model)]
            )
            + (
                []
                if self.platform_unit is None
                else [str.format("PU:{:s}", self.platform_unit)]
            ),
        )

    @staticmethod
    def of_dict(fields: Dict[str, str]) -> Optional["PlatformFields"]:
        """Parse fields related to a sequencing platform."""

        pl_field = common.get_and_parse(fields, "PL", Platform.of_string)
        pm_field = common.get_and_parse(fields, "PM", lambda x: x)
        pu_field = common.get_and_parse(fields, "PU", lambda x: x)

        if not all([pl_field[0], pm_field[0], pu_field[0]]):
            return None

        return PlatformFields(pl_field[1], pm_field[1], pu_field[1])


class GeneralFields(NamedTuple):
    """General fields related to a read group."""

    seq_center: Optional[str]
    description: Optional[str]
    date: Optional[datetime.datetime]
    programs: Optional[str]

    def __str__(self) -> str:
        return str.join(
            "\t",
            (
                []
                if self.seq_center is None
                else [str.format("CN:{:s}", self.seq_center)]
            )
            + (
                []
                if self.description is None
                else [str.format("DS:{:s}", self.description)]
            )
            + (
                []
                if self.date is None
                else [str.format("DT:{:s}", datetime.datetime.isoformat(self.date))]
            )
            + ([] if self.programs is None else [str.format("PG:{:s}", self.programs)]),
        )

    @staticmethod
    def of_dict(fields: Dict[str, str]) -> Optional["GeneralFields"]:
        """Parse general fields."""

        def parse_datetime(line: str) -> Optional[datetime.datetime]:
            """Helper function to parse data and time objects."""
            try:
                return datetime.datetime.fromisoformat(line)
            except ValueError:
                return None

        cn_field = common.get_and_parse(fields, "CN", lambda x: x)
        ds_field = common.get_and_parse(fields, "DS", lambda x: x)
        dt_field = common.get_and_parse(fields, "DT", parse_datetime)
        pg_field = common.get_and_parse(fields, "PG", lambda x: x)

        if not all([cn_field[0], ds_field[0], dt_field[0], pg_field[0]]):
            return None

        return GeneralFields(cn_field[1], ds_field[1], dt_field[1], pg_field[1])


class RGLineOptionalFields(NamedTuple):
    """Optional fields for an RG header line."""

    general_part: GeneralFields
    library_part: LibraryFields
    platform_part: PlatformFields

    def __str__(self) -> str:
        general_str = str(self.general_part)
        library_str = str(self.library_part)
        platform_str = str(self.platform_part)

        return str.join(
            "\t",
            ([general_str] if general_str else [])
            + ([library_str] if library_str else [])
            + ([platform_str] if platform_str else []),
        )

    @staticmethod
    def of_dict(fields: Dict[str, str]) -> Optional["RGLineOptionalFields"]:
        """Parse optional predefined fields of a read group line."""

        general_part = GeneralFields.of_dict(fields)
        library_part = LibraryFields.of_dict(fields)
        platform_part = PlatformFields.of_dict(fields)

        if general_part is None or library_part is None or platform_part is None:
            return None

        return RGLineOptionalFields(general_part, library_part, platform_part)


class RGLine(common.HeaderLine):
    """Read group metadata line (@RG)."""

    _read_group_id: str
    _optional_fields: RGLineOptionalFields

    _code = "RG"
    _required_fields = frozenset(["ID"])
    _predefined_fields = frozenset(
        [
            "ID",
            "BC",
            "CN",
            "DS",
            "DT",
            "FO",
            "KS",
            "LB",
            "PG",
            "PI",
            "PL",
            "PM",
            "PU",
            "SM",
        ]
    )

    def __init__(
        self,
        read_group_id: str,
        optional_fields: RGLineOptionalFields,
        extra: List[datafield.OptionalDataField],
    ):
        super().__init__(extra)
        self._read_group_id = read_group_id
        self._optional_fields = optional_fields
        self._extra = extra

    @property
    def read_group_id(self) -> str:
        """Read group identifier."""
        return self._read_group_id

    @property
    def optional_fields(self) -> RGLineOptionalFields:
        """Optional fields of the RG header line."""
        return self._optional_fields

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, RGLine):
            return NotImplemented
        return (
            self.read_group_id == other.read_group_id
            and self.optional_fields == other.optional_fields
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(read_group_id={:s}, optional_fields={:s}, extra={:s})",
            self.__class__.__name__,
            repr(self.read_group_id),
            repr(self.optional_fields),
            repr(self.extra),
        )

    def __str__(self) -> str:
        optional_part = str(self.optional_fields)
        return str.join(
            "\t",
            [str.format("@{:s}\tID:{:s}", self._code, self.read_group_id)]
            + ([] if not optional_part else [optional_part])
            + [str(k) for k in self.extra],
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["RGLine"]:
        """Parse a read group line.

        :param parts: Line for a read group from a SAM header.

        :return: Parsed read group line if `line` properly encodes
          it, `None` otherwise.

        """

        def parse_predefined_fields(
            parts: List[datafield.DataField],
        ) -> Optional[Tuple[str, RGLineOptionalFields]]:
            """Parse predefined fields: the read ID field and the optional ones."""
            predefined_dict = {k.tag: k.value for k in parts}

            id_field = common.get_and_parse(predefined_dict, "ID", lambda x: x)
            optional_part = RGLineOptionalFields.of_dict(predefined_dict)

            if id_field[0] is None or optional_part is None:
                return None

            assert id_field[1] is not None
            return (id_field[1], optional_part)

        parts = str.split(line, "\t")

        if parts[0] == "@" + cls._code:
            parsed_fields = cls.parse_fields(parts[1:])
            if parsed_fields is not None:
                predefined, extra = parsed_fields
                if cls.check_predefined_fields(predefined):
                    parsed = parse_predefined_fields(predefined)
                    if parsed is not None:
                        try:
                            return RGLine(parsed[0], parsed[1], extra)
                        except common.Error:
                            return None
        return None
