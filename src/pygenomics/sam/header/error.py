"""Exceptions for subpackage `pygenomics.sam.header`."""

from pygenomics.sam import error


class Error(error.Error):
    """Base class for exceptions related to SAM headers."""
