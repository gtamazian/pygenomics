"""Common routines for SAM header lines."""

import collections
from typing import Callable, Dict, FrozenSet, List, Optional, Tuple, TypeVar

import pygenomics.common
from pygenomics.sam.datafield import DataField, OptionalDataField
from pygenomics.sam.header import error

ExtraFields = List[Tuple[str, str]]


class Error(error.Error):
    """Base class for exceptions related to SAM header lines."""


class DuplicateExtraFieldTag(Error):
    """Error indicating extra fields with duplicate tags."""

    tag: str

    def __init__(self, tag: str):
        super().__init__(self, tag)
        self.tag = tag


class PredefinedTagInExtraFields(Error):
    """Error indicating that an extra field has a predefined tag."""

    field: Tuple[str, str]

    def __init__(self, field: Tuple[str, str]):
        super().__init__(self, field)
        self.field = field


class HeaderLine:
    """SAM header lines."""

    _extra: List[OptionalDataField]

    _required_fields: FrozenSet[str] = frozenset()
    _predefined_fields: FrozenSet[str] = frozenset()

    @classmethod
    def _check_versus_predefined_tags(cls, fields: List[OptionalDataField]) -> None:
        """Check that no extra field has a predefined tag."""
        incorrect_tags = [
            (k.field.tag, k.field.value)
            for k in fields
            if k.field.tag in cls._predefined_fields
        ]
        if incorrect_tags:
            raise PredefinedTagInExtraFields(incorrect_tags[0])

    @classmethod
    def _check_extra_tag_uniqueness(cls, fields: List[OptionalDataField]) -> None:
        """Check that extra fields have unique tags."""
        duplicate_tags = [
            tag
            for tag, count in dict.items(
                collections.Counter(k.field.tag for k in fields)
            )
            if count > 1
        ]
        if duplicate_tags:
            raise DuplicateExtraFieldTag(duplicate_tags[0])

    def __init__(self, extra: List[OptionalDataField]):
        self._check_versus_predefined_tags(extra)
        self._check_extra_tag_uniqueness(extra)

        self._extra = extra

    @property
    def extra(self) -> List[OptionalDataField]:
        """Extra data fields."""
        return self._extra

    def __repr__(self) -> str:
        return str.format("{:s}(extra={:s})", self.__class__.__name__, repr(self.extra))

    @classmethod
    def parse_fields(
        cls, parts: List[str]
    ) -> Optional[Tuple[List[DataField], List[OptionalDataField]]]:
        """Parse predefined and extra fields of the file-level metadata line.

        :param parts: Parts of a line from the SAM header.

        :return: Tuple containing lists of predefined and extra
          fields if `parts` properly encode them, `None` otherwise.

        """
        split_parts = pygenomics.common.filter_list(
            [pygenomics.common.split2(k, ":") for k in parts]
        )
        if split_parts is None:
            return None

        predefined = pygenomics.common.filter_list(
            [
                DataField.of_string(part)
                for (tag, _), part in zip(split_parts, parts)
                if tag in cls._predefined_fields
            ]
        )
        extra = pygenomics.common.filter_list(
            [
                OptionalDataField.of_string(part)
                for (tag, _), part in zip(split_parts, parts)
                if tag not in cls._predefined_fields
            ]
        )

        if predefined is None or extra is None:
            return None

        return (predefined, extra)

    @classmethod
    def check_predefined_fields(cls, fields: List[DataField]) -> bool:
        """Check that parsed predefined data fields are correct.

        The function checks that all required fields are present and
          that there are no fields with duplicate tags.

        :param fields: List of predefined data fields.

        :return: Does the list of the predefined data fields contain
          all required fields and no duplicate tags?

        """
        tags = {k.tag for k in fields}
        return len(tags) == len(fields) and all(k in tags for k in cls._required_fields)

    @classmethod
    def get_predefined_fields(cls) -> FrozenSet[str]:
        """Get the set of predefined field tags."""
        return cls._predefined_fields


_T = TypeVar("_T")


def get_and_parse(
    pairs: Dict[str, str], key: str, parse_fn: Callable[[str], Optional[_T]]
) -> Tuple[bool, Optional[_T]]:
    """Search for a value with the specified key and parse it.

    :param pairs: Dictionary of key-value pairs.

    :param key: Key for searching the value.

    :param parse_fn: Function that parses the value and returns `None`
      if the parsing failed.

    :return: Tuple of two elements: the first element is the success
      parsing flag and the second element is the parsed value; the
      second element is `None` if the specified key is missing from
      the dictionary.

    """
    if key in pairs:
        value = parse_fn(pairs[key])
        return (value is not None, value)
    return (True, None)
