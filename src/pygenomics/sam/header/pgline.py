"""Program line (@PG) in the SAM header."""

from typing import List, NamedTuple, Optional, Tuple

from pygenomics.sam import datafield
from pygenomics.sam.header import common


class PGLineOptionalFields(NamedTuple):
    """Optional fields for a PG header line."""

    program_name: Optional[str]
    command_line: Optional[str]
    previous_program_id: Optional[str]
    description: Optional[str]
    version: Optional[str]

    def __str__(self) -> str:
        return str.join(
            "\t",
            (
                []
                if self.program_name is None
                else [str.format("PN:{:s}", self.program_name)]
            )
            + (
                []
                if self.command_line is None
                else [str.format("CL:{:s}", self.command_line)]
            )
            + (
                []
                if self.previous_program_id is None
                else [str.format("PP:{:s}", self.previous_program_id)]
            )
            + (
                []
                if self.description is None
                else [str.format("DS:{:s}", self.description)]
            )
            + ([] if self.version is None else [str.format("VN:{:s}", self.version)]),
        )


class PGLine(common.HeaderLine):
    """Program line (@PG)."""

    _program_id: str
    _optional_fields: PGLineOptionalFields

    _code = "PG"
    _required_fields = frozenset(["ID"])
    _predefined_fields = frozenset(["ID", "PN", "CL", "PP", "DS", "VN"])

    def __init__(
        self,
        program_id: str,
        optional_fields: PGLineOptionalFields,
        extra: List[datafield.OptionalDataField],
    ):
        super().__init__(extra)
        self._program_id = program_id
        self._optional_fields = optional_fields
        self._extra = extra

    @property
    def program_id(self) -> str:
        """Program record identifier."""
        return self._program_id

    @property
    def optional_fields(self) -> PGLineOptionalFields:
        """Optional fields of the PG header line."""
        return self._optional_fields

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, PGLine):
            return NotImplemented
        return (
            self.program_id == other.program_id
            and self.optional_fields == other.optional_fields
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(program_id='{:s}', optional_fields={:s}, extra={:s})",
            self.__class__.__name__,
            self.program_id,
            repr(self.optional_fields),
            repr(self.extra),
        )

    def __str__(self) -> str:
        optional_part = str(self.optional_fields)
        return str.join(
            "\t",
            [str.format("@{:s}\tID:{:s}", self._code, self.program_id)]
            + ([] if not optional_part else [optional_part])
            + [str(k) for k in self.extra],
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["PGLine"]:
        """Parse a program record line.

        :param line: Line possibly encoding a program record.

        :return: Parsed program record if `line` properly encodes it,
          `None` otherwise.

        """

        def parse_predefined_fields(
            parts: List[datafield.DataField],
        ) -> Optional[Tuple[str, PGLineOptionalFields]]:
            """Parse predefined fields of a @PG line."""
            predefined_dict = {k.tag: k.value for k in parts}

            id_field = common.get_and_parse(predefined_dict, "ID", lambda x: x)
            pn_field = common.get_and_parse(predefined_dict, "PN", lambda x: x)
            cl_field = common.get_and_parse(predefined_dict, "CL", lambda x: x)
            pp_field = common.get_and_parse(predefined_dict, "PP", lambda x: x)
            ds_field = common.get_and_parse(predefined_dict, "DS", lambda x: x)
            vn_field = common.get_and_parse(predefined_dict, "VN", lambda x: x)

            if not all(
                [
                    id_field[0],
                    pn_field[0],
                    cl_field[0],
                    pp_field[0],
                    ds_field[0],
                    vn_field[0],
                ]
            ):
                return None

            assert id_field[1] is not None
            return (
                id_field[1],
                PGLineOptionalFields(
                    pn_field[1], cl_field[1], pp_field[1], ds_field[1], vn_field[1]
                ),
            )

        parts = str.split(line, "\t")

        if parts[0] == "@" + cls._code:
            parsed_fields = cls.parse_fields(parts[1:])
            if parsed_fields is not None:
                predefined, extra = parsed_fields
                if cls.check_predefined_fields(predefined):
                    parsed = parse_predefined_fields(predefined)
                    if parsed is not None:
                        try:
                            return PGLine(parsed[0], parsed[1], extra)
                        except common.Error:
                            return None
        return None
