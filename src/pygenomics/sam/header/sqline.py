"""Reference sequence dictionary line (@SQ) in the SAM header."""

from enum import Enum, auto
from typing import List, NamedTuple, Optional, Tuple

import pygenomics.common
from pygenomics.sam import datafield, rname
from pygenomics.sam.header import common, error


class Error(error.Error):
    """Base class for exceptions related to sequence dictionary lines."""


class SequenceLengthError(Error):
    """Error indicating an incorrect sequence length value."""

    length: int

    def __init__(self, length: int):
        super().__init__(length)
        self.length = length


class SequenceLength:
    """Reference sequence length (tag LN).

    The class guarantees that the length value is in the range from 1
    to 2^31 - 1.

    """

    _value: int

    def __init__(self, length: int):
        if not 1 <= length <= 2**31 - 1:
            raise SequenceLengthError(length)
        self._value = length

    @property
    def value(self) -> int:
        """Sequence length value."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SequenceLength):
            return NotImplemented
        return self.value == other.value

    def __repr__(self) -> str:
        return str.format("{:s}(value={:d})", self.__class__.__name__, self.value)

    def __str__(self) -> str:
        return str.format("{:d}", self.value)


Interval = Tuple[int, int]


class IncorrectRegion(Error):
    """Error indicating incorrect start and end coordinates of a region."""

    interval: Interval

    def __init__(self, interval: Interval):
        super().__init__(interval)
        self.interval = interval


class AlternativeLocusRegion:
    """Alternative locus region on a chromosome."""

    _chrom: rname.RSeqName
    _region: Optional[Interval]

    def __init__(self, chrom: rname.RSeqName, region: Optional[Interval]):
        def check_region() -> None:
            """Check if the specified region is correct."""
            if region is not None and not 0 <= region[0] < region[1]:
                raise IncorrectRegion(region)

        check_region()
        self._chrom = chrom
        self._region = region

    @property
    def chrom(self) -> rname.RSeqName:
        """Chromosome of the alternative locus region."""
        return self._chrom

    @property
    def region(self) -> Optional[Tuple[int, int]]:
        """Start and end coordinates of the alternative locus region."""
        return self._region

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AlternativeLocusRegion):
            return NotImplemented
        return self.chrom == other.chrom and self.region == other.region

    def __repr__(self) -> str:
        return str.format(
            "{:s}(chrom={:s}, region={:s})",
            self.__class__.__name__,
            repr(self.chrom),
            repr(self.region),
        )

    def __str__(self) -> str:
        return (
            str.format(
                "{:s}:{:d}-{:d}", self.chrom.value, self.region[0], self.region[1]
            )
            if self.region is not None
            else self.chrom.value
        )

    @staticmethod
    def of_string(line: str) -> Optional["AlternativeLocusRegion"]:
        """Parse the alternative locus region from a line.

        :param line: Line possibly encoding the alternative locus region.

        :return: Parsed alternative locus region if `line` properly
          encodes it, `None` otherwise.

        """
        parts = str.split(line, ":", 1)

        try:
            chrom = rname.RSeqName(parts[0])
        except rname.Error:
            return None

        if len(parts) == 1:
            return AlternativeLocusRegion(chrom, None)

        coordinates = pygenomics.common.split2(parts[1], "-")
        if coordinates is None:
            return None
        try:
            start = int(coordinates[0])
            end = int(coordinates[1])
        except ValueError:
            return None

        return AlternativeLocusRegion(chrom, (start, end))


class AlternativeLocus(NamedTuple):
    """Alternative locus specification (tag AH)."""

    region: Optional[AlternativeLocusRegion]

    def __str__(self) -> str:
        return str(self.region) if self.region is not None else "*"

    @staticmethod
    def of_string(line: str) -> Optional["AlternativeLocus"]:
        """Parse the alternative locus from a line.

        :param line: Line possibly encoding the alternative locus.

        :return: Parsed alternative locus if `line` properly encodes
          it, `None` otherwise.

        """
        if line == "*":
            return AlternativeLocus(None)
        chrom_region = AlternativeLocusRegion.of_string(line)
        return AlternativeLocus(chrom_region) if chrom_region is not None else None


class AltRefSeqNames(NamedTuple):
    """Alternative reference sequence names (tag AN)."""

    seqnames: List[rname.RSeqName]

    def __str__(self) -> str:
        return str.join(",", (k.value for k in self.seqnames))

    @staticmethod
    def of_string(line: str) -> Optional["AltRefSeqNames"]:
        """Parse the list of alternative reference sequence names from a line.

        :param line: Line possibly encoding the list of alternative
          reference sequence names.

        :return: Parsed list of alternative reference sequence names
          if `line` properly encodes it, `None` otherwise.

        """
        if not line:
            return AltRefSeqNames([])
        try:
            return AltRefSeqNames([rname.RSeqName(k) for k in str.split(line, ",")])
        except rname.Error:
            return None


class MolecularTopology(Enum):
    """Molecular topology type."""

    LINEAR = auto()
    CIRCULAR = auto()

    def __str__(self) -> str:
        if self == MolecularTopology.LINEAR:
            return "linear"
        assert self == MolecularTopology.CIRCULAR
        return "circular"

    @staticmethod
    def of_string(line: str) -> Optional["MolecularTopology"]:
        """Parse the molecular topology value from a line.

        :param line: Line possibly encoding the molecular topology value.

        :return: Parsed molecular topology value if `line` properly
          encodes it, `None` otherwise.

        """
        if line == "linear":
            return MolecularTopology.LINEAR
        if line == "circular":
            return MolecularTopology.CIRCULAR
        return None


class SQLineOptionalFields(NamedTuple):
    """Optional fields for an SQ header line."""

    alt_locus: Optional[AlternativeLocus]
    alt_seq_names: Optional[AltRefSeqNames]
    assembly: Optional[str]
    description: Optional[str]
    md5sum: Optional[str]
    species: Optional[str]
    mol_topology: Optional[MolecularTopology]
    uri: Optional[str]

    def __str__(self) -> str:
        return str.join(
            "\t",
            ([] if self.alt_locus is None else [str.format("AH:{}", self.alt_locus)])
            + (
                []
                if self.alt_seq_names is None or not self.alt_seq_names.seqnames
                else [str.format("AN:{}", self.alt_seq_names)]
            )
            + ([] if self.assembly is None else [str.format("AS:{}", self.assembly)])
            + (
                []
                if self.description is None
                else [str.format("DS:{}", self.description)]
            )
            + ([] if self.md5sum is None else [str.format("M5:{}", self.md5sum)])
            + ([] if self.species is None else [str.format("SP:{}", self.species)])
            + (
                []
                if self.mol_topology is None
                else [str.format("TP:{}", self.mol_topology)]
            )
            + ([] if self.uri is None else [str.format("UR:{}", self.uri)]),
        )


class SQLine(common.HeaderLine):
    """Reference sequence dictionary metadata line (@SQ)."""

    _seq_name: rname.RSeqName
    _seq_length: SequenceLength
    _optional_fields: SQLineOptionalFields

    _code = "SQ"
    _required_fields = frozenset(["SN", "LN"])
    _predefined_fields = frozenset(
        ["SN", "LN", "AH", "AN", "AS", "DS", "M5", "SP", "TP", "UR"]
    )

    def __init__(
        self,
        seq_name: rname.RSeqName,
        seq_length: SequenceLength,
        optional_fields: SQLineOptionalFields,
        extra: List[datafield.OptionalDataField],
    ):
        super().__init__(extra)
        self._seq_name = seq_name
        self._seq_length = seq_length
        self._optional_fields = optional_fields
        self._extra = extra

    @property
    def seq_name(self) -> rname.RSeqName:
        """Reference sequence name."""
        return self._seq_name

    @property
    def seq_length(self) -> SequenceLength:
        """Reference sequence length."""
        return self._seq_length

    @property
    def optional_fields(self) -> SQLineOptionalFields:
        """Optional fields of the SQ header line."""
        return self._optional_fields

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SQLine):
            return NotImplemented
        return (
            self.seq_name == other.seq_name
            and self.seq_length == other.seq_length
            and self.optional_fields == other.optional_fields
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(seq_name={:s}, seq_length={:s}, optional_fields={:s}, extra={:s})",
            self.__class__.__name__,
            repr(self.seq_name),
            repr(self.seq_length),
            repr(self.optional_fields),
            repr(self.extra),
        )

    def __str__(self) -> str:
        optional_part = str(self.optional_fields)
        return str.join(
            "\t",
            [
                str.format(
                    "@{:s}\tSN:{}\tLN:{}", self._code, self.seq_name, self.seq_length
                )
            ]
            + ([] if not optional_part else [optional_part])
            + [str(k) for k in self.extra],
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["SQLine"]:
        """Parse a reference sequence line.

        :param line: Line possibly encoding a reference sequence
          record.

        :return: Parsed reference sequence record if `line` properly
          encodes it, `None` otherwise.

        """

        def parse_predefined_fields(
            parts: List[datafield.DataField],
        ) -> Optional[Tuple[rname.RSeqName, SequenceLength, SQLineOptionalFields]]:
            """Parse predefined fields of a @SQ line."""
            predefined_dict = {k.tag: k.value for k in parts}

            try:
                sn_field = rname.RSeqName(predefined_dict["SN"])
            except rname.Error:
                return None

            try:
                ln_field_value = int(predefined_dict["LN"])
            except ValueError:
                return None

            try:
                ln_field = SequenceLength(ln_field_value)
            except SequenceLengthError:
                return None

            ah_field = common.get_and_parse(
                predefined_dict, "AH", AlternativeLocus.of_string
            )
            an_field = common.get_and_parse(
                predefined_dict, "AN", AltRefSeqNames.of_string
            )
            as_field = common.get_and_parse(predefined_dict, "AS", lambda x: x)
            ds_field = common.get_and_parse(predefined_dict, "DS", lambda x: x)
            m5_field = common.get_and_parse(predefined_dict, "M5", lambda x: x)
            sp_field = common.get_and_parse(predefined_dict, "SP", lambda x: x)
            tp_field = common.get_and_parse(
                predefined_dict, "TP", MolecularTopology.of_string
            )
            ur_field = common.get_and_parse(predefined_dict, "UR", lambda x: x)

            if not all(
                [
                    ah_field[0],
                    an_field[0],
                    as_field[0],
                    ds_field[0],
                    m5_field[0],
                    sp_field[0],
                    tp_field[0],
                    ur_field[0],
                ]
            ):
                return None

            return (
                sn_field,
                ln_field,
                SQLineOptionalFields(
                    ah_field[1],
                    an_field[1],
                    as_field[1],
                    ds_field[1],
                    m5_field[1],
                    sp_field[1],
                    tp_field[1],
                    ur_field[1],
                ),
            )

        parts = str.split(line, "\t")

        if parts[0] == "@" + cls._code:
            parsed_fields = cls.parse_fields(parts[1:])
            if parsed_fields is not None:
                predefined, extra = parsed_fields
                if cls.check_predefined_fields(predefined):
                    parsed = parse_predefined_fields(predefined)
                    if parsed is not None:
                        try:
                            return SQLine(parsed[0], parsed[1], parsed[2], extra)
                        except common.Error:
                            return None
        return None
