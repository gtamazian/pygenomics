"""Comment line (@CO) in the SAM header."""

from typing import NamedTuple, Optional


class COLine(NamedTuple):
    """Comment line (@CO)."""

    comment: str

    def __str__(self) -> str:
        return str.format("@CO\t{:s}", self.comment) if self.comment else "@CO"

    @staticmethod
    def of_string(line: str) -> Optional["COLine"]:
        """Parse the comment line (@CO).

        :param line: Line that might be a @CO line.

        :return: Parsed comment line.

        """
        parts = str.split(line, "\t", 1)
        if parts[0] != "@CO":
            return None
        return COLine(parts[1] if len(parts) == 2 else "")
