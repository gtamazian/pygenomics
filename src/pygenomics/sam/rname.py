"""Character set restrictions for fields in the SAM format.

Classes in the module implement Section 1.2.1 "Character set
restrictions" from the SAM format specification.

"""

from typing import FrozenSet

from pygenomics.sam import error

assert ord("!") < ord("~"), "incorrect character range [!-~]"

_RNAME_CHARACTERS = frozenset(
    chr(k) for k in range(ord("!"), ord("~")) if k not in {ord("@"), ord(",")}
)


class Error(error.Error):
    """Base class for errors related to non-empty restricted-character names."""


class EmptyName(Error):
    """Exception for violating the non-empty name invariant."""


class IncorrectCharacter(Error):
    """Incorrect character present in a name."""

    value: str

    def __init__(self, value: str):
        super().__init__(value)
        self.value = value


class RName:
    """Query or read names.

    The class guarantees that its value is non-empty and contain only
      printable ASCII characters in the range `[!-~]` except for `@`.

    """

    _allowed_characters: FrozenSet[str] = _RNAME_CHARACTERS
    _value: str

    @classmethod
    def _check_value(cls, value: str) -> bool:
        """Check whether the specified value contains only allowed characters."""
        return all(k in cls._allowed_characters for k in value)

    def __init__(self, value: str):
        if not value:
            raise EmptyName()
        if not self._check_value(value):
            raise IncorrectCharacter(value)
        self._value = value

    @property
    def value(self) -> str:
        """The name value."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, RName):
            return NotImplemented
        return self.value == other.value

    def __repr__(self) -> str:
        return str.format("{:s}(value='{:s}')", self.__class__.__name__, self.value)

    def __str__(self) -> str:
        return self.value

    @classmethod
    def get_allowed_characters(cls) -> str:
        """Return the string of allowed characters."""
        return str.join("", sorted(cls._allowed_characters))


class IncorrectStartingCharacter(Error):
    """Error indicating that a reference sequence name starts with `*` or `=`."""

    value: str

    def __init__(self, value: str):
        super().__init__(value)
        self.value = value


class RSeqName(RName):
    """Reference sequence names.

    The class guarantees that its value is non-empty, conform to
      restrictions of :py:class:`RName`, and does not start with
      characters `*` and `=`.

    """

    def __init__(self, value: str):
        super().__init__(value)
        assert value
        if value[0] in {"*", "="}:
            raise IncorrectStartingCharacter(value)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, RSeqName):
            return NotImplemented
        return self.value == other.value
