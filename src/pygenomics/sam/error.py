"""Exceptions for subpackage `pygenomics.sam`."""

from pygenomics import error


class Error(error.Error):
    """Base class for exceptions related to the SAM format."""
