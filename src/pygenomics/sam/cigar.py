"""CIGAR operations."""

import functools
from enum import Enum, unique
from typing import List, Optional, Tuple

_OPERATION_CHARACTERS = frozenset("MIDNSHP=X")


@unique
class Operation(Enum):
    """CIGAR operation."""

    ALNMATCH = 0
    INSERTION = 1
    DELETION = 2
    SKIPPED = 3
    SOFTCLIP = 4
    HARDCLIP = 5
    PADDING = 6
    SEQMATCH = 7
    SEQMISMATCH = 8

    def __str__(self) -> str:
        # pylint: disable=too-many-return-statements
        if self == Operation.ALNMATCH:
            return "M"
        if self == Operation.INSERTION:
            return "I"
        if self == Operation.DELETION:
            return "D"
        if self == Operation.SKIPPED:
            return "N"
        if self == Operation.SOFTCLIP:
            return "S"
        if self == Operation.HARDCLIP:
            return "H"
        if self == Operation.PADDING:
            return "P"
        if self == Operation.SEQMATCH:
            return "="
        assert self == Operation.SEQMISMATCH
        return "X"

    @staticmethod
    def of_string(line: str) -> Optional["Operation"]:
        # pylint: disable=too-many-return-statements
        """Parse a CIGAR operation from the line.

        :param line: Line possibly encoding a single CIGAR operation.

        :return: Single CIGAR operation if `line` properly encodes it,
          `None` otherwise.

        """
        if line == "M":
            return Operation.ALNMATCH
        if line == "I":
            return Operation.INSERTION
        if line == "D":
            return Operation.DELETION
        if line == "N":
            return Operation.SKIPPED
        if line == "S":
            return Operation.SOFTCLIP
        if line == "H":
            return Operation.HARDCLIP
        if line == "P":
            return Operation.PADDING
        if line == "=":
            return Operation.SEQMATCH
        if line == "X":
            return Operation.SEQMISMATCH
        return None


def create_cigar_string(operations: List[Operation]) -> str:
    """Convert a series of CIGAR operations to a string.

    :param operations: CIGAR operations.

    :return: Representation of `operations` in the CIGAR string
      format.

    """

    Acc = Tuple[List[str], Tuple[int, Operation]]

    def encode_series(symbol: str, count: int) -> str:
        """Encode a series of operations."""
        return str.format("{:d}{:s}", count, symbol) if count > 1 else symbol

    def process_operation(acc: Acc, new: Operation) -> Acc:
        """Process a new CIGAR operation."""
        processed, (count, operation) = acc
        if new == operation:
            return (processed, (count + 1, operation))
        list.append(processed, encode_series(str(operation), count))
        return (processed, (1, new))

    if not operations:
        return ""

    init: Acc = ([], (1, operations[0]))
    parts, last = functools.reduce(process_operation, operations[1:], init)
    return str.join("", parts + [encode_series(str(last[1]), last[0])])


def parse_cigar_string(line: str) -> Optional[List[Operation]]:
    """Parse a CIGAR string.

    :param line: Line possibly encoding a CIGAR string.

    :return: List of CIGAR operations if `line` properly encodes them,
      `None` otherwise.

    """

    Acc = Tuple[List[Operation], str]

    def process_character(acc: Acc, new: str) -> Acc:
        """Process a new character from the CIGAR string."""
        processed, buf = acc
        if new in _OPERATION_CHARACTERS:
            count = int(buf) if buf else 1
            operation = Operation.of_string(new)
            assert operation is not None
            processed += [operation] * count
            return (processed, "")
        assert str.isdecimal(new)
        buf += new
        return (processed, buf)

    init: Acc = ([], "")
    cigar_operations, _ = functools.reduce(process_character, line, init)

    return cigar_operations
