"""Read alignments in the SAM format."""

import itertools
from enum import Enum, unique
from typing import FrozenSet, List, NamedTuple, Optional

import pygenomics.sam.cigar
import pygenomics.sam.rname
from pygenomics.sam.datafield import OptionalDataField
from pygenomics.sam.error import Error


class IncorrectQName(Error):
    """Error for an incorrect query template name."""

    value: str

    def __init__(self, value: str):
        super().__init__(value)
        self.value = value


class QName:
    """Class for query template names."""

    _value: str

    _allowed_characters = frozenset(
        chr(k)
        for k in itertools.chain(
            range(ord("!"), ord("?") + 1), range(ord("A"), ord("~") + 1)
        )
    )

    _max_length = 254

    @classmethod
    def allowed_characters(cls) -> FrozenSet[str]:
        """Get the set of characters allowed in query template names."""
        return cls._allowed_characters

    @classmethod
    def max_length(cls) -> int:
        """Get the maximum length for a query template name."""
        return cls._max_length

    def __init__(self, value: str):
        if len(value) > self.max_length() or not all(
            k in self.allowed_characters() for k in value
        ):
            raise IncorrectQName(value)
        self._value = value

    @property
    def value(self) -> str:
        """Query template name value."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, QName):
            return NotImplemented
        return self.value == other.value

    def __repr__(self) -> str:
        return str.format("{:s}(value={:s})", self.__class__.__name__, self.value)

    def __str__(self) -> str:
        return self.value


@unique
class Flag(Enum):
    """Flags for alignment features."""

    MULTIPLE_SEGMENTS = 0x1
    PROPERLY_ALIGNED = 0x2
    UNMAPPED = 0x4
    SEGMENT_UNMAPPED = 0x8
    REVERSED = 0x10
    SEGMENT_REVERSED = 0x20
    FIRST_SEGMENT = 0x40
    LAST_SEGMENT = 0x80
    SECONDARY = 0x100
    QCFAIL = 0x200
    DUPLICATE = 0x400
    SUPPLEMENTARY = 0x800


_MISSING = "*"


class Record(NamedTuple):
    """Read alignment record."""

    qname: QName
    flag: int
    rname: Optional[pygenomics.sam.rname.RSeqName]
    pos: int
    mapq: int
    cigar: Optional[List[pygenomics.sam.cigar.Operation]]
    rnext: Optional[str]
    pnext: int
    tlen: int
    seq: Optional[str]
    qual: str
    extra: List[OptionalDataField]

    def __str__(self) -> str:
        return str.format(
            "{}\t{:d}\t{:s}\t{:d}\t{:d}\t{:s}\t{:s}\t{:d}\t{:d}\t{:s}\t{:s}",
            self.qname,
            self.flag,
            str(self.rname) if self.rname is not None else _MISSING,
            self.pos,
            self.mapq,
            pygenomics.sam.cigar.create_cigar_string(self.cigar)
            if self.cigar is not None
            else _MISSING,
            self.rnext if self.rnext is not None else _MISSING,
            self.pnext,
            self.tlen,
            self.seq if self.seq is not None else _MISSING,
            self.qual,
        ) + (
            ("\t" + str.join("\t", [str(k) for k in self.extra])) if self.extra else ""
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Parse the SAM alignment record from a line.

        :param line: Line encoding a read alignment in the SAM format.

        :return: Parsed read alignment record if `line` properly
          encodes it, `None` otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) < 11:
            return None

        extra = [OptionalDataField.of_string(k) for k in parts[11:]]
        filtered_extra = [k for k in extra if k is not None]
        if len(filtered_extra) < len(extra):
            return None

        if parts[5] != _MISSING:
            cigar = pygenomics.sam.cigar.parse_cigar_string(parts[5])
            if cigar is None:
                return None
        else:
            cigar = None

        try:
            return Record(
                QName(parts[0]),
                int(parts[1]),
                pygenomics.sam.rname.RSeqName(parts[2])
                if parts[2] != _MISSING
                else None,
                int(parts[3]),
                int(parts[4]),
                cigar,
                parts[6] if parts[6] != _MISSING else None,
                int(parts[7]),
                int(parts[8]),
                parts[9] if parts[9] != _MISSING else None,
                parts[10],
                filtered_extra,
            )
        except (ValueError, IncorrectQName, pygenomics.sam.rname.Error):
            return None
