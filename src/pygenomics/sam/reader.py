"""Reading a SAM file."""

from typing import Iterator, List, Optional, TextIO

from pygenomics.sam.alignment import Record
from pygenomics.sam.error import Error
from pygenomics.sam.header import Header


class HeaderError(Error):
    """Incorrect header of a SAM file."""

    lines: List[str]

    def __init__(self, lines: List[str]):
        super().__init__(lines)
        self.lines = lines


class AlignmentError(Error):
    """Incorrect alignment record in a SAM file."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


class Reader:
    """Data structure for reading records from a SAM file."""

    _stream: TextIO
    _header: Header
    _first_line: Optional[str]

    def __init__(self, stream: TextIO):
        empty_stream = False
        try:
            line = next(stream)
        except StopIteration:
            empty_stream = True

        if empty_stream:
            parsed_header = Header.of_list([])
            assert parsed_header is not None
            self._first_line = None
        else:
            header_lines: List[str] = [line]
            for line in stream:
                if str.startswith(line, "@"):
                    list.append(header_lines, line)
                else:
                    break

            parsed_header = Header.of_list(header_lines)
            if parsed_header is None:
                raise HeaderError(header_lines)

            self._first_line = line

        self._header = parsed_header
        self._stream = stream

    @property
    def header(self) -> Header:
        """Header records of the SAM file."""
        return self._header

    @property
    def alignments(self) -> Iterator[Record]:
        """Iterator of alignment records in the SAM file."""
        if self._first_line is None:
            return
        parsed_record = Record.of_string(self._first_line)
        if parsed_record is None:
            raise AlignmentError(self._first_line)
        yield parsed_record
        for line in self._stream:
            parsed_record = Record.of_string(line)
            if parsed_record is None:
                raise AlignmentError(line)
            yield parsed_record
