"""Data fields for SAM header and alignment lines."""

from enum import Enum, auto
from typing import NamedTuple, Optional

from pygenomics import common
from pygenomics.sam import error


class DataFieldErrorType(Enum):
    """Types of invariant violations for data fields."""

    COLON_IN_TAG = auto()
    TAB_IN_TAG = auto()
    EMPTY_TAG = auto()
    TAB_IN_VALUE = auto()
    EMPTY_VALUE = auto()


class DataFieldError(error.Error):
    """Error for a violation of the data field invariants."""

    error_type: DataFieldErrorType
    value: str

    def __init__(self, error_type: DataFieldErrorType, value: str):
        super().__init__(error_type, value)
        self.error_type = error_type
        self.value = value


class DataField:
    """Data field in the format TAG:VALUE.

    The class guarantees that :py:attr:`tag` does not contain a colon
      or a tab, :py:attr:`value` does not contain a tab, and both
      fields are non-empty.

    """

    _tag: str
    _value: str

    def __init__(self, tag: str, value: str):
        def check_tag() -> None:
            """Check the specified tag."""
            if ":" in tag:
                raise DataFieldError(DataFieldErrorType.COLON_IN_TAG, tag)
            if "\t" in tag:
                raise DataFieldError(DataFieldErrorType.TAB_IN_TAG, tag)
            if not tag:
                raise DataFieldError(DataFieldErrorType.EMPTY_TAG, tag)

        def check_value() -> None:
            """Check the specified value."""
            if "\t" in value:
                raise DataFieldError(DataFieldErrorType.TAB_IN_VALUE, value)
            if not value:
                raise DataFieldError(DataFieldErrorType.EMPTY_VALUE, value)

        check_tag()
        check_value()
        self._tag = tag
        self._value = value

    @property
    def tag(self) -> str:
        """Data field tag."""
        return self._tag

    @property
    def value(self) -> str:
        """Data field value."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, DataField):
            return NotImplemented
        return self.tag == other.tag and self.value == other.value

    def __repr__(self) -> str:
        return str.format(
            "{:s}(tag='{:s}', value='{:s}')",
            self.__class__.__name__,
            self.tag,
            self.value,
        )

    def __str__(self) -> str:
        return str.format("{:s}:{:s}", self.tag, self.value)

    @staticmethod
    def of_string(line: str) -> Optional["DataField"]:
        """Parse the data field from a line.

        :param line: Line possibly encoding the data field.

        :return: Parsed data field if `line` properly encodes it,
          `None` otherwise.

        """
        parts = common.split2(line, ":")
        if parts is not None:
            try:
                return DataField(parts[0], parts[1])
            except DataFieldError:
                return None
        return None


class ValueType(Enum):
    """Value type for an optional data field."""

    CHARACTER = auto()
    GENERAL_ARRAY = auto()
    REAL_NUMBER = auto()
    HEXADECIMAL_ARRAY = auto()
    INTEGER = auto()
    STRING = auto()

    def __str__(self) -> str:
        if self is ValueType.CHARACTER:
            return "A"
        if self is ValueType.GENERAL_ARRAY:
            return "B"
        if self is ValueType.REAL_NUMBER:
            return "f"
        if self is ValueType.HEXADECIMAL_ARRAY:
            return "H"
        if self is ValueType.INTEGER:
            return "i"
        assert self is ValueType.STRING
        return "Z"

    @staticmethod
    def of_string(line: str) -> Optional["ValueType"]:
        # pylint: disable=too-many-return-statements
        """Parse the value type from a line.

        :param line: Line possibly encoding the value type.

        :return: Parsed value type if `line` properly encodes it,
          `None` otherwise.

        """
        if line == "A":
            return ValueType.CHARACTER
        if line == "B":
            return ValueType.GENERAL_ARRAY
        if line == "f":
            return ValueType.REAL_NUMBER
        if line == "H":
            return ValueType.HEXADECIMAL_ARRAY
        if line == "i":
            return ValueType.INTEGER
        if line == "Z":
            return ValueType.STRING
        return None


class OptionalDataField(NamedTuple):
    """Optional data field in the format TAG:TYPE:VALUE."""

    field: DataField
    value_type: ValueType

    def __str__(self) -> str:
        return str.format(
            "{:s}:{}:{:s}", self.field.tag, self.value_type, self.field.value
        )

    @staticmethod
    def of_string(line: str) -> Optional["OptionalDataField"]:
        """Parse the optional data field from a line.

        :param line: Line possibly encoding the optional data field.

        :return: Parsed optional data field if `line` properly encodes
          it, `None` otherwise.

        """
        parts = common.split2(line, ":")
        if parts is not None:
            further_parts = common.split2(parts[1], ":")
            if further_parts is not None:
                value_type = ValueType.of_string(further_parts[0])
                if value_type is not None:
                    try:
                        field = DataField(parts[0], further_parts[1])
                    except DataFieldError:
                        return None
                    return OptionalDataField(field, value_type)
        return None
