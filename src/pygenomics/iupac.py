"""IUPAC nucleotide symbols."""

from enum import Enum, auto
from typing import FrozenSet, List, Optional


class Nucleotide(Enum):
    """Exact nucleotide symbol."""

    A = auto()
    C = auto()
    G = auto()
    T = auto()
    U = auto()

    def __str__(self) -> str:
        return self.name

    @staticmethod
    def of_string(line: str) -> Optional["Nucleotide"]:
        """Parse a nucleotide string.

        :param line: Line encoding a nucleotide.

        :return: Nucleotide if `line` encodes it, `None` otherwise.

        """
        if line == "A":
            return Nucleotide.A
        if line == "C":
            return Nucleotide.C
        if line == "G":
            return Nucleotide.G
        if line == "T":
            return Nucleotide.T
        if line == "U":
            return Nucleotide.U
        return None


_NUCLEOTIDE_STRINGS = frozenset(k.name for k in Nucleotide)


def get_nucleotide_characters() -> FrozenSet[str]:
    """Get characters for all possible nucleotide symbols."""
    return _NUCLEOTIDE_STRINGS


class Symbol(Enum):
    """IUPAC nucleotide symbol."""

    A = auto()
    B = auto()
    C = auto()
    D = auto()
    G = auto()
    H = auto()
    K = auto()
    M = auto()
    N = auto()
    R = auto()
    S = auto()
    T = auto()
    U = auto()
    V = auto()
    W = auto()
    Y = auto()

    def __str__(self) -> str:
        return self.name

    @staticmethod
    def of_string(line: str) -> Optional["Symbol"]:
        # pylint: disable=too-many-branches,too-many-return-statements
        """Parse an IUPAC nucleotide symbol.

        :param line: Line encoding a symbol.

        :return: Symbol if `line` encodes it, `None` otherwise.

        """
        if line == "A":
            return Symbol.A
        if line == "B":
            return Symbol.B
        if line == "C":
            return Symbol.C
        if line == "D":
            return Symbol.D
        if line == "G":
            return Symbol.G
        if line == "H":
            return Symbol.H
        if line == "K":
            return Symbol.K
        if line == "M":
            return Symbol.M
        if line == "N":
            return Symbol.N
        if line == "R":
            return Symbol.R
        if line == "S":
            return Symbol.S
        if line == "T":
            return Symbol.T
        if line == "U":
            return Symbol.U
        if line == "V":
            return Symbol.V
        if line == "W":
            return Symbol.W
        if line == "Y":
            return Symbol.Y
        return None

    def to_nucleotides(self) -> List[Nucleotide]:
        # pylint: disable=too-many-branches,too-many-return-statements
        """Get the list of nucleotides for the symbol.

        :return: Nucleotides that the symbol encodes.

        """
        if self is Symbol.A:
            return [Nucleotide.A]
        if self is Symbol.B:
            return [Nucleotide.C, Nucleotide.G, Nucleotide.T]
        if self is Symbol.C:
            return [Nucleotide.C]
        if self is Symbol.D:
            return [Nucleotide.A, Nucleotide.G, Nucleotide.T]
        if self is Symbol.G:
            return [Nucleotide.G]
        if self is Symbol.H:
            return [Nucleotide.A, Nucleotide.C, Nucleotide.T]
        if self is Symbol.K:
            return [Nucleotide.G, Nucleotide.T]
        if self is Symbol.M:
            return [Nucleotide.A, Nucleotide.C]
        if self is Symbol.N:
            return [Nucleotide.A, Nucleotide.C, Nucleotide.G, Nucleotide.T]
        if self is Symbol.R:
            return [Nucleotide.A, Nucleotide.G]
        if self is Symbol.S:
            return [Nucleotide.C, Nucleotide.G]
        if self is Symbol.T:
            return [Nucleotide.T]
        if self is Symbol.U:
            return [Nucleotide.U]
        if self is Symbol.V:
            return [Nucleotide.A, Nucleotide.C, Nucleotide.G]
        if self is Symbol.W:
            return [Nucleotide.A, Nucleotide.T]
        assert self is Symbol.Y
        return [Nucleotide.C, Nucleotide.T]


_SYMBOL_STRINGS = frozenset(k.name for k in Symbol)


def get_symbol_characters() -> FrozenSet[str]:
    """Get characters for all possible IUPAC nucleotide symbols."""
    return _SYMBOL_STRINGS


_AMBIGUOUS_SYMBOL_STRINGS = frozenset(
    k for k in _SYMBOL_STRINGS if k not in _NUCLEOTIDE_STRINGS and k != "N"
)


def get_ambiguous_symbol_characters() -> FrozenSet[str]:
    """Get the set of characters for ambiguous IUPAC nucleotide symbols."""
    return _AMBIGUOUS_SYMBOL_STRINGS


_AMBIGUOUS_SYMBOLS = [
    Symbol.M,
    Symbol.R,
    Symbol.W,
    Symbol.S,
    Symbol.Y,
    Symbol.K,
    Symbol.V,
    Symbol.H,
    Symbol.D,
    Symbol.B,
]

assert len(_AMBIGUOUS_SYMBOLS) == len(_AMBIGUOUS_SYMBOL_STRINGS) == 10


def get_ambiguous_symbols() -> List[Symbol]:
    """Get the list of ambiguous IUPAC nucleotide symbols."""
    return _AMBIGUOUS_SYMBOLS
