"""Reading and writing GFF3 files."""

import urllib.parse
from typing import NamedTuple, Optional

from pygenomics import common
from pygenomics.gff import base


class Attributes(base.Attributes):
    """Attributes in the GFF3 format."""

    def __str__(self) -> str:
        return str.join(
            ";",
            (
                str.format("{:s}={:s}", key, urllib.parse.quote(value))
                for key, value in self.pairs
            ),
        )

    @staticmethod
    def of_string(line: str) -> Optional["Attributes"]:
        """Parse GFF3 attributes from a line.

        :param line: Line possibly encoding GFF3 attributes.

        :return: GFF3 attributes if `line` represents them, `None`
          otherwise.

        """
        parts = [common.split2(k, "=") for k in str.split(line, ";")]
        pairs = [(k[0], urllib.parse.unquote(k[1])) for k in parts if k is not None]
        if len(pairs) == len(parts):
            try:
                return Attributes(pairs)
            except base.GffError:
                return None
        return None


class Record(NamedTuple):
    """GFF3 record."""

    pos: base.Record
    attributes: Attributes

    def __str__(self) -> str:
        return (
            str.format("{}\t{}", self.pos, self.attributes)
            if self.attributes.pairs
            else str(self.pos)
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Convert a string to a GFF3 record.

        :param line: String possibly encoding a GFF3 record.

        :return: GFF3 record if `line` encodes it, `None` otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) in {base.Record.num_columns(), base.Record.num_columns() + 1}:
            pos = base.Record.of_list(parts[: base.Record.num_columns()])
            if len(parts) == base.Record.num_columns() + 1:
                attributes = Attributes.of_string(parts[-1])
            else:
                attributes = Attributes([])
            if pos is not None and attributes is not None:
                return Record(pos, attributes)
        return None
