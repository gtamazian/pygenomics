"""Reading and writing GTF files."""

import functools
from typing import List, NamedTuple, Optional, Tuple

from pygenomics import common
from pygenomics.gff import base


def split_by_semicolons(line: str) -> List[str]:
    """Split a line by semicolons ignoring ones within double-quotes.

    :param line: Line to be split.

    :return: Line parts separated by semicolons outside double-quotes.

    """

    Acc = Tuple[List[str], List[str], bool]

    def process_character(acc: Acc, character: str) -> Acc:
        """Process a character from the line being split."""
        parts, current, within_quotes = acc
        if character == ";":
            if within_quotes:
                list.append(current, character)
            else:
                list.append(parts, str.join("", current))
                current = []
        else:
            list.append(current, character)
            if character == '"':
                within_quotes = not within_quotes
        return (parts, current, within_quotes)

    if not line:
        return []

    init: Acc = ([], [line[0]], line[0] == '"')
    line_parts, last, _ = functools.reduce(process_character, line[1:], init)
    if last:
        list.append(line_parts, str.join("", last))

    return line_parts


class Attributes(base.Attributes):
    """Attributes in the GTF format."""

    def __str__(self) -> str:
        return str.join(
            " ", (str.format('{:s} "{:s}";', key, value) for key, value in self.pairs)
        )

    @staticmethod
    def of_string(line: str) -> Optional["Attributes"]:
        """Parse GTF attributes from a line.

        :param line: Line possibly encoding GTF attributes.

        :return: GTF attributes if `line` represents them, `None`
          otherwise.

        """
        parts = [
            common.split2(str.strip(k), " ") for k in split_by_semicolons(line) if k
        ]
        pairs = [
            (k[0], common.strip_prefix_suffix(k[1], '"', '"'))
            for k in parts
            if k is not None
        ]
        if len(pairs) == len(parts):
            proper_pairs = [(key, value) for key, value in pairs if value is not None]
            if len(proper_pairs) == len(pairs):
                try:
                    return Attributes(proper_pairs)
                except base.GffError:
                    return None
        return None


class Record(NamedTuple):
    """GTF record."""

    pos: base.Record
    attributes: Attributes

    def __str__(self) -> str:
        return (
            str.format("{}\t{}", self.pos, self.attributes)
            if self.attributes.pairs
            else str(self.pos)
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Convert a string to a GTF record.

        :param line: String possibly encoding a GTF record.

        :return: GTF record if `line` encodes it, `None` otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) in {base.Record.num_columns(), base.Record.num_columns() + 1}:
            pos = base.Record.of_list(parts[: base.Record.num_columns()])
            if len(parts) == base.Record.num_columns() + 1:
                attributes = Attributes.of_string(parts[-1])
            else:
                attributes = Attributes([])
            if pos is not None and attributes is not None:
                return Record(pos, attributes)
        return None
