"""Base routines for the GFF format."""

from enum import Enum, auto
from typing import List, Optional, Tuple

from pygenomics import common, error
from pygenomics.strand import Strand


class GffErrorType(Enum):
    """Types of invariant violations for a base of a GFF record."""

    EMPTY_SEQID = auto()
    EMPTY_SOURCE = auto()
    EMPTY_FEATURE_TYPE = auto()
    INCORRECT_RANGE = auto()
    INCORRECT_PHASE = auto()
    EMPTY_ATTRIBUTE_KEY = auto()


class GffError(error.Error):
    """Error for a violation of GFF record invariants."""

    def __init__(self, error_type: GffErrorType):
        super().__init__(error_type)
        self.error_type = error_type


class Range(common.Range):
    """Range in the GFF format."""

    def __init__(self, start: int, end: int):
        if not 1 <= start <= end:
            raise GffError(GffErrorType.INCORRECT_RANGE)
        super().__init__(start, end)

    @staticmethod
    def of_strings(start_str: str, end_str: str) -> Optional["Range"]:
        """Convert a pair of strings to a GFF range.

        :param start_str: String of the start coordinate.

        :param end_str: String of the end coordinate.

        :return: Range from `start_str` to `end_str` if the strings
          encode a proper GFF range, `None` otherwise.

        """
        start = common.str_to_int(start_str)
        end = common.str_to_int(end_str)
        if start is None or end is None:
            return None
        try:
            return Range(start, end)
        except GffError:
            return None


class Phase:
    """Phase in the GFF format."""

    _value: int

    def __init__(self, value: int):
        if value not in {0, 1, 2}:
            raise GffError(GffErrorType.INCORRECT_PHASE)
        self._value = value

    @property
    def value(self) -> int:
        """Phase value."""
        return self._value

    def __repr__(self) -> str:
        return str.format("{:s}(value={:d})", self.__class__.__name__, self.value)

    def __str__(self) -> str:
        return str.format("{:d}", self.value)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Phase):
            return NotImplemented
        return self.value == other.value

    @staticmethod
    def of_string(value: str) -> Optional["Phase"]:
        """Convert a string to the phase value.

        :param value: String possibly encoding the phase value.

        :return: Phase if `value` encodes a proper one, `None`
          otherwise.

        """
        number = common.str_to_int(value)
        if number is not None:
            try:
                return Phase(number)
            except GffError:
                return None
        return None


class Record:
    """Base part of a GFF record."""

    _seqid: str
    _source: Optional[str]
    _feature_type: str
    _record_range: Range
    _score: Optional[float]
    _strand: Strand
    _phase: Optional[Phase]

    _num_columns = 8

    def __init__(
        self,
        seqid: str,
        source: Optional[str],
        feature_type: str,
        record_range: Range,
        score: Optional[float],
        strand: Strand,
        phase: Optional[Phase],
    ):  # pylint: disable=too-many-arguments
        if not seqid:
            raise GffError(GffErrorType.EMPTY_SEQID)
        if source is not None and not source:
            raise GffError(GffErrorType.EMPTY_SOURCE)
        if not feature_type:
            raise GffError(GffErrorType.EMPTY_FEATURE_TYPE)

        self._seqid = seqid
        self._source = source
        self._feature_type = feature_type
        self._record_range = record_range
        self._score = score
        self._strand = strand
        self._phase = phase

    @property
    def seqid(self) -> str:
        """Sequence name."""
        return self._seqid

    @property
    def source(self) -> Optional[str]:
        """Source name."""
        return self._source

    @property
    def feature_type(self) -> str:
        """Feature type."""
        return self._feature_type

    @property
    def record_range(self) -> Range:
        """Range in the sequence."""
        return self._record_range

    @property
    def start(self) -> int:
        """Start position in the sequence."""
        return self.record_range.start

    @property
    def end(self) -> int:
        """End position in the sequence."""
        return self.record_range.end

    @property
    def score(self) -> Optional[float]:
        """Score value."""
        return self._score

    @property
    def strand(self) -> Strand:
        """Strand of the sequence."""
        return self._strand

    @property
    def phase(self) -> Optional[Phase]:
        """Phase of the feature."""
        return self._phase

    @classmethod
    def num_columns(cls) -> int:
        """Number of columns for the GFF3 base record type."""
        return cls._num_columns

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Record):
            return NotImplemented
        return (
            self.seqid == other.seqid
            and self.source == other.source
            and self.feature_type == other.feature_type
            and self.record_range == other.record_range
            and self.score == other.score
            and self.strand == other.strand
            and self.phase == other.phase
        )

    def __repr__(self) -> str:
        return str.format(
            '{:s}(seqid="{:s}", source={:s}, feature_type="{:s}", record_range={:s}, '
            "score={:s}, strand={:s}, phase={:s})",
            self.__class__.__name__,
            self.seqid,
            repr(self.source),
            self.feature_type,
            repr(self.record_range),
            repr(self.score),
            repr(self.strand),
            repr(self.phase),
        )

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:s}\t{:s}\t{:d}\t{:d}\t{:s}\t{:s}\t{:s}",
            self.seqid,
            common.optional_to_string(self.source, "."),
            self.feature_type,
            self.start,
            self.end,
            str.format("{:2f}", self.score) if self.score is not None else ".",
            str(self.strand),
            common.optional_to_string(self.phase, "."),
        )

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Record"]:
        """Convert line parts to the GFF base record.

        :param parts: Line parts possibly encoding the GFF base record.

        :return: GFF base record if `parts` properly encodes it,
          `None` otherwise.

        """

        def get_score() -> Tuple[bool, Optional[float]]:
            """Attempt to parse the score value."""
            if parts[5] == ".":
                return (True, None)
            score = common.str_to_float(parts[5])
            return (True, score) if score is not None else (False, score)

        def get_phase() -> Tuple[bool, Optional[Phase]]:
            """Attempt to parse the phase value."""
            if parts[7] == ".":
                return (True, None)
            phase = Phase.of_string(parts[7])
            return (True, phase) if phase is not None else (False, phase)

        if len(parts) == cls.num_columns():
            source = parts[1] if parts[1] != "." else None
            record_range = Range.of_strings(parts[3], parts[4])
            correct_score_value, score = get_score()
            strand = Strand.of_string(parts[6])
            correct_phase_value, phase = get_phase()

            if (
                record_range is not None
                and correct_score_value
                and strand is not None
                and correct_phase_value
            ):
                try:
                    return Record(
                        parts[0], source, parts[2], record_range, score, strand, phase
                    )
                except GffError:
                    return None
        return None


class Attributes:
    """Base class for attributes in the GFF format.

    The class guarantees that no attribute has an empty key.

    """

    _pairs: List[Tuple[str, str]]

    def __init__(self, pairs: List[Tuple[str, str]]):
        if any(not key for key, _ in pairs):
            raise GffError(GffErrorType.EMPTY_ATTRIBUTE_KEY)
        self._pairs = pairs

    @property
    def pairs(self) -> List[Tuple[str, str]]:
        """List of attribute key-value pairs."""
        return self._pairs

    def __repr__(self) -> str:
        return str.format("{:s}(pairs={:s})", self.__class__.__name__, repr(self.pairs))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Attributes):
            return NotImplemented
        return self.pairs == other.pairs
