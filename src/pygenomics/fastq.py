"""Reading and writing reads in the FASTQ format."""

import itertools
from enum import Enum, auto
from typing import IO, Iterator, NamedTuple

from pygenomics import error


class Record(NamedTuple):
    """Read record as specified by the FASTQ format."""

    name: str
    """Read name."""
    seq: str
    """Read sequence."""
    qual: str
    """Quality values for read sequence."""


class ReadingErrorType(Enum):
    """Types of errors that may occur while reading a FASTQ file."""

    INCORRECT_HEADER = auto()
    INCORRECT_SEPARATOR = auto()
    MISSING_LINES = auto()
    DIFFERENT_SEQ_QUAL_LEN = auto()


class ReadingError(error.Error):
    """Error that occurred while reading a FASTQ file."""

    error_type: ReadingErrorType
    """Error type."""
    line: str
    """Line that caused the error."""

    def __init__(self, error_type: ReadingErrorType, line: str):
        super().__init__(error_type, line)
        self.error_type = error_type
        self.line = line


def read(stream: IO[str]) -> Iterator[Record]:
    """Iterate reads from a stream in the FASTQ format.

    :note: The function produces a side effect by reading from
      `stream`. It raises :py:class:`ReadingError` if an error occurs
      while reading a FASTQ record. The error types are specified in
      :py:class:`ReadingErrorType`.

    :param stream: Stream in the FASTQ format.

    :return: Iterator of reads from `stream`.

    """

    def check_read(name: str, seq: str, sep_line: str, qual: str) -> str:
        """Check read parts and raise an exception if the check fails.

        The function returns the read name without the "@" character
        if a read passes the check.

        """
        if not str.startswith(name, "@"):
            raise ReadingError(ReadingErrorType.INCORRECT_HEADER, name)
        if seq is None or sep_line is None or qual is None:
            raise ReadingError(ReadingErrorType.MISSING_LINES, name)
        if not str.startswith(sep_line, "+"):
            raise ReadingError(ReadingErrorType.INCORRECT_SEPARATOR, sep_line)
        if len(seq) != len(qual):
            raise ReadingError(ReadingErrorType.DIFFERENT_SEQ_QUAL_LEN, qual)
        return name[1:]

    stripped_stream = map(str.strip, stream)
    for name, seq, sep_line, qual in itertools.zip_longest(*[stripped_stream] * 4):
        yield Record(check_read(name, seq, sep_line, qual), seq, qual)


def write(stream: IO[str], record: Record) -> None:
    """Write a read to a stream in the FASTQ format.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param record: Read to write to `stream`.

    """
    print("@", record.name, file=stream, sep="")
    print(record.seq, file=stream)
    print("+", file=stream)
    print(record.qual, file=stream)
