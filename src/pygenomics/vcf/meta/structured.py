"""Structured meta-information lines in the VCF format.

The module implements structured meta-information lines as defined in
Section 1.4 "Meta-information lines" from the VCF specification.

"""

from typing import FrozenSet, Optional

from pygenomics.vcf.meta import common, error

Fields = common.Fields


class StructuredLine:
    """Structured meta-information line.

    :note: The line key, ID, and key-value pairs of a structured
      meta-information line are guaranteed to be non-empty.

    Structured meta-information lines are defined in Section 1.4
    "Meta-information lines" of the VCF specification.

    """

    _key: str
    _id_field: str
    _extra: Fields

    _unquoted_fields: FrozenSet[str] = frozenset()
    _required_fields: FrozenSet[str] = frozenset()

    def __init__(self, key: str, id_field: str, extra: Fields):

        error.check_key(key)
        error.check_id_field(id_field)
        error.check_extra_fields(extra)

        self._key = key
        self._id_field = id_field
        self._extra = extra

    @property
    def key(self) -> str:
        """Key of a structured meta-information line."""
        return self._key

    @property
    def id_field(self) -> str:
        """ID of a structured meta-information line."""
        return self._id_field

    @property
    def extra(self) -> Fields:
        """Key-value pairs of a structured meta-information line."""
        return self._extra

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, StructuredLine):
            return NotImplemented
        return (
            self.key == other.key
            and self.id_field == other.id_field
            and self.extra == other.extra
        )

    def id_field_repr(self) -> str:  # pragma: no cover
        """ID field part for the __repr__() output."""
        return str.format("id_field='{:s}'", self.id_field)

    def extra_repr(self) -> str:  # pragma: no cover
        """Extra part for the __repr__() output."""
        return str.format("extra={:s}", repr(self.extra))

    def __repr__(self) -> str:
        return str.format(
            "{:s}(key='{:s}', {:s}, {:s})",
            self.__class__.__name__,
            self.key,
            self.id_field_repr(),
            self.extra_repr(),
        )

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(self.key, self.id_field, [], self.extra),
            self._unquoted_fields,
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["StructuredLine"]:
        """Convert a line to the structured meta-information line.

        :param line: Line possibly encoding a structured
          meta-information line.

        :return: Structured meta-information line if `line` encodes
          it, `None` otherwise.

        """

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )
        return (
            StructuredLine(parts.key, parts.id_field, parts.extra_fields)
            if parts is not None
            else None
        )


class StructuredWithDescriptionLine(StructuredLine):
    """Structured meta-information line with a description.

    :note: In addition to invariants of :py:class:`StructuredLine`,
      the class guarantees that its description is not empty.

    """

    _description: str

    _required_fields = frozenset(["Description"])

    def __init__(self, key: str, id_field: str, description: str, extra: Fields):
        super().__init__(key, id_field, extra)
        error.check_description(description)
        self._description = description

    @property
    def description(self) -> str:
        """Description of a structured meta-information line."""
        return self._description
