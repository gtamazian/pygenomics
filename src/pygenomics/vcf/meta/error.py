"""Exceptions for subpackage pygenomics.vcf.meta."""

from enum import Enum, auto

from pygenomics.vcf import error
from pygenomics.vcf.meta import common


class Error(error.Error):
    """Base class for exceptions in the subpackage."""


class LineErrorType(Enum):
    """Types of invariant violations for a meta-information line."""

    EMPTY_META_INFORMATION_LINE_KEY = auto()
    """Empty key in a structured meta-information line."""
    EMPTY_ID = auto()
    """Empty ID value in a structured meta-information line."""
    EMPTY_KEY = auto()
    """Empty key in a regular or structured meta-information line."""
    EMPTY_VALUE = auto()
    """Empty value in a regular or structured meta-information line."""
    EMPTY_DESCRIPTION = auto()
    """Empty description of an INFO or FORMAT line."""
    INCORRECT_FLAG_NUMBER = auto()
    """Incorrect value number for an INFO line with the Flag type."""
    FLAG_TYPE = auto()
    """Incorrect FORMAT line type (that is, Flag)."""
    INCORRECT_CONTIG_NAME = auto()
    """Incorrect contig name."""
    INCORRECT_CONTIG_LENGTH = auto()
    """Incorrect contig length."""
    INCORRECT_ALT_IUPAC_CODE = auto()
    """Incorrect IUPAC ambiguity code in an ALT meta-information line."""
    INCORRECT_ALT_FIRST_LEVEL = auto()
    """Incorrect first-level structural variant type in an ALT line."""
    EMPTY_METALINE_VALUE_LIST = auto()
    """Empty list of values in a META meta-information line."""
    EMPTY_METALINE_VALUE = auto()
    """Empty value in a META meta-information line."""


class LineError(Error):
    """Error for a violation of meta-information line invariants."""

    error_type: LineErrorType

    def __init__(self, error_type: LineErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


def check_key(key: str) -> None:
    """Check if the meta-information line key is valid.

    :note: The function raises the :py:class:`LineError` exception
      with the `EMPTY_META_INFORMATION_LINE_KEY` line error type if the
      check fails.

    :param key: Key of a structured meta-information line.

    """
    if not key:
        raise LineError(LineErrorType.EMPTY_META_INFORMATION_LINE_KEY)


def check_value(value: str) -> None:
    """Check if the value of a regular meta-information line is valid.

    :note: The function raises the :py:class:`LineError` exception
      with the `EMPTY_VALUE` line error type if the check fails.

    :param value: Value of a regular meta-information line.

    """
    if not value:
        raise LineError(LineErrorType.EMPTY_VALUE)


def check_id_field(id_field: str) -> None:
    """Check if the ID field is valid.

    :note: The function raises the :py:class:`LineError` exception with
      the `EMPTY_ID` line error type if the check fails.

    :param id_field: ID field of a meta-information line.

    """
    if not id_field:
        raise LineError(LineErrorType.EMPTY_ID)


def check_description(description: str) -> None:
    """Check if the Description field is valid.

    :note: The function raises :py:class:`LineError` exception with
      the `EMPTY_DESCRIPTION` line error type if the check fails.

    :param description: Description of a meta-information line.

    """
    if not description:
        raise LineError(LineErrorType.EMPTY_DESCRIPTION)


def check_extra_fields(fields: common.Fields) -> None:
    """Check if keys and values of extra fields are valid.

    :note: The function raises :py:class:`LineError` exception with
      the `EMPTY_KEY` or `EMPTY_VALUE` line error type if the check
      fails.

    :param fields: Extra fields of a meta-information line.

    """
    for key, value in fields:
        if not key:
            raise LineError(LineErrorType.EMPTY_KEY)
        if not value:
            raise LineError(LineErrorType.EMPTY_VALUE)


class MetaHeaderError(Error):
    """Error that occurred while reading a VCF file meta header."""

    line: str

    def __init__(self, line: str):
        super().__init__(self, line)
        self.line = line
