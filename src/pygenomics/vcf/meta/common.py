"""Common routines for subpackage pygenomics.vcf.meta."""

import functools
import itertools
from typing import FrozenSet, List, NamedTuple, Optional, Tuple

from pygenomics import common

Fields = List[Tuple[str, str]]


def split_line(line: str) -> Optional[Tuple[str, str]]:
    """Split a meta-information line into a key and a body.

    :param line: Meta-information line to be split.

    :return: Key and body parts of `line` if it is a proper
      meta-information line, `None` otherwise.

    """
    if not str.startswith(line, "##"):
        return None
    parts = common.split2(line[len("##") :], "=")
    if parts is None:
        return None
    return parts


def split_by_commas(line: str, delimiters: List[Tuple[str, str]]) -> List[str]:
    """Split a line by commas and ignore commas within delimiters.

    The function recognizes escaped double-quote and backslash
    characters.

    :param line: Line to split.

    :param delimiters: Pairs of delimiters to ignore commas within.

    :return: Parts of `line` separated by commas outside double-quotes.

    """
    Acc = Tuple[Tuple[List[str], List[str]], Tuple[Optional[str], bool]]

    delimiter_pairs = dict(delimiters)

    def process_character(acc: Acc, character: str) -> Acc:
        """Helper function to be passed to functools.reduce."""
        (parts, current), (current_delimiter, escaped) = acc
        if character == ",":
            if current_delimiter is None:
                list.append(parts, str.join("", current))
                return ((parts, []), (None, False))
        elif current_delimiter is None and character in delimiter_pairs:
            if not escaped:
                current_delimiter = character
        elif (
            current_delimiter is not None
            and character == delimiter_pairs[current_delimiter]
        ):
            if not escaped:
                current_delimiter = None

        list.append(current, character)
        return (
            (parts, current),
            (current_delimiter, character == "\\" if not escaped else False),
        )

    if not line:
        return []

    init: Acc = (
        (
            ([], [line[0]]),
            (line[0] if line[0] in delimiter_pairs else None, line[0] == "\\"),
        )
        if line[0] != ","
        else (([""], []), (None, False))
    )
    (parts, last), _ = functools.reduce(process_character, line[1:], init)
    list.append(parts, str.join("", last))

    return parts


_ESCAPE_TRANS = str.maketrans({'"': r"\"", "\\": r"\\"})


def escape(line: str) -> str:

    """Escape double-quote and backslash characters.

    The characters are escaped as required in Subsection 1.4.2
    "Information field format" of the VCF specification.

    :param line: Line to be escaped.

    :return: Line with double-quotes and backslashes escaped.

    """
    return str.translate(line, _ESCAPE_TRANS)


def unescape(line: str) -> str:
    """Unescape double-quote and backslash characters.

    This function is inverse to :py:func:`escape`.

    :param line: Line to be unescaped.

    :return: List with double-quotes and backslashes restored.

    """

    Acc = Tuple[List[str], bool]

    def process_character(acc: Acc, current: str) -> Acc:
        """Helper function to be passed to functools.reduce."""
        characters, escaped = acc
        if escaped:
            if current == '"':
                list.append(characters, '"')
            elif current == "\\":
                list.append(characters, "\\")
            else:
                characters += ["\\", current]
            escaped = False
        else:
            if current == "\\":
                escaped = True
            else:
                list.append(characters, current)

        return (characters, escaped)

    if not line:
        return ""

    init: Acc = ([line[0]], False) if line[0] != "\\" else ([], True)
    final_characters, final_escaped = functools.reduce(
        process_character, line[1:], init
    )

    if final_escaped:
        list.append(final_characters, "\\")

    return str.join("", final_characters)


class StructuredLineParts(NamedTuple):
    """Parsed parts of a structured meta-information line."""

    key: str
    """Line key."""
    id_field: str
    """Line ID."""
    required_fields: Fields
    """List of required key-value pairs."""
    extra_fields: Fields
    """List of extra key-value pairs."""


def structured_line_of_string(
    line: str,
    required_keys: FrozenSet[str],
    unquoted_keys: FrozenSet[str],
    delimiters: List[Tuple[str, str]],
) -> Optional[StructuredLineParts]:
    """Parse a structured meta-information line.

    :param line: Line possibly encoding a structured meta-information line.

    :param required_keys: Keys required to be present in the line.

    :param unquoted_keys: Keys which values must not be double-quoted.

    :param delimiters: Pairs of delimiters to ignore commas within.

    :return: Parsed line parts if `line` encodes a proper
      meta-information line, `None` otherwise.

    """

    def split_body(line: str) -> Optional[Fields]:
        """Split a meta-information line body into parts."""

        def strip_angle_brackets(line: str) -> Optional[str]:
            """Strip angle brackets."""
            return common.strip_prefix_suffix(line, "<", ">")

        body_without_angle_brackets = strip_angle_brackets(line)
        if body_without_angle_brackets is None:
            return None
        body_field_pairs = [
            common.split2(field, "=")
            for field in split_by_commas(body_without_angle_brackets, delimiters)
        ]
        filtered_field_pairs = [pair for pair in body_field_pairs if pair is not None]
        assert len(filtered_field_pairs) <= len(body_field_pairs)
        return (
            None
            if len(filtered_field_pairs) < len(body_field_pairs)
            else filtered_field_pairs
        )

    def process_body_values(fields: Fields) -> Optional[Fields]:
        """Unescape and remove double-quotes in fields."""

        def strip_double_quotes(line: str) -> Optional[str]:
            """Strip double-quotes."""
            return common.strip_prefix_suffix(line, '"', '"')

        processed_fields = [
            (
                (key, value)
                if key in unquoted_keys
                else (key, strip_double_quotes(unescape(value)))
            )
            for key, value in fields
        ]
        filtered_processed_fields = [
            (key, value) for key, value in processed_fields if value is not None
        ]
        assert len(filtered_processed_fields) <= len(processed_fields)
        return (
            None
            if len(filtered_processed_fields) < len(processed_fields)
            else filtered_processed_fields
        )

    def check_required_fields(fields: Fields) -> bool:
        """Check for required fields."""
        return frozenset.issubset(required_keys, set(key for key, _ in fields))

    def process_body(body_line: str) -> Optional[Tuple[str, Fields, Fields]]:
        """Parse the body part of a structured meta-information line."""
        body_parts = split_body(body_line)
        if body_parts is not None:
            id_fields, rest = common.assoc_extract(body_parts, "ID")
            if len(id_fields) == 1 and check_required_fields(rest):
                processed_body_pairs = process_body_values(rest)
                if processed_body_pairs is not None and common.assoc_unique(
                    processed_body_pairs
                ):
                    required_fields, extra_fields = common.partition(
                        processed_body_pairs, lambda x: x[0] in set(required_keys)
                    )
                    return (id_fields[0], required_fields, extra_fields)
        return None

    key_body = split_line(line)
    if key_body is not None:
        key, body = key_body
        body_parts = process_body(body)
        if body_parts is not None:
            id_field, required_fields, extra_fields = body_parts
            return StructuredLineParts(key, id_field, required_fields, extra_fields)
    return None


def structured_line_to_string(
    parts: StructuredLineParts, unquoted_keys: FrozenSet[str]
) -> str:
    """Convert structured meta-information line parts to a string.

    :param parts: Parsed parts of a structured meta-information line.

    :param unquoted_keys: Keys which values must not be double-quoted.

    :return: String of the line in the VCF format,

    """

    def field_to_string(field: Tuple[str, str]) -> str:
        """Convert the key-value field to the proper string."""
        key, value = field
        return str.format(
            "{:s}={:s}",
            key,
            value if key in unquoted_keys else '"' + escape(value) + '"',
        )

    return str.format(
        "##{:s}=<{:s}>",
        parts.key,
        str.join(
            ",",
            itertools.chain(
                [str.format("ID={:s}", parts.id_field)],
                (
                    field_to_string(k)
                    for k in itertools.chain(parts.required_fields, parts.extra_fields)
                ),
            ),
        ),
    )
