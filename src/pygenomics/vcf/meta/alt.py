"""ALT meta-information lines in the VCF format.

The module implements ALT meta-information lines as defined in
Subsection 1.4.5 "Alternative allele field format" from the VCF
specification.

"""

from enum import Enum, auto
from typing import List, Optional, Tuple

from pygenomics import common as pcommon
from pygenomics import iupac
from pygenomics.vcf.meta import common, error, structured

Fields = common.Fields


class SVType(Enum):
    """First-level structural variant types."""

    DEL = auto()
    """Deletion relative to the reference"""
    INS = auto()
    """Insertion of novel sequence relative to the reference"""
    DUP = auto()
    """Region of elevated copy number relative to the reference"""
    INV = auto()
    """Inversion of reference sequence"""
    CNV = auto()
    """Copy number variable region"""
    BND = auto()
    """Breakend"""


SVTYPE_VALUE_STRING_PAIRS = [
    (SVType.DEL, "DEL"),
    (SVType.INS, "INS"),
    (SVType.DUP, "DUP"),
    (SVType.INV, "INV"),
    (SVType.CNV, "CNV"),
    (SVType.BND, "BND"),
]

SVTYPE_STRINGS = [k for _, k in SVTYPE_VALUE_STRING_PAIRS]

_SVTYPE_TO_STR_DICT = dict(SVTYPE_VALUE_STRING_PAIRS)

_SVTYPE_OF_STR_DICT = {k: j for j, k in SVTYPE_VALUE_STRING_PAIRS}


def svtype_to_string(variant_type: SVType) -> str:
    """Convert a first-level structural variant type to a string.

    :param variant_type: First-level structural variant type.

    :return: String that designates `variant_type`.

    """
    assert variant_type in _SVTYPE_TO_STR_DICT
    return _SVTYPE_TO_STR_DICT[variant_type]


def svtype_of_string(line: str) -> Optional[SVType]:
    """Convert a line to the first-level structural variant type.

    :param line: Line possibly encoding a first-level structural
      variant type.

    :return: First-level structural variant type.

    """
    return dict.get(_SVTYPE_OF_STR_DICT, line)


class AltLine(structured.StructuredWithDescriptionLine):
    """ALT meta-information line.

    :note: The ID and Description fields and keys and values of extra
      fields are guaranteed to be non-empty. The ID is guaranteed to
      be a valid one-character IUPAC ambiguous nucleotide code or a
      list of structural variant type values which first level is a
      value from :py:class:`SVType`.

    """

    _levels: Optional[Tuple[SVType, List[str]]]
    _iupac_code: Optional[str]

    _key = "ALT"

    def __init__(self, id_field: str, description: str, extra: Fields):
        super().__init__(self._key, id_field, description, extra)

        def check_iupac_code(id_field: str) -> None:
            """Check that the ALT ID field is a valid IUPAC ambiguity code."""
            if id_field not in iupac.get_ambiguous_symbol_characters():
                raise error.LineError(error.LineErrorType.INCORRECT_ALT_IUPAC_CODE)

        if len(id_field) == 1:
            check_iupac_code(id_field)
            self._levels = None
            self._iupac_code = id_field
        else:
            parts = str.split(id_field, ":")
            first_level = svtype_of_string(parts[0])
            if first_level is None:
                raise error.LineError(error.LineErrorType.INCORRECT_ALT_FIRST_LEVEL)
            self._levels = (first_level, parts[1:])
            self._iupac_code = None

        self._description = description
        self._extra = extra

    @property
    def levels(self) -> Optional[Tuple[SVType, List[str]]]:
        """Structural variant type levels of an ALT meta-information line."""
        return self._levels

    @property
    def iupac_code(self) -> Optional[str]:
        """IUPAC code of an ALT meta-information line."""
        return self._iupac_code

    def is_iupac_ambiguous_code(self) -> bool:
        """Does the ALT line represent an IUPAC ambiguity code?"""
        return self.iupac_code is not None

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AltLine):
            return NotImplemented
        return (
            self.iupac_code == other.iupac_code
            and self.levels == other.levels
            and self.description == other.description
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(levels={:s}, iupac_code={:s}, description='{:s}', {:s})",
            self.__class__.__name__,
            repr(self.levels),
            repr(self.iupac_code),
            self.description,
            self.extra_repr(),
        )

    def _id_to_string(self) -> str:
        """Form the ALT ID string."""
        if self.is_iupac_ambiguous_code():
            assert self.iupac_code is not None
            return self.iupac_code
        assert self.levels is not None
        return str.join(":", [svtype_to_string(self.levels[0])] + self.levels[1])

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(
                self._key,
                self._id_to_string(),
                [("Description", self.description)],
                self.extra,
            ),
            self._unquoted_fields,
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["AltLine"]:
        """Convert a line to the ALT meta-information line.

        :param line: Line possible encoding an ALT meta-information line.

        :return: ALT meta-information line if `line` encodes it,
          `None` otherwise.

        """

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )
        if parts is not None and parts.key == cls._key:
            description = pcommon.assoc_single(parts.required_fields, "Description")
            assert description is not None
            try:
                return AltLine(parts.id_field, description, parts.extra_fields)
            except error.LineError:
                return None
        return None


_IUPAC_CODE_LINES = [
    AltLine(
        k.name,
        str.format(
            "IUPAC code {:s} = {:s}",
            k.name,
            str.join("/", (str(j) for j in iupac.Symbol.to_nucleotides(k))),
        ),
        [],
    )
    for k in iupac.get_ambiguous_symbols()
]


def get_iupac_symbol_lines() -> List[AltLine]:
    """Return the list of ALT meta lines for IUPAC ambiguity codes.

    :return: List containing one ALT meta line record for each
      ambiguous IUPAC nucleotide symbol except for 'N'.

    """
    return _IUPAC_CODE_LINES
