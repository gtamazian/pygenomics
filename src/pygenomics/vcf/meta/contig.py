"""Contig meta-information lines in the VCF format.

The module implements meta-information lines describing contigs or
other genome assembly units as defined in Subsection 1.4.7 "Contig
field format" from the VCF specification.

"""

from typing import FrozenSet, Optional, Tuple

from pygenomics.common import assoc_extract, str_to_int
from pygenomics.vcf.meta import common, error, structured

Fields = common.Fields


DISALLOWED_CHARACTERS = frozenset("\\,\"`'()[]{}<>")
"""Characters not allowed in contig names."""


def is_correct_name(name: str) -> bool:
    """Check if a contig name is correct.

    Correct contig names are defined in Subsection 1.4.7 "Contig field
    format" of the VCF specification.

    :param name: Contig name to be checked.

    :return: Is `name` a correct name for a contig?

    """
    return not (
        str.startswith(name, "*")
        or str.startswith(name, "=")
        or any(k in DISALLOWED_CHARACTERS for k in name)
    )


class ContigLine(structured.StructuredLine):
    """Contig meta-information line.

    :note: The ID field and keys and values of its extra fields are
      guaranteed to be non-empty. The ID is also guaranteed to be a
      correct contig name as defined in Subsection 1.4.7 of the VCF
      specification and the Length field is guaranteed to store a
      positive number if it is not `None`.

    """

    _length: Optional[int]

    _key = "contig"
    _unquoted_fields = frozenset(["length", "URL", "md5", "assembly", "taxonomy"])
    _required_fields: FrozenSet[str] = frozenset()

    def __init__(self, name: str, length: Optional[int], extra: Fields):
        def check_name() -> None:
            """Check the contig name value."""
            if not name or not is_correct_name(name):
                raise error.LineError(error.LineErrorType.INCORRECT_CONTIG_NAME)

        def check_length() -> None:
            """Check the contig length value."""
            if length is not None and length <= 0:
                raise error.LineError(error.LineErrorType.INCORRECT_CONTIG_LENGTH)

        check_name()
        check_length()
        error.check_extra_fields(extra)

        super().__init__(self.key, name, extra)
        self._length = length

    @property
    def name(self) -> str:
        """Contig name."""
        return self._id_field

    @property
    def length(self) -> Optional[int]:
        """Contig length."""
        return self._length

    @property
    def extra(self) -> Fields:
        """Extra fields of a contig meta-information line."""
        return self._extra

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, ContigLine):
            return NotImplemented
        return (
            self.name == other.name
            and self.length == other.length
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(name={:s}, length={:s}, extra={:s})",
            self.__class__.__name__,
            self.name,
            repr(self.length),
            repr(self.extra),
        )

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(
                self._key,
                self.name,
                [("length", str.format("{:d}", self.length))]
                if self.length is not None
                else [],
                self.extra,
            ),
            self._unquoted_fields,
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["ContigLine"]:
        """Convert a line to the contig meta-information line.

        :param line: Line possibly encoding a contig meta-information line.

        :return: Contig meta-information line if `line` encodes it,
          `None` otherwise.

        """

        def parse_extra_fields(fields: Fields) -> Tuple[bool, Optional[int], Fields]:
            """Parse the length field for the contig meta-information line."""
            length_fields, rest = assoc_extract(fields, "length")
            fail_flag = False
            length: Optional[int] = None
            if length_fields:
                assert len(length_fields) == 1
                length = str_to_int(length_fields[0])
                fail_flag = length is None

            return (fail_flag, length, rest)

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )
        if parts is not None and parts.key == cls._key:
            fail_flag, length, extra = parse_extra_fields(parts.extra_fields)
            if fail_flag:
                return None
            try:
                return ContigLine(parts.id_field, length, extra)
            except error.LineError:
                return None
        return None
