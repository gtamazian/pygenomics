"""Regular meta-information lines in the VCF format.

The module implements regular (non-structured) meta-information lines
as defined in Section 1.4 "Meta-information lines" from the VCF
specification.

"""

from typing import Optional

from pygenomics.vcf.meta import common, error


class RegularLine:
    """Regular meta-information line.

    :note: The key and the value of a meta-information line are
      guaranteed to be non-empty.

    Regular meta-information lines are defined in Section 1.4
    "Meta-information lines" of the VCF specification.

    """

    _key: str
    _value: str

    def __init__(self, key: str, value: str):

        error.check_key(key)
        error.check_value(value)

        self._key = key
        self._value = value

    @property
    def key(self) -> str:
        """Key of a regular meta-information line."""
        return self._key

    @property
    def value(self) -> str:
        """Value of a regular meta-information line."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, RegularLine):
            return NotImplemented
        return self.key == other.key and self.value == other.value

    def __str__(self) -> str:
        return str.format("##{:s}={:s}", self.key, self.value)

    def __repr__(self) -> str:
        return str.format(
            "{:s}(key='{:s}', value='{:s}')",
            self.__class__.__name__,
            self.key,
            self.value,
        )

    @staticmethod
    def of_string(line: str) -> Optional["RegularLine"]:
        """Convert a line to the regular meta-information line.

        :param line: Line possibly encoding a regular meta-information line.

        :return: Regular meta-information line if `line` encodes it,
          `None` otherwise.

        """
        parts = common.split_line(line)
        if parts is None:
            return None
        try:
            key, value = parts
            return RegularLine(key, value)
        except error.LineError:
            return None
