"""FILTER meta-information lines.

The module implements FILTER meta-information lines as defined in
Subsection 1.4.3 "Filter field format" of the VCF specification.

"""

from typing import Optional

from pygenomics.vcf.meta import common, structured

Fields = common.Fields


class FilterLine(structured.StructuredLine):
    """FILTER meta-information line.

    :note: The ID and extra key-value pairs of a FILTER
      meta-information line are guaranteed to be non-empty.

    FILTER meta-information lines are defined in Subsection 1.4.3
    "Filter field format" of the VCF specification.

    """

    _key = "FILTER"

    def __init__(self, id_field: str, extra: Fields):
        super().__init__(self._key, id_field, extra)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, FilterLine):
            return NotImplemented
        return self.id_field == other.id_field and self.extra == other.extra

    @classmethod
    def of_string(cls, line: str) -> Optional["FilterLine"]:
        """Convert a line to the FILTER meta-information line.

        :param line: Line possibly encoding a FILTER meta-information line.

        :return: FILTER meta-information line if `line` encodes it,
          `None` otherwise.

        """

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )
        if parts is not None and parts.key == cls._key:
            return FilterLine(parts.id_field, parts.extra_fields)
        return None
