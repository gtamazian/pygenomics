"""PEDIGREE meta-information lines in the VCF format.

The module implements meta-information lines describing relationships
between genomes as defined in Subsection 1.4.9 "Pedigree field format"
of the VCF specification.

"""

from typing import Optional

from pygenomics.vcf.meta import common, structured

Fields = common.Fields


class PedigreeLine(structured.StructuredLine):
    """PEDIGREE meta-information line.

    :note: The ID field and keys and values of its extra fields are
      guaranteed to be non-empty.

    """

    _key = "PEDIGREE"
    _unquoted_fields = frozenset(["Original", "Father", "Mother"])

    def __init__(self, id_field: str, extra: Fields):
        super().__init__(self._key, id_field, extra)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, PedigreeLine):
            return NotImplemented
        return self.id_field == other.id_field and self.extra == other.extra

    @classmethod
    def of_string(cls, line: str) -> Optional["PedigreeLine"]:
        """Convert a line to the PEDIGREE meta-information line.

        :param line: Line possibly encoding a PEDIGREE
          meta-information line.

        :return: PEDIGREE meta-information line if `line` encodes it,
          `None` otherwise.

        """

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )

        if parts is not None and parts.key == cls._key:
            return PedigreeLine(parts.id_field, parts.extra_fields)
        return None
