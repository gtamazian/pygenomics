"""Value numbers for INFO and FORMAT meta-information lines.

The value numbers are defined in Subsection 1.4.2 "Information field
format" of the VCF specification.
"""

from enum import Enum, auto
from typing import Optional, Union

from pygenomics.vcf.meta import error


class SpecialNumber(Enum):
    """Special cases for the number of the field values.

    The special cases are defined in Subsection 1.4.2 "Information
    field format" of the VCF specification.

    """

    A = auto()
    """One value per alternative allele."""
    R = auto()
    """One value per each allele, including the reference one."""
    G = auto()
    """One value for each possible genotype."""
    UNKNOWN = auto()
    """Arbitrary number of values."""

    def __str__(self) -> str:
        if self == SpecialNumber.A:
            return "A"
        if self == SpecialNumber.R:
            return "R"
        if self == SpecialNumber.G:
            return "G"
        assert self == SpecialNumber.UNKNOWN
        return "."

    @staticmethod
    def of_string(line: str) -> Optional["SpecialNumber"]:
        """Convert a line to the special value number.

        :param line: Line possibly encoding a special value number.

        :return: Special value number if `line` encodes it, `None`
          otherwise.

        """
        if line == "A":
            return SpecialNumber.A
        if line == "R":
            return SpecialNumber.R
        if line == "G":
            return SpecialNumber.G
        if line == ".":
            return SpecialNumber.UNKNOWN
        return None


class NegativeNumberError(error.Error):
    """Error indicating the negative value number for class :py:class:`Number`

    Class :py:class:`Number` guarantees that its regular number
    value is a non-negative number.

    """

    number: int

    def __init__(self, number: int):
        super().__init__(self, number)
        self.number = number


class Number:
    """Number of values for the INFO and FORMAT field values.

    :note: The class guarantees that either `regular_number` or
      `special_number` is not `None`. For a regular number, only
      non-negative values are allowed.

    """

    _regular_number: Optional[int]
    _special_number: Optional[SpecialNumber]

    def __init__(self, number: Union[int, SpecialNumber]):
        if isinstance(number, int):
            if number < 0:
                raise NegativeNumberError(number)
            self._regular_number = number
            self._special_number = None
        else:
            self._regular_number = None
            self._special_number = number

    @property
    def regular_number(self) -> Optional[int]:
        """Regular number of values."""
        return self._regular_number

    @property
    def special_number(self) -> Optional[SpecialNumber]:
        """Special number of values."""
        return self._special_number

    @staticmethod
    def of_string(line: str) -> Optional["Number"]:
        """Convert a line to the value number.

        :param number: Line possibly encoding the value number.

        :return: Value number if `line` encodes it, `None` otherwise.

        """
        try:
            regular_number = int(line)
            try:
                return Number(regular_number)
            except NegativeNumberError:
                return None
        except ValueError:
            special_number = SpecialNumber.of_string(line)
            if special_number is not None:
                return Number(special_number)
            return None

    def is_regular(self) -> bool:
        """Is the value number a regular integer?

        :return: `True` if the value number is a regular integer,
          `False` otherwise.
        """
        return self.regular_number is not None

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Number):
            return NotImplemented

        if self.is_regular() == Number.is_regular(other):
            if self.is_regular():
                return self.regular_number == other.regular_number
            return self.special_number == other.special_number
        return False

    def __repr__(self) -> str:
        return str.format(
            "{:s}(regular_number={:s}, special_number={:s})",
            self.__class__.__name__,
            repr(self.regular_number),
            repr(self.special_number),
        )

    def __str__(self) -> str:
        if self.is_regular():
            return str.format("{:d}", self.regular_number)
        return str(self.special_number)
