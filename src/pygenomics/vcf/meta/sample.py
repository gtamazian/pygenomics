"""SAMPLE and META meta-information lines in the VCF format.

The module implements META and SAMPLE meta-information lines as
defined in Subsection 1.4.8 "Sample field format" of the VCF
specification.

"""

from typing import List, Optional

from pygenomics.common import assoc_single, partition
from pygenomics.vcf import datatypes
from pygenomics.vcf.meta import common, error, valuenumber
from pygenomics.vcf.meta.structured import StructuredLine

Fields = common.Fields


class MetaLine(StructuredLine):
    """META meta-information line.

    :note: The ID field, the list of values and elements of the list
      are guaranteed to be non-empty.

    """

    _type_field = datatypes.DataType.STRING
    _number = valuenumber.Number(valuenumber.SpecialNumber.UNKNOWN)

    _values: List[str]

    _key = "META"
    _unquoted_fields = frozenset(["Type", "Number", "Values"])
    _required_fields = frozenset(["Type", "Number", "Values"])

    def __init__(self, id_field: str, values: List[str], extra: Fields):
        def check_values() -> None:
            """Check that the list of values is valid."""
            if not values:
                raise error.LineError(error.LineErrorType.EMPTY_METALINE_VALUE_LIST)
            if any(not k for k in values):
                raise error.LineError(error.LineErrorType.EMPTY_METALINE_VALUE)

        check_values()

        super().__init__(self._key, id_field, extra)
        self._values = values

    @property
    def values(self) -> List[str]:
        """Values specified in the META meta-information line."""
        return self._values

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, MetaLine):
            return NotImplemented
        return (
            self.id_field == other.id_field
            and self.values == other.values
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}({:s}, values={:s}, {:s})",
            self.__class__.__name__,
            self.id_field_repr(),
            repr(self.values),
            self.extra_repr(),
        )

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(
                self._key,
                self._id_field,
                [
                    ("Type", str(self._type_field)),
                    ("Number", str(self._number)),
                    ("Values", "[" + str.join(", ", self.values) + "]"),
                ],
                self.extra,
            ),
            self._unquoted_fields,
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["MetaLine"]:
        """Convert a line to the META meta-information line.

        :param line: Line possibly encoding a META meta-information line.

        :return: META meta-information line if `line` encodes it,
          `None` otherwise.

        """

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [("[", "]"), ('"', '"')]
        )

        def check_type_field(field: Optional[str]) -> bool:
            """Check the Type field of the line."""
            assert field is not None
            type_value = datatypes.DataType.of_string(field)
            return type_value == cls._type_field if type_value is not None else False

        def check_number_field(field: Optional[str]) -> bool:
            """Check the Number field of the line."""
            assert field is not None
            number_value = valuenumber.Number.of_string(field)
            return number_value == cls._number if number_value is not None else False

        def parse_values(field: Optional[str]) -> Optional[List[str]]:
            """Parse the Values field of the line."""
            assert field is not None
            if len(field) > 1 and field[0] == "[" and field[-1] == "]":
                return [str.strip(k) for k in str.split(field[1:-1], ",")]
            return None

        if (
            parts is not None
            and parts.key == cls._key
            and check_type_field(assoc_single(parts.required_fields, "Type"))
            and check_number_field(assoc_single(parts.required_fields, "Number"))
        ):
            values = parse_values(assoc_single(parts.required_fields, "Values"))
            if values:
                try:
                    return MetaLine(parts.id_field, values, parts.extra_fields)
                except error.LineError:
                    return None
        return None


class SampleLine(StructuredLine):
    """SAMPLE meta-information line.

    :note: The ID field, sample features defined in META line and keys
      and values of extra fields are guaranteed to be non-empty.

    """

    _meta: Fields

    _key = "SAMPLE"

    def __init__(self, id_field: str, meta: Fields, extra: Fields):
        error.check_extra_fields(meta + extra)
        super().__init__(self._key, id_field, extra)
        self._meta = meta

    @property
    def meta(self) -> Fields:
        """META fields for the sample."""
        return self._meta

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SampleLine):
            return NotImplemented
        return (
            self.id_field == other.id_field
            and self.meta == other.meta
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}({:s}, meta={:s}, {:s})",
            self.__class__.__name__,
            self.id_field_repr(),
            repr(self.meta),
            self.extra_repr(),
        )

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(
                self._key, self._id_field, [], self.meta + self.extra
            ),
            frozenset(k for k, _ in self.meta),
        )

    @classmethod
    def sample_of_string(
        cls, meta_lines: List[MetaLine], line: str
    ) -> Optional["SampleLine"]:
        """Convert a line to the SAMPLE meta-information line.

        :param line: Line possibly encoding a SAMPLE meta-information line.

        :param meta_fields: META lines that define sample attributes.

        :return: SAMPLE meta-information line if `line` encodes it,
          `None` otherwise.

        """

        meta_values = {k.id_field: set(k.values) for k in meta_lines}

        def check_meta_fields(fields: Fields) -> bool:
            """Check if values of META fields are valid."""
            return all(value in meta_values[key] for key, value in fields)

        parts = common.structured_line_of_string(
            line, cls._required_fields, frozenset(dict.keys(meta_values)), [('"', '"')]
        )

        if parts is not None and parts.key == cls._key:
            meta, extra = partition(parts.extra_fields, lambda x: x[0] in meta_values)
            if check_meta_fields(meta):
                return SampleLine(parts.id_field, meta, extra)
        return None
