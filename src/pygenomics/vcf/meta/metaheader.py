"""Routines for reading the VCF header formed by meta-information line."""

from enum import Enum, auto
from typing import List, NamedTuple, Optional, TypeVar

from pygenomics.vcf.meta import (
    alt,
    contig,
    error,
    filterline,
    infoformat,
    pedigree,
    regular,
    sample,
    structured,
)


class MetaHeader(NamedTuple):
    """Meta-information lines that define a VCF file."""

    regular: List[regular.RegularLine]
    """Regular meta-information lines."""
    structured: List[structured.StructuredLine]
    """Structured non-specific meta-information lines."""
    contigs: List[contig.ContigLine]
    """CONTIG meta-information lines."""
    info_lines: List[infoformat.InfoFormatLine]
    """INFO meta-information lines."""
    format_lines: List[infoformat.InfoFormatLine]
    """FORMAT meta-information lines."""
    alt: List[alt.AltLine]
    """ALT meta-information lines."""
    filter_lines: List[filterline.FilterLine]
    """FILTER meta-information lines."""
    pedigree: List[pedigree.PedigreeLine]
    """PEDIGREE meta-information lines."""
    meta_lines: List[sample.MetaLine]
    """META meta-information lines."""
    sample_lines: List[sample.SampleLine]
    """SAMPLE meta-information lines."""

    @classmethod
    def initialize(cls) -> "MetaHeader":
        """Create an empty environment."""
        return cls([], [], [], [], [], [], [], [], [], [])


class LineType(Enum):
    """Meta-information line types."""

    REGULAR = auto()
    """Key-value pair."""
    STRUCTURED = auto()
    """Structured unspecified line."""
    CONTIG = auto()
    """CONTIG meta-information line."""
    INFO = auto()
    """INFO meta-information line."""
    FORMAT = auto()
    """FORMAT meta-information line."""
    ALT = auto()
    """ALT meta-information line."""
    FILTER = auto()
    """FILTER meta-information line."""
    PEDIGREE = auto()
    """PEDIGREE meta-information line."""
    SAMPLE = auto()
    """SAMPLE meta-information line."""
    META = auto()
    """META meta-information line."""

    @classmethod
    def get_line_type(cls, line: str) -> Optional["LineType"]:
        """Get the meta-information line type.

        :param line: A meta-information line.

        :return: The line type if the line prefix is correct, `None`
          otherwise.

        """

        values = {
            "contig": cls.CONTIG,
            "INFO": cls.INFO,
            "FORMAT": cls.FORMAT,
            "ALT": cls.ALT,
            "FILTER": cls.FILTER,
            "PEDIGREE": cls.PEDIGREE,
            "SAMPLE": cls.SAMPLE,
            "META": cls.META,
        }

        preliminary = regular.RegularLine.of_string(line)
        if preliminary is None:
            raise error.MetaHeaderError(line)
        if preliminary.key in values:
            return values[preliminary.key]
        return (
            cls.STRUCTURED
            if str.startswith(preliminary.value, "<")
            and str.startswith(preliminary.value, ">")
            else cls.REGULAR
        )


_T = TypeVar("_T")


def add_line(current: MetaHeader, new_line: str) -> MetaHeader:
    """Add a meta-information line to the meta-information header."""

    def add_parsed_line(target: List[_T], parsing_result: Optional[_T]) -> None:
        """Add the parsed line or raise the exception if the line parsing failed."""
        if parsing_result is None:
            raise error.MetaHeaderError(new_line)
        list.append(target, parsing_result)

    line_type = LineType.get_line_type(new_line)
    if line_type is None:
        raise error.MetaHeaderError(new_line)
    if line_type == LineType.REGULAR:
        add_parsed_line(current.regular, regular.RegularLine.of_string(new_line))
    elif line_type == LineType.STRUCTURED:
        add_parsed_line(
            current.structured, structured.StructuredLine.of_string(new_line)
        )
    elif line_type == LineType.CONTIG:
        add_parsed_line(current.contigs, contig.ContigLine.of_string(new_line))
    elif line_type == LineType.INFO:
        add_parsed_line(
            current.info_lines, infoformat.InfoFormatLine.of_string(new_line)
        )
    elif line_type == LineType.FORMAT:
        add_parsed_line(
            current.format_lines, infoformat.InfoFormatLine.of_string(new_line)
        )
    elif line_type == LineType.ALT:
        add_parsed_line(current.alt, alt.AltLine.of_string(new_line))
    elif line_type == LineType.FILTER:
        add_parsed_line(current.filter_lines, filterline.FilterLine.of_string(new_line))
    elif line_type == LineType.PEDIGREE:
        add_parsed_line(current.pedigree, pedigree.PedigreeLine.of_string(new_line))
    elif line_type == LineType.META:
        add_parsed_line(current.meta_lines, sample.MetaLine.of_string(new_line))
    else:
        assert line_type == LineType.SAMPLE
        add_parsed_line(
            current.sample_lines,
            sample.SampleLine.sample_of_string(current.meta_lines, new_line),
        )

    return current
