"""INFO and FORMAT meta-information lines.

The module implements INFO and FORMAT meta-information lines as
defined in Subsections 1.4.2 "Information field format" and 1.4.4
"Individual format field" of the VCF specification.

"""

from enum import Enum, auto
from typing import Optional

from pygenomics.common import assoc_single
from pygenomics.vcf import datatypes
from pygenomics.vcf.meta import common, error, valuenumber
from pygenomics.vcf.meta.structured import StructuredWithDescriptionLine

Fields = common.Fields


class LineKind(Enum):
    """Kind of a meta-information line: INFO or FORMAT."""

    INFO = auto()
    """INFO meta-information line."""
    FORMAT = auto()
    """FORMAT meta-information line."""

    def __str__(self) -> str:
        return "INFO" if self == LineKind.INFO else "FORMAT"


class InfoFormatLine(StructuredWithDescriptionLine):
    """INFO or FORMAT meta-information line."""

    _kind: LineKind
    _number: valuenumber.Number
    _type_field: datatypes.DataType

    _unquoted_fields = frozenset(["Number", "Type"])
    _required_fields = frozenset(["Number", "Type", "Description"])

    def __init__(
        self,
        kind: LineKind,
        id_field: str,
        number: valuenumber.Number,
        type_field: datatypes.DataType,
        description: str,
        extra: Fields,
    ):  # pylint: disable=too-many-arguments
        def check_type_and_number() -> None:
            """Check if the Type and Number fields are valid."""
            if kind == LineKind.INFO:
                if (
                    type_field == datatypes.DataType.FLAG
                    and number != valuenumber.Number(0)
                ):
                    raise error.LineError(error.LineErrorType.INCORRECT_FLAG_NUMBER)
            else:
                # the Flag type is not allowed in FORMAT lines
                if type_field == datatypes.DataType.FLAG:
                    raise error.LineError(error.LineErrorType.FLAG_TYPE)

        error.check_id_field(id_field)
        check_type_and_number()
        error.check_description(description)
        error.check_extra_fields(extra)

        super().__init__(str(kind), id_field, description, extra)
        self._kind = kind
        self._number = number
        self._type_field = type_field

    @property
    def kind(self) -> LineKind:
        """Meta-information line kind: INFO or FORMAT."""
        return self._kind

    @property
    def id_field(self) -> str:
        """ID of an INFO or FORMAT meta-information line."""
        return self._id_field

    @property
    def number(self) -> valuenumber.Number:
        """ "Value number of an INFO or FORMAT meta-information line."""
        return self._number

    @property
    def type_field(self) -> datatypes.DataType:
        """Data type of an INFO or FORMAT meta-information line."""
        return self._type_field

    @property
    def description(self) -> str:
        """Description of an INFO or FORMAT meta-information line."""
        return self._description

    @property
    def extra(self) -> Fields:
        """Extra fields of an INFO or FORMAT meta-information line."""
        return self._extra

    def is_consistent(self, other: "InfoFormatLine") -> bool:
        """Check consistency with another INFO or FORMAT line.

        The function checks whether line kinds, IDs and Number and
          Type fields are equal. Description and extra fields are
          ignored.

        :param other: INFO or FORMAT line to compare with.

        :return: Is `other` consistent with the current line?

        """
        return (
            self.kind == other.kind
            and self.id_field == other.id_field
            and self.number == other.number
            and self.type_field == other.type_field
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, InfoFormatLine):
            return NotImplemented
        return (
            self.is_consistent(other)
            and self.description == other.description
            and self.extra == other.extra
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(kind={:s}, {:s}, number={:s}, type_field={:s}, "
            "description='{:s}', {:s})",
            self.__class__.__name__,
            repr(self.kind),
            self.id_field_repr(),
            repr(self.number),
            repr(self.type_field),
            self.description,
            self.extra_repr(),
        )

    def __str__(self) -> str:
        return common.structured_line_to_string(
            common.StructuredLineParts(
                str(self.kind),
                self.id_field,
                [
                    ("Number", str(self.number)),
                    ("Type", str(self.type_field)),
                    ("Description", self.description),
                ],
                self.extra,
            ),
            self._unquoted_fields,
        )

    @classmethod
    def of_string(cls, line: str) -> Optional["InfoFormatLine"]:
        """Convert a line to the INFO or FORMAT meta-information line.

        :param line: Line possibly encoding an INFO or FORMAT
          meta-information line.

        :return: INFO or FORMAT meta-information line if `line`
          encodes it, `None` otherwise.

        """

        def parse_type_field(line: Optional[str]) -> Optional[datatypes.DataType]:
            """Parse the Type field value."""
            return datatypes.DataType.of_string(line) if line is not None else None

        def parse_number_field(line: Optional[str]) -> Optional[valuenumber.Number]:
            """Parse the value number (Number) field."""
            return valuenumber.Number.of_string(line) if line is not None else None

        parts = common.structured_line_of_string(
            line, cls._required_fields, cls._unquoted_fields, [('"', '"')]
        )
        if parts is not None and parts.key in {"INFO", "FORMAT"}:
            assert len(parts.required_fields) <= len(cls._required_fields)
            type_field = parse_type_field(assoc_single(parts.required_fields, "Type"))
            number = parse_number_field(assoc_single(parts.required_fields, "Number"))
            description = assoc_single(parts.required_fields, "Description")
            assert (
                type_field is not None
                and number is not None
                and description is not None
            )
            try:
                return InfoFormatLine(
                    LineKind.INFO if parts.key == "INFO" else LineKind.FORMAT,
                    parts.id_field,
                    number,
                    type_field,
                    description,
                    parts.extra_fields,
                )
            except error.LineError:
                return None
        return None
