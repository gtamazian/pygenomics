"""Exceptions for subpackage pygenomics.vcf."""

from enum import Enum, auto

from pygenomics import error


class Error(error.Error):
    """Base class for exceptions related to the VCF format."""


class ReaderErrorType(Enum):
    """Types of errors that may occur while reading a VCF file."""

    INCORRECT_HEADER_LINE = auto()
    """Incorrect VCF header line."""
    INCORRECT_DATA_LINE = auto()
    """Incorrect VCF data line."""


class ReaderError(Error):
    """Error that occurred while reading a VCF file."""

    error_type: ReaderErrorType
    line: str

    def __init__(self, error_type: ReaderErrorType, line: str):
        super().__init__(self, error_type, line)
        self.error_type = error_type
        self.line = line
