"""Reading and writing genomic variants in the VCF format.

The module implements routines for processing VCF files according to
The Variant Call Format (VCF) Version 4.3 Specification (the VCF
specification).

"""

import os

from pygenomics.vcf import characters

assert os.linesep in characters.LINE_SEPARATORS, "incorrect line separator"
