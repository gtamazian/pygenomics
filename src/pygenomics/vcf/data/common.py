"""Common routines for subpackage pygenomics.vcf.data."""

from typing import Callable, Optional, Tuple, TypeVar

from pygenomics import common

_T = TypeVar("_T")

_MISSING = "."


def encode_missing_value(encoder: Callable[[_T], str], value: Optional[_T]) -> str:
    """Convert a value that may be missing to a string.

    :param encoder: Function that converts a non-missing value to a
      string.

    :param value: Value to be converted.

    :return: String representing `value` if `value` is not `None`,
      :py:const:`MISSING` otherwise.

    """
    return encoder(value) if value is not None else _MISSING


def decode_missing_value(
    decoder: Callable[[str], Optional[_T]], value_string: str
) -> Tuple[bool, Optional[_T]]:
    """Decode a string that may represent a missing value.

    :param decoder: Function that decodes a potentially missing value;
      the function should return `None` if the specified string does not
      encode a proper value.

    :param value_string: String representing the value to be decoded.

    :return: Tuple that contains a flag indicating the specified
      string was successfully parsed and the value derived from the
      string.

    """
    return common.optional_from_string(decoder, value_string, _MISSING)
