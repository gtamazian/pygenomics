"""ALT fields for a variant record in the VCF format."""

from enum import Enum, auto
from typing import Optional

from pygenomics import common, nucleotide
from pygenomics.vcf.data import error


class FieldType(Enum):
    """Type of the ALT field value."""

    BASE_PAIRS = auto()
    """Base pairs of an alternative allele."""
    SPANNING_DELETION = auto()
    """An alternative allele is missing due to spanning deletion."""
    UNKNOWN = auto()
    """Unknown alternative allele as specified in a gVCF file."""
    NON_REF = auto()
    """Non-reference symbolic allele <NON_REF>."""
    ANGLE_BRACKETED = auto()
    """An angle-bracketed ID string specified in an ALT meta-information line."""

    @classmethod
    def identify_allele(cls, allele: str) -> Optional["FieldType"]:
        """Identify the alternative allele type."""
        if nucleotide.is_exact_or_gap(allele):
            return cls.BASE_PAIRS
        if allele == "*":
            return cls.SPANNING_DELETION
        if allele == "<*>":
            return cls.UNKNOWN
        if allele == "<NON_REF>":
            return cls.NON_REF
        return (
            cls.ANGLE_BRACKETED
            if common.strip_prefix_suffix(allele, "<", ">") is not None
            else None
        )


class Field:
    """ALT field for a variant record."""

    _content: Optional[str]
    _value_type: FieldType

    def __init__(self, value_type: FieldType, content: Optional[str]):
        def check_values() -> None:
            if value_type == FieldType.BASE_PAIRS:
                if not content:
                    raise error.AltFieldError(error.AltFieldErrorType.EMPTY_CONTENT)
                if not nucleotide.is_exact_or_gap(content):
                    raise error.AltFieldError(error.AltFieldErrorType.INCORRECT_BASE)
            elif value_type == FieldType.ANGLE_BRACKETED:
                if not content:
                    raise error.AltFieldError(
                        error.AltFieldErrorType.EMPTY_ANGLE_BRACKETED_ID
                    )
                if "," in content:
                    raise error.AltFieldError(
                        error.AltFieldErrorType.COMMA_WITHIN_ANGLE_BRACKETS
                    )

        check_values()
        self._content = content
        self._value_type = value_type

    @property
    def value_type(self) -> FieldType:
        """Alternative allele value type."""
        return self._value_type

    @property
    def content(self) -> Optional[str]:
        """Alternative allele content."""
        return self._content

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Field):
            return NotImplemented
        if self.value_type == other.value_type:
            return (
                self.content == other.content
                if self.value_type in {FieldType.BASE_PAIRS, FieldType.ANGLE_BRACKETED}
                else True
            )
        return False

    def __lt__(self, other: object) -> bool:
        if not isinstance(other, Field):
            return NotImplemented
        if self.value_type == other.value_type == FieldType.BASE_PAIRS:
            assert self.content is not None and other.content is not None
            return self.content < other.content
        return False

    def __repr__(self) -> str:
        return str.format(
            "{:s}(content={:s}, value_type={:s})",
            self.__class__.__name__,
            '"' + self.content + '"' if self.content is not None else "None",
            repr(self.value_type),
        )

    def __str__(self) -> str:
        if self.value_type == FieldType.BASE_PAIRS:
            assert self.content is not None
            return self.content
        if self.value_type == FieldType.SPANNING_DELETION:
            return "*"
        if self.value_type == FieldType.UNKNOWN:
            return "<*>"
        if self.value_type == FieldType.NON_REF:
            return "<NON_REF>"
        assert self.value_type == FieldType.ANGLE_BRACKETED and self.content is not None
        return "<" + self.content + ">"

    @staticmethod
    def of_string(line: str) -> Optional["Field"]:
        """Parse the ALT field string.

        :param line: ALT field string.

        :return: Parsed ALT field if the line encodes it, `None`
          otherwise.

        """

        value_type = FieldType.identify_allele(line)
        if value_type is None:
            return None

        def get_content() -> Optional[str]:
            """Get the ALT value content."""
            if value_type == FieldType.ANGLE_BRACKETED:
                return line[1:-1]
            if value_type == FieldType.BASE_PAIRS:
                return line
            return None

        return Field(value_type, get_content())
