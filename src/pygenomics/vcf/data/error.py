"""Exceptions for subpackage pygenomics.vcf.data."""

from enum import Enum, auto

from pygenomics.vcf import error


class Error(error.Error):
    """Base class for exceptions in the subpackage."""


class GenotypeErrorType(Enum):
    """Types of invariant violations for a VCF genotype record."""

    ALLELES_SPECIFIED_FOR_MISSING_GENOTYPE = auto()
    """No alleles must be specified for a missing genotype."""
    ALLELES_NOT_SPECIFIED = auto()
    """Alleles must be specified for a non-missing genotype."""
    NON_POSITIVE_PLOIDY = auto()
    """Ploidy must be a positive integer."""
    INCORRECT_PLOIDY = auto()
    """Specified ploidy is not equal to the allele number."""


class GenotypeError(Error):
    """Error for a violation of VCF genotype invariants."""

    error_type: GenotypeErrorType

    def __init__(self, error_type: GenotypeErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class EmptyChrom(Error):
    """Error for an empty chromosome name."""


class IdFieldErrorType(Enum):
    """Types of invariant violations for the ID field."""

    DUPLICATE_IDS = auto()
    """Duplicate identifiers."""
    EMPTY_ID = auto()
    """An empty ID value."""
    MISSING_VALUE_ID = auto()
    """The missing value must not be an ID."""


class IdFieldError(Error):
    """Error for a violation of the ID field invariants."""

    error_type: IdFieldErrorType

    def __init__(self, error_type: IdFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class RefFieldErrorType(Enum):
    """Types of invariant violations for the REF field."""

    EMPTY_FIELD = auto()
    """Empty REF field."""
    INCORRECT_BASE = auto()
    """Incorrect base in the field."""


class RefFieldError(Error):
    """Error for a violation of the REF field invariants."""

    error_type: RefFieldErrorType

    def __init__(self, error_type: RefFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class FilterFieldErrorType(Enum):
    """Types of invariant violations for the FILTER field."""

    EMPTY_FIELD = auto()
    """Empty FILTER field."""
    ZERO_FIELD = auto()
    """Field "0" is forbidded by the VCF format specification."""


class FilterFieldError(Error):
    """Error for a violation of the FILTER field invariants."""

    error_type: FilterFieldErrorType

    def __init__(self, error_type: FilterFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class InfoFieldErrorType(Enum):
    """Types of invariant violations for the INFO field."""

    EMPTY_KEY = auto()
    """Empty key of the INFO field part."""
    INCORRECT_KEY = auto()
    """Incorrect key of the INFO field part."""
    EMPTY_VALUE = auto()
    """Empty value of the INFO field part."""
    INCORRECT_VALUE = auto()
    """Incorrect value of the INFO field part."""


class InfoFieldError(Error):
    """Error for a violation of the INFO field invariants."""

    error_type: InfoFieldErrorType

    def __init__(self, error_type: InfoFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class FormatFieldErrorType(Enum):
    """Types of invariant violations for the FORMAT field."""

    EMPTY_FIELD = auto()
    """Empty FORMAT field."""
    EMPTY_PART = auto()
    """Empty FORMAT field part."""
    INCORRECT_PART = auto()
    """Incorrect FORMAT field part."""


class FormatFieldError(Error):
    """Error for a violation of the FORMAT field invariants."""

    error_type: FormatFieldErrorType

    def __init__(self, error_type: FormatFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class AltFieldErrorType(Enum):
    """Types of invariant violations for the ALT field."""

    EMPTY_CONTENT = auto()
    """Empty ALT field content."""
    INCORRECT_BASE = auto()
    """Incorrect base pair."""
    EMPTY_ANGLE_BRACKETED_ID = auto()
    """Empty IDs within angle brackets."""
    COMMA_WITHIN_ANGLE_BRACKETS = auto()
    """There must be no commas within angle brackets."""


class AltFieldError(Error):
    """Error for a violation of the ALT field invariants."""

    error_type: AltFieldErrorType

    def __init__(self, error_type: AltFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class NegativePos(Error):
    """Error for a negative reference position value."""


class NegativeQual(Error):
    """Error for a negative variant quality score."""


class SampleFieldErrorType(Enum):
    """Types of incorrect values within the sample fields."""

    EMPTY_FIELD = auto()
    """Empty sample field."""
    EMPTY_VALUE = auto()
    """Empty element in a part of the field."""
    INCORRECT_VALUE = auto()
    """Incorrect element within parts of the field."""
    DOT_PART = auto()
    """Parts containing a dot only are disallowed."""
    COMMA_IN_ELEMENT = auto()
    """Elements must not contain commas."""


class SampleFieldError(Error):
    """Error for an incorrect sample field."""

    error_type: SampleFieldErrorType

    def __init__(self, error_type: SampleFieldErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type
