"""Variant records in the VCF format.

The module implements variant records as defined in Subsection 1.6
"Data lines" from the VCF specification.

"""

from typing import List, NamedTuple, Optional, Tuple

from pygenomics import nucleotide
from pygenomics.common import split2, str_to_float, str_to_int, strip_prefix_suffix
from pygenomics.vcf.data import common, error
from pygenomics.vcf.data.alt import Field as AltField

_MISSING = "."


def missing_value() -> str:
    """Return the missing value string.

    This function protects the module-level variable and should be
    used in routines outside the module (e.g., in test functions).

    """
    return _MISSING


class Chrom:
    """CHROM field for a variant record.

    The class guarantees that the chromosome name is not empty.
    """

    _name: str
    _angle_bracketed: bool

    def __init__(self, name: str, angle_bracketed: bool):
        if not name:
            raise error.EmptyChrom()
        self._name = name
        self._angle_bracketed = angle_bracketed

    @property
    def name(self) -> str:
        """Chromosome name without angle brackets if they were present."""
        return self._name

    @property
    def angle_bracketed(self) -> bool:
        """Is the chromosome name angle-bracketed?"""
        return self._angle_bracketed

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Chrom):
            return NotImplemented
        return self.name == other.name and self.angle_bracketed == other.angle_bracketed

    def __lt__(self, other: object) -> bool:
        if not isinstance(other, Chrom):
            return NotImplemented
        return self.name < other.name

    def __repr__(self) -> str:
        return str.format(
            '{:s}(name="{:s}", angle_bracketed={:s})',
            self.__class__.__name__,
            self.name,
            repr(self.angle_bracketed),
        )

    def __str__(self) -> str:
        return str.format("<{:s}>", self.name) if self.angle_bracketed else self.name

    @staticmethod
    def of_string(line: str) -> Optional["Chrom"]:
        """Parse the CHROM field string.

        :param line: CHROM field string.

        :return: Parsed CHROM field if the line encodes it, `None`
          otherwise.

        """
        name = strip_prefix_suffix(line, "<", ">")
        try:
            return Chrom(name, True) if name is not None else Chrom(line, False)
        except error.EmptyChrom:
            return None


class IdField:
    """ID field for a variant record.

    The class guarantees that the identifiers are unique and non-empty.
    """

    _identifiers: List[str]

    def __init__(self, identifiers: List[str]):
        def check_identifiers() -> None:
            if len(set(identifiers)) < len(identifiers):
                raise error.IdFieldError(error.IdFieldErrorType.DUPLICATE_IDS)
            for k in identifiers:
                if not k:
                    raise error.IdFieldError(error.IdFieldErrorType.EMPTY_ID)
                if k == _MISSING:
                    raise error.IdFieldError(error.IdFieldErrorType.MISSING_VALUE_ID)

        check_identifiers()
        self._identifiers = identifiers

    @property
    def identifiers(self) -> List[str]:
        """Identifiers of a variant."""
        return self._identifiers

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, IdField):
            return NotImplemented
        return self.identifiers == other.identifiers

    def __repr__(self) -> str:
        return str.format(
            "{:s}(identifiers={:s})", self.__class__.__name__, repr(self.identifiers)
        )

    def __str__(self) -> str:
        return str.join(";", self.identifiers) if self.identifiers else _MISSING

    @staticmethod
    def of_string(line: str) -> Optional["IdField"]:
        """Parse the ID field string.

        :param line: ID field string

        :return: Parsed ID field if the line encodes it, `None`
          otherwise.

        """
        if line == _MISSING:
            return IdField([])
        try:
            return IdField(str.split(line, ";"))
        except error.IdFieldError:
            return None


class RefField:
    """REF field for a variant record.

    The field must not be empty and its bases must be one of A, C, G,
    T, or N. Both cases are allowed. The field is case insensitive,
    which affects equality checks.

    """

    _bases: str

    def __init__(self, bases: str):
        if not bases:
            raise error.RefFieldError(error.RefFieldErrorType.EMPTY_FIELD)
        if not nucleotide.is_exact_or_gap(bases):
            raise error.RefFieldError(error.RefFieldErrorType.INCORRECT_BASE)
        self._bases = bases

    @property
    def bases(self) -> str:
        """Reference allele base pairs."""
        return self._bases

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, RefField):
            return NotImplemented
        return self.bases == other.bases

    def __repr__(self) -> str:
        return str.format('{:s}(bases="{:s}")', self.__class__.__name__, self.bases)

    def __str__(self) -> str:
        return self.bases

    @staticmethod
    def of_string(line: str) -> Optional["RefField"]:
        """Parse the REF field string.

        :param line: REF field string.

        :return: Parsed REF field if the line encodes it, `None`
          otherwise.

        """
        try:
            return RefField(line)
        except error.RefFieldError:
            return None


class FilterField:
    """FILTER field for a variant record.

    The class guarantees that filter values are not empty and do not
    contain "0".

    """

    _failed_filters: List[str]

    def __init__(self, failed_filters: List[str]) -> None:
        def check_filters() -> None:
            for k in failed_filters:
                if not k:
                    raise error.FilterFieldError(error.FilterFieldErrorType.EMPTY_FIELD)
                if k == "0":
                    raise error.FilterFieldError(error.FilterFieldErrorType.ZERO_FIELD)

        check_filters()
        self._failed_filters = failed_filters

    @property
    def failed_filters(self) -> List[str]:
        """Return the list of failed filter names."""
        return self._failed_filters

    def passed(self) -> bool:
        """Does the field indicate that the filters were passed?"""
        return not self.failed_filters

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, FilterField):
            return NotImplemented
        return self.failed_filters == other.failed_filters

    def __repr__(self) -> str:
        return str.format(
            "{:s}(failed_filters={:s})",
            self.__class__.__name__,
            repr(self.failed_filters),
        )

    def __str__(self) -> str:
        return "PASS" if self.passed() else str.join(";", self.failed_filters)

    @staticmethod
    def of_string(line: str) -> Optional["FilterField"]:
        """Parse the FILTER field string.

        :param line: FILTER field string.

        :return: Parsed FILTER field if the line encodes it, `None`
          otherwise.

        """
        if line == "PASS":
            return FilterField([])
        try:
            return FilterField(str.split(line, ";"))
        except error.FilterFieldError:
            return None


class InfoField:
    """INFO field for a variant record.

    The field parts must have non-empty keys and their value lists
    must not contain empty elements. The keys must not contain ";" and
    "=". The value elements must not contain ",", ";", and "=".

    """

    _parts: List[Tuple[str, List[str]]]

    def __init__(self, parts: List[Tuple[str, List[str]]]):
        def check_keys() -> None:
            for key, _ in parts:
                if not key:
                    raise error.InfoFieldError(error.InfoFieldErrorType.EMPTY_KEY)
                if ";" in key or "=" in key:
                    raise error.InfoFieldError(error.InfoFieldErrorType.INCORRECT_KEY)

        def check_values() -> None:
            for _, values in parts:
                for k in values:
                    if not k:
                        raise error.InfoFieldError(error.InfoFieldErrorType.EMPTY_VALUE)
                    if "," in k or ";" in k or "=" in k:
                        raise error.InfoFieldError(
                            error.InfoFieldErrorType.INCORRECT_VALUE
                        )

        check_keys()
        check_values()
        self._parts = parts

    @property
    def parts(self) -> List[Tuple[str, List[str]]]:
        """Parts of the INFO field."""
        return self._parts

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, InfoField):
            return NotImplemented
        return self.parts == other.parts

    def __repr__(self) -> str:
        return str.format("{:s}(parts={:s})", self.__class__.__name__, repr(self.parts))

    def __str__(self) -> str:
        return (
            str.join(
                ";",
                (
                    str.format("{:s}={:s}", key, str.join(",", values))
                    if values
                    else key
                    for key, values in self.parts
                ),
            )
            if self.parts
            else _MISSING
        )

    @staticmethod
    def of_string(line: str) -> Optional["InfoField"]:
        """Parse the INFO field string.

        :param line: INFO field string.

        :return: Parsed INFO field if the line encodes it, `None`
          otherwise.

        """

        if line == _MISSING:
            return InfoField([])

        def parse_part(part: str) -> Tuple[str, List[str]]:
            """Parse a single part of the INFO field."""
            pair = split2(part, "=")
            return (part, []) if pair is None else (pair[0], str.split(pair[1], ","))

        try:
            return InfoField([parse_part(k) for k in str.split(line, ";")])
        except error.InfoFieldError:
            return None


class FormatField:
    """FORMAT field for a variant record.

    The field parts must not be empty or contain ":". At least one
    part must be specified.

    """

    _parts: List[str]

    def __init__(self, parts: List[str]):
        def check_parts() -> None:
            if not parts:
                raise error.FormatFieldError(error.FormatFieldErrorType.EMPTY_FIELD)
            for k in parts:
                if not k:
                    raise error.FormatFieldError(error.FormatFieldErrorType.EMPTY_PART)
                if ":" in k:
                    raise error.FormatFieldError(
                        error.FormatFieldErrorType.INCORRECT_PART
                    )

        check_parts()
        self._parts = parts

    @property
    def parts(self) -> List[str]:
        """Parts of the FORMAT field."""
        return self._parts

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, FormatField):
            return NotImplemented
        return self.parts == other.parts

    def __repr__(self) -> str:
        return str.format("{:s}(parts={:s})", self.__class__.__name__, repr(self.parts))

    def __str__(self) -> str:
        return str.join(":", self.parts)

    @staticmethod
    def of_string(line: str) -> Optional["FormatField"]:
        """Parse the FORMAT field string.

        :param line: FORMAT field string.

        :return: Parsed FORMAT field if the line encodes it, `None`,
          otherwise.

        """

        if line == _MISSING:
            return FormatField([])

        try:
            return FormatField(str.split(line, ":"))
        except error.FormatFieldError:
            return None


class Pos:
    """POS field for a variant record.

    The class guarantees that POS is non-negative.

    """

    _value: int

    def __init__(self, pos: int):
        if pos < 0:
            raise error.NegativePos
        self._value = pos

    @property
    def value(self) -> int:
        """Reference position."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Pos):
            return NotImplemented
        return self.value == other.value

    def __lt__(self, other: object) -> bool:
        if not isinstance(other, Pos):
            return NotImplemented
        return self.value < other.value

    def __repr__(self) -> str:
        return str.format("{:s}(value={:d})", self.__class__.__name__, self.value)

    def __str__(self) -> str:
        return str.format("{:d}", self.value)

    @staticmethod
    def of_string(line: str) -> Optional["Pos"]:
        """Parse the POS field string.

        :param line: POS field string.

        :return: Parsed POS field if the line encodes it, `None`
          otherwise.

        """
        parsed_value = str_to_int(line)
        if parsed_value is None:
            return None
        try:
            return Pos(parsed_value)
        except error.NegativePos:
            return None


class Qual:
    """QUAL field for a variant record.

    The class guarantees that QUAL is non-negative.

    """

    _value: Optional[float]

    def __init__(self, qual: Optional[float]):
        if qual is not None and qual < 0:
            raise error.NegativeQual
        self._value = qual

    @property
    def value(self) -> Optional[float]:
        """Variant quality score or `None` if the score is missing."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Qual):
            return NotImplemented
        return self.value == other.value

    def __repr__(self) -> str:
        return (
            str.format("{:s}(value={:g})", self.__class__.__name__, self.value)
            if self.value
            else str.format("{:s}(value=None)", self.__class__.__name__)
        )

    def __str__(self) -> str:
        return common.encode_missing_value(lambda x: str.format("{:g}", x), self.value)

    @staticmethod
    def of_string(line: str) -> Optional["Qual"]:
        """Parse the QUAL field string.

        :param line: QUAL field string.

        :return: Parsed QUAL field if the line encodes it, `None`
          otherwise.

        """
        success_flag, value = common.decode_missing_value(str_to_float, line)
        if not success_flag:
            return None
        try:
            return Qual(value)
        except error.NegativeQual:
            return None


class SampleField:
    """Sample genotype fields for a variant record.

    The field must contain at least one part.
    """

    _parts: List[List[str]]

    def __init__(self, parts: List[List[str]]):
        def check_parts() -> None:
            if not parts:
                raise error.SampleFieldError(error.SampleFieldErrorType.EMPTY_FIELD)
            for k in parts:
                if k == ["."]:
                    raise error.SampleFieldError(error.SampleFieldErrorType.DOT_PART)
                for element in k:
                    if not element:
                        raise error.SampleFieldError(
                            error.SampleFieldErrorType.EMPTY_VALUE
                        )
                    if ":" in element:
                        raise error.SampleFieldError(
                            error.SampleFieldErrorType.INCORRECT_VALUE
                        )
                    if element == ",":
                        raise error.SampleFieldError(
                            error.SampleFieldErrorType.COMMA_IN_ELEMENT
                        )

        check_parts()
        self._parts = parts

    @property
    def parts(self) -> List[List[str]]:
        """Parts of the sample field."""
        return self._parts

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SampleField):
            return NotImplemented
        return self.parts == other.parts

    def __repr__(self) -> str:
        return str.format("{:s}(parts={:s})", self.__class__.__name__, repr(self.parts))

    def __str__(self) -> str:
        return (
            str.join(":", (str.join(",", k) if k else _MISSING for k in self.parts))
            if self.parts
            else _MISSING
        )

    @staticmethod
    def of_string(line: str) -> Optional["SampleField"]:
        """Parse genotype fields of a single sample in a variant record.

        :param line: Line encoding the genotype fields.

        :return: Parsed genotype fields if the line encodes them,
          `None` otherwise.

        """

        def parse_part(part: str) -> List[str]:
            """Parse a part of the field."""
            if part == _MISSING:
                return []
            return str.split(part, ",")

        try:
            return SampleField([parse_part(part) for part in str.split(line, ":")])
        except error.SampleFieldError:
            return None


class GenotypeFields(NamedTuple):
    """Genotype and sample fields in the VCF format."""

    format_field: FormatField
    samples: List[SampleField]

    def __str__(self) -> str:
        return str(self.format_field) + (
            ("\t" + str.join("\t", (str(k) for k in self.samples)))
            if self.samples
            else ""
        )

    @staticmethod
    def of_string(line: str) -> Optional["GenotypeFields"]:
        """Parse genotype and sample fields from parts of a variant record line.

        :param parts: FORMAT field and sample parts of a variant record line.

        :return: Parsed genotype and sample fields if the line encodes
          them, `None` otherwise.

        """
        if not line:
            return None

        parts = str.split(line, "\t")

        format_field = FormatField.of_string(parts[0])
        if format_field is None:
            return None
        samples = [SampleField.of_string(k) for k in parts[1:]]
        correct_samples = [k for k in samples if k is not None]
        return (
            GenotypeFields(format_field, correct_samples)
            if len(correct_samples) == len(samples)
            else None
        )


class Record(NamedTuple):
    """Variant record in the VCF format."""

    chrom: Chrom
    """Reference chromosome."""
    pos: Pos
    """Reference position."""
    variant_id: IdField
    """Variant identifiers."""
    ref: RefField
    """Reference bases."""
    alt: List[AltField]
    """Alternate bases."""
    qual: Qual
    """Variant quality."""
    filter_field: Optional[FilterField]
    """Filter status."""
    info_field: InfoField
    """Additional information about the variant site."""
    genotype_line: str
    """Sample genotypes and the related additional information."""

    def __str__(self) -> str:
        return str.join(
            "\t",
            [
                str(self.chrom),
                str(self.pos),
                str(self.variant_id),
                str(self.ref),
                str.join(",", (str(k) for k in self.alt)) if self.alt else _MISSING,
                str(self.qual),
                str(self.filter_field) if self.filter_field is not None else _MISSING,
                str(self.info_field),
            ]
            + ([self.genotype_line] if self.genotype_line else []),
        )

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Parse a variant record.

        :param line: Line encoding a variant record.

        :return: Parsed variant record if the line encodes it, `None`
          otherwise.

        """

        def parse_alt(part: str) -> Optional[List[AltField]]:
            """Parse the ALT field."""
            if part == _MISSING:
                return []
            raw = [AltField.of_string(k) for k in str.split(part, ",")]
            filtered = [k for k in raw if k is not None]
            return filtered if len(filtered) == len(raw) else None

        parts = str.split(line, "\t", 8)
        if len(parts) < 8:
            return None

        chrom = Chrom.of_string(parts[0])
        pos = Pos.of_string(parts[1])
        variant_id = IdField.of_string(parts[2])
        ref = RefField.of_string(parts[3])
        alt = parse_alt(parts[4])
        qual = Qual.of_string(parts[5])
        filter_check, filter_field = common.decode_missing_value(
            FilterField.of_string, parts[6]
        )
        info_field = InfoField.of_string(parts[7])

        if (
            chrom is None  # pylint: disable=too-many-boolean-expressions
            or pos is None
            or variant_id is None
            or ref is None
            or alt is None
            or qual is None
            or not filter_check
            or info_field is None
        ):
            return None

        genotype_line = parts[8] if len(parts) == 9 else ""

        return Record(
            chrom,
            pos,
            variant_id,
            ref,
            alt,
            qual,
            filter_field,
            info_field,
            genotype_line,
        )
