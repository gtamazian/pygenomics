""""Genotype records in the VCF format."""

from typing import List, Optional

from pygenomics import common
from pygenomics.vcf.data import error

_MISSING = "."


class GenotypeRecord:
    """Genotype record encoded as specified in the VCF format."""

    _alleles: Optional[List[int]]
    _phased: bool
    _missing: bool
    _ploidy: int

    def __init__(
        self, alleles: Optional[List[int]], phased: bool, missing: bool, ploidy: int
    ):
        if missing:
            if alleles is not None:
                raise error.GenotypeError(
                    error.GenotypeErrorType.ALLELES_SPECIFIED_FOR_MISSING_GENOTYPE
                )
            self._alleles = None
            self._missing = True
        else:
            if alleles is None:
                raise error.GenotypeError(error.GenotypeErrorType.ALLELES_NOT_SPECIFIED)
            self._alleles = sorted(alleles)
            self._missing = False

        if ploidy <= 0:
            raise error.GenotypeError(error.GenotypeErrorType.NON_POSITIVE_PLOIDY)
        if alleles is not None and ploidy != len(alleles):
            raise error.GenotypeError(error.GenotypeErrorType.INCORRECT_PLOIDY)

        self._ploidy = ploidy
        self._phased = phased

    @property
    def alleles(self) -> Optional[List[int]]:
        """Genotype alleles."""
        return self._alleles

    @property
    def phased(self) -> bool:
        """Is the genotype phased?"""
        return self._phased

    @property
    def missing(self) -> bool:
        """Is the genotype missing?"""
        return self._missing

    @property
    def ploidy(self) -> int:
        """Genotype ploidy."""
        return self._ploidy

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, GenotypeRecord):
            return NotImplemented
        return (
            self.alleles == other.alleles
            and self.phased == other.phased
            and self.missing == other.missing
            and self.ploidy == other.ploidy
        )

    def _separator(self) -> str:
        """Allele separator."""
        return "|" if self.phased else "/"

    def is_haploid(self) -> bool:
        """Is the genotype haploid?"""
        return self.ploidy == 1

    def is_diploid(self) -> bool:
        """Is the genotype diploid?"""
        return self.ploidy == 2

    def is_heterozygous(self) -> bool:
        """Is the genotype heterozygous?"""
        if self.missing or self.is_haploid():
            return False
        assert self.alleles is not None
        return len(set(self.alleles)) > 1

    def is_reference(self) -> bool:
        """Is the genotype the same as its reference genome?"""
        if self.missing:
            return False
        assert self.alleles is not None
        return all(k == 0 for k in self.alleles)

    def __repr__(self) -> str:
        return str.format(
            "{:s}(alleles={:s}, phased={:s}, missing={:s}, ploidy={:d})",
            self.__class__.__name__,
            repr(self.alleles),
            repr(self.phased),
            repr(self.missing),
            self.ploidy,
        )

    def __str__(self) -> str:
        if self.missing:
            return str.join(self._separator(), ["."] * self.ploidy)
        assert self.alleles is not None
        return str.join(
            self._separator(), (str.format("{:d}", k) for k in self.alleles)
        )

    @staticmethod
    def of_string(line: str) -> Optional["GenotypeRecord"]:
        """Parse a genotype record.

        :param line: A genotype record line.

        :return: The parsed genotype if `line` encodes it, `None`
          otherwise.

        """

        def process_haploid_genotype() -> Optional["GenotypeRecord"]:
            """Parse the haploid genotype."""
            assert "/" not in line and "|" not in line
            if line == _MISSING:
                return GenotypeRecord(None, False, True, 1)
            allele = common.str_to_int(line)
            return (
                GenotypeRecord([allele], False, False, 1)
                if allele is not None
                else None
            )

        if "/" not in line and "|" not in line:
            return process_haploid_genotype()

        def determine_separator() -> Optional[str]:
            """Determine the allele separator character.

            The separators must not be mixed, otherwise `None` is
            returned.

            """
            assert "/" in line or "|" in line
            if "/" in line and "|" in line:
                return None
            return "/" if "/" in line else "|"

        sep = determine_separator()
        if sep is None:
            return None
        alleles = common.str_to_int_list(line, sep)
        if alleles is None:
            # check if all alleles are the missing ones
            probably_missing_alleles = str.split(line, sep)
            return (
                GenotypeRecord(None, sep == "|", True, len(probably_missing_alleles))
                if all(k == _MISSING for k in probably_missing_alleles)
                else None
            )
        return (
            GenotypeRecord(alleles, sep == "|", False, len(alleles))
            if all(k >= 0 for k in alleles)
            else None
        )
