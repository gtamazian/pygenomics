"""Characters used in the VCF format.

The module defines characters that are disallowed, require special
encoding or can be used as line separators in a VCF file. It follows
Section 1.2 "Character encoding, non-printable characters and
characters with special meaning" from the VCF specification.

"""

import itertools
import urllib.parse
from typing import List, Tuple

DISALLOWED_CHARACTER_RANGES = [
    ("\u0000", "\u0008"),
    ("\u000B", "\u000C"),
    ("\u000E", "\u001F"),
]
"""Unicodes codes of characters foribidden in contents of a VCF file."""


def disallowed_characters() -> List[str]:
    """Return the list of characters disallowed in a VCF file.

    The VCF specification forbids using the returned characters in
    contents of a VCF file.

    :return: List of the disallowed characters.

    """
    return [
        chr(k)
        for k in itertools.chain.from_iterable(
            range(ord(j), ord(k)) for j, k in DISALLOWED_CHARACTER_RANGES
        )
    ]


ENCODED_CHARACTERS = [":", ";", "=", "%", ",", "\r", "\n", "\t"]
"""Characters that must be represented in a VCF file using the URL quoting."""


def encoded_characters() -> List[Tuple[str, str]]:
    """Return the associative list of special characters and their encoded forms.

    The VCF specification requires that eight characters, which have
    special meaning in the VCF format, must be represented in a VCF
    file using the URL quoting (the specification calls it "the
    capitalized percent encoding").

    :return: Associative list of special characters and their encoded forms.

    """
    return [(k, urllib.parse.quote(k)) for k in ENCODED_CHARACTERS]


LINE_SEPARATORS = ["\r\n", "\n"]
"""Line separators allowed in the VCF format by the specification."""
