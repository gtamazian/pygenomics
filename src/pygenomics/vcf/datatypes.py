"""Data types used in the VCF format.

The module provides functions to check float and integer values as
defined in Section 1.3 "Data types" from the VCF specification. The
module contains two regular expressions for floating-point number
representations which were taken from the VCF specification.

"""

import re
from enum import Enum, auto
from typing import Optional

FLOAT_REGEXP = r"^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$"
"""Regular expression for regular floating-point numbers."""

FLOAT_REGEXP_SPECIAL = r"^[-+]?(INF|INFINITY|NAN)$"
"""Regular expression for special floating-point number values."""

_FLOAT_COMPILED_REGEXP = re.compile(FLOAT_REGEXP)
_FLOAT_COMPILED_REGEXP_SPECIAL = re.compile(FLOAT_REGEXP_SPECIAL)


def is_valid_float_str(float_str: str) -> bool:
    """Check if the floating-point number string is valid for the VCF format.

    :param float_str: String representing a floating-point number.

    :return: Is `float_str` valid for a VCF file?

    """
    return (
        re.fullmatch(_FLOAT_COMPILED_REGEXP, float_str) is not None
        or re.fullmatch(_FLOAT_COMPILED_REGEXP_SPECIAL, float_str) is not None
    )


MIN_INVALID_INT = -(2**31)
"""Minimum invalid integer value as defined in the VCF specification."""

MAX_INVALID_INT = -(2**31) + 7
"""Maximum invalid integer value as defined in the VCF specification."""


def is_valid_int(number: int) -> bool:
    """Check if an integer number is valid for the VCF format.

    :param number: Integer.

    :return: Is `number` a valid integer for a VCF file?

    """
    return number < MIN_INVALID_INT or MAX_INVALID_INT < number


class DataType(Enum):
    """Data types supported in VCF files."""

    INTEGER = auto()
    FLOAT = auto()
    FLAG = auto()
    CHARACTER = auto()
    STRING = auto()

    def __str__(self) -> str:
        if self == DataType.INTEGER:
            return "Integer"
        if self == DataType.FLOAT:
            return "Float"
        if self == DataType.FLAG:
            return "Flag"
        if self == DataType.CHARACTER:
            return "Character"
        assert self == DataType.STRING
        return "String"

    @staticmethod
    def of_string(line: str) -> Optional["DataType"]:
        """Convert a line to the data type value.

        :param line: Line possibly encoding a data type value.

        :return: Data type value if `line` encodes it, `None`
          otherwise.

        """
        if line == "Integer":
            return DataType.INTEGER
        if line == "Float":
            return DataType.FLOAT
        if line == "Flag":
            return DataType.FLAG
        if line == "Character":
            return DataType.CHARACTER
        if line == "String":
            return DataType.STRING
        return None
