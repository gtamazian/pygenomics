"""Header line of a VCF file.

The header line is defined in Section 1.5 "Header line syntax" of the
VCF specification.

"""

from typing import List, Optional


class Line:
    """VCF header line."""

    _COLUMNS = ["CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"]
    _HEADER_PREFIX = "#" + str.join("\t", _COLUMNS)

    _samples: List[str]

    def __init__(self, samples: List[str]):
        self._samples = samples

    @property
    def samples(self) -> List[str]:
        """Samples in a VCF header line."""
        return self._samples

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Line):
            return NotImplemented
        return self.samples == other.samples

    def __repr__(self) -> str:
        return str.format(
            "{:s}(samples={:s})", self.__class__.__name__, repr(self.samples)
        )

    def __str__(self) -> str:
        if self.samples:
            return (
                Line._HEADER_PREFIX + "\t" + str.join("\t", ["FORMAT"] + self.samples)
            )
        return Line._HEADER_PREFIX

    @staticmethod
    def of_string(line: str) -> Optional["Line"]:
        """Get sample names from a VCF header line.

        :param line: VCF header line

        :return: Names of samples from `line`. If `line` is not a VCF
          header line, the empty list is returned.

        """
        if not str.startswith(line, Line._HEADER_PREFIX):
            return None

        parts = str.split(line, "\t")
        if len(parts) == len(Line._COLUMNS):
            return Line([])

        if len(parts) > len(Line._COLUMNS) and parts[len(Line._COLUMNS)] != "FORMAT":
            return None

        return Line(parts[len(Line._COLUMNS) + 1 :])

    @classmethod
    def is_header_line(cls, line: str) -> bool:
        """Is the given line a VCF header line?

        :param line: A line from a VCF file.

        :return: Is the line a VCF header?

        """
        return str.startswith(line, "#" + str.join("\t", cls._COLUMNS))
