"""Reading a VCF file."""

from typing import Iterator, List, TextIO

from pygenomics.vcf import error, header
from pygenomics.vcf.data import record
from pygenomics.vcf.meta import metaheader


class Reader:
    """Data structure for reading records from a VCF file."""

    _stream: TextIO
    _meta: metaheader.MetaHeader
    _header: header.Line

    def __init__(self, stream: TextIO):
        self._meta = metaheader.MetaHeader.initialize()
        for line in stream:
            if not header.Line.is_header_line(line):
                metaheader.add_line(self._meta, str.rstrip(line))
            else:
                parsed_header = header.Line.of_string(str.rstrip(line))
                if parsed_header is None:
                    raise error.ReaderError(
                        error.ReaderErrorType.INCORRECT_HEADER_LINE, line
                    )
                self._header = parsed_header
                break

        self._stream = stream

    @property
    def meta(self) -> metaheader.MetaHeader:
        """Meta-information lines that specify the VCF environment."""
        return self._meta

    @property
    def samples(self) -> List[str]:
        """VCF file samples in the order as given in the file."""
        return self._header.samples

    @property
    def data(self) -> Iterator[record.Record]:
        """Iterator of variant records in the VCF file."""
        for line in self._stream:
            variant_record = record.Record.of_string(str.rstrip(line))
            if variant_record is None:
                raise error.ReaderError(error.ReaderErrorType.INCORRECT_DATA_LINE, line)
            yield variant_record
