"""Reading and writing sequence records in the FASTA format."""

import os
from typing import IO, Iterator, List, NamedTuple, Tuple

from pygenomics import error


class Record(NamedTuple):
    """Named sequence as specified by the FASTA format."""

    name: str
    """Name of a sequence record."""
    comment: str
    """Comment for a sequence record."""
    seq: str
    """Record sequence."""


class IncorrectFirstLine(error.Error):
    """Raised if the first line from a FASTA stream is not a header."""

    line: str
    """Incorrect first line from the stream."""

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def read(stream: IO[str]) -> Iterator[Record]:
    """Iterate sequence records from a stream in the FASTA format.

    :note: The function produces a side effect by reading from
      `stream`. It raises :py:class:`IncorrectFirstLine` if the first
      line from `stream` is not a FASTA record header (that is, the
      line does not start with `'>'`).

    :param stream: Stream in the FASTA format.

    :return: Iterator of sequence records from `stream`.

    """
    try:
        current = [next(stream)]
    except StopIteration:
        return

    if not str.startswith(current[0], ">"):
        raise IncorrectFirstLine(current[0])

    def separate_name_and_comment(line: str) -> Tuple[str, str]:
        """Separate record name and comment from a header line."""
        parts = str.split(line, None, 1)
        assert str.startswith(parts[0], ">")
        return (parts[0][1:], parts[1] if len(parts) == 2 else "")

    def parse_record(lines: List[str]) -> Record:
        """Produce FASTA record lines to the record."""
        assert lines
        name, comment = separate_name_and_comment(str.rstrip(lines[0]))
        seq = str.join("", (str.rstrip(k) for k in lines[1:]))
        return Record(name, comment, seq)

    for line in stream:
        if str.startswith(line, ">"):
            yield parse_record(current)
            current = [line]
        else:
            list.append(current, line)

    yield parse_record(current)


def format_header(name: str, comment: str) -> str:
    """Format a record header line in the FASTA format.


    :param name: Record name.

    :param comment: Record comment.

    :return: Record header line in the FASTA format.

    """
    return str.format(">{:s} {:s}", name, comment) if comment else ">" + name


def write(stream: IO[str], record: Record) -> None:
    """Write a sequence record to a stream in the FASTA format.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param record: Record to write to `stream`.

    """
    print(format_header(record.name, record.comment), file=stream)
    print(record.seq, file=stream)


def write_wrapped(stream: IO[str], record: Record, width: int) -> None:
    """Write a sequence record to a stream in the wrapped FASTA format.

    This function is provides for compatibility with old bioinformatic
    software that cannot read long sequences in a single line. Users
    are encouraged to use function :py:func:`write` that writes a
    sequence in a single line and thus enables efficient reading and
    writing of sequences in the FASTA format.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param record: Record to write to `stream`.

    :param width: Record line width.

    """
    print(format_header(record.name, record.comment), file=stream)
    stream.writelines(
        (
            record.seq[k : k + width] + os.linesep
            for k in range(0, len(record.seq), width)
        )
    )
