"""Auxiliary data fields in the BAM format.

The implementation corresponds to Section 4.2.4 "Auxiliary data
  encoding" from the SAM format specification.

"""

import itertools
import struct
from enum import Enum, auto
from typing import List, Optional, Tuple, Union

import pygenomics.sam.datafield
from pygenomics.bam import error


class Error(error.Error):
    """Base class for exceptions related to BAM optional data fields."""


class BamSpecificValueType(Enum):
    """Value types that are specific to the BAM format.

    These types are not used in the SAM format.

    """

    INT8 = auto()
    UINT8 = auto()
    INT16 = auto()
    UINT16 = auto()
    INT32 = auto()
    UINT32 = auto()

    def __str__(self) -> str:
        if self is BamSpecificValueType.INT8:
            return "c"
        if self is BamSpecificValueType.UINT8:
            return "C"
        if self is BamSpecificValueType.INT16:
            return "s"
        if self is BamSpecificValueType.UINT16:
            return "S"
        if self is BamSpecificValueType.INT32:
            return "i"
        assert self is BamSpecificValueType.UINT32
        return "I"

    @staticmethod
    def of_string(line: str) -> Optional["BamSpecificValueType"]:
        # pylint: disable=too-many-return-statements
        """Parse the BAM data value type from a line.

        :param line: Line possibly encoding the BAM data type value.

        :return: Parsed BAM value type if `line` properly encodes it,
          `None` otherwise.

        """
        if line == "c":
            return BamSpecificValueType.INT8
        if line == "C":
            return BamSpecificValueType.UINT8
        if line == "s":
            return BamSpecificValueType.INT16
        if line == "S":
            return BamSpecificValueType.UINT16
        if line == "i":
            return BamSpecificValueType.INT32
        if line == "I":
            return BamSpecificValueType.UINT32
        return None


class ValueType:
    """Type of values in BAM optional data fields.

    The class guarantees that only one of its fields (`bam_value` and
      `sam_value`) is not `None`.

    """

    _bam_value: Optional[BamSpecificValueType]
    _sam_value: Optional[pygenomics.sam.datafield.ValueType]

    def __init__(
        self, value: Union[BamSpecificValueType, pygenomics.sam.datafield.ValueType]
    ):
        if isinstance(value, BamSpecificValueType):
            self._bam_value = value
            self._sam_value = None
        else:
            assert isinstance(value, pygenomics.sam.datafield.ValueType)
            if value is pygenomics.sam.datafield.ValueType.INTEGER:
                self._bam_value = BamSpecificValueType.INT32
                self._sam_value = None
            else:
                self._bam_value = None
                self._sam_value = value

    @property
    def bam_value(self) -> Optional[BamSpecificValueType]:
        """BAM-specific data type value."""
        return self._bam_value

    @property
    def sam_value(self) -> Optional[pygenomics.sam.datafield.ValueType]:
        """SAM data type value."""
        return self._sam_value

    def is_special_array(self) -> bool:
        """Does the type designate a string or a hex-byte array?"""
        return (
            self.sam_value is pygenomics.sam.datafield.ValueType.HEXADECIMAL_ARRAY
            or self.sam_value is pygenomics.sam.datafield.ValueType.STRING
        )

    def is_array(self) -> bool:
        """Does the type designate a string, a hex-byte array, or a general array?"""
        return (
            self.is_special_array()
            or self.sam_value is pygenomics.sam.datafield.ValueType.GENERAL_ARRAY
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, ValueType):
            return NotImplemented
        return self.bam_value is other.bam_value and self.sam_value is other.sam_value

    def __repr__(self) -> str:
        return str.format(
            "{:s}(bam_value={:s}, sam_value={:s})",
            self.__class__.__name__,
            repr(self.bam_value),
            repr(self.sam_value),
        )

    def __str__(self) -> str:
        return (
            str(self.bam_value) if self.bam_value is not None else str(self.sam_value)
        )

    @staticmethod
    def of_string(line: str) -> Optional["ValueType"]:
        """Parse the BAM optional data field type from a line.

        :param line: Line possibly encoding the BAM optional data field type.

        :return: Parsed BAM optional data field type if `line`
          properly encodes it, `None` otherwise.

        """
        bam_result = BamSpecificValueType.of_string(line)
        if bam_result is not None:
            return ValueType(bam_result)
        sam_result = pygenomics.sam.datafield.ValueType.of_string(line)
        return ValueType(sam_result) if sam_result is not None else None

    def value_size(self) -> int:
        # pylint: disable=too-many-return-statements,too-many-branches
        """Get the size of a value of the type in bytes."""
        if self.bam_value is not None:
            if self.bam_value is BamSpecificValueType.INT8:
                return 1
            if self.bam_value is BamSpecificValueType.UINT8:
                return 1
            if self.bam_value is BamSpecificValueType.INT16:
                return 2
            if self.bam_value is BamSpecificValueType.UINT16:
                return 2
            if self.bam_value is BamSpecificValueType.INT32:
                return 4
            if self.bam_value is BamSpecificValueType.UINT32:
                return 4
        assert (
            self.sam_value is not None
            and self.sam_value is not pygenomics.sam.datafield.ValueType.INTEGER
        )
        if self.sam_value is pygenomics.sam.datafield.ValueType.CHARACTER:
            return 1
        if self.sam_value is pygenomics.sam.datafield.ValueType.GENERAL_ARRAY:
            return 1
        if self.sam_value is pygenomics.sam.datafield.ValueType.REAL_NUMBER:
            return 4
        if self.sam_value is pygenomics.sam.datafield.ValueType.HEXADECIMAL_ARRAY:
            return 2
        assert self.sam_value is pygenomics.sam.datafield.ValueType.STRING
        return 1


class IncorrectTag(Error):
    """Field tag must consist of two characters."""

    incorrect_tag: str

    def __init__(self, incorrect_tag: str):
        super().__init__(incorrect_tag)
        self.incorrect_tag = incorrect_tag


class MissingTerminatingNul(Error):
    """Indicates that a string field or a hex-formatted byte array misses the terminator."""

    value: bytes

    def __init__(self, value: bytes):
        super().__init__(value)
        self.value = value


class IncorrectArrayElementType(Error):
    """Indicates that an incorrect element type is specified for a general array."""

    type_value: ValueType

    def __init__(self, type_value: ValueType):
        super().__init__(type_value)
        self.type_value = type_value


class IncorrectTypeValue(Error):
    """Indicates that an incorrect type value is specified."""

    value: str

    def __init__(self, value: str):
        super().__init__(value)
        self.value = value


class IncorrectArrayDataSize(Error):
    """Indicate that the array data size is not consistent with its element size."""

    element_type: ValueType
    data: bytes

    def __init__(self, element_type: ValueType, data: bytes):
        super().__init__(element_type, data)
        self.element_type = element_type
        self.data = data


class DataField:
    """BAM optional data field.

    The class guarantees that the field tag consists of two
      characters, the field type does not combine a general array and
      strings or hex-formatted byte arrays, and data for strings and
      hex-formatted byte arrays have a proper terminator.

    """

    _tag: str
    _value_type: ValueType
    _is_array: bool
    _data: bytes

    def __init__(self, tag: str, value_type: ValueType, is_array: bool, data: bytes):
        self._tag = tag
        self._value_type = value_type
        self._is_array = is_array
        self._data = data

        if ValueType.is_array(value_type):
            if is_array:
                raise IncorrectArrayElementType(value_type)
            if ValueType.is_special_array(value_type):
                if data[-1] != 0:
                    raise MissingTerminatingNul(data)
            else:
                if len(data) % ValueType.value_size(value_type) != 0:
                    raise IncorrectArrayDataSize(value_type, data)

        if len(tag) != 2:
            raise IncorrectTag(tag)

    @property
    def tag(self) -> str:
        """Optional data field tag."""
        return self._tag

    @property
    def value_type(self) -> ValueType:
        """Value type of the optional data field."""
        return self._value_type

    @property
    def is_array(self) -> bool:
        """Does the data field encode a general array?

        String fields and hex-formatted byte arrays are not considered
          general arrays.

        """
        return self._is_array

    @property
    def data(self) -> bytes:
        """Data field value."""
        return self._data

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, DataField):
            return NotImplemented
        return (
            self.tag == other.tag
            and self.value_type == other.value_type
            and self.is_array == other.is_array
            and self.data == other.data
        )

    def __repr__(self) -> str:
        return str.format(
            '{:s}(tag="{:s}", value_type={:s}, is_array={:s}, data={:s})',
            self.__class__.__name__,
            self.tag,
            repr(self.value_type),
            repr(self.is_array),
            repr(self.data),
        )

    def to_bytes(self) -> bytes:
        """Get the byte representation of an optional data field."""
        if self.is_array:
            return (
                str.encode(self.tag)
                + b"B"
                + str.encode(str(self.value_type))
                + struct.pack(
                    "<I", len(self.data) // ValueType.value_size(self.value_type)
                )
                + self.data
            )
        return str.encode(self.tag) + str.encode(str(self.value_type)) + self.data

    @staticmethod
    def of_bytes(value: bytes) -> Optional[List["DataField"]]:
        # pylint: disable=too-many-return-statements
        """Parse optional data fields from their byte representation.

        :param value: Byte representation of data fields.

        :return: Parsed data fields if `value` properly encodes them,
          `None` otherwise.

        """

        def parse_single_value(current: bytes) -> Optional[Tuple["DataField", int]]:
            """Parse one optional data field."""
            if len(current) < 3:
                return None

            header = bytes.decode(current[:3])
            if header[2] == "B":
                is_array = True
                if len(current) < 7:
                    return None
                value_type = ValueType.of_string(chr(current[3]))
                if value_type is None:
                    return None
                count = struct.unpack("<I", current[4:8])[0]
                array_size = ValueType.value_size(value_type) * count
                if len(current) < 8 + array_size:
                    return None
                data = current[8 : 8 + array_size]
                data_length = 8 + array_size
            else:
                is_array = False
                value_type = ValueType.of_string(header[2])
                if value_type is None:
                    return None
                if not ValueType.is_array(value_type) and len(
                    current
                ) < 3 + ValueType.value_size(value_type):
                    return None
                if ValueType.is_special_array(value_type):
                    data = (
                        bytes(itertools.takewhile(lambda x: x != 0, current[3:]))
                        + b"\x00"
                    )
                else:
                    data = current[3 : 3 + ValueType.value_size(value_type)]
                data_length = 3 + len(data)
            try:
                return (DataField(header[:2], value_type, is_array, data), data_length)
            except Error:
                return None

        result: List[DataField] = []
        offset = 0
        while offset < len(value):
            parsing_result = parse_single_value(value[offset:])
            if parsing_result is None:
                return None
            new_field, new_offset = parsing_result
            list.append(result, new_field)
            offset += new_offset

        if offset != len(value):
            return None

        return result
