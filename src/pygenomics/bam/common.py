"""Common routines for processing BAM files."""

import struct
from typing import BinaryIO


def read_uint32(stream: BinaryIO) -> int:
    """Read an unsigned 32-bit integer from a binary stream.

    :note: The function produces a side effect by reading from
      `stream`.

    """
    result: int = struct.unpack("<I", stream.read(4))[0]
    return result


def read_int32(stream: BinaryIO) -> int:
    """Read a signed 32-bit integer from a binary stream.

    :note: The function produces a side effect by reading from
      `stream`.

    """
    result: int = struct.unpack("<i", stream.read(4))[0]
    return result


def read_uint8(stream: BinaryIO) -> int:
    """Read an unsigned 8-bit integer from a binary stream.

    :note: The function produces a side effect by reading from
      `stream`.

    """
    result: int = struct.unpack("<B", stream.read(1))[0]
    return result


def read_uint16(stream: BinaryIO) -> int:
    """Read an unsigned 16-bit integer from a binary stream.

    :note: The function produces a side effect by reading from
      `stream`.

    """
    result: int = struct.unpack("<H", stream.read(2))[0]
    return result
