"""BAM header class and routines."""

import struct
from typing import BinaryIO, List, NamedTuple, Tuple

from pygenomics.bam import error
from pygenomics.bam.common import read_uint32

_BAM_MAGIC = b"BAM\x01"


class Error(error.Error):
    """Base class for exceptions related to the BAM header."""


class IncorrectMagicString(Error):
    """Incorrect magic string at the beginning of a BAM file."""

    value: bytes

    def __init__(self, value: bytes):
        super().__init__(value)
        self.value = value


class Header(NamedTuple):
    """BAM header class."""

    text: str
    ref_sequences: List[Tuple[str, int]]

    def to_bytes(self) -> bytes:
        """Get the byte representation of the header."""

        def encode_ref_sequence(seq_info: Tuple[str, int]) -> bytes:
            """Encode the reference sequence name and length."""
            name, length = seq_info
            return (
                struct.pack("<I", len(name) + 1)
                + str.encode(name)
                + b"\x00"
                + struct.pack("<I", length)
            )

        return (
            _BAM_MAGIC
            + struct.pack("<I", len(self.text))
            + str.encode(self.text)
            + struct.pack("<I", len(self.ref_sequences))
            + bytes.join(b"", (encode_ref_sequence(k) for k in self.ref_sequences))
        )

    @staticmethod
    def read(stream: BinaryIO) -> "Header":
        """Read a BAM header from a binary stream.

        :note: The function produces a side effect by reading from
          `stream` and can raise exceptions.

        :param stream: Binary stream corresponding to a BAM file.

        :return: Parsed BAM header.

        """
        magic = stream.read(len(_BAM_MAGIC))
        if magic != _BAM_MAGIC:
            raise IncorrectMagicString(magic)
        header_length = read_uint32(stream)
        text = bytes.decode(stream.read(header_length))
        num_ref = read_uint32(stream)
        ref_sequences: List[Tuple[str, int]] = []
        for _ in range(num_ref):
            name_length = read_uint32(stream)
            name = bytes.decode(stream.read(name_length))
            assert name[-1] == "\x00"
            seq_length = read_uint32(stream)
            list.append(ref_sequences, (name[:-1], seq_length))
        return Header(text, ref_sequences)

    def write(self, stream: BinaryIO) -> None:
        """Write a BAM header to a binary stream.

        :note: The function produces a side effect by writing to
          `stream`.

        :param stream: Binary stream to write the BAM header to.

        """
        stream.write(self.to_bytes())
