"""Exceptions for subpackage `pygenomics.bam`."""

from pygenomics import error


class Error(error.Error):
    """Base class for exceptions related to the BAM format."""
