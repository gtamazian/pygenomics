"""Read alignments in the BAM format."""

import struct
from typing import List, NamedTuple, Optional, Tuple

from pygenomics.bam import datafield

_BASES = "=ACMGRSVTWYHKDBN"
assert len(_BASES) == 16

_BASE_MAPPING_DICT = dict((k, j) for j, k in enumerate(_BASES))

_CIGAR_OPS = "MIDNSHP=X"
assert len(_CIGAR_OPS) == 9

_CIGAR_OP_MAPPING_DICT = dict((k, j) for j, k in enumerate(_CIGAR_OPS))


def get_read_bases() -> str:
    """Get nucleotide read base pair characters."""
    return _BASES


def get_cigar_symbols() -> str:
    """Get symbols of CIGAR operations."""
    return _CIGAR_OPS


def map_base(base: str) -> int:
    """Map a base pair according to the BAM 4-bit read encoding.

    The BAM 4-bit encoding is described in Section 4.2.3 "SEQ and QUAL
      encoding" of the SAM specification.

    :param base: Base pair from a read.

    :return: Base pair mapped to the 4-bit value.

    """
    return dict.get(_BASE_MAPPING_DICT, base, 15)


def rev_map_base(value: int) -> str:
    """Map a nucleotide base pair back from its 4-bit representation.

    :param value: Integer representing a 4-bit encoded base pair.

    :return: Base pair character.

    """
    assert value < len(_BASES)
    return _BASES[value]


def encode_seq(read: str) -> List[int]:
    """Convert a read sequence to the 4-bit format.

    The encoding procedure is implemented as described in Section
      4.2.3 of the SAM specification.

    :param read: Nucleotide sequence of a read.

    :return: 4-bit representation of `read`.

    """
    result_length = len(read) // 2 + len(read) % 2
    result: List[int] = [0] * result_length
    for k in range(len(read) // 2):
        result[k] = (map_base(read[2 * k]) << 4) | map_base(read[2 * k + 1])
    if len(read) % 2 == 1:
        result[result_length - 1] = map_base(read[-1]) << 4

    return result


def decode_seq(encoded_seq: List[int]) -> str:
    """Decode a read sequence from its 4-bit representation.

    The decoding procedure is implemented as described in Section
      4.2.3 of the SAM specification. Note that this function is not a
      complete inverse to :py:func:`encode_seq` because of empty bytes
      that correspond to the end of the encoded sequence. Since these
      bytes are zeros, the output sequence may have trailing `'='`
      characters that should be trimmed using the read sequence length
      from the corresponding field of a BAM alignment record.

    :param encoded_seq: Sequence in the 4-bit representation.

    :return: Sequence as a string.

    """

    def decode_int(value: int) -> str:
        """Decode a single integer from a 4-bit sequence representation."""
        return rev_map_base((value & 0xF0) >> 4) + rev_map_base(value & 0xF)

    return str.join("", (decode_int(k) for k in encoded_seq))


def map_cigar_op(operation: str) -> int:
    """Get the integer value encoding a CIGAR operation.

    :param operation: CIGAR operation symbolic code.

    :return: Integer that represents the operation.

    """
    return _CIGAR_OP_MAPPING_DICT[operation]


def rev_map_cigar_op(code: int) -> str:
    """Get the CIGAR operation for the specified code.

    :param code: CIGAR operation integer code.

    :return: Operation symbol that corresponds to the code.

    """
    return _CIGAR_OPS[code]


def encode_cigar_op(operation: Tuple[str, int]) -> int:
    """Encode a CIGAR operation for a BAM alignment record.

    The CIGAR operation encoding is defined in Section 4.2.2 of the
      SAM specification.

    :param operation: 2-tuple of a CIGAR operation code and its count.

    :return: CIGAR operation encoded as a 32-bit integer.

    """
    code, count = operation
    return map_cigar_op(code) + (count << 4)


def decode_cigar_op(code: int) -> Tuple[str, int]:
    """Decode a CIGAR operation from a BAM alignment record.

    This function is inverse of :py:func:`encode_cigar_op`.

    :param code: 32-bit integer representing an encoded CIGAR operation.

    :return: The CIGAR operation code and count.

    """
    return (rev_map_cigar_op(code & 0xF), code >> 4)


_PREFIX_FORMAT = "<2i2B3HI3i"
_PREFIX_STRUCT = struct.Struct(_PREFIX_FORMAT)
_PREFIX_SIZE: int = _PREFIX_STRUCT.size


class Prefix(NamedTuple):
    """Prefix with fixed fields of a read alignment record."""

    ref_id: int
    pos: int
    l_read_name: int
    mapq: int
    index_bin: int
    n_cigar_op: int
    flag: int
    l_seq: int
    next_ref_id: int
    next_pos: int
    tlen: int

    def to_bytes(self) -> bytes:
        """Convert the alignment record prefix to bytes."""
        return struct.Struct.pack(
            _PREFIX_STRUCT,
            self.ref_id,
            self.pos,
            self.l_read_name,
            self.mapq,
            self.index_bin,
            self.n_cigar_op,
            self.flag,
            self.l_seq,
            self.next_ref_id,
            self.next_pos,
            self.tlen,
        )

    @staticmethod
    def of_bytes(data: bytes) -> Optional["Prefix"]:
        """Parse an alignment record prefix from a byte array.

        :param data: Byte array possibly encoding an alignment record
          prefix.

        :return: Parsed alignment record prefix if `data` properly
          encodes it, `None` otherwise.

        """
        try:
            return Prefix(*struct.Struct.unpack(_PREFIX_STRUCT, data))
        except struct.error:
            return None


class Alignment(NamedTuple):
    """BAM alignment record."""

    prefix: Prefix
    read_name: bytes
    cigar: List[int]
    seq: List[int]
    qual: bytes
    aux_data: List[datafield.DataField]

    def to_bytes(self) -> bytes:
        """Get the byte representation of the alignment record."""
        return bytes.join(
            b"",
            [Prefix.to_bytes(self.prefix), self.read_name]
            + [struct.pack("<I", k) for k in self.cigar]
            + [struct.pack("<B", k) for k in self.seq]
            + [self.qual]
            + [datafield.DataField.to_bytes(k) for k in self.aux_data],
        )

    @staticmethod
    def of_bytes(data: bytes) -> Optional["Alignment"]:
        """Parse an alignment record from a byte array.

        :param data: Byte array possibly encoding an alignment record.

        :return: Parsed alignment record if `data` properly encodes
          it, `None` otherwise.

        """

        def extract_part(offset: int, length: int) -> bytes:
            """Extract the specified part of the byte array."""
            return data[offset : offset + length]

        def parse_cigar(
            offset: int, num_cigar_operations: int
        ) -> Tuple[List[int], int]:
            """Parse the cigar field."""
            data_part = extract_part(offset, num_cigar_operations * 4)
            return (
                [
                    struct.unpack("<I", data_part[4 * k : 4 * (k + 1)])[0]
                    for k in range(num_cigar_operations)
                ],
                num_cigar_operations * 4,
            )

        def parse_seq(offset: int, l_seq: int) -> Tuple[List[int], int]:
            """Parse the seq field."""
            assert prefix is not None
            seq_len = (l_seq + 1) // 2
            data_part = extract_part(offset, seq_len)
            return (
                [struct.unpack("<B", data_part[k : k + 1])[0] for k in range(seq_len)],
                seq_len,
            )

        def parse_qual(offset: int, l_seq: int) -> Tuple[bytes, int]:
            """Parse the sequence quality field."""
            assert prefix is not None
            return (extract_part(offset, l_seq), l_seq)

        offset = 0

        prefix = Prefix.of_bytes(extract_part(offset, _PREFIX_SIZE))
        offset += _PREFIX_SIZE
        if prefix is None:
            return None

        read_name = extract_part(_PREFIX_SIZE, prefix.l_read_name)
        offset += prefix.l_read_name

        cigar, cigar_size = parse_cigar(offset, prefix.n_cigar_op)
        offset += cigar_size

        seq, seq_size = parse_seq(offset, prefix.l_seq)
        offset += seq_size

        qual, qual_size = parse_qual(offset, prefix.l_seq)
        offset += qual_size

        aux_data = datafield.DataField.of_bytes(data[offset:])
        if aux_data is None:
            return None

        return Alignment(prefix, read_name, cigar, seq, qual, aux_data)
