"""Reading a BAM file."""

import struct
from typing import BinaryIO, Iterator

from pygenomics.bam.alignment import Alignment
from pygenomics.bam.error import Error
from pygenomics.bam.header import Header


class HeaderError(Error):
    """Incorrect header of a BAM file."""


class IncorrectBlockSize(Error):
    """Incorrect size of a BAM alignment record."""

    value: int

    def __init__(self, value: int):
        super().__init__(value)
        self.value = value


class IncompleteBlockRead(Error):
    """Incomplete data block was read from a BAM file."""

    block_size: int
    block_data: bytes

    def __init__(self, block_size: int, block_data: bytes):
        super().__init__(block_size, block_data)
        self.block_size = block_size
        self.block_data = block_data


class AlignmentError(Error):
    """Incorrect BAM alignment record."""

    record: bytes

    def __init__(self, record: bytes):
        super().__init__(record)
        self.record = record


class Reader:
    """Data structure for reading alignments from a BAM file."""

    _stream: BinaryIO
    _header: Header

    def __init__(self, stream: BinaryIO):
        self._header = Header.read(stream)
        if self._header is None:
            raise HeaderError()
        self._stream = stream

    @property
    def header(self) -> Header:
        """Header of a BAM file."""
        return self._header

    @property
    def alignments(self) -> Iterator[Alignment]:
        """Iterator for alignment records in a BAM file."""
        block_size_data = self._stream.read(4)
        while block_size_data:
            block_size = struct.unpack("<I", block_size_data)[0]
            if block_size <= 0:
                raise IncorrectBlockSize(block_size)
            block_data = self._stream.read(block_size)
            if len(block_data) < block_size:
                raise IncompleteBlockRead(block_size, block_data)
            parsed_alignment = Alignment.of_bytes(block_data)
            if parsed_alignment is None:
                raise AlignmentError(block_data)
            yield parsed_alignment
            block_size_data = self._stream.read(4)
