"""Class and routines for representing sequence strand values."""

import itertools
from enum import Enum, auto
from typing import Iterable, Optional

STRAND_SYMBOLS = "-.+"
"""Symbols representing strand values."""


class Strand(Enum):
    """Sequence strand values."""

    MINUS = auto()
    UNKNOWN = auto()
    PLUS = auto()

    def __str__(self) -> str:
        if self == Strand.PLUS:
            return "+"

        if self == Strand.MINUS:
            return "-"

        assert self == Strand.UNKNOWN
        return "."

    def invert(self) -> "Strand":
        """Return the opposite strand value."""
        if self == Strand.PLUS:
            return Strand.MINUS

        if self == Strand.MINUS:
            return Strand.PLUS

        assert self == Strand.UNKNOWN
        return Strand.UNKNOWN

    def is_known(self) -> bool:
        """Check if the strand value is known."""
        return self != Strand.UNKNOWN

    @staticmethod
    def of_string(line: str) -> Optional["Strand"]:
        """Convert a line to the strand value.

        :param line: Line possibly encoding a strand.

        :returns: Strand value if `line` encodes a strand, `None`
          otherwise.

        """
        if line == "+":
            return Strand.PLUS

        if line == "-":
            return Strand.MINUS

        if line == ".":
            return Strand.UNKNOWN

        return None


def consistent(first: Iterable[Strand], second: Iterable[Strand]) -> bool:
    """Check if two series of strand values are consistent with each other.

    The series are consistent if they contain the same values or all
    their values are opposite to each other (e.g., `["+", "-", "+"]`
    are opposite to `["-", "+", "-"]`. The unknown value is consistent
    with both known strand values and does not disrupt the strand consistency.

    :param first: First series of strand values.

    :param second: Second series of strand values.

    :return: Are strand values in `first` consistent with the values
      in `second`?

    """
    pairs = itertools.tee(
        (
            (j, k)
            for j, k in zip(first, second)
            if Strand.is_known(j) and Strand.is_known(k)
        ),
        2,
    )

    return all(
        (j[0] == j[1]) == (k[0] == k[1])
        for j, k in zip(pairs[0], itertools.islice(pairs[1], 1, None))
    )
