"""Exceptions for package pygenomics."""


class Error(Exception):
    """Base class for exceptions in the package."""
