"""Read and writing bedGraph files."""

from typing import NamedTuple, Optional

from pygenomics import bed


class Record(NamedTuple):
    """Record in the bedGraph format."""

    interval: bed.Bed3
    value: float

    def __str__(self) -> str:
        return str.format("{}\t{:g}", self.interval, self.value)

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Convert a line to the bedGraph record.

        :param line: Line possibly encoding the bedGraph record.

        :return: BedGraph record if `line` properly encodes it, `None`
          otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) != 4:
            return None
        interval = bed.Bed3.of_list(parts[:3])
        if interval is not None:
            try:
                value = float(parts[3])
            except ValueError:
                return None
            return Record(interval, value)
        return None
