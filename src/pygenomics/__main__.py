"""Entry point to the command-line interface (CLI) of the package."""

import argparse
from typing import List, Optional

from pygenomics.cli import fasta, interval
from pygenomics.cli.fasta import fasta_oneseq, fasta_revcomp, fasta_size, fasta_unmask
from pygenomics.cli.interval import (
    interval_complement,
    interval_find,
    interval_find_all,
    interval_intersect,
    interval_merge,
    interval_subtract,
)


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Main parser of the package CLI."""
    parser = argparse.ArgumentParser(prog="pygenomics")
    subparsers = parser.add_subparsers(help="Groups of package commands", dest="group")
    subparsers.required = True
    fasta.register_parser(subparsers)
    interval.register_parser(subparsers)

    return parser.parse_args(args)


_TOOL_FUNCTIONS = {
    ("fasta", "oneseq"): fasta_oneseq.main,
    ("fasta", "revcomp"): fasta_revcomp.main,
    ("fasta", "size"): fasta_size.main,
    ("fasta", "unmask"): fasta_unmask.main,
    ("interval", "complement"): interval_complement.main,
    ("interval", "find"): interval_find.main,
    ("interval", "find_all"): interval_find_all.main,
    ("interval", "intersect"): interval_intersect.main,
    ("interval", "merge"): interval_merge.main,
    ("interval", "subtract"): interval_subtract.main,
}


def cli_main(args: argparse.Namespace) -> None:
    """Main function of the package CLI."""
    if "group" in args and "tool" in args and args.tool is not None:
        caller_fn = dict.get(_TOOL_FUNCTIONS, (args.group, args.tool))
        assert caller_fn is not None
        caller_fn(args)


def main() -> None:
    """Entry point for the executable named after the package."""
    cli_main(parse_args())


if __name__ == "__main__":
    main()
