"""Compressing data in the BGZF format.

The BGZF format is described in Section 4.1 of the SAM format
specification.

"""

import struct
import zlib
from typing import Iterator, List

_HEADER = struct.pack("<4BI2BH2BH", 31, 139, 8, 4, 0, 0, 0xFF, 6, 66, 67, 2)
assert len(_HEADER) == 16

_MAX_BLOCK_SIZE = 2**16

_CRC32_ISIZE = struct.Struct("<2I")
assert _CRC32_ISIZE.size == 8

_BSIZE = struct.Struct("<H")
assert _BSIZE.size == 2


class Writer:
    """Producing compressed data in the BGZF format."""

    _buffer: bytes

    def __init__(self) -> None:
        self._buffer = b""

    @staticmethod
    def _compress_block(data: bytes) -> bytes:
        """Compress a single block of data."""
        assert len(data) <= _MAX_BLOCK_SIZE
        compressor = zlib.compressobj(wbits=-15)
        compressor.compress(data)
        compressed_data = compressor.flush()
        # length(CDATA) = BSIZE - XLEN - 19 => BSIZE = length(CDATA) + XLEN + 19
        # and XLEN = 6
        bsize = 25 + len(compressed_data)
        return bytes.join(
            b"",
            [
                _HEADER,
                _BSIZE.pack(bsize),
                compressed_data,
                _CRC32_ISIZE.pack(zlib.crc32(data), len(data)),
            ],
        )

    def compress(self, data: bytes) -> Iterator[bytes]:
        """Produce compressed data in the BGZF format.

        :note: The function produces a side effect by changing the
          state of the :py:class:`Writer` object.

        :param data: Data to be compressed in the BGZF format.

        :return: Iterator of BGZF blocks.

        """
        current_data = self._buffer + data

        for k in range(len(current_data) // _MAX_BLOCK_SIZE):
            yield self._compress_block(
                current_data[k * _MAX_BLOCK_SIZE : (k + 1) * _MAX_BLOCK_SIZE]
            )

        self._buffer = (
            current_data[-(len(current_data) % _MAX_BLOCK_SIZE) :]
            if len(current_data) % _MAX_BLOCK_SIZE > 0
            else b""
        )

    def finalize(self) -> List[bytes]:
        """Get the end-of-file marker and last block of compressed data.

        :note: The function produces a side effect by changing the
          state of the :py:class:`Writer` object (the buffer is set
          empty).

        :return: List containing one or two compressed blocks: the
          end-of-file marker and the block of remaining data to be
          compressed if something is left in the writer buffer.

        """
        result = (
            [self._compress_block(self._buffer), self._compress_block(b"")]
            if self._buffer
            else [self._compress_block(b"")]
        )
        self._buffer = b""
        return result
