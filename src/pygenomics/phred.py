"""Functions for Phred quality score."""

import math


def to_score(probability: float) -> int:
    """Convert a probability value to the Phred quality score.

    :param probability: Probability value.

    :return: Phred score that corresponds to `probability`.

    """
    return round(-10 * math.log10(probability))


def of_score(score: int) -> float:
    """Convert a Phred score to the probability value.

    :param score: Phred score.

    :return: Probability that corresponds to `score`.

    """
    return math.pow(10, -score / 10)
