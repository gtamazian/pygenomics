"""Common routines for FASTA-related CLI functions."""

import sys
from typing import Callable

from pygenomics import fasta


def apply_function(path: str, process_fn: Callable[[fasta.Record], None]) -> None:
    """Apply the specified function to all records in a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        try:
            for record in fasta.read(fasta_file):
                process_fn(record)
        except fasta.IncorrectFirstLine:
            print("Incorrect first line of the specified FASTA file.", file=sys.stderr)
            sys.exit(1)
