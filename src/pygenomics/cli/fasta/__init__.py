"""Subparser that groups commands related to FASTA files."""

from typing import Any

from pygenomics.cli.fasta import fasta_oneseq, fasta_revcomp, fasta_size, fasta_unmask


def register_parser(parent_parser: Any) -> None:
    """Register the group of FASTA-related commands to the subparser."""
    parser = parent_parser.add_parser("fasta", help="Processing FASTA files")
    subparser = parser.add_subparsers(dest="tool")
    subparser.required = True
    fasta_oneseq.register_parser(subparser)
    fasta_revcomp.register_parser(subparser)
    fasta_size.register_parser(subparser)
    fasta_unmask.register_parser(subparser)
