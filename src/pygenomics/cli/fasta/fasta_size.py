"""CLI tool to print sizes of sequences from a FASTA file."""

import argparse
from typing import Any

from pygenomics.cli.fasta import common


def register_parser(subparser: Any) -> None:
    """Register the tool command-line argument parser in the parent parser."""
    parser = subparser.add_parser(
        name="size", help="Print sizes of sequences from a FASTA file"
    )
    parser.add_argument("in_fasta", help="input FASTA file")


def print_sequence_sizes(path: str) -> None:
    """Print sizes of sequences from the specified FASTA file."""
    common.apply_function(
        path, lambda x: print(str.format("{:s}\t{:d}", x.name, len(x.seq)))
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `fasta size`."""
    print_sequence_sizes(args.in_fasta)
