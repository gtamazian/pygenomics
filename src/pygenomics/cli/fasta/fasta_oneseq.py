"""CLI tool to extract a single sequence from a FASTA file."""

import argparse
import sys
from typing import Any

from pygenomics import fasta


def register_parser(subparser: Any) -> None:
    """Register the parser for `pygenomics fasta oneseq`."""
    parser = subparser.add_parser(
        name="oneseq", help="Extract a single sequence from a FASTA file."
    )
    parser.add_argument("in_fasta", help="input FASTA file")
    parser.add_argument("sequence", help="name of the sequence to extact")


def extract_sequence(path: str, seq_name: str) -> None:
    """Extract the sequence and print it to standard output."""
    with open(path, encoding="utf-8") as fasta_file:
        try:
            for record in fasta.read(fasta_file):
                if record.name == seq_name:
                    fasta.write(sys.stdout, record)
                    return
        except fasta.IncorrectFirstLine:
            print("Incorrect first line of the specified FASTA file.", file=sys.stderr)
            sys.exit(1)


def main(args: argparse.Namespace) -> None:
    """Main function of tool `fasta oneseq`."""
    extract_sequence(args.in_fasta, args.sequence)
