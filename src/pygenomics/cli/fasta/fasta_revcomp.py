"""CLI tool to get reverse complement sequences from a FASTA file."""

import argparse
import sys
from typing import Any

from pygenomics import fasta, nucleotide
from pygenomics.cli.fasta import common


def register_parser(subparser: Any) -> None:
    """Register the parser for `pygenomics fasta revcomp`."""
    parser = subparser.add_parser(
        name="revcomp", help="Get reverse complement sequences from a FASTA file"
    )
    parser.add_argument("in_fasta", help="input FASTA file")


def print_revcomp_sequences(path: str) -> None:
    """Print reverse complement sequences."""
    common.apply_function(
        path,
        lambda x: fasta.write(
            sys.stdout, fasta.Record(x.name, x.comment, nucleotide.rev_comp(x.seq))
        ),
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `fasta revcomp`."""
    print_revcomp_sequences(args.in_fasta)
