"""CLI tool to remove soft masking from a FASTA file."""

import argparse
import sys
from typing import Any

from pygenomics import fasta, nucleotide
from pygenomics.cli.fasta import common


def register_parser(subparser: Any) -> None:
    """Register the parser for `pygenomics fasta unmask`."""
    parser = subparser.add_parser(
        name="unmask", help="Remove soft masking from a FASTA file"
    )
    parser.add_argument("in_fasta", help="input FASTA file")


def remove_soft_masking(path: str) -> None:
    """Print FASTA records with soft masking removed."""
    common.apply_function(
        path,
        lambda x: fasta.write(
            sys.stdout,
            fasta.Record(x.name, x.comment, nucleotide.remove_soft_mask(x.seq)),
        ),
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `fasta unmask`."""
    remove_soft_masking(args.in_fasta)
