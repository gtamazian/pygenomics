"""CLI for intersecting intervals from multiple files."""

import argparse
import functools
import itertools
from typing import Any

from pygenomics.bed import Bed3, Range, Record
from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval intersect`."""
    parser = subparser.add_parser(
        name="intersect", help="Intersect intervals from the specified files"
    )
    parser.add_argument(
        "interval_files",
        metavar="FILE",
        nargs="+",
        help="input files of genomic intervals",
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval intersect`."""

    def process_file(acc: GenomicBase, new_file: str) -> GenomicBase:
        """Intersect intervals from the file with the base."""
        return GenomicBase(
            itertools.chain.from_iterable(
                GenomicBase.intersect(acc, k)
                for _, k in common.iterate_records_and_intervals(new_file)
            )
        )

    init = GenomicBase(
        k for _, k in common.iterate_records_and_intervals(args.interval_files[0])
    )
    result = functools.reduce(process_file, args.interval_files[1:], init)

    for seq, start, end in GenomicBase.iterate_reduced(result):
        print(Record(Bed3(seq, Range(start - 1, end))))
