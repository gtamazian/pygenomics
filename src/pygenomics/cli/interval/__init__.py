"""Subparser that groups commands related to genomic intervals."""

from typing import Any

from pygenomics.cli.interval import (
    interval_complement,
    interval_find,
    interval_find_all,
    interval_intersect,
    interval_merge,
    interval_subtract,
)


def register_parser(parent_parser: Any) -> None:
    """Register the group of commands related to genomic intervals."""
    parser = parent_parser.add_parser("interval", help="Processing genomic intervals")
    subparser = parser.add_subparsers(dest="tool")
    subparser.required = True
    interval_complement.register_parser(subparser)
    interval_find.register_parser(subparser)
    interval_find_all.register_parser(subparser)
    interval_intersect.register_parser(subparser)
    interval_merge.register_parser(subparser)
    interval_subtract.register_parser(subparser)
