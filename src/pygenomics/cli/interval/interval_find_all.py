"""CLI for finding all target intervals that intersect with a query."""

import argparse
from typing import Any

from pygenomics.bed import Bed3, Range, Record
from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval find`."""
    parser = subparser.add_parser(
        name="find_all",
        help="Get target intervals that intersect with the query",
    )
    parser.add_argument("target", help="target intervals")
    parser.add_argument("query", help="query intervals")


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval find_all`."""
    target = common.create_genomic_base(args.target)
    for record, query in common.iterate_records_and_intervals(args.query):
        intersections = GenomicBase.find_all(target, query)
        for seq, start, end in intersections:
            print(record, Record(Bed3(seq, Range(start - 1, end))), sep="\t")
