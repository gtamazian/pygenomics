"""CLI for merging intervals from multiple files."""

import argparse
import functools
import itertools
from typing import Any

from pygenomics.bed import Bed3, Range, Record
from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval merge`."""
    parser = subparser.add_parser(
        name="merge", help="Merge intervals from the specified files"
    )
    parser.add_argument(
        "interval_files",
        metavar="FILE",
        nargs="+",
        help="input files of genomic intervals",
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval merge`."""

    def process_file(acc: GenomicBase, new_file: str) -> GenomicBase:
        """Process a new file and merge its intervals into the base."""
        return GenomicBase(
            itertools.chain(
                GenomicBase.iterate_reduced(acc),
                (k for _, k in common.iterate_records_and_intervals(new_file)),
            )
        )

    result = functools.reduce(process_file, args.interval_files, GenomicBase([]))

    for seq, start, end in GenomicBase.iterate_reduced(result):
        print(Record(Bed3(seq, Range(start - 1, end))))
