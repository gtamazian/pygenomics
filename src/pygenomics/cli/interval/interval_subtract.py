"""CLI for subtracting genomic intervals using multiple files."""

import argparse
import functools
from typing import Any, List

from pygenomics.bed import Bed3, Range, Record
from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase, GenomicInterval


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval subtract`."""
    parser = subparser.add_parser(
        name="subtract", help="Subtract intervals from the first specified file"
    )
    parser.add_argument(
        "interval_files",
        metavar="FILE",
        nargs="+",
        help="input files of genomic intervals",
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval subtract`."""

    def process_file(
        acc: List[GenomicInterval], new_file: str
    ) -> List[GenomicInterval]:
        """Subtract from the accumulator intervals from the new file."""
        current_base = common.create_reduced_genomic_base(new_file)
        return [k for query in acc for k in GenomicBase.subtract(current_base, query)]

    init = GenomicBase.to_list(common.create_genomic_base(args.interval_files[0]))
    result = functools.reduce(process_file, args.interval_files[1:], init)

    for seq, start, end in result:
        print(Record(Bed3(seq, Range(start - 1, end))))
