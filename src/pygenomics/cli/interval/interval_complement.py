"""CLI for getting complement intervals."""

import argparse
import itertools
from typing import Any, List, Tuple

from pygenomics.bed import Bed3, Range, Record
from pygenomics.cli.interval import common
from pygenomics.common import split2
from pygenomics.interval import GenomicBase, GenomicInterval


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval complement`."""
    parser = subparser.add_parser(
        name="complement", help="Complement intervals from the file"
    )
    parser.add_argument("interval_file", help="file of genomic intervals")
    parser.add_argument("-g", "--genome", help="table of genome sequence sizes")


class IncorrectGenomeSizeLine(Exception):
    """Incorrect line in a file of genome sequence sizes."""

    path: str
    line: str

    def __init__(self, path: str, line: str):
        super().__init__(path, line)
        self.path = path
        self.line = line


def load_genome_sizes(path: str) -> List[Tuple[str, int]]:
    """Load genome sequence sizes from a 2-column tabular file."""

    def parse_line(line: str) -> Tuple[str, int]:
        """Parse a line from the genome sequence size file."""
        parts = split2(line, "\t")
        if parts is None:
            raise IncorrectGenomeSizeLine(path, line)
        try:
            return (parts[0], int(parts[1]))
        except ValueError as error:
            raise IncorrectGenomeSizeLine(path, line) from error

    with open(path, encoding="utf-8") as genome_size_file:
        return [parse_line(str.rstrip(k)) for k in genome_size_file]


def get_edges(
    seq_ranges: List[GenomicInterval], seq_sizes: List[Tuple[str, int]]
) -> List[GenomicInterval]:
    """Get intervals located at the beginning and end of genomic sequences."""
    range_dict = {k[0]: (k[1], k[2]) for k in seq_ranges}

    def process_sequence(name: str, size: int) -> List[GenomicInterval]:
        """Produce edge intervals for a single sequence."""
        if name in range_dict:
            leftmost, rightmost = range_dict[name]
            return ([(name, 1, leftmost - 1)] if leftmost > 1 else []) + (
                [(name, rightmost + 1, size)] if rightmost < size else []
            )
        return [(name, 1, size)]

    return list(
        itertools.chain.from_iterable(process_sequence(j, k) for j, k in seq_sizes)
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval complement`."""
    base = common.create_genomic_base(args.interval_file)
    if args.genome is not None:
        complement_ranges = sorted(
            itertools.chain(
                GenomicBase.iterate_complement(base),
                get_edges(GenomicBase.seq_ranges(base), load_genome_sizes(args.genome)),
            )
        )
    else:
        complement_ranges = list(GenomicBase.iterate_complement(base))

    for seq, start, end in complement_ranges:
        print(Record(Bed3(seq, Range(start - 1, end))))
