"""Common routines for CLI commands related to genomic intervals."""

import gzip
from enum import Enum, auto
from typing import Any, Iterator, Tuple, Union

from pygenomics import bed
from pygenomics.gff import gff3, gtf
from pygenomics.interval import GenomicBase, GenomicInterval
from pygenomics.vcf.data.record import Record as VcfRecord
from pygenomics.vcf.error import Error as VcfError
from pygenomics.vcf.reader import Reader as VcfReader


class FileFormat(Enum):
    """Data file format."""

    BED = auto()
    GFF3 = auto()
    GTF = auto()
    VCF = auto()


FileType = Tuple[FileFormat, bool]


_GZIP_MAGIC_NUMBER = b"\x1f\x8b"


def is_gzipped(path: str) -> bool:
    """Is the file compressed with gzip?

    :note: The function produces side effects by opening `path` and
      reading first two bytes from it to compare them to the gzip
      magic number.

    :param path: Name of the file to be checked.

    :return: Does the file start with the gzip magic number?

    """
    with open(path, "rb") as file_to_check:
        return file_to_check.read(2) == _GZIP_MAGIC_NUMBER


def is_bed(path: str) -> bool:
    """Check if the file is in the BED format.

    :note: The function produces side effects by calling
      :py:func:`is_gzipped`, re-opening `path` and attempting to read a
      BED record from it.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as bed_file:
        attempt = bed.Record.of_string(str.rstrip(next(bed_file)))
        return attempt is not None


def is_gff3(path: str) -> bool:
    """Check if the file is in the GFF3 format.

    :note: The function produces side effects by calling
      :py:func:`is_gzipped`, re-opening `path` and attempting to read a
      GFF3 record from it.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as gff3_file:
        line = next(gff3_file)
        while str.startswith(line, "#"):
            line = next(gff3_file)
        attempt = gff3.Record.of_string(str.rstrip(line))
        return attempt is not None


def is_gtf(path: str) -> bool:
    """Check if the file is in the GTF format.

    :note: The function produces side effects by calling
      :py:func:`is_gzipped`, re-opening `path` and attempting to read a
      GTF record from it.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as gtf_file:
        line = next(gtf_file)
        while str.startswith(line, "#"):
            line = next(gtf_file)
        attempt = gtf.Record.of_string(str.rstrip(line))
        return attempt is not None


def is_vcf(path: str) -> bool:
    """Check if the file is in the VCF format.

    :note: The function produces side effects by calling
      :py:func:`is_gzipped`, re-opening `path` and attempting to read
      the VCF header and a VCF record from it.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as vcf_file:
        try:
            reader = VcfReader(vcf_file)
            next(reader.data)
            return True
        except VcfError:
            return False


def bed_to_range(record: bed.Record) -> GenomicInterval:
    """Get the genomic interval for a BED record."""
    return (record.chrom, record.start + 1, record.end)


def gff3_to_range(record: gff3.Record) -> GenomicInterval:
    """Get the genomic interval for a GFF3 record."""
    return (record.pos.seqid, record.pos.start, record.pos.end)


def gtf_to_range(record: gtf.Record) -> GenomicInterval:
    """Get the genomic interval for a GTF record."""
    return (record.pos.seqid, record.pos.start, record.pos.end)


def vcf_to_range(record: VcfRecord) -> GenomicInterval:
    """Get the genomic interval for a VCF record based on its reference allele."""
    return (
        record.chrom.name,
        record.pos.value,
        record.pos.value + len(record.ref.bases) - 1,
    )


class IncorrectRecord(Exception):
    """Indicates an incorrect record in the BED, GFF3 or GTF format."""

    format_type: FileFormat
    line: str

    def __init__(self, format_type: FileFormat, line: str):
        super().__init__(format_type, line)
        self.format_type = format_type
        self.line = line


def iterate_bed_records(path: str) -> Iterator[bed.Record]:
    """Iterate records from a BED file.

    :note: The function produces side effects by opening the specified
      BED file and reading data from it. The function can also raise
      an exception if an incorrect BED record is read.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as bed_file:
        for line in map(str.rstrip, bed_file):
            parsed_record = bed.Record.of_string(line)
            if parsed_record is None:
                raise IncorrectRecord(FileFormat.BED, line)
            yield parsed_record


def iterate_gff3_records(path: str) -> Iterator[gff3.Record]:
    """Iterate records from a GFF3 file.

    :note: The function produces side effects by opening the specified
      GFF3 file and reading data from it. The function can also raise
      an exception if an incorrect GFF3 record is read.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as gff3_file:
        for line in map(str.rstrip, gff3_file):
            if str.startswith(line, "#"):
                continue
            parsed_record = gff3.Record.of_string(line)
            if parsed_record is None:
                raise IncorrectRecord(FileFormat.GFF3, line)
            yield parsed_record


def iterate_gtf_records(path: str) -> Iterator[gtf.Record]:
    """Iterate records from a GTF file.

    :note: The function produces side effects by opening the specified
      GTF file and reading data from it. The function can also raise
      the exception if an incorrect GTF record is read.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as gtf_file:
        for line in map(str.rstrip, gtf_file):
            if str.startswith(line, "#"):
                continue
            parsed_record = gtf.Record.of_string(line)
            if parsed_record is None:
                raise IncorrectRecord(FileFormat.GTF, line)
            yield parsed_record


def iterate_vcf_records(path: str) -> Iterator[VcfRecord]:
    """Iterate records from a VCF file.

    :note: The function produces side effects by opening the specified
      VCF file and reading data from it.

    """
    open_fn: Any = gzip.open if is_gzipped(path) else open
    with open_fn(path, mode="rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        for k in reader.data:
            yield k


class IncorrectFileFormat(Exception):
    """Indicates that the file of genomic intervals has an incorrect format."""

    path: str

    def __init__(self, path: str):
        super().__init__(path)
        self.path = path


def create_genomic_base(path: str) -> GenomicBase:
    """Create the base of genomic intervals from the specified file.

    :note: The function produces side effects by reading from the file
      and can raise exception :py:class:`IncorrectFileFormat`.

    """
    if is_bed(path):
        return GenomicBase(bed_to_range(k) for k in iterate_bed_records(path))
    if is_gff3(path):
        return GenomicBase(gff3_to_range(k) for k in iterate_gff3_records(path))
    if is_gtf(path):
        return GenomicBase(gtf_to_range(k) for k in iterate_gtf_records(path))
    if is_vcf(path):
        return GenomicBase(vcf_to_range(k) for k in iterate_vcf_records(path))
    raise IncorrectFileFormat(path)


def create_reduced_genomic_base(path: str) -> GenomicBase:
    """Create the base of non-overlapping genomic intervals.

    :note: The function produces the same side effects as function
      :py:func:`create_genomic_base`.

    """
    return GenomicBase(GenomicBase.iterate_reduced(create_genomic_base(path)))


Record = Union[bed.Record, gff3.Record, gtf.Record, VcfRecord]


def iterate_records_and_intervals(
    path: str,
) -> Iterator[Tuple[Record, GenomicInterval]]:
    """Iterate records and the corresponding genomic intervals."""
    if is_bed(path):
        return ((k, bed_to_range(k)) for k in iterate_bed_records(path))
    if is_gff3(path):
        return ((k, gff3_to_range(k)) for k in iterate_gff3_records(path))
    if is_gtf(path):
        return ((k, gtf_to_range(k)) for k in iterate_gtf_records(path))
    if is_vcf(path):
        return ((k, vcf_to_range(k)) for k in iterate_vcf_records(path))
    raise IncorrectFileFormat(path)
