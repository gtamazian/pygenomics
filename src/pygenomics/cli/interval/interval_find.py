"""CLI for finding intervals that intersect with the specified target."""

import argparse
from typing import Any

from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase


def register_parser(subparser: Any) -> None:
    """Register the subparser for `pygenomics interval find`."""
    parser = subparser.add_parser(
        name="find", help="Get query intervals that intersect with the target"
    )
    parser.add_argument("target", help="target intervals")
    parser.add_argument("query", help="query intervals")
    parser.add_argument(
        "-v",
        "--invert-match",
        action="store_true",
        help="get intervals that do not intersect with the target",
    )


def main(args: argparse.Namespace) -> None:
    """Main function of tool `interval find`."""
    target_base = common.create_genomic_base(args.target)
    for record, record_interval in common.iterate_records_and_intervals(args.query):
        if GenomicBase.find(target_base, record_interval) != args.invert_match:
            print(record)
