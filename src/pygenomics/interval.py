"""Genomic intervals and the related operations."""

import itertools
import typing
from collections import defaultdict
from enum import Enum, auto
from typing import DefaultDict, Iterable, Iterator, List, Optional, Tuple


class IncorrectInterval(Exception):
    """Incorrect start and end coordinates of a range."""

    start: int
    end: int

    def __init__(self, start: int, end: int):
        super().__init__(start, end)
        self.start = start
        self.end = end


class _Color(Enum):
    """Node color type."""

    RED = auto()
    BLACK = auto()


class _Interval:
    """Integer interval which includes both start and end positions."""

    _start: int
    _end: int

    def __init__(self, start: int, end: int):
        if start > end:
            raise IncorrectInterval(start, end)
        self._start = start
        self._end = end

    @property
    def start(self) -> int:
        """Start position of the range."""
        return self._start

    @property
    def end(self) -> int:
        """End position of the range."""
        return self._end

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, _Interval):
            return NotImplemented
        return self.start == other.start and self.end == other.end

    def __lt__(self, other: object) -> bool:
        if not isinstance(other, _Interval):
            return NotImplemented
        return self.start < other.start

    def __repr__(self) -> str:
        return str.format(
            "{:s}(start={:d}, end={:d})", self.__class__.__name__, self.start, self.end
        )


def overlap(first: _Interval, second: _Interval) -> bool:
    """Check if two intervals overlap.

    :param first: First interval.

    :param second: Second interval.

    :return: Do the intervals overlap?

    """
    return first.start <= second.end and second.start <= first.end


class _Node:
    # pylint: disable=too-few-public-methods
    """Node of an interval red-black tree."""

    color: _Color = _Color.RED
    key: _Interval
    max_end: int
    parent: Optional["_Node"] = None
    left: Optional["_Node"] = None
    right: Optional["_Node"] = None

    def __init__(self, key: _Interval):
        self.key = key
        self.max_end = self.key.end

    def __repr__(self) -> str:
        return str.format(
            "{:s}(color={:s}, key={:s}, max_end={:d}, parent={:s}, left={:s}, right={:s})",
            self.__class__.__name__,
            repr(self.color),
            repr(self.key),
            self.max_end,
            str.format("<_Node at {:x}>", id(self.parent))
            if self.parent is not None
            else "None",
            str.format("<_Node at {:x}>", id(self.left))
            if self.left is not None
            else "None",
            str.format("<_Node at {:x}>", id(self.right))
            if self.right is not None
            else "None",
        )


class _Tree:
    """Red-black interval tree."""

    root: Optional[_Node] = None

    def _btree_insert(self, node_z: _Node) -> None:
        """Insert a new range in the tree using the binary search technique.

        The red-black tree invariants can become violated after the
         insertion and the tree should be rebalanced to restore them.

        """
        node_y: Optional[_Node] = None
        node_x: Optional[_Node] = self.root

        while node_x is not None:
            node_y = node_x
            node_x.max_end = max(node_x.max_end, node_z.max_end)
            if node_z.key < node_x.key:
                node_x = node_x.left
            else:
                node_x = node_x.right

        node_z.parent = node_y
        if node_y is None:
            self.root = node_z
        else:
            if node_z.key < node_y.key:
                node_y.left = node_z
            else:
                node_y.right = node_z

    def _left_rotate(self, node_x: _Node) -> None:
        """Left rotation of the specified node."""
        assert node_x.right is not None
        node_y = node_x.right
        node_x.right = node_y.left

        if node_y.left is not None:
            node_y.left.parent = node_x
        node_y.parent = node_x.parent
        if node_x.parent is None:
            self.root = node_y
        else:
            if node_x == node_x.parent.left:
                node_x.parent.left = node_y
            else:
                node_x.parent.right = node_y
        node_y.left = node_x
        node_x.parent = node_y

        node_x.max_end = max(
            node_x.key.end,
            node_x.left.max_end if node_x.left is not None else -1,
            node_x.right.max_end if node_x.right is not None else -1,
        )
        node_y.max_end = max(node_y.max_end, node_x.max_end)

    def _right_rotate(self, node_y: _Node) -> None:
        """Right notation of the specified node."""
        assert node_y.left is not None
        node_x = node_y.left
        node_y.left = node_x.right

        if node_x.right is not None:
            node_x.right.parent = node_y
        node_x.parent = node_y.parent
        if node_y.parent is None:
            self.root = node_x
        else:
            if node_y == node_y.parent.left:
                node_y.parent.left = node_x
            else:
                node_y.parent.right = node_x
        node_x.right = node_y
        node_y.parent = node_x

        node_y.max_end = max(
            node_y.key.end,
            node_y.left.max_end if node_y.left is not None else -1,
            node_y.right.max_end if node_y.right is not None else -1,
        )
        node_x.max_end = max(node_x.max_end, node_y.max_end)

    @typing.no_type_check
    def insert(self, node_x: _Node) -> None:
        """Insert a new node in the tree."""
        self._btree_insert(node_x)
        while node_x != self.root and node_x.parent.color is _Color.RED:
            if node_x.parent == node_x.parent.parent.left:
                node_y = node_x.parent.parent.right
                if node_y is not None and node_y.color is _Color.RED:
                    node_x.parent.color = _Color.BLACK
                    node_y.color = _Color.BLACK
                    node_x.parent.parent.color = _Color.RED
                    node_x = node_x.parent.parent
                else:
                    if node_x == node_x.parent.right:
                        node_x = node_x.parent
                        self._left_rotate(node_x)
                    node_x.parent.color = _Color.BLACK
                    node_x.parent.parent.color = _Color.RED
                    self._right_rotate(node_x.parent.parent)
            else:
                # node_x.parent == node_x.parent.parent.right
                node_y = node_x.parent.parent.left
                if node_y is not None and node_y.color is _Color.RED:
                    node_x.parent.color = _Color.BLACK
                    node_y.color = _Color.BLACK
                    node_x.parent.parent.color = _Color.RED
                    node_x = node_x.parent.parent
                else:
                    if node_x == node_x.parent.left:
                        node_x = node_x.parent
                        self._right_rotate(node_x)
                    node_x.parent.color = _Color.BLACK
                    node_x.parent.parent.color = _Color.RED
                    self._left_rotate(node_x.parent.parent)
        assert self.root is not None
        self.root.color = _Color.BLACK

    def to_list(self) -> List[_Interval]:
        """Get the list of the tree intervals."""

        def to_list_traverse(node: Optional[_Node]) -> List[_Interval]:
            """Traverse the tree to collect its intervals."""
            if node is None:
                return []
            return (
                to_list_traverse(node.left) + [node.key] + to_list_traverse(node.right)
            )

        return to_list_traverse(self.root)

    def find(self, interval: _Interval) -> bool:
        """Check if the specified interval overlaps with an interval from the tree."""
        node_x = self.root
        while node_x is not None and not overlap(interval, node_x.key):
            if node_x.left is not None and node_x.left.max_end >= interval.start:
                node_x = node_x.left
            else:
                node_x = node_x.right
        return node_x is not None

    def find_all(self, interval: _Interval) -> List[_Interval]:
        """Find all overlaps between the interval and the tree."""

        def find_all_traverse(node: _Node) -> List[_Interval]:
            """Traverse the tree to find all overlaps with the interval."""
            result: List[_Interval] = []
            if overlap(interval, node.key):
                result += [node.key]
            if node.left is not None and node.left.max_end >= interval.start:
                result += find_all_traverse(node.left)
            if (
                node.right is not None
                and node.key.start <= interval.end
                and node.right.max_end >= interval.start
            ):
                result += find_all_traverse(node.right)
            return result

        return find_all_traverse(self.root) if self.root is not None else []

    @staticmethod
    def minimum(node_x: _Node) -> Optional[_Node]:
        """Get the minimum key node in the subtree which root is the specified node."""
        while node_x.left is not None:
            node_x = node_x.left
        return node_x

    @staticmethod
    def _successor(node_x: _Node) -> Optional[_Node]:
        """Get the node following the specified one."""
        if node_x.right is not None:
            return _Tree.minimum(node_x.right)
        node_y = node_x.parent
        while node_y is not None and node_x == node_y.right:
            node_x = node_y
            node_y = node_y.parent
        return node_y

    def iterate_reduced(self) -> Iterator[_Interval]:
        """Iterate non-overlapping (reduced) intervals from the tree."""
        if self.root is None:
            return
        start_node = _Tree.minimum(self.root)
        assert start_node is not None

        accumulator = start_node.key
        current_node = _Tree._successor(start_node)
        while current_node is not None:
            if current_node.key.start - accumulator.end <= 1:
                accumulator = _Interval(
                    accumulator.start, max(accumulator.end, current_node.key.end)
                )
            else:
                yield accumulator
                accumulator = current_node.key
            current_node = _Tree._successor(current_node)

        yield accumulator

    def iterate_complement(self) -> Iterator[_Interval]:
        """Iterate intervals that are complement to the tree."""
        reduced_it = self.iterate_reduced()
        try:
            j = next(reduced_it)
        except StopIteration:
            return

        for k in reduced_it:
            yield _Interval(j.end + 1, k.start - 1)
            j = k


GenomicInterval = Tuple[str, int, int]


def _subtract_overlapping(first: _Interval, second: _Interval) -> List[_Interval]:
    """Subtract the second interval from the first assuming that they overlap."""
    assert overlap(first, second)
    return (
        [_Interval(first.start, second.start - 1)] if first.start < second.start else []
    ) + ([_Interval(second.end + 1, first.end)] if second.end < first.end else [])


class GenomicBase:
    """Collection of genomic intervals arranged by their sequences."""

    _intervals: DefaultDict[str, _Tree]

    def __init__(self, values: Iterable[GenomicInterval]):
        self._intervals = defaultdict(_Tree)
        for seq, start, end in values:
            _Tree.insert(self._intervals[seq], _Node(_Interval(start, end)))

    def to_list(self) -> List[GenomicInterval]:
        """Get the list of intervals from the tree."""
        return [
            (seq, interval.start, interval.end)
            for seq, tree in dict.items(self._intervals)
            for interval in _Tree.to_list(tree)
        ]

    def find(self, query: GenomicInterval) -> bool:
        """Check if the specified genomic interval is in the collection."""
        seq, start, end = query
        if seq in self._intervals:
            return _Tree.find(self._intervals[seq], _Interval(start, end))
        return False

    def find_all(self, query: GenomicInterval) -> List[GenomicInterval]:
        """Find all overlaps between the genomic interval and the collection."""
        seq, start, end = query
        if seq in self._intervals:
            return [
                (seq, k.start, k.end)
                for k in _Tree.find_all(self._intervals[seq], _Interval(start, end))
            ]
        return []

    def iterate_reduced(self) -> Iterator[GenomicInterval]:
        """Iterate non-overlapping genomic intervals."""
        return (
            (seq, interval.start, interval.end)
            for seq, tree in dict.items(self._intervals)
            for interval in _Tree.iterate_reduced(tree)
        )

    def iterate_complement(self) -> Iterator[GenomicInterval]:
        """Iterate complement intervals."""
        return (
            (seq, interval.start, interval.end)
            for seq, tree in dict.items(self._intervals)
            for interval in _Tree.iterate_complement(tree)
        )

    def intersect(self, query: GenomicInterval) -> List[GenomicInterval]:
        """Intersect the query interval with genomic intervals from the collection.

        The empty list is returned if there are no intersections.

        """
        seq, start, end = query
        if seq in self._intervals:
            query_interval = _Interval(start, end)
            return [
                (seq, max(start, k.start), min(end, k.end))
                for k in _Tree.find_all(self._intervals[seq], query_interval)
                if overlap(query_interval, k)
            ]
        return []

    def subtract(self, query: GenomicInterval) -> List[GenomicInterval]:
        """Subtract the base intervals from the query interval."""
        seq, start, end = query
        if seq in self._intervals:
            query_interval = _Interval(start, end)
            base_overlaps = _Tree.find_all(self._intervals[seq], query_interval)
            result = [query_interval]
            for k in base_overlaps:
                updated_result = list(
                    itertools.chain.from_iterable(
                        (
                            (
                                _subtract_overlapping(query_part, k)
                                if overlap(query_part, k)
                                else [query_part]
                            )
                            for query_part in result
                        )
                    )
                )
                result = updated_result
                if not updated_result:
                    break
            return [(seq, k.start, k.end) for k in result]
        return []

    def seq_ranges(self) -> List[GenomicInterval]:
        """ "Get ranges for sequences in the base."""
        result: List[GenomicInterval] = []
        for seq, base in dict.items(self._intervals):
            assert base.root is not None
            base_minimum = _Tree.minimum(base.root)
            assert base_minimum is not None
            new_range: GenomicInterval = (
                seq,
                base_minimum.key.start,
                base.root.max_end,
            )
            list.append(result, new_range)

        return result
