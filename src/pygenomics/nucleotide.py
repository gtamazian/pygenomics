"""Symbols representing nucleotide sequences."""

LOWERCASE = "acgtmrwsykvhdbn"
"""Lowercase nucleotide characters."""
UPPERCASE = str.upper(LOWERCASE)
"""Uppercase nucleotide characters."""
LETTERS = LOWERCASE + UPPERCASE
"""Lowercase and uppercase nucleotide characters."""
GAPS = "nN"
"""Symbols representing gaps in a nucleotide sequence."""

# The following two module-level variables are required for efficient
# implementation of function rev_comp().

_COMP_LOWERCASE = "tgcakywsrmbdhvn"
_COMP_TRANSLATION_TABLE = str.maketrans(
    LETTERS, _COMP_LOWERCASE + str.upper(_COMP_LOWERCASE)
)

# The following set is required for efficient checking whether a
# nucleotide character is a gap in functions is_gap(),
# gap_prefix_len() and gap_suffix_len().

_GAP_SET = frozenset(GAPS)

EXACT_LOWERCASE = "acgt"
EXACT_UPPERCASE = str.upper(EXACT_LOWERCASE)
EXACT_LETTERS = EXACT_LOWERCASE + EXACT_UPPERCASE

# The following set is required for efficient checking whether a
# nucleotide character represents an exact nucleotide. This check is
# used in function is_ambiguous().

_EXACT_SET = frozenset(EXACT_LETTERS)

# The following set is used in function is_exact_or_gap().

_EXACT_OR_GAP_SET = frozenset(EXACT_LETTERS + GAPS)


def rev_comp(sequence: str) -> str:
    """Get reverse complement of a nucleotide sequence.

    :param sequence: Nucleotide sequence.

    :return: Reverse complement of `sequence`.

    """
    return str.translate(sequence, _COMP_TRANSLATION_TABLE)[::-1]


def remove_soft_mask(sequence: str) -> str:
    """Remove soft masking from a nucleotide sequence.

    :param sequence: Nucleotide sequence.

    :return: `sequence` with soft-masked characters unmasked.

    """
    return str.upper(sequence)


def soft_to_hard_mask(sequence: str) -> str:
    """Replace soft-masked (i.e. lowercase) nucleotides with Ns.

    Lowercase N characters are also replaced with uppercase ones.

    :param sequence: Nucleotide sequence.

    :return: `sequence` with any lowercase characters replaced with Ns.

    """
    return str.join("", (k if str.isupper(k) else "N" for k in sequence))


def is_gap(character: str) -> bool:
    """Check if a nucleotide character is a gap.

    :param character: Nucleotide sequence character.

    :return: Does `character` designate a gap?

    """
    return character in _GAP_SET


def is_gap_sequence(sequence: str) -> bool:
    """Check if a nucleotide sequence consists of gaps only.

    :param sequence: Nucleotide sequence.

    :return: Does `sequence` contain only gap characters?

    """
    return all(is_gap(k) for k in sequence) if sequence else False


def is_ambiguous(sequence: str) -> bool:
    """Check if a nucleotide sequence is ambiguous.

    :param sequence: Nucleotide sequence.

    :return: Does `sequence` contain any ambiguous nucleotide
      characters (that is, characters other than uppercase and
      lowercase A, C, G and T)?

    """
    return any(k not in _EXACT_SET for k in sequence)


def is_exact_or_gap(sequence: str) -> bool:
    """Check if a nucleotide sequence contains exact bases or gaps only.

    :param sequence: Nucleotide sequence.

    :return: Does `sequence` contain exact bases or gaps only (both
      cases are allowed)?

    """
    return all(k in _EXACT_OR_GAP_SET for k in sequence)


def gap_prefix_len(sequence: str) -> int:

    """Get the length of the gap prefix for a nucleotide sequence.

    :param sequence: Nucleotide sequence.

    :return: Length of gap prefix in `sequence`.

    """
    if not sequence:
        return 0

    prefix_len = 0
    for prefix_len, nucl in enumerate(sequence):
        if not is_gap(nucl):
            return prefix_len

    return prefix_len + 1


def gap_suffix_len(sequence: str) -> int:
    """Get the length of the gap suffix for a nucleotide sequence.

    :param sequence: Nucleotide sequence.

    :return: Length of gap suffix in `sequence`.

    """
    if not sequence:
        return 0

    suffix_len = 0
    for suffix_len, nucl in enumerate(reversed(sequence)):
        if not is_gap(nucl):
            return suffix_len

    return suffix_len + 1
