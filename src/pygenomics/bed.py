"""Reading and writing BED files."""

from enum import Enum, auto
from typing import List, Optional, Tuple, Union

from pygenomics import common, error
from pygenomics.strand import Strand


class BedErrorType(Enum):
    """Types of invariant violations for a BED record."""

    EMPTY_FIELD = auto()
    """Empty field (chrom, name, extra)."""
    INCORRECT_RANGE = auto()
    """Incorrect start and end coordinates."""
    INCORRECT_SCORE = auto()
    """Incorrect score value."""
    INCORRECT_THICK_RANGE = auto()
    """Incorrect thick range."""
    INCORRECT_BLOCKS = auto()
    """Incorrect blocks of a BED12 record."""


class BedError(error.Error):
    """Error for a violation of BED record invariants."""

    def __init__(self, error_type: BedErrorType):
        super().__init__(error_type)
        self.error_type = error_type


class Range(common.Range):
    """Range in the BED format."""

    def __init__(self, start: int, end: int):
        if not 0 <= start < end:
            raise BedError(BedErrorType.INCORRECT_RANGE)
        super().__init__(start, end)

    @staticmethod
    def of_strings(start_str: str, end_str: str) -> Optional["Range"]:
        """Convert a pair of strings to a BED range.

        :param start_str: String of the start coordinate.

        :param end_str: String of the end coordinate.

        :return: Range from `start_str` to `end_str` if the strings
          encode a proper BED range, `None` otherwise.

        """
        start = common.str_to_int(start_str)
        end = common.str_to_int(end_str)
        if start is None or end is None:
            return None
        try:
            return Range(start, end)
        except BedError:
            return None


class Bed3:
    """BED3 record."""

    _chrom: str
    _range: Range

    _num_columns = 3

    def __init__(self, chrom: str, bed_range: Range):
        if not chrom:
            raise BedError(BedErrorType.EMPTY_FIELD)

        self._chrom = chrom
        self._range = bed_range

    @classmethod
    def num_columns(cls) -> int:
        """Number of columns in a BED3 line."""
        return cls._num_columns

    @property
    def chrom(self) -> str:
        """Chromosome or sequence name."""
        return self._chrom

    @property
    def start(self) -> int:
        """Start coordinate."""
        return self._range.start

    @property
    def end(self) -> int:
        """End coordinate."""
        return self._range.end

    @property
    def record_range(self) -> Range:
        """Range of the BED record."""
        return self._range

    def __repr__(self) -> str:
        return str.format(
            '{:s}(chrom="{:s}", start={:d}, end={:d})',
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.end,
        )

    def __str__(self) -> str:
        return str.format("{:s}\t{:d}\t{:d}", self.chrom, self.start, self.end)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Bed3):
            return NotImplemented
        return self.chrom == other.chrom and self.record_range == other.record_range

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Bed3"]:
        """Convert line parts to the BED3 record.

        :param parts: Line parts possibly encoding the BED3 record.

        :return: BED3 record if `parts` properly encodes it, `None`
          otherwise.

        """
        if len(parts) == cls.num_columns():
            record_range = Range.of_strings(parts[1], parts[2])
            if record_range is not None:
                try:
                    return Bed3(parts[0], record_range)
                except BedError:
                    return None
        return None


class Bed4(Bed3):
    """BED4 record."""

    _name: str

    _num_columns = 4

    def __init__(self, chrom: str, bed_range: Range, name: str):
        if not name:
            raise BedError(BedErrorType.EMPTY_FIELD)
        super().__init__(chrom, bed_range)
        self._name = name

    @property
    def name(self) -> str:
        """Name of the BED record."""
        return self._name

    def __repr__(self) -> str:
        return str.format(
            '{:s}(chrom="{:s}", start={:d}, end={:d}, name="{:s}")',
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.end,
            self.name,
        )

    def __str__(self) -> str:
        return str.format("{:s}\t{:s}", super().__str__(), self.name)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Bed4):
            return NotImplemented
        return super().__eq__(other) and self.name == other.name

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Bed4"]:
        """Convert line parts to the BED4 record.

        :param parts: Line parts possibly encoding the BED4 record.

        :return: BED4 record if `parts` properly encodes it, `None`
          otherwise.

        """
        if len(parts) == cls.num_columns():
            bed3_part = Bed3.of_list(parts[: Bed3.num_columns()])
            if bed3_part is not None:
                try:
                    return Bed4(bed3_part.chrom, bed3_part.record_range, parts[3])
                except BedError:
                    return None
        return None


class Bed6(Bed4):
    """BED6 record."""

    _score: int
    _strand: Strand

    _num_columns = 6

    def __init__(
        self, chrom: str, bed_range: Range, name: str, score: int, strand: Strand
    ):  # pylint: disable=too-many-arguments
        if not 0 <= score <= 1000:
            raise BedError(BedErrorType.INCORRECT_SCORE)
        super().__init__(chrom, bed_range, name)
        self._score = score
        self._strand = strand

    @property
    def score(self) -> int:
        """Score of the BED record."""
        return self._score

    @property
    def strand(self) -> Strand:
        """Strand of the BED record."""
        return self._strand

    def __repr__(self) -> str:
        return str.format(
            '{:s}(chrom="{:s}", start={:d}, end={:d}, name="{:s}", score={:d}, '
            "strand={:s})",
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.end,
            self.name,
            self.score,
            self.strand,
        )

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:d}\t{:s}", super().__str__(), self.score, self.strand
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Bed6):
            return NotImplemented
        return (
            super().__eq__(other)
            and self.score == other.score
            and self.strand == other.strand
        )

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Bed6"]:
        """Convert line parts to the BED6 record.

        :param parts: Line parts possibly encoding the BED6 record.

        :return: BED6 record if `parts` properly encodes it, `None`
          otherwise.

        """
        if len(parts) == cls.num_columns():
            bed4_part = Bed4.of_list(parts[: Bed4.num_columns()])
            score = common.str_to_int(parts[4])
            strand = Strand.of_string(parts[5])
            if bed4_part is not None and score is not None and strand is not None:
                try:
                    return Bed6(
                        bed4_part.chrom,
                        bed4_part.record_range,
                        bed4_part.name,
                        score,
                        strand,
                    )
                except BedError:
                    return None
        return None


class Bed9(Bed6):
    """BED9 record."""

    _thick_range: Range
    _extra: str

    _num_columns = 9

    def __init__(
        self,
        chrom: str,
        bed_range: Range,
        name: str,
        score: int,
        strand: Strand,
        thick_range: Range,
        extra: str,
    ):  # pylint: disable=too-many-arguments
        if not (
            bed_range.start <= thick_range.start and thick_range.end <= bed_range.end
        ):
            raise BedError(BedErrorType.INCORRECT_THICK_RANGE)
        if not extra:
            raise BedError(BedErrorType.EMPTY_FIELD)

        super().__init__(chrom, bed_range, name, score, strand)
        self._thick_range = thick_range
        self._extra = extra

    @property
    def thick_start(self) -> int:
        """Start coordinate of the thick region."""
        return self._thick_range.start

    @property
    def thick_end(self) -> int:
        """End coordinate of the thick region."""
        return self._thick_range.end

    @property
    def thick_range(self) -> Range:
        """Thick range of the BED record."""
        return self._thick_range

    @property
    def extra(self) -> str:
        """Extra field of the BED record."""
        return self._extra

    def __repr__(self) -> str:
        return str.format(
            '{:s}(chrom="{:s}", start={:d}, end={:d}, name="{:s}", score={:d}, '
            'strand={:s}, thick_start={:d}, thick_end={:d}, extra="{:s}")',
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.end,
            self.name,
            self.score,
            self.strand,
            self.thick_start,
            self.thick_end,
            self.extra,
        )

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:d}\t{:d}\t{:s}",
            super().__str__(),
            self.thick_start,
            self.thick_end,
            self.extra,
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Bed9):
            return NotImplemented
        return (
            super().__eq__(other)
            and self.thick_range == other.thick_range
            and self.extra == other.extra
        )

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Bed9"]:
        """Convert line parts to the BED9 record.

        :param parts: Line parts possibly encoding the BED9 record.

        :return: BED9 record if `parts` properly encodes it, `None`
          otherwise.

        """
        if len(parts) == cls.num_columns():
            bed6_part = Bed6.of_list(parts[: Bed6.num_columns()])
            thick_range = Range.of_strings(parts[6], parts[7])
            if bed6_part is not None and thick_range is not None:
                try:
                    return Bed9(
                        bed6_part.chrom,
                        bed6_part.record_range,
                        bed6_part.name,
                        bed6_part.score,
                        bed6_part.strand,
                        thick_range,
                        parts[8],
                    )
                except BedError:
                    return None
        return None


class Bed12(Bed9):
    """BED12 record."""

    _blocks: List[Tuple[int, int]]

    _num_columns = 12

    def __init__(
        self,
        chrom: str,
        bed_range: Range,
        name: str,
        score: int,
        strand: Strand,
        thick_range: Range,
        extra: str,
        blocks: List[Tuple[int, int]],
    ):  # pylint: disable=too-many-arguments
        def check_blocks() -> bool:
            for start, size in blocks:
                if start < 0 or size <= 0:
                    return False
            for (prev_start, prev_size), (curr_start, curr_size) in zip(
                blocks[:-1], blocks[1:]
            ):
                if prev_start + prev_size > curr_start + curr_size:
                    return False
            return True

        def check_end_position() -> bool:
            if not blocks:
                return False
            last_start, last_size = blocks[-1]
            return bed_range.start + last_start + last_size == bed_range.end

        print(check_blocks())
        print(check_end_position())
        if not (blocks and check_blocks() and check_end_position()):
            raise BedError(BedErrorType.INCORRECT_BLOCKS)

        super().__init__(chrom, bed_range, name, score, strand, thick_range, extra)
        self._blocks = blocks

    @property
    def block_count(self) -> int:
        """Number of blocks."""
        return len(self._blocks)

    @property
    def block_sizes(self) -> List[int]:
        """Sizes of blocks."""
        return [k for _, k in self._blocks]

    @property
    def block_starts(self) -> List[int]:
        """Start coordinates of blocks relative to the record start coordinate."""
        return [k for k, _ in self._blocks]

    def __repr__(self) -> str:
        return str.format(
            '{:s}(chrom="{:s}", start={:d}, end={:d}, name="{:s}", score={:d}, '
            'strand={:s}, thick_start={:d}, thick_end={:d}, extra="{:s}", '
            "blocks={:s})",
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.end,
            self.name,
            self.score,
            self.strand,
            self.thick_start,
            self.thick_end,
            self.extra,
            repr(self._blocks),
        )

    def __str__(self) -> str:
        return str.format(
            "{:s}\t{:d}\t{:s}\t{:s}",
            super().__str__(),
            self.block_count,
            str.join(",", (str.format("{:d}", k) for k in self.block_sizes)),
            str.join(",", (str.format("{:d}", k) for k in self.block_starts)),
        )

    @classmethod
    def of_list(cls, parts: List[str]) -> Optional["Bed12"]:
        if len(parts) == cls.num_columns():
            bed9_part = Bed9.of_list(parts[: Bed9.num_columns()])
            block_count = common.str_to_int(parts[9])
            block_sizes = common.str_to_int_list(parts[10], ",")
            block_starts = common.str_to_int_list(parts[11], ",")
            if (
                bed9_part is not None
                and block_count is not None
                and block_sizes is not None
                and block_starts is not None
                and block_count == len(block_sizes) == len(block_starts)
            ):
                try:
                    return Bed12(
                        bed9_part.chrom,
                        bed9_part.record_range,
                        bed9_part.name,
                        bed9_part.score,
                        bed9_part.strand,
                        bed9_part.thick_range,
                        bed9_part.extra,
                        list(zip(block_starts, block_sizes)),
                    )
                except BedError:
                    return None
        return None


BedRecord = Union[Bed3, Bed4, Bed6, Bed9, Bed12]


class Record:
    """General BED record."""

    _record: BedRecord

    def __init__(self, record: BedRecord):
        self._record = record

    @property
    def record(self) -> BedRecord:
        """Record object of the specific BED type."""
        return self._record

    @property
    def chrom(self) -> str:
        """Chromosome or sequence name."""
        return self.record.chrom

    @property
    def start(self) -> int:
        """Start coordinate."""
        return self.record.start

    @property
    def end(self) -> int:
        """End coordinate."""
        return self.record.end

    @property
    def name(self) -> Optional[str]:
        """Record name."""
        return (
            self.record.name
            if isinstance(self.record, (Bed4, Bed6, Bed9, Bed12))
            else None
        )

    @property
    def score(self) -> Optional[int]:
        """Record score."""
        return (
            self.record.score if isinstance(self.record, (Bed6, Bed9, Bed12)) else None
        )

    @property
    def strand(self) -> Optional[Strand]:
        """Record strand."""
        return (
            self.record.strand if isinstance(self.record, (Bed6, Bed9, Bed12)) else None
        )

    @property
    def thick_start(self) -> Optional[int]:
        """Start coordinate of the thick region."""
        return (
            self.record.thick_start if isinstance(self.record, (Bed9, Bed12)) else None
        )

    @property
    def thick_end(self) -> Optional[int]:
        """End coordinate of the thick region."""
        return self.record.thick_end if isinstance(self.record, (Bed9, Bed12)) else None

    @property
    def extra(self) -> Optional[str]:
        """Extra field."""
        return self.record.extra if isinstance(self.record, (Bed9, Bed12)) else None

    @property
    def block_count(self) -> Optional[int]:
        """Number of blocks."""
        return self.record.block_count if isinstance(self.record, Bed12) else None

    @property
    def block_sizes(self) -> Optional[List[int]]:
        """Block sizes."""
        return self.record.block_sizes if isinstance(self.record, Bed12) else None

    def block_starts(self) -> Optional[List[int]]:
        """Block starts relative to the record start coordinate."""
        return self.record.block_starts if isinstance(self.record, Bed12) else None

    def __repr__(self) -> str:
        return str.format(
            "{:s}(record={:s})", self.__class__.__name__, repr(self.record)
        )

    def __str__(self) -> str:
        return str(self.record)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Record):
            return NotImplemented
        return self.record == other.record

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Convert a line to the BED record.

        :param line: Line possibly encoding the BED record.

        :return: BED record if `line` properly encodes it, `None`
          otherwise.

        """
        parts = str.split(line, "\t")
        if len(parts) == Bed3.num_columns():
            record = Bed3.of_list(parts)
        elif len(parts) == Bed4.num_columns():
            record = Bed4.of_list(parts)
        elif len(parts) == Bed6.num_columns():
            record = Bed6.of_list(parts)
        elif len(parts) == Bed9.num_columns():
            record = Bed9.of_list(parts)
        elif len(parts) == Bed12.num_columns():
            record = Bed12.of_list(parts)
        else:
            record = None
        return Record(record) if record is not None else None
