"""Routines to process contigs and scaffolds."""

import functools
from enum import Enum, auto
from typing import List, Optional, Tuple

from pygenomics import error, nucleotide


class ContigErrorType(Enum):
    """Types of invariant violations for a contig object."""

    NEGATIVE_GAP_LENGTH = auto()
    GAPS_IN_SEQUENCE = auto()


class ContigError(error.Error):
    """Error for a violation of contig object invariants."""

    error_type: ContigErrorType

    def __init__(self, error_type: ContigErrorType):
        super().__init__(self, error_type)
        self.error_type = error_type


class Contig:
    """Contig sequence with the following gap.

    The gap length is guaranteed to be a non-negative integer. The
    contig sequence may be empty; in that case, the object denotes a
    gap only. The contig sequence must not contain gaps.

    """

    _seq: str
    _gap_length: int

    def __init__(self, seq: str, gap_length: int):
        if gap_length < 0:
            raise ContigError(ContigErrorType.NEGATIVE_GAP_LENGTH)
        if any(nucleotide.is_gap(k) for k in seq):
            raise ContigError(ContigErrorType.GAPS_IN_SEQUENCE)
        self._seq = seq
        self._gap_length = gap_length

    @property
    def seq(self) -> str:
        """Contig sequence."""
        return self._seq

    @property
    def gap_length(self) -> int:
        """Length of a gap following the contig sequence."""
        return self._gap_length

    def __repr__(self) -> str:
        return str.format(
            '{:s}(seq="{:s}", gap_length={:d})',
            self.__class__.__name__,
            self.seq,
            self.gap_length,
        )

    def __str__(self) -> str:
        return self.seq + "N" * self.gap_length

    @staticmethod
    def of_string(line: str) -> Optional["Contig"]:
        """Convert a line to the contig object.

        :param line: Line possibly encoding a contig.

        :return: Contig object derived from `line` if `line` is a
          non-gap sequence followed by an optional gap, `None`
          otherwise.

        """
        gap_length = nucleotide.gap_suffix_len(line)
        try:
            return Contig(line[: len(line) - gap_length], gap_length)
        except ContigError:
            return None


def to_contigs(sequence: str) -> List[Contig]:
    """Split a nucleotide sequence into contigs.

    :param sequence: Nucleotide sequence.

    :return: Contigs the sequence is split into.

    """

    def find_gap(start: int) -> int:
        """Find the minimal gap character index after the specified position."""
        for k in range(start, len(sequence)):
            if nucleotide.is_gap(sequence[k]):
                return k
        return -1

    def find_nongap(start: int) -> int:
        """Find the minimal nongap character index after the specified position."""
        for k in range(start, len(sequence)):
            if not nucleotide.is_gap(sequence[k]):
                return k
        return -1

    Acc = Tuple[List[Contig], int]

    def process_nucleotide(acc: Acc) -> Acc:
        """Helper function passed to functools.reduce."""
        contigs, start = acc
        if start == len(sequence):
            return acc
        gap_index = find_gap(start)
        if gap_index == -1:
            # no gaps left
            list.append(contigs, Contig(sequence[start:], 0))
            return (contigs, len(sequence))
        nongap_index = find_nongap(gap_index)
        if nongap_index == -1:
            # the gap spans till the sequence end
            new_contig = Contig(sequence[start:gap_index], len(sequence) - gap_index)
            list.append(contigs, new_contig)
            return (contigs, len(sequence))
        list.append(
            contigs, Contig(sequence[start:gap_index], nongap_index - gap_index)
        )
        return (contigs, nongap_index)

    acc: Acc = ([], 0)
    while acc[1] < len(sequence):
        acc = process_nucleotide(acc)

    return acc[0]


def of_contigs(contigs: List[Contig]) -> str:
    """Merge contigs into a sequence.

    :param contigs: Contigs to be merged.

    :return: Sequence of the merged contigs with gaps.

    """
    return functools.reduce(lambda x, y: x + str(y), contigs, "")
