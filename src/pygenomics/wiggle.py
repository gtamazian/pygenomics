"""Reading and writing wiggle files."""

from enum import Enum, auto
from typing import IO, Iterator, List, NamedTuple, Optional, Tuple, Union

from pygenomics import common, error


class Error(error.Error):
    """Base type for exceptions related to wiggle files."""


class DeclarationErrorType(Enum):
    """Types of invariant violations for wiggle data."""

    EMPTY_CHROMOSOME = auto()
    NONPOSITIVE_SPAN = auto()
    NONPOSITIVE_STEP = auto()
    NONPOSITIVE_START = auto()


class DeclarationError(error.Error):
    """Error for a violation of the wiggle record invariants."""

    error_type: DeclarationErrorType

    def __init__(self, error_type: DeclarationErrorType):
        super().__init__(error_type)
        self.error_type = error_type


class NonPositiveDataLineStart(error.Error):
    """Error for specifying a nonpositive start value."""

    value: int

    def __init__(self, value: int):
        super().__init__(value)
        self.value = value


def extract_value(line: str, key: str, sep: str) -> Optional[str]:
    """Extract the value with the specified key.

    :param line: Line possibly encoding a key-value pair.

    :return: Value that corresponds to the key if `line` is a proper
      key-value pair, `None` otherwise.

    """
    parts = common.split2(line, sep)
    if parts is not None and parts[0] == key:
        return parts[1]
    return None


def extract_int_value(line: str, key: str) -> Optional[int]:
    """Extract an integer value from a declaration line part.

    :param line: Part of the declaration line that encodes the integer value.

    :return: Integer value if `line` properly encodes its key-value pair, `None` otherwise.

    """
    value_str = extract_value(line, key, "=")
    if value_str is not None:
        try:
            return int(value_str)
        except ValueError:
            return None
    return None


class VariableStepDeclaration:
    """Declaration line for the variable step data.

    The class guarantees that its :py:attr:`chrom` field is non-empty
      and its :py:attr:`span` field is a positive integer.

    """

    _chrom: str
    _span: int

    def __init__(self, chrom: str, span: int):
        if not chrom:
            raise DeclarationError(DeclarationErrorType.EMPTY_CHROMOSOME)
        if span <= 0:
            raise DeclarationError(DeclarationErrorType.NONPOSITIVE_SPAN)
        self._chrom = chrom
        self._span = span

    @property
    def chrom(self) -> str:
        """Chromosome of the declaration line."""
        return self._chrom

    @property
    def span(self) -> int:
        """Span of the declaration line."""
        return self._span

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, VariableStepDeclaration):
            return NotImplemented
        return self.chrom == other.chrom and self.span == other.span

    def __repr__(self) -> str:
        return str.format(
            "{:s}(chrom='{:s}', span={:d})",
            self.__class__.__name__,
            self.chrom,
            self.span,
        )

    def __str__(self) -> str:
        return (
            str.format("variableStep chrom={:s} span={:d}", self.chrom, self.span)
            if self.span > 1
            else str.format("variableStep chrom={:s}", self.chrom)
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["VariableStepDeclaration"]:
        """Parse the variable step declaration from line parts.

        :param parts: Parts of a line possibly encoding the variable
          step declaration.

        :return: Parsed variable step declaration if `parts` property
          encodes it, `None` otherwise.

        """
        if len(parts) not in {2, 3}:
            return None

        if parts[0] == "variableStep":
            chrom = extract_value(parts[1], "chrom", "=")
            if chrom is not None:
                span = extract_int_value(parts[2], "span") if len(parts) == 3 else 1
                if span is not None:
                    try:
                        return VariableStepDeclaration(chrom, span)
                    except DeclarationError:
                        return None
        return None


class VariableStepData:
    """Variable step data line.

    The class guarantees that its :py:attr:`start` field is a positive
      integer.

    """

    _start: int
    _value: float

    def __init__(self, start: int, value: float):
        if start <= 0:
            raise NonPositiveDataLineStart(start)
        self._start = start
        self._value = value

    @property
    def start(self) -> int:
        """Start position on a chromosome."""
        return self._start

    @property
    def value(self) -> float:
        """Data line value."""
        return self._value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, VariableStepData):
            return NotImplemented
        return self.start == other.start and self.value == other.value

    def __repr__(self) -> str:
        return str.format(
            "{:s}(start={:d}, value={:g})",
            self.__class__.__name__,
            self.start,
            self.value,
        )

    def __str__(self) -> str:
        return str.format("{:d} {:g}", self.start, self.value)

    @staticmethod
    def of_list(parts: List[str]) -> Optional["VariableStepData"]:
        """Parse the variable step data from its line parts.

        :param parts: Parts of a line possibly encoding the variable step data.

        :return: Parsed variable step data if `parts` properly encodes
          it, `None` otherwise.

        """
        if len(parts) == 2:
            start = common.str_to_int(parts[0])
            data = common.str_to_float(parts[1])
            if start is not None and data is not None:
                try:
                    return VariableStepData(start, data)
                except NonPositiveDataLineStart:
                    return None
        return None


class VariableStepBlock(NamedTuple):
    """Variable step data block."""

    declaration: VariableStepDeclaration
    data: List[VariableStepData]


class FixedStepDeclaration(VariableStepDeclaration):
    """Declaration for the fixed step data.

    The class guarantees that its :py:attr:`chrom` field is non-empty
      and its fields :py:attr:`span`, :py:attr:`start` and
      :py:attr:`step` are positive integers.

    """

    _start: int
    _step: int

    def __init__(self, chrom: str, start: int, step: int, span: int):
        super().__init__(chrom, span)
        if start <= 0:
            raise DeclarationError(DeclarationErrorType.NONPOSITIVE_START)
        if step <= 0:
            raise DeclarationError(DeclarationErrorType.NONPOSITIVE_STEP)
        self._start = start
        self._step = step

    @property
    def start(self) -> int:
        """Start position on the chromosome."""
        return self._start

    @property
    def step(self) -> int:
        """Number of bases between items."""
        return self._step

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, FixedStepDeclaration):
            return NotImplemented
        return (
            self.chrom == other.chrom
            and self.span == other.span
            and self.start == other.start
            and self.step == other.step
        )

    def __repr__(self) -> str:
        return str.format(
            "{:s}(chrom='{:s}', start={:d}, step={:d}, span={:d})",
            self.__class__.__name__,
            self.chrom,
            self.start,
            self.step,
            self.span,
        )

    def __str__(self) -> str:
        return (
            str.format("fixedStep chrom={:s} start={:d}", self.chrom, self.start)
            + (str.format(" step={:d}", self.step) if self.step > 1 else "")
            + (str.format(" span={:d}", self.span) if self.span > 1 else "")
        )

    @staticmethod
    def of_list(parts: List[str]) -> Optional["FixedStepDeclaration"]:
        """Parse the fixed step declaration from line parts.

        :param parts: Parts of a line possibly encoding the fixed step
          declaration.

        :return: Parse fixed step declaration if `parts` properly
          encodes it, `None` otherwise.

        """

        def parse_step_span() -> Optional[Tuple[int, int]]:
            """Parse the step and span values in the line."""
            if len(parts) == 3:
                return (1, 1)
            if len(parts) == 5:
                step = extract_int_value(parts[3], "step")
                span = extract_int_value(parts[4], "span")
                if step is not None and span is not None:
                    return (step, span)
                return None

            assert len(parts) == 4
            step_value: Optional[int]
            span_value: Optional[int]
            if "step" in parts[3]:
                step_value = extract_int_value(parts[3], "step")
                span_value = 1
            else:
                step_value = 1
                span_value = extract_int_value(parts[3], "span")

            if step_value is not None and span_value is not None:
                return (step_value, span_value)
            return None

        if len(parts) not in {3, 4, 5}:
            return None

        if parts[0] == "fixedStep":
            chrom = extract_value(parts[1], "chrom", "=")
            if chrom is not None:
                start = extract_int_value(parts[2], "start")
                if start is not None:
                    step_span = parse_step_span()
                    if step_span is not None:
                        try:
                            return FixedStepDeclaration(
                                chrom, start, step_span[0], step_span[1]
                            )
                        except DeclarationError:
                            return None
        return None


class FixedStepBlock(NamedTuple):
    """Fixed step data block."""

    declaration: FixedStepDeclaration
    data: List[float]


Block = Union[VariableStepBlock, FixedStepBlock]


class ReadingError(Error):
    """Error that occurs while reading wiggle data from a stream."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def read(stream: IO[str]) -> Iterator[Block]:
    """Iterate step blocks from a stream in the wiggle format.

    :note: The function produces a side effect by reading from
      `stream`. It raises :py:class:`ReadingError` if malformed data
      is read.

    :param stream: Stream in the wiggle format.

    :return: Iterator of wiggle blocks.

    """
    try:
        line = next(stream)
    except StopIteration:
        return

    class StepType(Enum):
        """Block step type: fixed or variable."""

        FIXED = auto()
        VARIABLE = auto()

    parts = str.split(str.rstrip(line))

    def is_declaration(parts: List[str]) -> bool:
        """Does parts belong to a declaration line?"""
        return parts[0] in {"fixedStep", "variableStep"}

    Declaration = Union[FixedStepDeclaration, VariableStepDeclaration]

    def parse_declaration(parts: List[str]) -> Tuple[Declaration, StepType]:
        """Try to parse a declaration line."""
        parsed: Optional[Declaration]
        if parts[0] == "fixedStep":
            parsed = FixedStepDeclaration.of_list(parts)
            if parsed is None:
                raise ReadingError(line)
            return (parsed, StepType.FIXED)
        if parts[0] == "variableStep":
            parsed = VariableStepDeclaration.of_list(parts)
            if parsed is None:
                raise ReadingError(line)
            return (parsed, StepType.VARIABLE)
        raise ReadingError(line)

    declaration, current_type = parse_declaration(parts)
    current_fixed: List[float] = []
    current_variable: List[VariableStepData] = []

    def add_data() -> None:
        """Add data record to the block being read."""
        if current_type == StepType.FIXED:
            if len(parts) != 1:
                raise ReadingError(line)
            fixed_data = common.str_to_float(parts[0])
            if fixed_data is None:
                raise ReadingError(line)
            list.append(current_fixed, fixed_data)
        else:
            variable_data = VariableStepData.of_list(parts)
            if variable_data is None:
                raise ReadingError(line)
            list.append(current_variable, variable_data)

    def complete_block() -> Block:
        """Complete a current block by combining its declaration and data."""
        block: Block
        if current_type == StepType.FIXED:
            assert isinstance(declaration, FixedStepDeclaration)
            block = FixedStepBlock(declaration, current_fixed)
        else:
            assert isinstance(declaration, VariableStepDeclaration)
            block = VariableStepBlock(declaration, current_variable)
        return block

    for line in stream:
        parts = str.split(str.rstrip(line))
        if is_declaration(parts):
            yield complete_block()
            current_fixed = []
            current_variable = []
            declaration, current_type = parse_declaration(parts)
        else:
            add_data()

    yield complete_block()


def write(stream: IO[str], block: Block) -> None:
    """Write a step block to a stream in the wiggle format.

    :note: The function produces a side effect by writing to `stream`.

    :param stream: Output stream.

    :param block: Block to write to `stream`.

    """
    print(block.declaration, file=stream)
    for k in block.data:
        print(k, file=stream)
