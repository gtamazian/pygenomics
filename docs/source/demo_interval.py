from pygenomics import bed
from pygenomics.interval import GenomicBase, GenomicInterval


def bed_to_interval(record: bed.Record) -> GenomicInterval:
    """Convert a BED record to a genomic interval."""
    return (record.chrom, record.start, record.end - 1)


def interval_to_bed(interval: GenomicInterval) -> bed.Bed3:
    """Convert a genomic interval to a BED3 record."""
    return bed.Bed3(interval[0], bed.Range(interval[1], interval[2] + 1))


def create_collection(path: str) -> GenomicBase:
    """Create a collection of genomic intervals from a BED file."""
    with open(path, encoding="utf-8") as bed_file:
        return GenomicBase(
            bed_to_interval(record)
            for line in bed_file
            if (record := bed.Record.of_string(str.rstrip(line))) is not None
        )


def print_reduced_intervals(path: str) -> None:
    """Print non-overlapping intervals from a BED file."""
    for k in GenomicBase.iterate_reduced(create_collection(path)):
        print(interval_to_bed(k))
