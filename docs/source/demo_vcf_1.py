import gzip
from typing import Iterator, List

from pygenomics.vcf.data.record import Record as VcfRecord
from pygenomics.vcf.meta.metaheader import MetaHeader as VcfMetaHeader
from pygenomics.vcf.reader import Reader as VcfReader


def process_meta_header(header: VcfMetaHeader) -> VcfMetaHeader:
    """Process meta-information lines."""
    return header


def process_samples(samples: List[str]) -> List[str]:
    """Process sample names from the VCF file header."""
    return samples


def process_vcf_file(path: str) -> Iterator[VcfRecord]:
    """Template for a function that processes a VCF file."""
    with gzip.open(path, "rt") as vcf_file:
        vcf_reader = VcfReader(vcf_file)
        process_meta_header(vcf_reader.meta)
        process_samples(vcf_reader.samples)
        for record in vcf_reader.data:
            yield record
