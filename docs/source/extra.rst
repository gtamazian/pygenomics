Extra repositories
==================

*Pygenomics* is accompanied with three additional repositories:

1. `pygenomics-ext <https://gitlab.com/gtamazian/pygenomics-ext/>`_ is
   a Python package that provides extra routines for working with
   software-specific or unstandardized data formats.

2. `pygenomics-examples
   <https://gitlab.com/gtamazian/pygenomics-examples/>`_ is a Python
   package containing scripts for processing bioinformatic data. Each
   script is implemented as a stand-alone program with a command-line
   interface.

3. `pygenomics-papers
   <https://gitlab.com/gtamazian/pygenomics-paper/>`_ presents
   Snakemake pipelines that demonstrate using *pygenomics* for
   bioinformatic data analysis. Some of the pipelines implement
   performance comparison of *pygenomics* and other Python
   bioinformatic packages.

Packages *pygenomics-ext* and *pygenomics-examples* can be installed
using *pip*. *Pygenomics-papers* does not require an installation but
has its own dependencies that include `Snakemake
<snakemake.readthedocs.io/>`_ and `R
<https://www.r-project.org/>`_. Pipelines from *pygenomics-papers* can
be launched separately from each other.
