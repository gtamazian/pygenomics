Command-line interface
======================

*Pygenomics* provides a command-line interface (CLI) to routines for
manipulating genomic intervals and processing sequences from FASTA
files. After the package installation, the CLI can be accessed via the
``pygenomics`` command or by passing the module option to the Python
interpreter::

  python3 -m pygenomics

The second method allows to specify a Python interpreter in order to
select the Python version (e.g., ``python3.9``) or the Python
interpreter kind (e.g., ``pypy3``). The CLI can be also invoked from
the package source distribution without having *pygenomics*
installed::

  git clone https://gitlab.com/gtamazian/pygenomics.git
  cd pygenomics/src
  python3 -m pygenomics

The CLI commands are organized into two groups: ``fasta`` and
``interval``. The commands are listed in the table below.

.. list-table:: *Pygenomics* CLI commands
   :widths: 20 20 60
   :header-rows: 1

   * - Group
     - Command
     - Description
   * - ``fasta``
     - ``oneseq``
     - Extract a specified sequence
   * -
     - ``revcomp``
     - Get reverse complements of nucleotide sequences
   * -
     - ``size``
     - Print names and lengths of sequences
   * -
     - ``unmask``
     - Remove soft masking from sequences
   * - ``interval``
     - ``complement``
     - Get complement genomic intervals
   * -
     - ``find``
     - Find overlapping genomic intervals
   * -
     - ``find_all``
     - Find all overlaps between two interval sets
   * -
     - ``intersect``
     - Intersect genomic intervals from two or more files
   * -
     - ``merge``
     - Merge genomic intervals from one or more files
   * -
     - ``subtract``
     - Subtract genomic intervals
