pygenomics.gff package
======================

.. automodule:: pygenomics.gff
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

pygenomics.gff.base module
--------------------------

.. automodule:: pygenomics.gff.base
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.gff.gff3 module
--------------------------

.. automodule:: pygenomics.gff.gff3
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.gff.gtf module
-------------------------

.. automodule:: pygenomics.gff.gtf
   :members:
   :undoc-members:
   :show-inheritance:
