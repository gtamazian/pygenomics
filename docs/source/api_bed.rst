Browser Extensible Data format
==============================

`The Browser Extensible Data (BED) format
<https://www.genome.ucsc.edu/FAQ/FAQformat.html#format1>`_ defines a
plain-text file with tab-separated values. Each line in the BED format
describes one genomic interval. The number of BED columns ranges from
3 to 12. There are five variants of the format described in the table
below.

.. list-table:: Variants and columns of the BED format
   :header-rows: 1
   :widths: 20 5 75

   * - Format variant
     - #
     - Column description
   * -
     - 1
     - the interval sequence name
   * -
     - 2
     - the interval start position
   * - **BED3**
     - 3
     - the interval end position
   * - **BED4**
     - 4
     - a name
   * -
     - 5
     - a score
   * - **BED6**
     - 6
     - a strand
   * -
     - 7
     - the thick interval start position
   * -
     - 8
     - the thick interval end position
   * - **BED9**
     - 9
     - an extra value
   * -
     - 10
     - a block count
   * -
     - 11
     - the block sizes in base pairs
   * - **BED12**
     - 12
     - the block start positions

The numeric suffix in the format variant name gives the number of
columns in the file. BED3 is the minimal variant of the format: its
lines contain sequence names and start and end coordinates of genomic
intervals. `The complete specification of the BED format
<https://samtools.github.io/hts-specs/BEDv1.pdf>`_ is available on
`the samtools web site <https://samtools.github.io/hts-specs/>`_.

*Pygenomics* provides routines for working with the BED format in
module :py:mod:`pygenomics.bed`. Class
:py:class:`pygenomics.bed.Record` represents a BED record that belongs
to one of the five variants listed above. An example of iterating
records from a BED file is given below::

  from typing import Iterator

  from pygenomics import bed

  def iterate_records(path: str) -> Iterator[bed.Record]:
    with open(path, encoding="utf-8") as bed_file:
	for line in bed_file:
	    record = bed.Record.of_string(line)
	    if record is not None:
		yield record

To create a BED record, one should create an object of one of the
following classes:

* :py:class:`pygenomics.bed.Bed3`
* :py:class:`pygenomics.bed.Bed4`
* :py:class:`pygenomics.bed.Bed6`
* :py:class:`pygenomics.bed.Bed9`
* :py:class:`pygenomics.bed.Bed12`.

Intervals of BED records are specified by class
:py:class:`pygenomics.bed.Range`. The class constructor implements
constraints for a zero-based half-open interval. An attempt to create
an incorrect BED interval will raise an exception of class
:py:exc:`pygenomics.bed.BedError`. Class :py:class:`bed.Strand`
specifies strand values for the BED format. An example of creating a
BED6 record is given below::

  from pygenomics import bed

  bed.Bed6("chr1", bed.Range(0, 10), "record", 0, bed.Strand.PLUS)

To print or write a BED record to a file, one may apply the
:py:func:`print` function directly to a BED record::

  from pygenomics import bed

  with open("test.bed", "w", encoding="utf-8") as bed_file:
      print(bed.Bed3("chr", bed.Range(0, 10)), file=bed_file)
