pygenomics.sam.header package
=============================

.. automodule:: pygenomics.sam.header
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

pygenomics.sam.header.coline module
-----------------------------------

.. automodule:: pygenomics.sam.header.coline
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.common module
-----------------------------------

.. automodule:: pygenomics.sam.header.common
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.error module
----------------------------------

.. automodule:: pygenomics.sam.header.error
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.hdline module
-----------------------------------

.. automodule:: pygenomics.sam.header.hdline
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.pgline module
-----------------------------------

.. automodule:: pygenomics.sam.header.pgline
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.rgline module
-----------------------------------

.. automodule:: pygenomics.sam.header.rgline
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.header.sqline module
-----------------------------------

.. automodule:: pygenomics.sam.header.sqline
   :members:
   :undoc-members:
   :show-inheritance:
