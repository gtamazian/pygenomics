FASTQ format
============

`FASTQ <https://doi.org/10.1093/nar/gkp1137>`_ is a plain-text format
for storing named nucleotide sequences with quality scores associated
with each nucleotide base. The quality scores represent error
probabilities encoded as ASCII characters according to `the Phred
format
<https://academic.oup.com/nar/article/38/6/1767/3112533#82650890>`_. The
FASTQ format is usually used for keeping sequenced reads.

*Pygenomics* implements routines for reading, writing, and
manipulating FASTQ records in module
:py:mod:`pygenomics.fastq`. Functions :py:func:`pygenomics.fastq.read`
and :py:func:`pygenomics.fastq.write` are used for reading and writing
FASTQ records similar to the functions for :ref:`the FASTA format
<FASTA format>`. FASTQ files are usually gzip-compressed and one can
combine *pygenomics* routines with `the gzip module
<https://docs.python.org/3/library/gzip.html>`_ from the standard
Python library, for example::

  import gzip
  from typing import Iterator

  from pygenomics import fastq


  def iterate_reads(path: str) -> Iterator[fastq.Record]:
      """Iterate reads from a gzip-compressed FASTQ file."""
      with gzip.open(path, "rt") as fastq_file:
          for record in fastq.read(fastq_file):
	      yield record

FASTQ records can be created directly as instances of class
:py:class:`pygenomics.fastq.Record`::

  fastq.Record("Read", "ACGT", "1>>A")

Module :py:mod:`pygenomics.phred` provides routines for transforming
between Phred-encoded quality scores and error probabilities.
