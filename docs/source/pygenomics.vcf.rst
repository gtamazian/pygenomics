pygenomics.vcf package
======================

.. automodule:: pygenomics.vcf
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pygenomics.vcf.data
   pygenomics.vcf.meta

Submodules
----------

pygenomics.vcf.characters module
--------------------------------

.. automodule:: pygenomics.vcf.characters
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.datatypes module
-------------------------------

.. automodule:: pygenomics.vcf.datatypes
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.error module
---------------------------

.. automodule:: pygenomics.vcf.error
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.header module
----------------------------

.. automodule:: pygenomics.vcf.header
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.reader module
----------------------------

.. automodule:: pygenomics.vcf.reader
   :members:
   :undoc-members:
   :show-inheritance:
