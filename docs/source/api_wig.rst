Wiggle format
=============

`The Wiggle format (WIG)
<https://genome.ucsc.edu/goldenPath/help/wiggle.html>`_ is a
plain-text format that describes quantitative data in genomic
intervals. Unlike :ref:`the bedGraph format <bedGraph format>`, the
Wiggle format provides two variants of data formatting: the *variable
step* variant for irregular intervals and the *fixed step* variant for
regular intervals. A Wiggle file can contain blocks of both variants.

*Pygenomics* provides routines for processing Wiggle files in module
:py:mod:`pygenomics.wiggle`. Reading and writing files are implemented
by functions :py:func:`pygenomics.wiggle.read` and
:py:func:`pygenomics.wiggle.write`. Classes
:py:class:`pygenomics.wiggle.FixedStepBlock` and
:py:class:`pygenomics.wiggle.VariableStepBlock` represent Wiggle
blocks for fixed and variable step variants, respectively. An example
of reading a Wiggle file is given below.

.. include:: demo_wig.py
   :literal:
