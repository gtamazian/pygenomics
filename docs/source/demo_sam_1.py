from typing import Iterator

from pygenomics.sam.alignment import Record as SamRecord
from pygenomics.sam.header import Header as SamHeader
from pygenomics.sam.reader import Reader as SamReader


def process_header(header: SamHeader) -> SamHeader:
    """Process a SAM header."""
    return header


def process_sam_file(path: str) -> Iterator[SamRecord]:
    """Template for a function that processes a SAM file."""
    with open(path, encoding="utf-8") as sam_file:
        sam_reader = SamReader(sam_file)
        process_header(sam_reader.header)
        for alignment in sam_reader.alignments:
            yield alignment
