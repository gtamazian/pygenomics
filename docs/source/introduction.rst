Introduction
============

Installation
------------

*Pygenomics* can be installed directly from GitLab using ``pip``::

  pip install git+https://gitlab.com/gtamazian/pygenomics.git

A user may install *pygenomics* to `a virtual environment
<https://docs.python.org/3/tutorial/venv.html>`_ in order to isolate
the installation, avoid version conflicts, and use a specific Python
interpreter. Assuming that `PyPy <https://www.pypy.org/>`_ is
installed, the following code can be used to create the environment
and install the package into it::

  pypy3 -m venv pygenomics_env
  . pygenomics_env/bin/activate
  pip install git+https://gitlab.com/gtamazian/pygenomics.git

`The virtualenvwrapper package
<https://virtualenvwrapper.readthedocs.io>`_ can be used for
convenient creation and management of virtual environments.

*Pygenomics* can be also installed in `editable mode
<https://pip.pypa.io/en/stable/topics/local-project-installs/>`_ by
cloning the repository and launching ``pip`` from the cloned
repository directory::

  git clone https://gitlab.com/gtamazian/pygenomics.git
  cd pygenomics
  pip install -e .

More information about installing *pygenomics* for the package
development is given in :ref:`Developer's guide`.

Features
--------

*Pygenomics* is focused on reading, writing, and manipulating genomic
data from bioinformatic data files (see :ref:`Data formats`). Since
most of the bioinformatic data is based on genomic intervals,
*pygenomics* also implements operations with the intervals (see
:ref:`Interval operations`). The supported data formats include
:ref:`BED <Browser Extensible Data format>`, :ref:`GFF3 <General
Feature Format version 3>`, :ref:`SAM <Sequence Alignment/Map
format>`, and :ref:`VCF <Variant Call Format>`.

*Pygenomics* provides both :ref:`the application programming interface
(API) <API>` and :ref:`the command-line interface <Command-line
interface>`. Every routine in *pygenomics* has its type annotated
according to `the Python typing system
<https://typing.readthedocs.io>`_ and the package includes a unit
testing framework (see :ref:`Developer's guide`). :ref:`The complete
API documentation <Complete API documentation>`, that was
automatically generated from the package source code is also
available.
