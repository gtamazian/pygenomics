General Transfer Format
=======================

The General Transfer Format (GTF) is a plain-text format similar to
:ref:`the GFF3 format <General Feature Format version 3>`. Two format
specifications are available:

- `the Ensembl specification
  <ensembl.org/info/website/upload/gff.html>`_
- `GTF2.2 by the Brent Lab <http://mblab.wustl.edu/GTF22.html>`_.

*Pygenomics* implements routines for processing GTF files in module
:py:mod:`pygenomics.gff.gtf` in the way similar to :ref:`the GFF3
format <General Feature Format version 3>`. An example of reading
records from a GTF file is given below.

.. include:: gtf_demo.py
   :literal:
