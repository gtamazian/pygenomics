from pygenomics import strand
from pygenomics.gff import base, gff3

record = gff3.Record(
    base.Record(
        "chr1", "my_program", "gene", base.Range(1, 10), None, strand.Strand.PLUS, None
    ),
    gff3.Attributes([("ID", "gene_1")]),
)
