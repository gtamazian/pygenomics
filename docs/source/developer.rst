Developer's guide
=================

Design principles
-----------------

*Pygenomics* has been developed in accordance with the functional
programming paradigm, including the following principles:

* `immutability
  <https://cacm.acm.org/magazines/2016/1/195722-immutability-changes-everything/abstract>`_
* absence of side effects
* higher-order functions.

Every routine in the source code of *pygenomics* is provided with
`type hits <https://docs.python.org/3/library/typing.html>`_, thus
allowing for static type checking of the package source code.

Adherence to the function programming paradigm facilitates developing
tests for *pygenomics* using the property-based testing approach
implemented in `Hypothesis <https://hypothesis.readthedocs.io>`_.

Functions related to input and output (I/O) inevitably produce side
effects. To encapsulate the related side effects, I/O functions in
*pygenomics* work with streams rather than files. Having streams as
function arguments also enables to read or write from a variety of
sources like compressed files, network sockets, or `in-memory
destinations
<https://docs.python.org/3/library/io.html#io.StringIO>`_.

Common routines of *pygenomics* are arranged into :ref:`a module
hierarchy<Common routines>` for reducing code duplication. Error
classes, that represent exceptions specific to *pygenomics*, are
organized in :ref:`a class hierarchy<Errors and exceptions>` to
provide a developer with a flexible way to catch exceptions on various
levels of *pygenomics*, including catching every *pygenomics*-specific
exception.

Development framework
---------------------

`Poetry <https://python-poetry.org>`_ manages dependencies and
packaging of *pygenomics*, including the package development
framework. *Poetry* creates a separate virtual environment that stores
the development framework tools (e.g., *pytest*) and allows running
them without affecting a system-wide Python installation. Versions of
the development framework tools are stored in file ``poetry.lock``
(see :ref:`Source code organization`), that is `committed to version
control
<https://python-poetry.org/docs/basic-usage#commit-your-poetrylock-file-to-version-control>`_
for better reproducibility of the package testing results.

Source and wheel distribution of *pygenomics* can be created with the
following command::

  poetry build

The distribution files are created in the ``dist/`` subdirectory.

Code style
----------

The source code of *pygenomics* is formatted according to a style
implemented in the `Black <https://github.com/psf/black>`_
tool. Import statements in the source code are organized using the
`isort <https://pycqa.github.io>`_ tool. Extra code style checks are
performed by `Flake8 <https://flake8.pycqa.org>`_, including
cyclomatic complexity control implemented in `radon
<https://radon.readthedocs.io>`_. *Black*, *isort*, and *flake8* with
proper options are automatically launched by *tox* (see :ref:`Running
tests`).

Running tests
-------------

Tests and code style checks are organized using `tox
<https://tox.wiki>`_. We recommend launching ``tox`` by invoking
``poetry`` in the following way::

  poetry run tox

*Tox* automatically creates a separate virtual environment for every
test and installs packages required for the testing. For example, running
``poetry run tox -e mypy`` will create a Python environment, install
`mypy <https://mypy-lang.org/>`_ into it, and launch the static type
checking using the installed instance of `mypy`.

Running *tox* without command-line arguments will launch all
environments one after another. The environments are specified in file
``tox.ini`` (see :ref:`Source code organization`). *Tox* supports
parallel running of environments by passing the ``-p`` option with the
number of parallel threads or ``all`` to use the automatic CPU count.

Generating documentation
------------------------

The package documentation can be generated in multiple formats using
`Sphinx <https://www.sphinx-doc.org>`_. The following commands can be
used to generate documentation in the HTML format::

  cd pygenomics/docs
  make html

The index file ``index.html`` of the generated documentation will be
located in subdirectory ``build/html/``. The list of supported of
formats can be viewed by running ``make`` without command-line
arguments in the ``docs/`` subdirectory.

Source code organization
------------------------

*Pygenomics* uses `the "src layout"
<https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/>`_:
the package source code is located in the ``src/`` subdirectory. Tests
and files for generating documentation are located in subdirectories
``tests/`` and ``docs/``, respectively. The root directory of the
project contains the following files:

* ``CHANGELOG.md``
* ``COPYING``
* ``poetry.lock``
* ``pyproject.toml``
* ``README.md``
* ``tox.ini``

Files ``pyproject.toml`` and ``tox.ini`` keep configuration options
for running tools from :ref:`the package development
framework<Development framework>`. The ``CHANGELOG.md`` file, the
``tox.ini`` file, and the ``tests/`` subdirectory are included in `the
source distribution
<https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist>`_
of the package produced by *Poetry*.

Continuous integration
----------------------

:ref:`The design principles<Design principles>` and :ref:`the package
development framework<Development framework>` facilitate incorporating
*pygenomics* into a continuous integration (CI) system. The example of
such integration is presented in `the main repository
<https://gitlab.com/gtamazian/pygenomics/>`_ of *pygenomics* on GitLab
and uses `the GitLab CI tool <https://docs.gitlab.com/ee/ci/>`_. The
configuration of the GitLab CI pipeline for *pygenomics* is kept in
file ``.gitlab-ci.yml``, that is located in the root directory of the
*pygenomics* repository. The pipeline consists of four stages: source
code style checks, static analysis of the source code, running
unit tests, and generating online documentation for publishing on
`GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_.
