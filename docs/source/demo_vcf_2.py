from pygenomics.vcf.data import alt, record

chrom = record.Chrom("chr1", False)
pos = record.Pos(10)
id_field = record.IdField(["variant1"])
ref = record.RefField("A")
alt_alleles = [alt.Field(alt.FieldType.BASE_PAIRS, "T")]
qual = record.Qual(None)
filter_field = record.FilterField([])
info = record.InfoField([("TYPE", ["snp"])])

snp_record = record.Record(
    chrom, pos, id_field, ref, alt_alleles, qual, filter_field, info, ""
)
