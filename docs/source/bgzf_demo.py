import os
import random
from typing import List, Tuple

from pygenomics import bed, bgzf

Sequences = List[Tuple[str, int]]


def create_sequences(num_sequences: int, max_len: int) -> Sequences:
    """Create a series of numbered sequences with random lengths."""
    return [
        (str.format("seq_{:d}", k + 1), random.randint(1, max_len))
        for k in range(num_sequences)
    ]


def create_random_bed_record(sequences: Sequences) -> bed.Bed3:
    """Create a random BED3 record."""
    seq_name, seq_size = random.choice(sequences)
    start_pos = random.randint(0, seq_size - 1)
    length = random.randint(1, seq_size - start_pos)
    return bed.Bed3(seq_name, bed.Range(start_pos, start_pos + length))


def produce_file(sequences: Sequences, num_records: int, out_path: str) -> None:
    """Produce a random BGZF-compressed BED3 file."""
    records = sorted(
        (create_random_bed_record(sequences) for k in range(num_records)),
        key=lambda x: (x.chrom, x.start),
    )
    writer = bgzf.Writer()
    with open(out_path, "wb") as out_file:
        for k in records:
            for block in writer.compress(str.encode(str(k) + os.linesep)):
                out_file.write(block)
        for block in writer.finalize():
            out_file.write(block)


demo.produce_file(demo.create_sequences(10, 10_000), 100, "test.bed.gz")
