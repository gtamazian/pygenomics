Common routines
===============

*Pygenomics* keeps routines that are shared between source code units
in separate modules named ``common``. Module
:py:mod:`pygenomics.common` contains general purpose routines for the
whole package and separate modules are present in subpackages of
*pygenomics*, for example:

- :py:mod:`pygenomics.bam.common`
- :py:mod:`pygenomics.vcf.data.common`
- :py:mod:`pygenomics.vcf.meta.common`.

The ``common`` modules are organized in the hierarchy: modules in the
subpackages may import routines from modules in their parent
packages. Imports in the opposite direction are not used in order to
avoid circular dependencies between the modules.
