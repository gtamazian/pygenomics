Binary SAM format
=================

The Binary SAM (BAM) format represents read alignments in the binary
form similar to :ref:`the SAM format <Sequence Alignment/Map
format>`. BAM files can be further compressed using :ref:`BGZF
<Blocked GNU Zip Format>`. Unlike SAM files, BAM files must contain a
header. `The BAM format specification
<https://samtools.github.io/hts-specs/SAMv1.pdf>`_ is available on
`the samtools website <https://samtools.github.io/hts-specs/>`_.

*Pygenomics* implements processing BAM files in subpackage
:py:mod:`pygenomics.bam`, that contains submodules for manipulating
parts of a BAM file.

Class :py:class:`pygenomics.bam.reader.Reader` implements reading from
a BAM file. Unlike the SAM format reader, the BAM format reader
requires a binary input stream. An example of using the reader is
given below.

.. include:: demo_bam.py
   :literal:

The BAM header can be accessed from the reader object using property
:py:attr:`Reader.header <pygenomics.bam.reader.Reader.header>`. The
header object belongs to class
:py:class:`pygenomics.bam.header.Header` and consists of the two
following fields:

- :py:attr:`Header.text <pygenomics.bam.header.Header>` - the header
  lines in :ref:`the SAM format <Sequence Alignment/Map format>`
- :py:attr:`Header.ref_sequences
  <pygenomics.bam.header.Header.ref_sequences>` - the list of
  reference sequence names and lengths in base pairs.

A BAM alignment is represented by class
:py:class:`pygenomics.bam.alignment.Alignment`.
