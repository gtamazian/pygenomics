Nucleotide sequences
====================

*Pygenomics* keeps nucleotide sequences as regular Python strings and
provides routines for manipulating them in the following modules:

- :py:mod:`pygenomics.contig`
- :py:mod:`pygenomics.iupac`
- :py:mod:`pygenomics.nucleotide`.

Module :py:mod:`pygenomics.contig` presents the contig class
:py:class:`contig.Contig <pygenomics.contig.Contig>`, that combines a
gap-free nucleotide sequence followed by an optional gap of the
specified length. Functions for splitting a sequence into a series of
contigs and merging contigs into a sequence are provided, as shown in
the example below::

  from pygenomics import contig

  sequence = "ACGTNACGT"

  assert len(contig.to_contig(sequence)) == 2
  assert contig.of_contigs(contig.to_contigs(sequence)) == sequence

Module :py:mod:`pygenomics.iupac` provides class
:py:class:`iupac.Symbol <pygenomics.iupac.Symbol>`, that represents
`ambiguous nucleotide sequence bases
<https://pubmed.ncbi.nlm.nih.gov/2582368/>`_ and their conversion to a
series of regular bases::

  from pygenomics import iupac

  assert len(iupac.Symbol.to_nucleotides(iupac.Symbol.D)) == 3

Module :py:mod:`pygenomics.nucleotide` contains functions for
recognizing specific nucleotide base symbols and converting nucleotide
sequences, as illustrated by the following example::

  from pygenomics import nucleotide

  assert nucleotide.is_ambiguous("N")
  assert nucleotide.is_gap("N")
  assert nucleotide.is_gap_sequence("NNNnnn")
  assert nucleotide.rev_comp("ACCGG") == "CCGGT"
