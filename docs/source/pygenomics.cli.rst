pygenomics.cli package
======================

.. automodule:: pygenomics.cli
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pygenomics.cli.fasta
   pygenomics.cli.interval
