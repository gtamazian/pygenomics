Blocked GNU Zip Format
======================

The Blocked GNU Zip Format (BGZF) is based on `the gzip format
<https://doi.org/10.17487/RFC1952>`_ and allows to index the
compressed files. The BGZF compression is implemented by `the bgzip
tool <https://www.htslib.org/doc/bgzip.html>`_. The BGZF specification
is included in `the SAM format specification
<https://samtools.github.io/hts-specs/SAMv1.pdf>`_ available on `the
samtools web site <https://samtools.github.io/hts-specs/>`_.

Since the BGZF format is compatible with the gzip format, BGZF files
can be read using `the gzip module
<https://docs.python.org/3/library/gzip.html>`_ from the Python
standard library. *Pygenomics* provides routines for writing BGZF
files in module :py:mod:`pygenomics.bgzf`. An example of producing a
BGZF-compressed BED file is given below.

.. include:: bgzf_demo.py
   :literal:

The code produces a sorted and BGZF-compressed BED file that can be
indexed with `the tabix tool
<https://www.htslib.org/doc/tabix.html>`_.
