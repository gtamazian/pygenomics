import gzip
from typing import Iterator

from pygenomics.bam.alignment import Alignment
from pygenomics.bam.header import Header as BamHeader
from pygenomics.bam.reader import Reader as BamReader


def process_header(header: BamHeader) -> BamHeader:
    """Process a BAM header."""
    return header


def process_bam_file(path: str) -> Iterator[Alignment]:
    """Template for a function that processes a BAM file."""
    with gzip.open(path, "rb") as bam_file:
        bam_reader = BamReader(bam_file)
        process_header(bam_reader.header)
        for alignment in bam_reader.alignments:
            yield alignment
