Errors and exceptions
=====================

*Pygenomics* uses exceptions to indicate violations of class
invariants when attempting to create a class instance. For example,
the following code will raise an exception of class
:py:class:`bed.BedError <pygenomics.bed.BedError>`::

      from pygenomics import bed

      bed.Range(-1, 1)

The *pygenomics* functions that might fail usually return an optional
type value (that is, the value which might be ``None``). Examples of
such functions are static class methods that create the class
instances from lines:

- :py:func:`pygenomics.bed.Record.of_string`
- :py:func:`pygenomics.gff3.Record.of_string`
- :py:func:`pygenomics.gtf.Record.of_string`
- :py:func:`pygenomics.sam.alignment.Record.of_string`
- :py:func:`pygenomics.vcf.data.record.Record.of_string`.

Names of the exception classes end with ``Error`` and they are
organized into a class hierarchy inheriting from class
:py:class:`pygenomics.error.Error`. A part of the error class
hierarchy is shown below.

.. inheritance-diagram:: pygenomics.bam.datafield.Error
			 pygenomics.bam.error.Error
			 pygenomics.bam.header.Error
			 pygenomics.error.Error
			 pygenomics.sam.error.Error
			 pygenomics.sam.header.error.Error
			 pygenomics.sam.rname.Error
			 pygenomics.vcf.data.error.Error
			 pygenomics.vcf.error.Error
			 pygenomics.vcf.meta.error.Error
			 pygenomics.wiggle.Error

Having a single ancestor class for the *pygenomics* exceptions allows
to catch any exception that may raised by the package routines::

  import pygenomics.bed
  import pygenomics.error

  try:
      pygenomics.bed.Range(-1, 1)
  except pygenomics.error.Error:
      print("pygenomics error caught")
