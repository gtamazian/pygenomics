pygenomics.vcf.meta package
===========================

.. automodule:: pygenomics.vcf.meta
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

pygenomics.vcf.meta.alt module
------------------------------

.. automodule:: pygenomics.vcf.meta.alt
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.common module
---------------------------------

.. automodule:: pygenomics.vcf.meta.common
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.contig module
---------------------------------

.. automodule:: pygenomics.vcf.meta.contig
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.error module
--------------------------------

.. automodule:: pygenomics.vcf.meta.error
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.filterline module
-------------------------------------

.. automodule:: pygenomics.vcf.meta.filterline
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.infoformat module
-------------------------------------

.. automodule:: pygenomics.vcf.meta.infoformat
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.metaheader module
-------------------------------------

.. automodule:: pygenomics.vcf.meta.metaheader
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.pedigree module
-----------------------------------

.. automodule:: pygenomics.vcf.meta.pedigree
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.regular module
----------------------------------

.. automodule:: pygenomics.vcf.meta.regular
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.sample module
---------------------------------

.. automodule:: pygenomics.vcf.meta.sample
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.structured module
-------------------------------------

.. automodule:: pygenomics.vcf.meta.structured
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.vcf.meta.valuenumber module
--------------------------------------

.. automodule:: pygenomics.vcf.meta.valuenumber
   :members:
   :undoc-members:
   :show-inheritance:
