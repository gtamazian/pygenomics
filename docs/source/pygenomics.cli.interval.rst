pygenomics.cli.interval package
===============================

.. automodule:: pygenomics.cli.interval
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

pygenomics.cli.interval.common module
-------------------------------------

.. automodule:: pygenomics.cli.interval.common
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_complement module
---------------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_complement
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_find module
---------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_find
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_find\_all module
--------------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_find_all
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_intersect module
--------------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_intersect
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_merge module
----------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_merge
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.cli.interval.interval\_subtract module
-------------------------------------------------

.. automodule:: pygenomics.cli.interval.interval_subtract
   :members:
   :undoc-members:
   :show-inheritance:
