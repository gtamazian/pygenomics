from typing import Iterator

from pygenomics.gff import gff3


def iterate_gff3_records(path: str) -> Iterator[gff3.Record]:
    """Iterate GFF3 records from a file."""
    with open(path, encoding="utf-8") as gff3_file:
        for line in gff3_file:
            record = gff3.Record.of_string(str.rstrip(line))
            if record is not None:
                yield record
