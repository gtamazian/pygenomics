Interval operations
===================

*Pygenomics* implements interval operations in class
:py:class:`pygenomics.interval.GenomicBase`, which represents a
collection of intervals organized into `a red-black tree
<https://en.wikipedia.org/wiki/Red-black_tree>`_. The class objects
are immutable and created from a series of intervals. Operations
supported by the class are listed in the table below.

.. list-table:: Interval operations supported by class :py:class:`GenomicBase <pygenomics.interval.GenomicBase>`.
   :header-rows: 1
   :widths: 30 70

   * - Method
     - Description
   * - :py:func:`find <pygenomics.interval.GenomicBase.find>`
     - Check if an interval is in the collection
   * - :py:func:`find_all <pygenomics.interval.GenomicBase.find_all>`
     - Find all overlaps for an interval in the collection
   * - :py:func:`intersect <pygenomics.interval.GenomicBase.intersect>`
     - Intersect an interval with the collection
   * - :py:func:`iterate_complement <pygenomics.interval.GenomicBase.iterate_complement>`
     - Iterate complement intervals from the collection
   * - :py:func:`iterate_reduced <pygenomics.interval.GenomicBase.iterate_reduced>`
     - Iterate non-overlapping intervals from the collection
   * - :py:func:`subtract <pygenomics.interval.GenomicBase.subtract>`
     - Subtract the collection intervals from an interval

An example of using the interval collection class for printing merged
intervals from a BED file is given below. The example uses `the walrus
operator
<https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions>`_,
that was introduced in Python 3.8.

.. include:: demo_interval.py
   :literal:
