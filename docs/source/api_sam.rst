Sequence Alignment/Map format
=============================

The Sequence Alignment/Map (SAM) format is a plain-text format that
describes reads aligned to a reference genome sequence. A SAM file
consists of an optional header followed by read alignment
records. Each header or read alignment record occupies a line. The SAM
file may also contain unmapped reads, that is, reads without a
position in the reference genome sequence. `The SAM format
specification <https://samtools.github.io/hts-specs/SAMv1.pdf>`_ is
available on `the samtools web site
<https://samtools.github.io/hts-specs/>`_.

*Pygenomics* provides routines for working with SAM files in
subpackage :py:mod:`pygenomics.sam`. The subpackage contains
submodules for working with parts of a SAM file.

Class :py:class:`pygenomics.sam.reader.Reader` implements reading from
a SAM file. An example of reading alignments from a SAM file is given
below.

.. include:: demo_sam_1.py
   :literal:

Header lines can be accessed from the reader object using property
:py:class:`Reader.header <pygenomics.sam.reader.Reader.header>`. The
header object belongs to class
:py:class:`pygenomics.sam.header.Header`, which contents are listed in
the table below.

.. list-table:: Parts of a SAM header object.
   :header-rows: 1
   :widths: 15 35 50

   * - Field tag
     - Class
     - Description
   * - @HD
     - :py:class:`sam.header.hdline.HDLine <pygenomics.sam.header.hdline.HDLine>`
     - File-level metadata
   * - @SQ
     - :py:class:`sam.header.sqline.SQLine <pygenomics.sam.header.sqline.SQLine>`
     - Reference sequence dictionary
   * - @RG
     - :py:class:`sam.header.rgline.RGLine <pygenomics.sam.header.rgline.RGLine>`
     - Read group
   * - @PG
     - :py:class:`sam.header.pgline.PGLine <pygenomics.sam.header.pgline.PGLine>`
     - Program record
   * - @CO
     - :py:class:`sam.header.coline.COLine <pygenomics.sam.header.coline.COLine>`
     - Text comment

An alignment record is represented by class
:py:class:`pygenomics.sam.alignment.Record`. Classes that implement
contents of the record are listed in the table below.

.. list-table:: Parts of a SAM alignment object.
   :header-rows: 1
   :widths: 15 35 50

   * - Field
     - Class
     - Description
   * - QNAME
     - :py:class:`sam.alignment.QName <pygenomics.sam.alignment.QName>`
     - Read (query) name
   * - RNAME
     - :py:class:`sam.alignment.RName <pygenomics.sam.alignment.RName>`
     - Reference sequence name
   * - CIGAR
     - :py:class:`sam.cigar.Operation <pygenomics.sam.cigar.Operation>`
     - Alignment operation
   * - EXTRA
     - :py:class:`sam.datafield.OptionalDataField <pygenomics.sam.datafield.OptionalDataField>`
     - Optional fields

An alignment record can be created by specifying its contents, as
shown below.

.. include:: demo_sam_2.py
   :literal:

One may use the ``print()`` function to write an alignment record to a
file or to print it to standard output::

  print(record)

  with open("alignments.sam", "wt", encoding="utf-8") as sam_file:
      print(record, file=sam_file)
