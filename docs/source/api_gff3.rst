General Feature Format version 3
================================

`The General Feature Format version 3 (GFF3)
<https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md>`_
is a plain-text format for representing genomic features as intervals
in assembled genome sequences. `Its format specification
<https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md>`_
is provided by `the Sequence Ontology project
<https://github.com/The-Sequence-Ontology>`_. A number of
modifications of the GFF3 format also exist (e.g.,
`the NCBI GFF3 format <https://www.ncbi.nlm.nih.gov/datasets/docs/v2/reference-docs/file-formats/about-ncbi-gff3/>`_).

GFF3 differs from :ref:`the GTF format <General Transfer Format>` in
formatting of its last column, that corresponds to attributes of an
annotated genomic feature.

*Pygenomics* provides routines for working with the GFF3 format in
module :py:mod:`pygenomics.gff.gff3`, that is included in subpackage
:py:mod:`pygenomics.gff`. GFF3 records are represented by class
:py:class:`pygenomics.gff.gff3.Record`. Each record corresponds to a
single line in a GFF3 file and can be parsed using function
:py:func:`Record.of_string()
<pygenomics.gff.gff3.Record.of_string>`. An example of reading GFF3
records from a file is given below.

.. include:: gff3_demo_1.py
   :literal:

A GFF3 record can be directly created by combining the base part of
class :py:class:`gff.base.Record <pygenomics.gff.base.Record>` and the
attributes part of class :py:class:`gff.gff3.Attributes
<pygenomics.gff.gff3.Attributes>`:

.. include:: gff3_demo_2.py
   :literal:

GFF3 records can be printed to standard output or written to a file
using the ``print()`` function::

  print(record)  # printing to standard output

  # writing to a file
  with open("output.gff3", "wt", encoding="utf-8") as out_file:
      print(record, file=out_file)
