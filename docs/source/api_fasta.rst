FASTA format
============

`FASTA <https://www.ncbi.nlm.nih.gov/genbank/fastaformat/>`_ is a
plain-text format for storing named nucleotide or amino acid
sequences. A sequence record consists of the header and the actual
sequence. The header is a line that starts with the ``>`` character
and the sequence may be written in a single line or broken into
multiple lines. An example of a FASTA record is shown below::

  >Sequence
  ACGTACGTACGT

*Pygenomics* provides routines for reading and writing FASTA files in
module :py:mod:`pygenomics.fasta`. Both single-line and multiple-line
variants of the FASTA format are supported. *Pygenomics* also allows
FASTA records to have an empty header or an empty sequence, for
example::

  >Sequence
  >
  ACGT

Function :py:func:`pygenomics.fasta.read` implements reading FASTA
records from a plain-text input stream. It can used for reading a
FASTA file in the following way::

  from typing import Iterator

  from pygenomics import fasta

  def iterate_records(path: str) -> Iterator[fasta.Record]:
      """Iterate sequence records from a FASTA file."""
      with open(path, encoding="utf-8") as fasta_file:
          for record in fasta.read(fasta_file):
	      yield record

A user can create a FASTA record by providing its name, comment, and
sequence::

  from pygenomics import fasta

  fasta.Record("Sequence", "comment", "ACGT")

Function :py:func:`pygenomics.fasta.write` is provided for writing a
record in the FASTA format to an output stream. To print a record to
standard output, one may pass ``sys.stdout`` to the function::

  import sys

  from pygenomics import fasta

  fasta.write(sys.stdout, fasta.Record("Sequence", "", "ACGT"))

.. note::

   Function :py:func:`pygenomics.fasta.write` writes the FASTA record
   sequence in a single line. *Pygenomics* also provides function
   :py:func:`pygenomics.fasta.write_wrapped`, that writes the sequence
   in multiple lines of the same width but works slower because it
   adds line breaks to the sequence being written.
