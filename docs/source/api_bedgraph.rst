bedGraph format
===============

`The bedGraph format
<https://www.genome.ucsc.edu/goldenPath/help/bedgraph.html>`_ is a
plain-text format used for mapping continuous values to genome
intervals. The format can be represented as :ref:`the BED3 format
<Browser Extensible Data format>` with an extra column of numeric
values.

*Pygenomics* provides routines for reading and writing bedGraph files
in module :py:mod:`pygenomics.bedgraph`. Class
:py:class:`pygenomics.bedgraph.Record` represents a bedGraph record
and combines the following two values:

* :py:attr:`bedgraph.Record.interval
  <pygenomics.bedgraph.Record.interval>` - the genomic interval
* :py:attr:`bedgraph.Record.value
  <pygenomics.bedgraph.Record.value>` - the numeric value associated
  with the interval.

Reading, writing, and creating bedGraph records is similar to the same
operations for :ref:`BED records <Browser Extensible Data
format>`. For example, the following code creates a bedGraph record::

  from pygenomics import bed, bedgraph

  bedgraph.Record(bed.Bed3("chr1", bed.Range(0, 10)), 0.5)
