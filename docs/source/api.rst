API
===

.. toctree::
   :maxdepth: 1

   Common routines <api_common.rst>
   Errors and exceptions <api_errors.rst>
   Interval operations <api_interval.rst>
   Nucleotide sequences <api_nucleotide.rst>
   Data formats <api_formats.rst>
