pygenomics.sam package
======================

.. automodule:: pygenomics.sam
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pygenomics.sam.header

Submodules
----------

pygenomics.sam.alignment module
-------------------------------

.. automodule:: pygenomics.sam.alignment
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.cigar module
---------------------------

.. automodule:: pygenomics.sam.cigar
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.datafield module
-------------------------------

.. automodule:: pygenomics.sam.datafield
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.error module
---------------------------

.. automodule:: pygenomics.sam.error
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.reader module
----------------------------

.. automodule:: pygenomics.sam.reader
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.sam.rname module
---------------------------

.. automodule:: pygenomics.sam.rname
   :members:
   :undoc-members:
   :show-inheritance:
