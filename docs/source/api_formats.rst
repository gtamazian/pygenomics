Data formats
============

.. toctree::
   :maxdepth: 4

   BAM <api_bam.rst>
   bedGraph <api_bedgraph.rst>
   BED <api_bed.rst>
   BGZF <api_bgzf.rst>
   FASTA <api_fasta.rst>
   FASTQ <api_fastq.rst>
   GFF3 <api_gff3.rst>
   GTF <api_gtf.rst>
   SAM <api_sam.rst>
   VCF <api_vcf.rst>
   WIG <api_wig.rst>
