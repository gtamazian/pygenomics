from typing import Iterator, Union

from pygenomics import wiggle


def iterate_wig_file(
    path: str,
) -> Iterator[Union[wiggle.FixedStepBlock, wiggle.VariableStepBlock]]:
    """Iterate records in a Wiggle file."""
    with open(path, encoding="utf-8") as wiggle_file:
        for k in wiggle.read(wiggle_file):
            yield k
