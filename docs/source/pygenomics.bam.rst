pygenomics.bam package
======================

.. automodule:: pygenomics.bam
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

pygenomics.bam.alignment module
-------------------------------

.. automodule:: pygenomics.bam.alignment
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.bam.common module
----------------------------

.. automodule:: pygenomics.bam.common
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.bam.datafield module
-------------------------------

.. automodule:: pygenomics.bam.datafield
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.bam.error module
---------------------------

.. automodule:: pygenomics.bam.error
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.bam.header module
----------------------------

.. automodule:: pygenomics.bam.header
   :members:
   :undoc-members:
   :show-inheritance:

pygenomics.bam.reader module
----------------------------

.. automodule:: pygenomics.bam.reader
   :members:
   :undoc-members:
   :show-inheritance:
