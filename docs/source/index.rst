pygenomics: manipulating genomic intervals and data files in Python
===================================================================

*Pygenomics* is a Python package for reading and writing data in
commonly used bioinformatic formats (such as BED or GFF3) and
performing operations on genomic intervals. *Pygenomics* is implemented
in pure Python and requires the Python standard library only.

The latest release of the package can be installed with *pip*::

   pip install git+https://gitlab.com/gtamazian/pygenomics.git

.. toctree::
   :maxdepth: 1
   :caption: Contents

   Introduction <introduction.rst>
   API <api.rst>
   Command-line interface <cli.rst>
   Developer's guide <developer.rst>
   Extra repositories <extra.rst>
   Complete API documentation <pygenomics.rst>

Indices
-------

* :ref:`genindex`
* :ref:`modindex`
