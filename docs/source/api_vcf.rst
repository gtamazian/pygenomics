Variant Call Format
===================

The Variant Call Format (VCF) is a plain-text format for describing
genomic variants with respect to a reference genome sequence. `The VCF
format specification
<https://samtools.github.io/hts-specs/VCFv4.3.pdf>`_ in available on
`the samtools web site <https://samtools.github.io/hts-specs/>`_.

A VCF file starts with meta-information lines followed by a single
header line. Data lines, that describe genomic variants, are presented
after the header line.

*Pygenomics* implements routines related to the VCF format in
subpackage :py:mod:`pygenomics.vcf`, that contains its own subpackages
and submodules for processing meta-information and data lines and
their parts. A source code template for reading a gzip-compressed VCF
file is given below.

.. include:: demo_vcf_1.py
   :literal:

Class :py:class:`pygenomics.vcf.reader.Reader` implements reading
meta-information lines, sample names, and data lines from a VCF
file. The meta-information lines and sample names are read as the
:py:class:`Reader <pygenomics.vcf.reader.Reader>` object is created
from a text stream and can be accessed using the following object
properties:

- :py:attr:`Reader.meta <pygenomics.vcf.reader.Reader.meta>`
- :py:attr:`Reader.samples <pygenomics.vcf.reader.Reader.samples>`.

The iterator of variant records parsed from data lines is provided by
property :py:attr:`Reader.data
<pygenomics.vcf.reader.Reader.data>`. It returns parsed records, which
type is :py:class:`vcf.data.record.Record
<pygenomics.vcf.data.record.Record>`.

A variant record object can be also created from scratch. The table
below shows classes for each part of the variant record.

.. list-table:: Parts of a VCF variant record.
   :header-rows: 1
   :widths: 15 35 50

   * - Field
     - Class
     - Description
   * - CHROM
     - :py:class:`vcf.data.record.Chrom <pygenomics.vcf.data.record.Chrom>`
     - Sequence name
   * - POS
     - :py:class:`vcf.data.record.Pos <pygenomics.vcf.data.record.Pos>`
     - Position on the sequence
   * - ID
     - :py:class:`vcf.data.record.IdField <pygenomics.vcf.data.record.IdField>`
     - Identifier
   * - REF
     - :py:class:`vcf.data.record.RefField <pygenomics.vcf.data.record.RefField>`
     - Reference bases
   * - ALT
     - :py:class:`vcf.data.alt.Field <pygenomics.vcf.data.alt.Field>`
     - Alternate bases
   * - QUAL
     - :py:class:`vcf.data.record.Qual <pygenomics.vcf.data.record.Qual>`
     - Quality score
   * - FILTER
     - :py:class:`vcf.data.record.FilterField <pygenomics.vcf.data.record.FilterField>`
     - Filter status
   * - INFO
     - :py:class:`vcf.data.record.InfoField <pygenomics.vcf.data.record.InfoField>`
     - Additional information

Variant genotypes are represented in module
:py:class:`pygenomics.vcf.data.genotype`. An example of creating a
variant record is given below.

.. include:: demo_vcf_2.py
   :literal:

A variant record can be written to a file or printed to standard
output using the ``print()`` function:

.. include:: demo_vcf_3.py
   :literal:
