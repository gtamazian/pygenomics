from typing import Iterator

from pygenomics.gff import gtf


def iterate_gtf_records(path: str) -> Iterator[gtf.Record]:
    """Iterate records from a GTF file."""
    with open(path, encoding="utf-8") as gtf_file:
        for line in gtf_file:
            record = gtf.Record.of_string(str.rstrip(line))
            if record is not None:
                yield record
