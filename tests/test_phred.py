"""Test module pygenomics.phred."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import phred


@ht.given(st.integers(min_value=0, max_value=255))
def test_to_phred_of_phread(score: int) -> None:
    """Test functions to_score() and of_score()."""
    assert phred.to_score(phred.of_score(score)) == score
