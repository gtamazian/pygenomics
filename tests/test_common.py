"""Test module pygenomics.common."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import common


@ht.given(st.text(min_size=1), st.text(min_size=1))
def test_strip_prefix_suffix_empty(left: str, right: str) -> None:
    """Test that strip_prefix_suffix() detects overlapping parts."""
    assert (
        common.strip_prefix_suffix(left + right + left, left + right, right + left)
        is None
    )
