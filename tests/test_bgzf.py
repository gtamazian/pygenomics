"""Test module `pygenomics.bgzf`."""

import gzip
import io
import itertools
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import bgzf


@ht.given(st.lists(st.binary(max_size=2**17)))
def test_writer(data: List[bytes]) -> None:
    """Test the BGZF writer."""
    writer = bgzf.Writer()
    compressed_data = bytes.join(
        b"", (itertools.chain.from_iterable(writer.compress(k) for k in data))
    )
    compressed_data += bytes.join(b"", writer.finalize())

    stream = io.BytesIO(compressed_data)
    assert bytes.join(b"", gzip.open(stream)) == bytes.join(b"", data)
