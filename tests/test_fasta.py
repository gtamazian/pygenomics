"""Test module pygenomics.fasta."""

import io
from typing import List

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics import fasta, nucleotide
from tests import common

RECORD_STRATEGY = st.tuples(
    common.HT_SEQNAME,
    common.HT_SEQNAME,
    st.text(alphabet=nucleotide.LETTERS),
).map(lambda x: fasta.Record(x[0], x[1], x[2]))


@ht.given(st.lists(RECORD_STRATEGY))
def test_read_write(records: List[fasta.Record]) -> None:
    """Test read() and write()."""
    stream = io.StringIO()
    for k in records:
        fasta.write(stream, k)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(records, fasta.read(io.StringIO(contents))))


@ht.given(st.lists(RECORD_STRATEGY), st.integers(min_value=1, max_value=100))
def test_read_write_wrapped(records: List[fasta.Record], width: int) -> None:
    """Test read() and write_wrapped()."""
    stream = io.StringIO()
    for k in records:
        fasta.write_wrapped(stream, k, width)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(records, fasta.read(io.StringIO(contents))))


@ht.given(RECORD_STRATEGY)
def test_read_incorrect_first_line(record: fasta.Record) -> None:
    """Test that read() raised exception IncorrectFirstLine."""
    stream = io.StringIO()
    fasta.write(stream, record)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    with pytest.raises(fasta.IncorrectFirstLine):
        list(fasta.read(io.StringIO(str.strip(contents, ">"))))


def test_read_empty_stream() -> None:
    """Test that read() correctly processes an empty stream."""
    assert not list(fasta.read(io.StringIO()))
