"""Test module pygenomics.contig."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import contig, nucleotide

HT_SEQUENCE = st.text(alphabet=nucleotide.UPPERCASE)


@ht.given(HT_SEQUENCE)
def test_to_contigs(sequence: str) -> None:
    """Test function contig.to_contigs() using sequences with gaps."""
    assert contig.of_contigs(contig.to_contigs(sequence)) == sequence
