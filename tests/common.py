"""Common routines for testing pygenomics modules."""

import string

import hypothesis.strategies as st

from pygenomics import strand

PRINTABLE_NON_WHITESPACE = [
    k for k in string.printable if k not in set(string.whitespace)
]

HT_SEQNAME = st.text(alphabet=PRINTABLE_NON_WHITESPACE, max_size=100)

HT_NONEMPTY_SEQNAME = st.text(
    alphabet=PRINTABLE_NON_WHITESPACE, min_size=1, max_size=100
)

HT_STRANDS = st.sampled_from(
    [strand.Strand.PLUS, strand.Strand.MINUS, strand.Strand.UNKNOWN]
)

_MAX_COVERAGE_VALUE = 100

_COVERAGE_DENOMINATOR = 10_000

HT_COVERAGE_VALUES = st.integers(
    min_value=-_MAX_COVERAGE_VALUE, max_value=_MAX_COVERAGE_VALUE
).map(lambda x: x / _COVERAGE_DENOMINATOR)
