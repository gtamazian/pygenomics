"""Test module `pygenomics.sam.cigar`."""

from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam import cigar

HT_OPERATIONS = st.sampled_from(
    [
        cigar.Operation.ALNMATCH,
        cigar.Operation.INSERTION,
        cigar.Operation.DELETION,
        cigar.Operation.SKIPPED,
        cigar.Operation.SOFTCLIP,
        cigar.Operation.HARDCLIP,
        cigar.Operation.PADDING,
        cigar.Operation.SEQMATCH,
        cigar.Operation.SEQMISMATCH,
    ]
)

HT_CIGAR_STRINGS = st.lists(HT_OPERATIONS)


@ht.given(HT_CIGAR_STRINGS)
def test_parse_sigar_string(operations: List[cigar.Operation]) -> None:
    """Test parse_cigar_string() using correct CIGAR strings."""
    assert cigar.parse_cigar_string(cigar.create_cigar_string(operations)) == operations
