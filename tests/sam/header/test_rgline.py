"""Test module `pygenomics.sam.header.rgline`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam.header import rgline
from tests.sam import test_rname
from tests.sam.header import common

HT_PLATFORMS = st.sampled_from(
    [
        rgline.Platform.CAPILLARY,
        rgline.Platform.DNBSEQ,
        rgline.Platform.HELICOS,
        rgline.Platform.ILLUMINA,
        rgline.Platform.IONTORRENT,
        rgline.Platform.LS454,
        rgline.Platform.ONT,
        rgline.Platform.PACBIO,
        rgline.Platform.SOLID,
    ]
)


@ht.given(HT_PLATFORMS)
def test_platform_of_string(value: rgline.Platform) -> None:
    """Test Platform.of_string() using correct data."""
    assert rgline.Platform.of_string(str(value)) == value


HT_STRINGS = st.one_of(test_rname.HT_RNAME_LINES, st.just(None))

HT_LIBRARY_FIELDS = st.tuples(
    HT_STRINGS, HT_STRINGS, st.integers(), HT_STRINGS, HT_STRINGS, HT_STRINGS
).map(lambda x: rgline.LibraryFields(x[0], x[1], x[2], x[3], x[4], x[5]))

HT_PLATFORM_FIELDS = st.tuples(HT_PLATFORMS, HT_STRINGS, HT_STRINGS).map(
    lambda x: rgline.PlatformFields(x[0], x[1], x[2])
)

HT_GENERAL_FIELDS = st.tuples(HT_STRINGS, HT_STRINGS, st.datetimes(), HT_STRINGS).map(
    lambda x: rgline.GeneralFields(x[0], x[1], x[2], x[3])
)

HT_RGLINE_OPTIONAL_FIELDS = st.tuples(
    HT_GENERAL_FIELDS,
    HT_LIBRARY_FIELDS,
    HT_PLATFORM_FIELDS,
).map(lambda x: rgline.RGLineOptionalFields(x[0], x[1], x[2]))

PREDEFINED_FIELDS = rgline.RGLine.get_predefined_fields()

HT_EXTRA_FIELDS = common.HT_EXTRA_FIELDS.filter(
    lambda x: all(k.field.tag not in PREDEFINED_FIELDS for k in x)
)

HT_RGLINES = st.tuples(
    test_rname.HT_RNAME_LINES, HT_RGLINE_OPTIONAL_FIELDS, HT_EXTRA_FIELDS
).map(lambda x: rgline.RGLine(x[0], x[1], x[2]))


@ht.given(HT_RGLINES)
def test_rgline_of_string(record: rgline.RGLine) -> None:
    """Test RGLine.of_string() using correct data."""
    assert rgline.RGLine.of_string(str(record)) == record
