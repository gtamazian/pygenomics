"""Test module `pygenomics.sam.header.pgline`."""

import hypothesis as ht
import hypothesis.strategies as st

import tests.common
import tests.sam.header.common
from pygenomics.sam.header import pgline

HT_PROGRAM_IDS = tests.common.HT_NONEMPTY_SEQNAME
HT_OPTIONAL_FIELDS = st.one_of(tests.common.HT_NONEMPTY_SEQNAME, st.just(None))

HT_PGLINE_OPTIONAL_FIELDS = st.tuples(
    HT_OPTIONAL_FIELDS,
    HT_OPTIONAL_FIELDS,
    HT_OPTIONAL_FIELDS,
    HT_OPTIONAL_FIELDS,
    HT_OPTIONAL_FIELDS,
).map(lambda x: pgline.PGLineOptionalFields(x[0], x[1], x[2], x[3], x[4]))

HT_EXTRA_FIELDS = tests.sam.header.common.HT_EXTRA_FIELDS.filter(
    lambda x: all(k.field.tag not in pgline.PGLine.get_predefined_fields() for k in x)
)

HT_PGLINES = st.tuples(HT_PROGRAM_IDS, HT_PGLINE_OPTIONAL_FIELDS, HT_EXTRA_FIELDS).map(
    lambda x: pgline.PGLine(x[0], x[1], x[2])
)


@ht.given(HT_PGLINES)
def test_pgline_of_string(record: pgline.PGLine) -> None:
    """Test PGLine.of_string() using correct data."""
    assert pgline.PGLine.of_string(str(record)) == record
