"""Test module `pygenomics.sam.header`."""

import operator
import random
from typing import List, TypeVar

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam.header import Header
from tests.sam.header.test_coline import HT_COLINES
from tests.sam.header.test_hdline import HT_HDLINES
from tests.sam.header.test_pgline import HT_PGLINES
from tests.sam.header.test_rgline import HT_RGLINES
from tests.sam.header.test_sqline import HT_SQLINES

HT_HEADERS = st.tuples(
    st.one_of(HT_HDLINES, st.just(None)),
    st.lists(HT_SQLINES),
    st.lists(HT_RGLINES),
    st.lists(HT_PGLINES),
    st.lists(HT_COLINES),
).map(lambda x: Header(x[0], x[1], x[2], x[3], x[4]))


def sort_header_lines(header: Header) -> Header:
    """Sort @SQ, @RG, @PG, and @CO lines."""
    return Header(
        header.file_data,
        sorted(header.sequences, key=lambda x: x.seq_name.value),
        sorted(header.read_groups, key=operator.attrgetter("read_group_id")),
        sorted(header.programs, key=operator.attrgetter("program_id")),
        sorted(header.comments, key=operator.attrgetter("comment")),
    )


_T = TypeVar("_T")


def shuffle_list(values: List[_T]) -> List[_T]:
    """Shuffle a list.

    The function utilizes side effects by calling `random.shuffle()`.

    """
    random.shuffle(values)
    return values


def get_shuffled_header_lines(header: Header) -> List[str]:
    """Write header lines to the specified stream.

    The function produced side effects by calling :py:func:`shuffle_list`.

    """
    return shuffle_list(
        ([str(header.file_data)] if header.file_data is not None else [])
        + [str(k) for k in header.sequences]
        + [str(k) for k in header.read_groups]
        + [str(k) for k in header.programs]
        + [str(k) for k in header.comments]
    )


@ht.given(HT_HEADERS)
def test_header_of_list(header: Header) -> None:
    """Test Header.of_list()."""
    parsed_header = Header.of_list(get_shuffled_header_lines(header))
    assert parsed_header is not None and sort_header_lines(
        parsed_header
    ) == sort_header_lines(header)
