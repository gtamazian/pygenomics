"""Test module `pygenomics.sam.header.hdline`."""

import hypothesis as ht
import hypothesis.strategies as st

import tests.common
import tests.sam.header.common
from pygenomics.sam.header import hdline

HT_FORMAT_VERSIONS = st.tuples(st.integers(), st.integers()).map(
    lambda x: hdline.FormatVersion(x[0], x[1])
)

HT_SORTING_ORDERS = st.sampled_from(
    [
        hdline.SortingOrder.UNKNOWN,
        hdline.SortingOrder.UNSORTED,
        hdline.SortingOrder.QUERYNAME,
        hdline.SortingOrder.COORDINATE,
    ]
)

HT_ALIGNMENT_GROUPINGS = st.sampled_from(
    [
        hdline.AlignmentGrouping.NONE,
        hdline.AlignmentGrouping.QUERY,
        hdline.AlignmentGrouping.REFERENCE,
    ]
)

HT_SUBSORTING_ORDER = st.text(
    alphabet=tests.common.PRINTABLE_NON_WHITESPACE, min_size=1, max_size=10
)

HT_BOTH_SORTING_ORDERS = st.one_of(
    st.just((None, None)),
    HT_SORTING_ORDERS.map(lambda x: (x, None)),
    st.tuples(HT_SORTING_ORDERS, HT_SUBSORTING_ORDER).map(
        lambda x: (x[0], hdline.SubsortingOrder(x[0], x[1]))
    ),
)

HT_HDLINE_OPTIONAL_FIELDS = st.tuples(
    HT_BOTH_SORTING_ORDERS, HT_ALIGNMENT_GROUPINGS
).map(lambda x: hdline.HDLineOptionalFields(x[0][0], x[1], x[0][1]))

HT_EXTRA_FIELDS = tests.sam.header.common.HT_EXTRA_FIELDS.filter(
    lambda x: all(k.field.tag not in hdline.HDLine.get_predefined_fields() for k in x)
)

HT_HDLINES = st.tuples(
    HT_FORMAT_VERSIONS,
    HT_HDLINE_OPTIONAL_FIELDS,
    HT_EXTRA_FIELDS,
).map(lambda x: hdline.HDLine(x[0], x[1], x[2]))


@ht.given(HT_HDLINES)
def test_hdline_of_string(record: hdline.HDLine) -> None:
    """Test HDLine.of_string() using correct data."""
    assert hdline.HDLine.of_string(str(record)) == record
