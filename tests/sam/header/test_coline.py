"""Test module `pygenomics.sam.header.coline`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam.header import coline

HT_COLINES = st.text().map(coline.COLine)


@ht.given(HT_COLINES)
def test_coline_of_string(record: coline.COLine) -> None:
    """Test COLine.of_string() using correct data."""
    assert coline.COLine.of_string(str(record)) == record
