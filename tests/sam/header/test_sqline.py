"""Test module `pygenomics.sam.header.sqline`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam.header import sqline
from tests.sam import test_rname
from tests.sam.header import common

HT_SEQ_LENGHTS = st.integers(min_value=1, max_value=2**31 - 1).map(
    sqline.SequenceLength
)

HT_ALTENATIVE_LOCUS_REGIONS = st.tuples(
    test_rname.HT_RSEQNAMES.filter(lambda x: ":" not in x.value),
    st.one_of(
        st.integers(min_value=0).flatmap(
            lambda x: st.tuples(st.just(x), st.integers(min_value=x + 1))
        ),
        st.just(None),
    ),
).map(lambda x: sqline.AlternativeLocusRegion(x[0], x[1]))

HT_ALTENATIVE_LOCI = st.one_of(st.just(None), HT_ALTENATIVE_LOCUS_REGIONS).map(
    sqline.AlternativeLocus
)

HT_ALT_REF_SEQ_NAMES = st.lists(test_rname.HT_RSEQNAMES, min_size=1).map(
    sqline.AltRefSeqNames
)

HT_MOLECULAR_TOPOLOGIES = st.sampled_from(
    [sqline.MolecularTopology.LINEAR, sqline.MolecularTopology.CIRCULAR]
)

HT_COMMON_NAMES = st.one_of(test_rname.HT_RNAME_LINES, st.just(None))

HT_SQLINE_OPTIONAL_FIELDS = st.tuples(
    HT_ALTENATIVE_LOCI,
    HT_ALT_REF_SEQ_NAMES,
    HT_COMMON_NAMES,
    HT_COMMON_NAMES,
    HT_COMMON_NAMES,
    HT_COMMON_NAMES,
    HT_MOLECULAR_TOPOLOGIES,
    HT_COMMON_NAMES,
).map(
    lambda x: sqline.SQLineOptionalFields(
        x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]
    )
)

HT_EXTRA_FIELDS = common.HT_EXTRA_FIELDS.filter(
    lambda x: all(k.field.tag not in sqline.SQLine.get_predefined_fields() for k in x)
)

HT_SQLINES = st.tuples(
    test_rname.HT_RSEQNAMES, HT_SEQ_LENGHTS, HT_SQLINE_OPTIONAL_FIELDS, HT_EXTRA_FIELDS
).map(lambda x: sqline.SQLine(x[0], x[1], x[2], x[3]))


@ht.given(HT_SQLINES)
def test_sqline_of_string(record: sqline.SQLine) -> None:
    """Test SQLine.of_string() using correct data."""
    assert sqline.SQLine.of_string(str(record)) == record
