"""Shared routines for testing subpackage `pygenomics.sam.header`."""

import hypothesis.strategies as st

import pygenomics.common
from tests.sam import test_datafield

HT_EXTRA_FIELDS = st.lists(test_datafield.HT_OPTIONAL_DATA_FIELDS, max_size=10).filter(
    lambda x: pygenomics.common.are_unique(k.field.tag for k in x)
)
