"""Test module `pygenomics.sam.datafield`."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam import datafield
from tests import common

HT_TAGS = st.text(
    alphabet=[k for k in common.PRINTABLE_NON_WHITESPACE if k != ":"],
    min_size=2,
    max_size=2,
)

HT_VALUES = st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=1, max_size=10)

HT_VALUE_TYPES = st.sampled_from(datafield.ValueType)

HT_DATA_FIELDS = st.tuples(HT_TAGS, HT_VALUES).map(
    lambda x: datafield.DataField(x[0], x[1])
)

HT_OPTIONAL_DATA_FIELDS = st.tuples(HT_DATA_FIELDS, HT_VALUE_TYPES).map(
    lambda x: datafield.OptionalDataField(x[0], x[1])
)


@ht.given(HT_DATA_FIELDS)
def test_data_field_of_string(field: datafield.DataField) -> None:
    """Test DataField.of_string() using correct data."""
    assert datafield.DataField.of_string(str(field)) == field


@ht.given(HT_OPTIONAL_DATA_FIELDS)
def test_optional_data_field_of_string(field: datafield.OptionalDataField) -> None:
    """Test OptionalDataField.of_string() using correct data."""
    assert datafield.OptionalDataField.of_string(str(field)) == field
