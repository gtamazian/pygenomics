"""Test module `pygenomics.sam.alignment`."""

import string

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.sam import alignment
from tests.sam.test_cigar import HT_CIGAR_STRINGS
from tests.sam.test_datafield import HT_OPTIONAL_DATA_FIELDS
from tests.sam.test_rname import HT_RSEQNAMES

HT_QNAMES = st.text(
    alphabet=list(alignment.QName.allowed_characters()), min_size=1, max_size=254
).map(alignment.QName)

HT_SEQUENCES = st.text(alphabet=string.ascii_letters, min_size=1)

HT_ALIGNMENTS = st.tuples(
    HT_QNAMES,
    st.integers(),
    st.one_of(HT_RSEQNAMES, st.just(None)),
    st.integers(),
    st.integers(),
    st.one_of(HT_CIGAR_STRINGS, st.just(None)),
    st.one_of(HT_SEQUENCES, st.just(None)),
    st.integers(),
    st.integers(),
    st.one_of(HT_SEQUENCES, st.just(None)),
    HT_SEQUENCES,
    st.lists(HT_OPTIONAL_DATA_FIELDS),
).map(lambda x: alignment.Record(*x))


@ht.given(HT_ALIGNMENTS)
def test_record_of_string(record: alignment.Record) -> None:
    """Test Record.of_string() using correct data."""
    assert alignment.Record.of_string(str(record)) == record
