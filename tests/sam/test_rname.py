"""Test module `pygenomics.sam.rname`."""

import random

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.sam import rname

HT_RNAME_LINES = st.text(
    alphabet=rname.RName.get_allowed_characters(), min_size=1, max_size=10
)


@ht.given(HT_RNAME_LINES)
def test_rname_init(value: str) -> None:
    """Check initialization of `RName` objects using correct strings."""
    assert str(rname.RName(value)) == value


def test_rname_empty_string() -> None:
    """Check that `RName` does not allow empty strings."""
    with pytest.raises(rname.EmptyName):
        rname.RName("")


def shuffle_string(line: str) -> str:
    """Helper function to shuffle a string.

    The function utilizes side effects by calling `random.shuffle()`.

    """
    line_list = list(line)
    random.shuffle(line_list)
    return str.join("", line_list)


HT_INCORRECT_RNAMES = st.tuples(
    HT_RNAME_LINES, st.text(alphabet=["@", ","], min_size=1, max_size=5)
).map(lambda x: shuffle_string(x[0] + x[1]))


@ht.given(HT_INCORRECT_RNAMES)
def test_rname_incorrect_character(value: str) -> None:
    """Check that `RName` does not allow incorrect characters."""
    with pytest.raises(rname.IncorrectCharacter):
        rname.RName(value)


HT_INCORRECT_RSEQNAMES = st.tuples(st.sampled_from(["*", "="]), HT_RNAME_LINES).map(
    lambda x: x[0] + x[1]
)


@ht.given(HT_INCORRECT_RSEQNAMES)
def test_rseqname_incorrect_character(value: str) -> None:
    """Check that `RSeqName` does not allow incorrect starting characters."""
    with pytest.raises(rname.IncorrectStartingCharacter):
        rname.RSeqName(value)


HT_RNAMES = HT_RNAME_LINES.map(rname.RName)


@ht.given(HT_RNAMES)
def test_rname_eq(name: rname.RName) -> None:
    """Test `RName.__eq__()` using two objects with the same value."""
    assert name == rname.RName(name.value)


@ht.given(HT_RNAMES, st.text())
def test_rname_eq_different_type(name: rname.RName, line: str) -> None:
    """Test `RName.__eq__()` using an object of a different type."""
    assert name != line


HT_RSEQNAMES = HT_RNAME_LINES.filter(lambda x: x[0] not in {"*", "="}).map(
    rname.RSeqName
)


@ht.given(HT_RSEQNAMES)
def test_rseqname_eq(name: rname.RSeqName) -> None:
    """Test `RSeqName.__eq__()` using two objects with the same value."""
    assert name == rname.RSeqName(name.value)


@ht.given(HT_RSEQNAMES, st.text())
def test_rseqname_eq_different_type(name: rname.RSeqName, line: str) -> None:
    """Test `RSeqName.__eq__()` using an object of a different type."""
    assert name != line
