"""Test module `pygenomics.bam.header`."""

import io
import string

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.bam.header import Header
from pygenomics.sam import rname

HT_REF_SEQ_NAMES = st.text(
    alphabet=rname.RSeqName.get_allowed_characters(), min_size=1, max_size=10
)

HT_REF_SEQS = st.lists(
    st.tuples(HT_REF_SEQ_NAMES, st.integers(min_value=1, max_value=4294967295))
)

HT_HEADERS = st.tuples(st.text(alphabet=string.printable), HT_REF_SEQS).map(
    lambda x: Header(*x)
)


@ht.given(HT_HEADERS)
def test_read_write(header: Header) -> None:
    """Test Header.read() and Header.write()."""
    stream = io.BytesIO()
    Header.write(header, stream)
    contents = io.BytesIO.getvalue(stream)
    stream.close()

    assert header == Header.read(io.BytesIO(contents))
