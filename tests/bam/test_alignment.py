"""Test module `pygenomics.bam.alignment`."""

from typing import Tuple

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.bam import alignment
from tests.bam.test_datafield import HT_FIELDS

HT_SEQUENCES = st.text(alphabet=alignment.get_read_bases(), min_size=1)

HT_CIGAR_OPS = st.tuples(
    st.sampled_from(alignment.get_cigar_symbols()),
    st.integers(min_value=1, max_value=0xFFFF),
)

HT_UINT8 = st.integers(min_value=0, max_value=2**8 - 1)

HT_UINT16 = st.integers(min_value=0, max_value=2**16 - 1)

HT_UINT32 = st.integers(min_value=0, max_value=2**32 - 1)

HT_INT32 = st.integers(min_value=-(2**31), max_value=2**31 - 1)

HT_PREFIXES = st.tuples(
    HT_INT32,
    HT_INT32,
    HT_UINT8,
    HT_UINT8,
    HT_UINT16,
    HT_UINT16,
    HT_UINT16,
    HT_UINT32,
    HT_INT32,
    HT_INT32,
    HT_INT32,
).map(lambda x: alignment.Prefix(*x))

HT_SMALL_INT = st.integers(min_value=1, max_value=5)

HT_SMALL_PREFIXES = st.tuples(
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
    HT_SMALL_INT,
).map(lambda x: alignment.Prefix(*x))


HT_ALIGNMENTS = HT_SMALL_PREFIXES.flatmap(
    lambda x: st.tuples(
        st.just(x),
        st.binary(min_size=x.l_read_name - 1, max_size=x.l_read_name - 1).map(
            lambda x: x + b"\x00"
        ),
        st.lists(HT_UINT32, min_size=x.n_cigar_op, max_size=x.n_cigar_op),
        st.lists(HT_UINT8, min_size=(x.l_seq + 1) // 2, max_size=(x.l_seq + 1) // 2),
        st.binary(min_size=x.l_seq, max_size=x.l_seq),
        st.lists(HT_FIELDS, max_size=10),
    )
).map(lambda x: alignment.Alignment(*x))


@ht.given(HT_SEQUENCES)
def test_encode_decode_seq(seq: str) -> None:
    """Test encode_seq() and decode_seq()."""
    assert alignment.decode_seq(alignment.encode_seq(seq))[: len(seq)] == seq


@ht.given(HT_CIGAR_OPS)
def test_encode_decode_cigar(operation: Tuple[str, int]) -> None:
    """Test encode_cigar_op() and decode_cigar_op()."""
    assert alignment.decode_cigar_op(alignment.encode_cigar_op(operation)) == operation


@ht.given(HT_PREFIXES)
def test_prefix_of_bytes(prefix: alignment.Prefix) -> None:
    """Test Prefix.of_bytes() and Prefix.to_bytes() using correct data."""
    assert alignment.Prefix.of_bytes(alignment.Prefix.to_bytes(prefix)) == prefix


@ht.given(HT_ALIGNMENTS)
def test_alignment_of_bytes(record: alignment.Alignment) -> None:
    """Test Alignment.of_bytes() and Alignment.to_bytes() using correct data."""
    assert alignment.Alignment.of_bytes(alignment.Alignment.to_bytes(record)) == record
