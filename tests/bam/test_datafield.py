"""Test module `pygenomics.bam.datafield`."""

import string
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

import pygenomics.bam.datafield as bam_datafield
import pygenomics.sam.datafield as sam_datafield
from tests.sam.test_datafield import HT_TAGS

HT_VALUE_TYPES = st.one_of(
    st.sampled_from(bam_datafield.BamSpecificValueType),
    st.sampled_from(sam_datafield.ValueType),
).map(bam_datafield.ValueType)

HT_NON_ARRAY_FIELDS = (
    st.tuples(
        HT_TAGS,
        HT_VALUE_TYPES.filter(
            lambda x: x.sam_value is not sam_datafield.ValueType.GENERAL_ARRAY
        ),
    )
    .flatmap(
        lambda x: st.tuples(
            st.just(x[0]),
            st.just(x[1]),
            st.text(alphabet=string.hexdigits, max_size=10).map(
                lambda x: str.encode(x) + b"\x00"
            ),
        )
        if bam_datafield.ValueType.is_special_array(x[1])
        else st.tuples(
            st.just(x[0]),
            st.just(x[1]),
            st.binary(
                min_size=bam_datafield.ValueType.value_size(x[1]),
                max_size=bam_datafield.ValueType.value_size(x[1]),
            ),
        )
    )
    .map(lambda x: bam_datafield.DataField(x[0], x[1], False, x[2]))
)

HT_ARRAY_FIELDS = (
    st.tuples(
        HT_TAGS,
        HT_VALUE_TYPES.filter(lambda x: not bam_datafield.ValueType.is_array(x)),
        st.integers(min_value=1, max_value=10),
    )
    .flatmap(
        lambda x: st.tuples(
            st.just(x[0]),
            st.just(x[1]),
            st.binary(
                min_size=bam_datafield.ValueType.value_size(x[1]) * x[2],
                max_size=bam_datafield.ValueType.value_size(x[1]) * x[2],
            ),
        )
    )
    .map(lambda x: bam_datafield.DataField(x[0], x[1], True, x[2]))
)

HT_FIELDS = st.one_of(HT_NON_ARRAY_FIELDS, HT_ARRAY_FIELDS)


@ht.given(HT_VALUE_TYPES)
def test_valuetype_of_string(type_value: bam_datafield.ValueType) -> None:
    """Test ValueType.of_string() using correct data."""
    assert bam_datafield.ValueType.of_string(str(type_value)) == type_value


@ht.given(st.lists(HT_FIELDS, min_size=1, max_size=10))
def test_datafield_of_bytes(fields: List[bam_datafield.DataField]) -> None:
    """Test DataField.of_bytes() using correct data."""
    assert (
        bam_datafield.DataField.of_bytes(
            bytes.join(b"", (bam_datafield.DataField.to_bytes(k) for k in fields))
        )
        == fields
    )
