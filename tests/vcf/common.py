"""Common routines for testing vcf modules."""

import hypothesis.strategies as st

from pygenomics.vcf import datatypes

HT_DATATYPES = st.sampled_from(
    [
        datatypes.DataType.INTEGER,
        datatypes.DataType.FLOAT,
        datatypes.DataType.FLAG,
        datatypes.DataType.CHARACTER,
        datatypes.DataType.STRING,
    ]
)
