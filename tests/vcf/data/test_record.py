"""Test module pygenomics.vcf.data.record."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import nucleotide
from pygenomics.vcf.data import record
from tests.vcf.data.test_alt import HT_ALT_FIELDS

MAX_FIELDS = 3

HT_CHROM_FIELDS = st.tuples(
    st.text(alphabet=st.characters(blacklist_characters=["\t"]), min_size=1),
    st.booleans(),
).map(lambda x: record.Chrom(x[0], x[1]))

HT_POS_FIELDS = st.integers(min_value=0).map(record.Pos)

HT_ID_FIELDS = (
    st.lists(
        st.text(
            alphabet=st.characters(blacklist_characters=[";", "\t"]),
            min_size=1,
            max_size=1,
        ),
        max_size=MAX_FIELDS,
    )
    .filter(lambda x: len(set(x)) == len(x))
    .filter(lambda x: all(k != record.missing_value() for k in x))
    .map(record.IdField)
)


HT_REF_FIELDS = st.text(
    alphabet=nucleotide.EXACT_LETTERS + nucleotide.GAPS, min_size=1, max_size=100
).map(record.RefField)

HT_QUAL_FIELDS = st.one_of(st.just(None), st.integers(min_value=0, max_value=1000)).map(
    record.Qual
)

HT_FILTER_FIELDS = (
    st.lists(
        st.text(
            alphabet=st.characters(blacklist_characters=[";", "\t", "."]),
            min_size=1,
            max_size=10,
        ),
        max_size=MAX_FIELDS,
    )
    .filter(lambda x: all(k != "0" for k in x))
    .map(record.FilterField)
)

_HT_INFO_KEYS = st.text(
    alphabet=st.characters(blacklist_characters=[";", "=", "\t"]),
    min_size=1,
    max_size=MAX_FIELDS,
)

_HT_INFO_VALUES = st.lists(
    st.text(
        alphabet=st.characters(blacklist_characters=[",", ";", "=", "\t"]),
        min_size=1,
        max_size=10,
    )
)

HT_INFO_FIELDS = st.lists(
    st.tuples(_HT_INFO_KEYS, _HT_INFO_VALUES), max_size=MAX_FIELDS
).map(lambda x: record.InfoField([k for k in x if k[0] != "."]))

HT_FORMAT_FIELDS = (
    st.lists(
        st.text(
            alphabet=st.characters(blacklist_characters=[":", "\t"]),
            min_size=1,
            max_size=3,
        ),
        min_size=1,
        max_size=MAX_FIELDS,
    )
    .map(lambda x: [k for k in x if k != "."])
    .filter(lambda x: x)
    .map(record.FormatField)
)

HT_SAMPLES = st.lists(
    st.lists(
        st.text(
            alphabet=st.characters(blacklist_characters=[":", "\t", ","]),
            min_size=1,
            max_size=MAX_FIELDS,
        )
    ).filter(lambda x: x != ["."]),
    min_size=1,
    max_size=MAX_FIELDS,
).map(record.SampleField)

HT_GENOTYPE_FIELDS = st.tuples(HT_FORMAT_FIELDS, st.lists(HT_SAMPLES, max_size=10)).map(
    lambda x: record.GenotypeFields(x[0], x[1])
)

HT_RECORDS = st.tuples(
    HT_CHROM_FIELDS,
    HT_POS_FIELDS,
    HT_ID_FIELDS,
    HT_REF_FIELDS,
    st.lists(HT_ALT_FIELDS, min_size=1, max_size=MAX_FIELDS),
    HT_QUAL_FIELDS,
    HT_FILTER_FIELDS,
    HT_INFO_FIELDS,
    HT_GENOTYPE_FIELDS,
).map(
    lambda x: record.Record(x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], str(x[8]))
)


@ht.given(HT_CHROM_FIELDS)
def test_chrom_of_string(field: record.Chrom) -> None:
    """Test record.Chrom.of_string() using correct CHROM fields."""
    assert record.Chrom.of_string(str(field)) == field


@ht.given(HT_ID_FIELDS)
def test_idfield_of_string(field: record.IdField) -> None:
    """Test record.IdField.of_string() using correct ID fields."""
    assert record.IdField.of_string(str(field)) == field


@ht.given(HT_REF_FIELDS)
def test_reffield_of_string(field: record.RefField) -> None:
    """Test record.RefField.of_string() using correct REF fields."""
    assert record.RefField.of_string(str(field)) == field


@ht.given(HT_FILTER_FIELDS)
def test_filterfield_of_string(field: record.FilterField) -> None:
    """Test record.FilterField.of_string() using correct FILTER fields."""
    assert record.FilterField.of_string(str(field)) == field


@ht.given(HT_INFO_FIELDS)
def test_infofield_of_string(field: record.InfoField) -> None:
    """Test record.InfoField.of_string() using correct INFO fields."""
    assert record.InfoField.of_string(str(field)) == field


@ht.given(HT_FORMAT_FIELDS)
def test_formatfield_of_string(field: record.FormatField) -> None:
    """Test record.FormatField.of_string() using correct FORMAT fields."""
    assert record.FormatField.of_string(str(field)) == field


@ht.given(HT_POS_FIELDS)
def test_pos_of_string(field: record.Pos) -> None:
    """Test record.Pos.of_string() using correct POS fields."""
    assert record.Pos.of_string(str(field)) == field


@ht.given(HT_QUAL_FIELDS)
def test_qual_of_string(field: record.Qual) -> None:
    """Test record.Qual.of_string() using correct QUAL fields."""
    assert record.Qual.of_string(str(field)) == field


@ht.given(HT_GENOTYPE_FIELDS)
def test_genotypefields_of_string(fields: record.GenotypeFields) -> None:
    """Test record.GenotypeFields.of_list() using correct fields."""
    assert record.GenotypeFields.of_string(str(fields)) == fields


@ht.given(HT_RECORDS)
@ht.settings(suppress_health_check=[ht.HealthCheck.too_slow])
def test_record_of_string(variant_record: record.Record) -> None:
    """Test record.Record.of_string() using correct variant records."""
    assert record.Record.of_string(str(variant_record)) == variant_record
