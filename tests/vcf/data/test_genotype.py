"""Test module pygenomics.vcf.data.genotype."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf.data import genotype

MAX_ALLELES = 10

HT_PRESENT_GENOTYPES = st.tuples(
    st.lists(st.integers(min_value=0), min_size=1, max_size=MAX_ALLELES), st.booleans()
).map(
    lambda x: genotype.GenotypeRecord(
        x[0], x[1] if len(x[0]) > 1 else False, False, len(x[0])
    )
)

HT_MISSING_GENOTYPES = st.tuples(
    st.booleans(), st.integers(min_value=1, max_value=MAX_ALLELES)
).map(lambda x: genotype.GenotypeRecord(None, x[0] if x[1] > 1 else False, True, x[1]))

HT_GENOTYPES = st.one_of(HT_PRESENT_GENOTYPES, HT_MISSING_GENOTYPES)


@ht.given(HT_GENOTYPES)
def test_of_string(record: genotype.GenotypeRecord) -> None:
    """Test genotype.GenotypeRecord.of_string() using correct records."""
    assert genotype.GenotypeRecord.of_string(str(record)) == record
