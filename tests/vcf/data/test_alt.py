"""Test module pygenomics.vcf.data.alt."""

import string

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import nucleotide
from pygenomics.vcf.data import alt

_HT_ALT_BASES = st.text(
    alphabet=nucleotide.EXACT_LETTERS + nucleotide.GAPS, min_size=1, max_size=10
)

_HT_ALT_TAGS = st.text(
    alphabet=string.ascii_letters + string.digits, min_size=1, max_size=10
)

_HT_ALT_TYPES = st.sampled_from(
    [
        alt.FieldType.BASE_PAIRS,
        alt.FieldType.SPANNING_DELETION,
        alt.FieldType.UNKNOWN,
        alt.FieldType.NON_REF,
        alt.FieldType.ANGLE_BRACKETED,
    ]
)

HT_ALT_FIELDS = _HT_ALT_TYPES.flatmap(
    lambda x: st.tuples(
        st.just(x),
        st.just(None)
        if x not in {alt.FieldType.BASE_PAIRS, alt.FieldType.ANGLE_BRACKETED}
        else (_HT_ALT_BASES if x == alt.FieldType.BASE_PAIRS else _HT_ALT_TAGS),
    )
).map(lambda x: alt.Field(x[0], x[1]))


@ht.given(HT_ALT_FIELDS)
def test_value_of_string(value: alt.Field) -> None:
    """Test alt.Field.of_string() using correct ALT fields."""
    assert alt.Field.of_string(str(value)) == value
