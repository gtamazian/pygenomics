"""Test module pygenomics.vcf.datatypes."""

import math

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf import datatypes

DATATYPE_STRINGS = ["Integer", "Float", "Flag", "Character", "String"]


@ht.given(st.floats())
def test_is_valid_float_str(number: float) -> None:
    """Test that is_valid_float_str() correctly processes valid data."""
    assert datatypes.is_valid_float_str(
        str(number)
        if number not in {math.inf, -math.inf} and not math.isnan(number)
        else str.upper(str(number))
    )


def test_float_special_values() -> None:
    """Test that float() correctly processes special values.

    The special values are "INF", "INFINITY", and "NAN", as given in
    Section 1.3 of the VCF specification.

    """
    assert math.isnan(float("NAN"))
    assert math.inf == float("INF") == float("INFINITY")


@ht.given(
    st.integers(
        min_value=datatypes.MIN_INVALID_INT, max_value=datatypes.MAX_INVALID_INT
    )
)
def test_is_valid_int(number: int) -> None:
    """Test that is_valid_int() correctly detects invalid data."""
    assert not datatypes.is_valid_int(number)


@ht.given(st.one_of(st.text(), st.sampled_from(DATATYPE_STRINGS)))
def test_datatype_of_string(line: str) -> None:
    """Test if datatype.of_string() correctly parses data type names."""
    assert (
        datatypes.DataType.of_string(line) is None
        if line not in set(DATATYPE_STRINGS)
        else str(datatypes.DataType.of_string(line)) == line
    )
