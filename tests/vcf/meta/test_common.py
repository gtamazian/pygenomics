"""Test module pygenomics.vcf.meta."""

from typing import Tuple

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.vcf.meta import common


@ht.given(st.text())
def test_escape_unescape(line: str) -> None:
    """Test that escape() and unescape() are inverse to each other."""
    assert common.unescape(common.escape(line)) == line


@pytest.mark.parametrize("pair", [("\\", "\\\\"), ('"', '\\"')])
def test_escape_special(pair: Tuple[str, str]) -> None:
    """Test how escape() processes special characters."""
    original, escaped = pair
    assert common.escape(original) == escaped and common.unescape(escaped) == original


@ht.given(st.text())
def test_unescape_ending_with_backslash(line: str) -> None:
    """Test how unescape() processes lines ending with backslash."""
    assert str.endswith(common.unescape(line + "\\"), "\\")


@ht.given(st.text())
def test_split_by_commas(line: str) -> None:
    """Test split_by_commas() using arbitrary strings."""
    assert (
        common.unescape(
            str.join(",", common.split_by_commas(common.escape(line), [('"', '"')]))
        )
        == line
    )
