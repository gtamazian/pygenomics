"""Test module pygenomics.vcf.meta.filterline."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf.meta import filterline
from tests.vcf.meta import common

HT_LINES = st.tuples(common.HT_KEYS, common.HT_EXTRA_FIELDS).map(
    lambda x: filterline.FilterLine(x[0], x[1])
)


@ht.given(HT_LINES)
def test_filterline_of_string(line: filterline.FilterLine) -> None:
    """Test filterline.FilterLine.of_string() using correct lines."""
    assert filterline.FilterLine.of_string(str(line)) == line


@ht.given(HT_LINES, common.HT_KEYS.filter(lambda x: x != "FILTER"))
def test_filterline_of_string_incorrect_key(
    line: filterline.FilterLine, incorrect_key: str
) -> None:
    """Test filterline.FilterLine.of_string() using an incorrect line."""
    suffix = str(line)[len("##FILTER=") :]
    assert (
        filterline.FilterLine.of_string(
            str.format("##{:s}={:s}", incorrect_key, suffix)
        )
        is None
    )


@ht.given(HT_LINES, st.integers())
def test_filterline_eq(line: filterline.FilterLine, number: int) -> None:
    """Test filterline.FilterLine.__eq__() using a different object."""
    assert line != number
