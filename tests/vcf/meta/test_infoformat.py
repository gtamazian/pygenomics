"""Test module pygenomics.vcf.meta.infoformat."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.common import assoc_contains_none
from pygenomics.vcf import datatypes
from pygenomics.vcf.meta import infoformat, valuenumber
from tests.vcf.common import HT_DATATYPES
from tests.vcf.meta import common

_HT_EXTRA_FIELDS = common.HT_EXTRA_FIELDS.filter(
    lambda x: assoc_contains_none(x, ["Number", "Type", "Description"])
)

_HT_INFOLINES_NONFLAG = st.tuples(
    st.just(infoformat.LineKind.INFO),
    common.HT_KEYS,
    common.HT_VALUENUMBERS.filter(lambda x: x != valuenumber.Number(0)),
    HT_DATATYPES.filter(lambda x: x != datatypes.DataType.FLAG),
    common.HT_QUOTED_VALUES,
    _HT_EXTRA_FIELDS,
).map(lambda x: infoformat.InfoFormatLine(x[0], x[1], x[2], x[3], x[4], x[5]))

_HT_INFOLINES_FLAG = st.tuples(
    st.just(infoformat.LineKind.INFO),
    common.HT_KEYS,
    st.just(valuenumber.Number(0)),
    st.just(datatypes.DataType.FLAG),
    common.HT_QUOTED_VALUES,
    _HT_EXTRA_FIELDS,
).map(lambda x: infoformat.InfoFormatLine(x[0], x[1], x[2], x[3], x[4], x[5]))

HT_INFOLINES = st.one_of(_HT_INFOLINES_NONFLAG, _HT_INFOLINES_FLAG)

HT_FORMATLINES = st.tuples(
    st.just(infoformat.LineKind.FORMAT),
    common.HT_KEYS,
    common.HT_VALUENUMBERS.filter(lambda x: x != valuenumber.Number(0)),
    HT_DATATYPES.filter(lambda x: x != datatypes.DataType.FLAG),
    common.HT_QUOTED_VALUES,
    _HT_EXTRA_FIELDS,
).map(lambda x: infoformat.InfoFormatLine(x[0], x[1], x[2], x[3], x[4], x[5]))


HT_INFOFORMATLINES = st.one_of(HT_INFOLINES, HT_FORMATLINES)


@ht.given(HT_INFOFORMATLINES)
def test_infoformat_of_string(line: infoformat.InfoFormatLine) -> None:
    """Check infoformat.InfoFormatLine.of_string() using correct lines."""
    assert infoformat.InfoFormatLine.of_string(str(line)) == line


@ht.given(
    HT_INFOFORMATLINES,
    common.HT_KEYS.filter(lambda x: x not in {"INFO", "FORMAT"}),
)
def test_infoformat_of_string_incorrect_key(
    line: infoformat.InfoFormatLine, incorrect_key: str
) -> None:
    """Test infoformat.InfoFormatLine.of_string() using an incorrect key."""
    suffix = str(line)[len("##" + str(line.kind) + "=") :]
    assert (
        infoformat.InfoFormatLine.of_string(
            str.format("##{:s}={:s}", incorrect_key, suffix)
        )
        is None
    )


@ht.given(HT_INFOFORMATLINES, st.integers())
def test_infoformat_eq(line: infoformat.InfoFormatLine, number: int) -> None:
    """Test infoformat.InfoFormatLine.__eq__() using a different object."""
    assert line != number


@ht.given(common.HT_KEYS, st.integers(min_value=0))
def test_infoformat_incorrect_type_number(id_field: str, number: int) -> None:
    """Test that Flag records require Number equal to 0 in INFO lines."""
    record = infoformat.InfoFormatLine.of_string(
        str.format(
            '##INFO=<ID={:s},Number={:d},Type=Flag,Description="Test">',
            id_field,
            number,
        )
    )
    if number > 0:
        assert record is None


@ht.given(common.HT_KEYS, st.integers(min_value=0))
def test_infoformat_incorrect_format_type(id_field: str, number: int) -> None:
    """Test that FORMAT lines do not allow the Flag type."""
    assert (
        infoformat.InfoFormatLine.of_string(
            str.format(
                '##FORMAT=<ID={:s},Number={:d},Type=Flag,Description="Test">',
                id_field,
                number,
            )
        )
        is None
    )
