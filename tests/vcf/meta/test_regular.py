"""Test module pygenomics.vcf.meta.regular."""

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.vcf.meta import error, regular
from tests.vcf.meta import common


@ht.given(common.HT_KEYS, common.HT_UNQUOTED_VALUES)
def test_regularline_of_string(key: str, value: str) -> None:
    """Test regular.RegularLine.of_string() using correct lines."""
    record = regular.RegularLine(key, value)
    assert regular.RegularLine.of_string(str(record)) == record


@ht.given(common.HT_KEYS, common.HT_UNQUOTED_VALUES, st.integers())
def test_regularline_eq(key: str, value: str, number: int) -> None:
    """Test regular.RegularLine.__eq__() using a different object."""
    assert regular.RegularLine(key, value) != number


@ht.given(common.HT_KEYS, common.HT_UNQUOTED_VALUES)
def test_regularline_of_string_failures(key: str, value: str) -> None:
    """Test if regular.RegularLine.of_string() identifies malformed lines."""
    record_str = str(regular.RegularLine(key, value))
    assert regular.RegularLine.of_string(str.replace(record_str, "##", "")) is None
    assert regular.RegularLine.of_string(str.replace(record_str, "=", "")) is None
    assert regular.RegularLine.of_string(str.format("##={:s}", value)) is None
    assert regular.RegularLine.of_string(str.format("##{:s}=", key)) is None


@ht.given(common.HT_UNQUOTED_VALUES)
def test_regularline_empty_key(value: str) -> None:
    """Test that regular.RegularLine.__init__() detects the empty key."""
    with pytest.raises(error.LineError) as exception:
        regular.RegularLine("", value)
    assert (
        exception.value.error_type
        == error.LineErrorType.EMPTY_META_INFORMATION_LINE_KEY
    )


@ht.given(common.HT_KEYS)
def test_regularline_empty_value(key: str) -> None:
    """Test if regular.RegularLine.__init__() detects the empty value."""
    with pytest.raises(error.LineError) as exception:
        regular.RegularLine(key, "")
    assert exception.value.error_type == error.LineErrorType.EMPTY_VALUE
