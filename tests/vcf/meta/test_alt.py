"""Test module pygenomics.vcf.meta.alt."""

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics import iupac
from pygenomics.vcf.meta import alt
from tests.vcf.meta import common

HT_SVTYPES = st.sampled_from([k for k, _ in alt.SVTYPE_VALUE_STRING_PAIRS])

SVTYPE_VALUES = [k for k, _ in alt.SVTYPE_VALUE_STRING_PAIRS]
SVTYPE_STRINGS = frozenset(k for _, k in alt.SVTYPE_VALUE_STRING_PAIRS)


@pytest.mark.parametrize("value", SVTYPE_VALUES)
def test_svtype_of_string(value: alt.SVType) -> None:
    """Test alt.svtype_of_string() and alt.svtype_to_string()."""
    assert alt.svtype_of_string(alt.svtype_to_string(value)) == value


@ht.given(st.text())
def test_svtype_of_string_incorrect(line: str) -> None:
    """Test alt.svtype_of_string() using incorrect data."""
    ht.assume(line not in SVTYPE_STRINGS)
    assert alt.svtype_of_string(line) is None


HT_ALTLINES = (
    st.tuples(
        st.one_of(
            st.sampled_from(list(iupac.get_ambiguous_symbol_characters())),
            st.sampled_from(alt.SVTYPE_STRINGS),
        ),
        common.HT_QUOTED_VALUES,
        common.HT_EXTRA_FIELDS,
    )
    .filter(lambda x: not any(k == "Description" for k, _ in x[2]))
    .map(lambda x: alt.AltLine(x[0], x[1], x[2]))
)


@ht.given(HT_ALTLINES)
def test_line_of_string(line: alt.AltLine) -> None:
    """Test alt.AltLine.of_string() using correct lines."""
    assert alt.AltLine.of_string(str(line)) == line


@ht.given(HT_ALTLINES, st.integers())
def test_line_eq(line: alt.AltLine, number: int) -> None:
    """Test alt.AltLine.__eq__() using an object of a different type."""
    assert line != number


@ht.given(HT_ALTLINES, common.HT_KEYS)
def test_line_of_string_incorrect_tag(line: alt.AltLine, tag: str) -> None:
    """Test that alt.AltLine.of_string() detects an incorrect tag."""
    ht.assume(tag != "ALT")
    assert alt.AltLine.of_string(str.replace(str(line), "##ALT", "##" + tag)) is None


HT_NON_IUPAC_AMBIGUITY_CODES = st.text(min_size=1, max_size=1).filter(
    lambda x: x not in iupac.get_ambiguous_symbol_characters()
)


@ht.given(
    HT_ALTLINES.filter(lambda x: x.is_iupac_ambiguous_code()),
    HT_NON_IUPAC_AMBIGUITY_CODES,
)
def test_line_non_iupac_ambiguity_code(line: alt.AltLine, code: str) -> None:
    """Test that alt.AltLine.__init__() detects an incorrect IUPAC code."""
    current_code = line.iupac_code
    assert (
        alt.AltLine.of_string(
            str.replace(
                str(line),
                str.format("##ALT=<ID={:s},", current_code),
                str.format("##ALT=<ID={:s},", code),
            )
        )
        is None
    )


HT_INCORRECT_SVTYPE_STRINGS = st.text(min_size=1).filter(
    lambda x: x not in SVTYPE_STRINGS
    and x not in iupac.get_ambiguous_symbol_characters()
)


@ht.given(
    HT_ALTLINES.filter(lambda x: not x.is_iupac_ambiguous_code()),
    HT_INCORRECT_SVTYPE_STRINGS,
)
def test_line_incorrect_first_level_svtype(line: alt.AltLine, svtype: str) -> None:
    """Test that alt.AltLine.__init__() detects an incorrect variant type."""
    assert line.levels is not None
    current_svtype = line.levels[0]
    assert (
        alt.AltLine.of_string(
            str.replace(
                str(line),
                str.format("##ALT=<ID={:s},", alt.svtype_to_string(current_svtype)),
                str.format("##ALT=<ID={:s},", svtype),
            )
        )
        is None
    )
