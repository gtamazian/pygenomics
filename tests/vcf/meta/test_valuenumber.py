"""Test module pygenomics.vcf.meta.valuenumber."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf.meta import valuenumber
from tests.vcf.meta import common

SPECIALNUMBER_STRINGS = ["A", "R", "G", "."]


@ht.given(common.HT_VALUENUMBERS)
def test_valuenumber_of_string(number: valuenumber.Number) -> None:
    """Test valuenumber.Number.of_string() using correct data."""
    assert valuenumber.Number.of_string(str(number)) == number


@ht.given(st.characters(blacklist_characters=SPECIALNUMBER_STRINGS))
def test_valuenumber_of_string_incorrect_special(line: str) -> None:
    """Test valuenumber.Number.of_string() using incorrect special values."""
    ht.assume(not str.isdecimal(line))
    assert valuenumber.Number.of_string(line) is None


@ht.given(st.integers(max_value=-1).map(lambda x: str.format("{:d}", x)))
def test_valuenumber_of_string_incorrect_negative(line: str) -> None:
    """Test valuenumber.Number.of_string() using incorrect negative numbers."""
    assert valuenumber.Number.of_string(line) is None


@ht.given(st.integers(min_value=0), st.integers(min_value=0))
def test_valuenumber_eq_regular(first: int, second: int) -> None:
    """Test valuenumber.Number.__eq__() using regular numbers."""
    assert (valuenumber.Number(first) == valuenumber.Number(second)) == (
        first == second
    )


@ht.given(common.HT_VALUENUMBERS, common.HT_VALUENUMBERS)
def test_valuenumber_eq_special(
    first: valuenumber.Number, second: valuenumber.Number
) -> None:
    """Test valuenumber.Number.__eq__() using special values."""
    assert (str(first) == str(second)) == (first == second)


@ht.given(common.HT_VALUENUMBERS, st.floats())
def test_valuenumber_eq_float(
    value_number: valuenumber.Number, float_number: float
) -> None:
    """Test valuenumber.Number.__eq__() with floats."""
    assert value_number != float_number
