"""Test module pygenomics.vcf.meta.structured."""

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.vcf.meta import error, structured
from tests.vcf.meta import common

HT_LINES = st.tuples(common.HT_KEYS, common.HT_KEYS, common.HT_EXTRA_FIELDS).map(
    lambda x: structured.StructuredLine(x[0], x[1], x[2])
)


@ht.given(HT_LINES)
def test_structuredline_of_string(line: structured.StructuredLine) -> None:
    """Test structured.StructuredLine.of_string() using correct lines."""
    assert structured.StructuredLine.of_string(str(line)) == line


@ht.given(HT_LINES, st.integers())
def test_structuredline_eq(line: structured.StructuredLine, number: int) -> None:
    """Test structured.StructuredLine.__eq__() using a different object."""
    assert line != number


@ht.given(HT_LINES)
def test_structuredline_empty_id(line: structured.StructuredLine) -> None:
    """Test that __init__() detects the empty ID."""
    with pytest.raises(error.LineError) as exception:
        structured.StructuredLine(line.key, "", line.extra)
    assert exception.value.error_type == error.LineErrorType.EMPTY_ID


@ht.given(HT_LINES)
def test_structuredline_empty_description(line: structured.StructuredLine) -> None:
    """Test that __init__() detects the empty Description field."""
    with pytest.raises(error.LineError) as exception:
        structured.StructuredWithDescriptionLine(
            line.key,
            line.id_field,
            "",
            [(key, value) for key, value in line.extra if key != "Description"],
        )
    assert exception.value.error_type == error.LineErrorType.EMPTY_DESCRIPTION


@ht.given(HT_LINES)
def test_structuredline_empty_extra_key(line: structured.StructuredLine) -> None:
    """Test that __init__() detects the empty extra field key."""
    ht.assume(line.extra)
    with pytest.raises(error.LineError) as exception:
        structured.StructuredLine(
            line.key, line.id_field, [("", value) for _, value in line.extra]
        )
    assert exception.value.error_type == error.LineErrorType.EMPTY_KEY


@ht.given(HT_LINES)
def test_structuredline_empty_extra_value(line: structured.StructuredLine) -> None:
    """Test that __init__() detects the empty extra field value."""
    ht.assume(line.extra)
    with pytest.raises(error.LineError) as exception:
        structured.StructuredLine(
            line.key, line.id_field, [(key, "") for key, _ in line.extra]
        )
    assert exception.value.error_type == error.LineErrorType.EMPTY_VALUE
