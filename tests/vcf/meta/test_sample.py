"""Test module pygenomics.vcf.meta.sample."""

import string
from typing import List, Tuple

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.common import assoc_unique
from pygenomics.vcf.meta import error, sample
from tests.vcf.meta import common

HT_KEYS = st.text(
    alphabet=string.ascii_letters,
    min_size=1,
    max_size=10,
)

HT_VALUES = st.lists(HT_KEYS, min_size=1)

MAX_FIELDS = 3

HT_EXTRA_FIELDS = st.lists(
    st.tuples(HT_KEYS, common.HT_QUOTED_VALUES).filter(
        lambda x: x[0] not in {"ID", "Type", "Number", "Values"}
    ),
    min_size=1,
    max_size=MAX_FIELDS,
).filter(assoc_unique)

HT_METALINES = st.tuples(HT_KEYS, HT_VALUES, HT_EXTRA_FIELDS).map(
    lambda x: sample.MetaLine(x[0], x[1], x[2])
)


@ht.given(HT_METALINES)
def test_metaline_empty_values(line: sample.MetaLine) -> None:
    """Test sample.MetaLine.__init__() using the empty value list."""
    with pytest.raises(error.LineError) as exception:
        sample.MetaLine(line.id_field, [], line.extra)
    assert exception.value.error_type == error.LineErrorType.EMPTY_METALINE_VALUE_LIST


@ht.given(HT_METALINES)
def test_metaline_of_string(line: sample.MetaLine) -> None:
    """Test sample.MetaLine.of_string() using correct lines."""
    assert sample.MetaLine.of_string(str(line)) == line


@ht.given(HT_METALINES, st.integers())
def test_metaline_eq(line: sample.MetaLine, number: int) -> None:
    """Test sample.MetaLine.__eq__() using an object of a different type."""
    assert line != number


@ht.given(HT_METALINES, common.HT_KEYS)
def test_metaline_of_string_incorrect_tag(line: sample.MetaLine, tag: str) -> None:
    """Test that sample.MetaLine.of_string() detects an incorrect tag."""
    ht.assume(tag != "META")
    assert sample.MetaLine.of_string("##" + tag + str(line)[len("##META") :]) is None


@ht.given(HT_METALINES, st.sampled_from(["Integer", "Float", "Flag", "Character"]))
def test_metaline_of_string_incorrect_type(
    line: sample.MetaLine, type_value: str
) -> None:
    """Test that sample.MetaLine.of_string() detects an incorrect type."""
    assert (
        sample.MetaLine.of_string(
            str.replace(str(line), "Type=String", str.format("Type={:s}", type_value))
        )
        is None
    )


@ht.given(HT_KEYS)
def test_metaline_of_string_empty_list(id_field: str) -> None:
    """Test that sample.MetaLine.of_string() detects the empty value list."""
    assert (
        sample.MetaLine.of_string(
            str.format("##META=<ID={:s},Type=String,Number=.,Values=[]>", id_field)
        )
        is None
    )


@ht.given(HT_KEYS)
def test_metaline_of_string_empty_values(id_field: str) -> None:
    """Test that sample.MetaLine.of_string() detects the empty value list."""
    assert (
        sample.MetaLine.of_string(
            str.format("##META=<ID={:s},Type=String,Number=.,Values=[,]>", id_field)
        )
        is None
    )


@ht.given(HT_KEYS, st.text(alphabet=string.ascii_letters, min_size=1))
def test_metaline_of_string_incorrect_values(id_field: str, value: str) -> None:
    """Test that sample.MetaLine.of_string() detects incorrect values."""
    assert (
        sample.MetaLine.of_string(
            str.format(
                "##META=<ID={:s},Type=String,Number=.,Values={:s}>", id_field, value
            )
        )
        is None
    )


HT_SAMPLELINES = (
    st.lists(HT_METALINES, max_size=3)
    .flatmap(
        lambda metalines: st.tuples(
            st.just(metalines),
            HT_KEYS,
            st.just(
                [(k.id_field, k.values[0]) for k in metalines if k.id_field != "ID"]
            ),
            HT_EXTRA_FIELDS,
        )
    )
    .map(lambda x: (x[0], sample.SampleLine(x[1], x[2], x[3])))
).filter(lambda x: assoc_unique(x[1].meta + x[1].extra))


@ht.given(HT_SAMPLELINES)
def test_sampleline_of_string(
    case: Tuple[List[sample.MetaLine], sample.SampleLine]
) -> None:
    """Test sample.SampleLine.of_string() using correct lines."""
    meta_lines, sample_line = case
    assert (
        sample.SampleLine.sample_of_string(meta_lines, str(sample_line)) == sample_line
    )


@ht.given(HT_SAMPLELINES, st.integers())
def test_sampleline_eq(
    case: Tuple[List[sample.MetaLine], sample.SampleLine], number: int
) -> None:
    """Test sample.SampleLine.__eq__() using an object of a different type."""
    _, sample_line = case
    assert sample_line != number


@ht.given(HT_SAMPLELINES, st.text(min_size=1, max_size=10))
def test_sampleline_of_string_incorrect_meta(
    case: Tuple[List[sample.MetaLine], sample.SampleLine], value_suffix: str
) -> None:
    """Test sample.SampleLine.of_string() using incorrect META fields."""
    meta_lines, sample_line = case
    ht.assume(meta_lines)
    incorrect_meta_lines = [
        sample.MetaLine(k.id_field, [j + value_suffix for j in k.values], k.extra)
        for k in meta_lines
    ]
    assert (
        sample.SampleLine.sample_of_string(incorrect_meta_lines, str(sample_line))
        is None
    )


@ht.given(HT_SAMPLELINES, common.HT_KEYS)
def test_sampleline_of_string_incorrect_tag(
    case: Tuple[List[sample.MetaLine], sample.SampleLine], tag: str
) -> None:
    """Test that sample.SampleLine.of_string() detects an incorrect tag."""
    ht.assume(tag != "SAMPLE")
    meta_lines, sample_line = case
    assert (
        sample.SampleLine.sample_of_string(
            meta_lines, "##" + tag + str(sample_line)[len("##SAMPLE") :]
        )
        is None
    )
