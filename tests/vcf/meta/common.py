"""Common routines for testing vcf.meta modules."""

import hypothesis.strategies as st

from pygenomics.common import assoc_unique
from pygenomics.vcf.meta import common, valuenumber

DISALLOWED_CHARACTERS = ["=", ",", '"']

HT_KEYS = st.text(
    alphabet=st.characters(blacklist_characters=DISALLOWED_CHARACTERS), min_size=1
)
HT_UNQUOTED_VALUES = st.text(
    alphabet=st.characters(blacklist_characters=DISALLOWED_CHARACTERS), min_size=1
)
HT_QUOTED_VALUES = st.text(min_size=1).map(lambda x: '"' + common.escape(x) + '"')
HT_EXTRA_FIELDS = st.lists(
    st.tuples(HT_KEYS, HT_QUOTED_VALUES).filter(
        lambda x: x[0] not in {"ID", "Description"}
    ),
    min_size=1,
).filter(assoc_unique)

HT_VALUENUMBERS = st.one_of(
    st.sampled_from(
        [
            valuenumber.SpecialNumber.A,
            valuenumber.SpecialNumber.G,
            valuenumber.SpecialNumber.R,
            valuenumber.SpecialNumber.UNKNOWN,
        ]
    ),
    st.integers(min_value=0),
).map(valuenumber.Number)
