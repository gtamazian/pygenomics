"""Test module pygenomics.vcf.meta.contig."""

from typing import Optional

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics.common import is_proper_integer
from pygenomics.vcf.meta import contig, error
from pygenomics.vcf.meta.common import Fields
from tests.vcf.meta import common

HT_CONTIG_NAMES = st.text(min_size=1).filter(contig.is_correct_name)
HT_CONTIG_LENGTHS = st.one_of(st.just(None), st.integers(min_value=1))
HT_CONTIGLINES = st.tuples(
    HT_CONTIG_NAMES, HT_CONTIG_LENGTHS, common.HT_EXTRA_FIELDS
).map(lambda x: contig.ContigLine(x[0], x[1], x[2]))


@ht.given(HT_CONTIGLINES)
def test_contigline_of_string(line: contig.ContigLine) -> None:
    """Test contig.Line.of_string() using correct lines."""
    assert contig.ContigLine.of_string(str(line)) == line


@ht.given(
    st.one_of(
        st.just(""),
        st.text(alphabet=str.join("", contig.DISALLOWED_CHARACTERS), min_size=1),
        HT_CONTIG_NAMES.map(lambda x: "*" + x),
        HT_CONTIG_NAMES.map(lambda x: "=" + x),
    ),
    HT_CONTIG_LENGTHS,
    common.HT_EXTRA_FIELDS,
)
def test_contigline_incorrect_name(
    name: str, length: Optional[int], extra: Fields
) -> None:
    """Test if contig.Line.__init__() detects an incorrect contig name."""
    with pytest.raises(error.LineError) as exception:
        contig.ContigLine(name, length, extra)
    assert exception.value.error_type == error.LineErrorType.INCORRECT_CONTIG_NAME


@ht.given(HT_CONTIG_NAMES, st.integers(max_value=0), common.HT_EXTRA_FIELDS)
def test_contigline_incorrect_length(
    name: str, length: Optional[int], extra: Fields
) -> None:
    """Test if contig.Line.__init__() detects an incorrect contig length."""
    with pytest.raises(error.LineError) as exception:
        contig.ContigLine(name, length, extra)
    assert exception.value.error_type == error.LineErrorType.INCORRECT_CONTIG_LENGTH


@ht.given(HT_CONTIGLINES, st.integers())
def test_contigline_eq(line: contig.ContigLine, number: int) -> None:
    """Test contig.Line.__eq__() using a different object."""
    assert line != number


@ht.given(
    HT_CONTIGLINES.filter(lambda x: x.length is None),
    st.text().filter(lambda x: not is_proper_integer(x)),
)
def test_contigline_of_string_incorrect_length(
    line: contig.ContigLine, value: str
) -> None:
    """Test if contig.ContigLine.of_string() detects an incorrect length value."""
    assert (
        contig.ContigLine.of_string(str(line)[:-1] + str.format(",length={:s}>", value))
        is None
    )


@ht.given(HT_CONTIGLINES.filter(lambda x: x.length is None), st.integers(max_value=-1))
def test_contigline_of_string_negative_length(
    line: contig.ContigLine, number: int
) -> None:
    """Test if contig.ContigLine.of_string() detects a negative length value."""
    assert (
        contig.ContigLine.of_string(
            str(line)[:-1] + str.format(",length={:d}>", number)
        )
        is None
    )


@ht.given(HT_CONTIGLINES, st.text().filter(lambda x: x != "contig"))
def test_contigline_of_string_incorrect_tag(
    line: contig.ContigLine, new_tag: str
) -> None:
    """Test if contig.ContigLine.of_string() detects an incorrect tag."""
    assert (
        contig.ContigLine.of_string("##" + new_tag + str(line)[len("##contig") :])
        is None
    )
