"""Test module pygenomics.vcf.meta.pedigree."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf.meta import pedigree
from tests.vcf.meta import common

HT_ORIGINAL_ID = st.tuples(st.just("Original"), common.HT_UNQUOTED_VALUES).map(
    lambda x: [x]
)
HT_PARENT_IDS = st.tuples(
    st.tuples(st.just("Father"), common.HT_UNQUOTED_VALUES),
    st.tuples(st.just("Mother"), common.HT_UNQUOTED_VALUES),
).map(lambda x: [x[0], x[1]])

HT_LINES = st.tuples(
    common.HT_KEYS, st.one_of(HT_ORIGINAL_ID, HT_PARENT_IDS), common.HT_EXTRA_FIELDS
).map(lambda x: pedigree.PedigreeLine(x[0], x[1] + x[2]))


@ht.given(HT_LINES)
def test_pedigreeline_of_string(line: pedigree.PedigreeLine) -> None:
    """Test pedigree.PedigreeLine.of_string() using correct lines."""
    assert pedigree.PedigreeLine.of_string(str(line)) == line


@ht.given(HT_LINES, common.HT_KEYS.filter(lambda x: x != "PEDIGREE"))
def test_pedigreeline_of_string_incorrect_key(
    line: pedigree.PedigreeLine, incorrect_key: str
) -> None:
    """Test pedigree.PedigreeLine.of_string() using an incorrect line."""
    suffix = str(line)[len("##PEDIGREE=") :]
    assert (
        pedigree.PedigreeLine.of_string(
            str.format("##{:s}={:s}", incorrect_key, suffix)
        )
        is None
    )


@ht.given(HT_LINES, st.integers())
def test_pedigreeline_eq(line: pedigree.PedigreeLine, number: int) -> None:
    """Test pedigree.PedigreeLine.__eq__() using a different object."""
    assert line != number
