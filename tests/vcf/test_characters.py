"""Test module pygenomics.vcf.characters."""

import string

from pygenomics.vcf import characters

ENCODED_CHARACTER_CODES = [
    (":", "%3A"),
    (";", "%3B"),
    ("=", "%3D"),
    ("%", "%25"),
    (",", "%2C"),
    ("\r", "%0D"),
    ("\n", "%0A"),
    ("\t", "%09"),
]
"""Encoded character codes from the VCF specification (Section 1.2)."""


def test_encoded_characters() -> None:
    """Test that the character codes are consistent with the VCF specification."""
    correct_codes = dict(ENCODED_CHARACTER_CODES)
    for j, k in characters.encoded_characters():
        assert correct_codes[j] == k


PRINTABLE_FORBIDDEN_CHARACTERS = ["\x0b", "\x0c"]
"""Two forbidden characters are printable in Python; we specify them explicitly."""


def test_forbidden_characters() -> None:
    """Test that every forbidden character is not printable.

    There are two exceptions: '\x0b' and '\x0c'.

    """
    assert all(
        k not in string.printable or k in PRINTABLE_FORBIDDEN_CHARACTERS
        for k in characters.disallowed_characters()
    )
