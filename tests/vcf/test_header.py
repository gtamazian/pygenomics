"""Test module pygenomics.vcf.header."""

from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.vcf import header

HT_SAMPLES = st.lists(
    st.text(alphabet=st.characters(blacklist_characters=["\t"])), max_size=10
)


@ht.given(HT_SAMPLES)
def test_line_of_string(samples: List[str]) -> None:
    """Test header.Line.of_string() using correct header lines."""
    line = header.Line(samples)
    assert header.Line.of_string(str(line)) == line


HT_HEADER_LINE = HT_SAMPLES.map(header.Line)


@ht.given(HT_HEADER_LINE, st.integers())
def test_line_eq(line: header.Line, number: int) -> None:
    """Test header.Line.__eq__() using a different object."""
    assert line != number


@ht.given(HT_HEADER_LINE)
def test_line_of_string_non_header_line(line: header.Line) -> None:
    """Test if header.Line.of_string() detects incorrect header lines."""
    assert header.Line.of_string(str(line)[1:]) is None


@ht.given(HT_SAMPLES)
def test_line_of_string_missing_format(samples: List[str]) -> None:
    """Test if header.Line.of_string() detects missing FORMAT column."""
    ht.assume(len(samples) > 0)
    assert (
        header.Line.of_string(str.replace(str(header.Line(samples)), "\tFORMAT", ""))
        is None
    )
