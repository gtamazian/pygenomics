"""Test module `pygenomics.interval`."""

import itertools
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.interval import GenomicBase, GenomicInterval

HT_INTERVALS = st.lists(
    st.tuples(st.integers(min_value=0), st.integers(min_value=1)).map(
        lambda x: ("a", x[0], x[0] + x[1])
    )
)

HT_REDUCED_INTERVALS = HT_INTERVALS.map(
    lambda x: list(GenomicBase.iterate_reduced(GenomicBase(x)))
)


@ht.given(HT_INTERVALS)
def test_to_list(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.to_list()."""
    assert sorted(GenomicBase.to_list(GenomicBase(records))) == sorted(records)


@ht.given(HT_REDUCED_INTERVALS)
def test_iterate_reduced(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.iterate_reduced() by ensuring the idempotence."""
    base = GenomicBase(records)
    assert all(
        j == k
        for j, k in itertools.zip_longest(
            GenomicBase.to_list(base), GenomicBase.iterate_reduced(base)
        )
    )


@ht.given(HT_INTERVALS)
def test_find(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.find()."""
    base = GenomicBase(records)
    assert all(GenomicBase.find(base, k) for k in records)


@ht.given(HT_REDUCED_INTERVALS)
def test_find_all(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.find_all()."""
    base = GenomicBase(records)
    assert all(GenomicBase.find_all(base, k) == [k] for k in records)


@ht.given(HT_REDUCED_INTERVALS)
def test_iterate_complement(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.iterate_complement()."""
    double_complement = list(
        GenomicBase.iterate_complement(
            GenomicBase(GenomicBase.iterate_complement(GenomicBase(records)))
        )
    )
    assert records[1:-1] == double_complement


@ht.given(HT_REDUCED_INTERVALS)
def test_intersect(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.intersect()."""
    base = GenomicBase(records)
    assert all(GenomicBase.intersect(base, k) == [k] for k in records)


@ht.given(HT_INTERVALS)
def test_intersect_complement(records: List[GenomicInterval]) -> None:
    """Check that intersections with complement intervals are empty."""
    complement_base = GenomicBase(GenomicBase.iterate_complement(GenomicBase(records)))
    assert all(GenomicBase.intersect(complement_base, k) == [] for k in records)


@ht.given(HT_INTERVALS)
def test_subtract(records: List[GenomicInterval]) -> None:
    """Check subtracting from an interval the collection that includes it."""
    base = GenomicBase(records)
    assert all(GenomicBase.subtract(base, k) == [] for k in records)


@ht.given(HT_REDUCED_INTERVALS.filter(lambda x: len(x) > 1))
def test_subtract_complement(records: List[GenomicInterval]) -> None:
    """Check that subtracting complement intervals does not change the interval."""
    complement_base = GenomicBase(GenomicBase.iterate_complement(GenomicBase(records)))
    assert all(GenomicBase.subtract(complement_base, k) == [k] for k in records)


@ht.given(HT_INTERVALS)
def test_seq_ranges(records: List[GenomicInterval]) -> None:
    """Test GenomicBase.seq_ranges()."""
    if records:
        min_start = min(k for _, k, _ in records)
        max_end = max(k for _, _, k in records)
        seq_ranges = GenomicBase.seq_ranges(GenomicBase(records))
        assert (
            len(seq_ranges) == 1
            and seq_ranges[0][1] == min_start
            and seq_ranges[0][2] == max_end
        )
    else:
        assert not GenomicBase.seq_ranges(GenomicBase(records))
