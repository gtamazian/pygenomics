"""Test module pygenomics.strand."""

from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import strand

STRAND_VALUES = [strand.Strand.MINUS, strand.Strand.UNKNOWN, strand.Strand.PLUS]


def test_str_and_of_string() -> None:
    """Check if strand symbols are correctly parsed and converted back."""
    for k in strand.STRAND_SYMBOLS:
        assert str(strand.Strand.of_string(k)) == k


def test_invert() -> None:
    """Check Strand.invert()."""
    for j, k in zip(STRAND_VALUES, STRAND_VALUES[::-1]):
        assert strand.Strand.invert(j) == k


def test_is_known() -> None:
    """Check Strand.is_known()."""
    assert strand.Strand.is_known(strand.Strand.PLUS)
    assert strand.Strand.is_known(strand.Strand.MINUS)
    assert not strand.Strand.is_known(strand.Strand.UNKNOWN)


def test_strand_symbols() -> None:
    """Check that STRAND_SYMBOLS is correct."""
    assert [j == str(k) for j, k in zip(strand.STRAND_SYMBOLS, STRAND_VALUES)]


@ht.given(st.one_of(st.characters(), st.sampled_from(strand.STRAND_SYMBOLS)))
def test_of_string(line: str) -> None:
    """Test if of_string() correctly parses strand symbols."""
    assert (
        strand.Strand.of_string(line) is None
        if line not in set(strand.STRAND_SYMBOLS)
        else str(strand.Strand.of_string(line)) == line
    )


@ht.given(st.lists(st.sampled_from(STRAND_VALUES)))
def test_is_consistent(values: List[strand.Strand]) -> None:
    """Test if consistent() correctly identifies series of inverted strand values."""
    assert strand.consistent(values, [strand.Strand.invert(k) for k in values])
