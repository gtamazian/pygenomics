"""Test module `pygenomics.iupac`."""

import pytest

from pygenomics import iupac


@pytest.mark.parametrize("nucleotide", iupac.Nucleotide)
def test_nucleotide_of_string(nucleotide: iupac.Nucleotide) -> None:
    """Test Nucleotide.of_string() using correct nucleotide values."""
    assert iupac.Nucleotide.of_string(str(nucleotide)) is nucleotide


@pytest.mark.parametrize("symbol", iupac.Symbol)
def test_symbol_of_string(symbol: iupac.Symbol) -> None:
    """Test Symbol.of_string() using correct IUPAC symbol values."""
    assert iupac.Symbol.of_string(str(symbol)) is symbol


@pytest.mark.parametrize("nucleotide", iupac.Nucleotide)
def test_nucleotide_symbol_consistency(nucleotide: iupac.Nucleotide) -> None:
    """Test that nucleotide correspond to single-value symbols."""
    nucleotide_symbol = iupac.Symbol.of_string(str(nucleotide))
    assert (
        nucleotide_symbol is not None
        and len(iupac.Symbol.to_nucleotides(nucleotide_symbol)) == 1
    )
