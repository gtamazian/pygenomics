"""Test module pygenomics.nucleotide."""

import random

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import nucleotide


@ht.given(st.text(alphabet=nucleotide.LETTERS))
def test_rev_comp_involution(sequence: str) -> None:
    """Test if rev_comp() is involutive."""
    assert nucleotide.rev_comp(nucleotide.rev_comp(sequence)) == sequence


@ht.given(st.text(alphabet=nucleotide.LETTERS))
def test_remove_soft_mask(sequence: str) -> None:
    """Test if remove_soft_mask() leaves no lowercase characters."""
    assert all(k in nucleotide.UPPERCASE for k in nucleotide.remove_soft_mask(sequence))


@ht.given(st.text(alphabet=nucleotide.LOWERCASE, min_size=1))
def test_soft_to_hard_mask_all_lowercase(sequence: str) -> None:
    """Test how soft_to_hard_mask() transforms soft-masked sequences."""
    assert nucleotide.is_gap_sequence(nucleotide.soft_to_hard_mask(sequence))


@ht.given(st.text(alphabet=nucleotide.UPPERCASE))
def test_soft_to_hard_mask_all_uppercase(sequence: str) -> None:
    """Test how soft_to_hard_mask() transforms unmasked sequences."""
    assert nucleotide.soft_to_hard_mask(sequence) == sequence


@ht.given(st.text(alphabet=nucleotide.GAPS, min_size=1))
def test_is_gap_gaps(sequence: str) -> None:
    """Test how is_gap_sequence() processes gap sequences."""
    assert nucleotide.is_gap_sequence(sequence)


@ht.given(st.text(alphabet=nucleotide.EXACT_LETTERS))
def test_is_gap_exact_nucleotides(sequence: str) -> None:
    """Test how is_gap_sequence() processes exact nucleotide sequences."""
    assert not nucleotide.is_gap_sequence(sequence)


@ht.given(st.text(alphabet=nucleotide.EXACT_LETTERS))
def test_is_ambiguous_exact_nucleotides(sequence: str) -> None:
    """Test how is_ambiguous() processes exact nucleotide sequences."""
    assert not nucleotide.is_ambiguous(sequence)


@ht.given(
    st.integers(min_value=0, max_value=1_000),
    st.text(alphabet=nucleotide.EXACT_LETTERS),
)
def test_gap_prefix_len_gap(prefix_len: int, non_gap_sequence: str) -> None:
    """Test how gap_prefix_len() processes sequences with gap prefixes."""
    gap_prefix = str.join("", random.choices(nucleotide.GAPS, k=prefix_len))
    assert nucleotide.gap_prefix_len(gap_prefix + non_gap_sequence) == prefix_len


@ht.given(
    st.integers(min_value=0, max_value=1_000),
    st.text(alphabet=nucleotide.EXACT_LETTERS),
)
def test_gap_suffix_len_gap(suffix_len: int, non_gap_sequence: str) -> None:
    """Test how gap_suffix_len() processes sequences with gap suffixes."""
    gap_suffix = str.join("", random.choices(nucleotide.GAPS, k=suffix_len))
    assert nucleotide.gap_suffix_len(non_gap_sequence + gap_suffix) == suffix_len


@ht.given(st.text(alphabet=nucleotide.EXACT_LETTERS))
def test_gap_prefix_len_and_gap_suffix_len(sequence: str) -> None:
    """Test consistency between gap_prefix_len() and gap_suffix_len()."""
    assert nucleotide.gap_prefix_len(sequence) == nucleotide.gap_suffix_len(
        sequence[::-1]
    )
