"""Test module pygenomics.wiggle."""

import io
from typing import List

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import wiggle
from tests import common

HT_CHROMS = common.HT_NONEMPTY_SEQNAME
HT_STARTS = st.integers(min_value=1)
HT_STEPS = st.integers(min_value=1)
HT_SPANS = st.integers(min_value=1)

HT_VARIABLE_STEP_DECLARATIONS = st.tuples(HT_CHROMS, HT_SPANS).map(
    lambda x: wiggle.VariableStepDeclaration(x[0], x[1])
)

HT_VARIABLE_STEP_LINES = st.tuples(HT_STARTS, common.HT_COVERAGE_VALUES).map(
    lambda x: wiggle.VariableStepData(x[0], x[1])
)

HT_FIXED_STEP_DECLARATIONS = st.tuples(HT_CHROMS, HT_STARTS, HT_STEPS, HT_SPANS).map(
    lambda x: wiggle.FixedStepDeclaration(x[0], x[1], x[2], x[3])
)


HT_FIXED_STEP_BLOCKS = st.tuples(
    HT_FIXED_STEP_DECLARATIONS, st.lists(common.HT_COVERAGE_VALUES)
).map(lambda x: wiggle.FixedStepBlock(x[0], x[1]))

HT_VARIABLE_STEP_BLOCKS = st.tuples(
    HT_VARIABLE_STEP_DECLARATIONS, st.lists(HT_VARIABLE_STEP_LINES)
).map(lambda x: wiggle.VariableStepBlock(x[0], x[1]))

HT_BLOCKS = st.lists(st.one_of(HT_FIXED_STEP_BLOCKS, HT_VARIABLE_STEP_BLOCKS))


@ht.given(HT_VARIABLE_STEP_DECLARATIONS)
def test_variable_step_declaration_of_list(
    record: wiggle.VariableStepDeclaration,
) -> None:
    """Test of_list() using correct variable step declaration records."""
    assert wiggle.VariableStepDeclaration.of_list(str.split(str(record))) == record


@ht.given(HT_VARIABLE_STEP_LINES)
def test_variable_step_data_of_list(record: wiggle.VariableStepData) -> None:
    """Test of_list() using correct variable step data records."""
    assert wiggle.VariableStepData.of_list(str.split(str(record))) == record


@ht.given(HT_FIXED_STEP_DECLARATIONS)
def test_fixed_step_declaration_of_list(record: wiggle.FixedStepDeclaration) -> None:
    """Test of_list() using correct foxed step declaration records."""
    assert wiggle.FixedStepDeclaration.of_list(str.split(str(record))) == record


@ht.given(HT_BLOCKS)
def test_read_write(records: List[wiggle.Block]) -> None:
    """Test read() and write() functions for wiggle blocks."""
    stream = io.StringIO()
    for k in records:
        wiggle.write(stream, k)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(records, wiggle.read(io.StringIO(contents))))
