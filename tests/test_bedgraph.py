"""Test module pygenomics.bedgraph."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import bedgraph
from tests import test_bed
from tests.common import HT_COVERAGE_VALUES

HT_BEDGRAPH = st.tuples(test_bed.HT_BED3, HT_COVERAGE_VALUES).map(
    lambda x: bedgraph.Record(x[0], x[1])
)


@ht.given(HT_BEDGRAPH)
def test_record_of_string(record: bedgraph.Record) -> None:
    """Test Record.of_string() using correct records."""
    assert bedgraph.Record.of_string(str(record)) == record
