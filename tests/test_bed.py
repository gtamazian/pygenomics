"""Test module pygenomics.bed."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics import bed
from tests import common

HT_INTERVALS = st.tuples(st.integers(min_value=0), st.integers(min_value=1)).map(
    lambda x: (x[0], x[0] + x[1])
)

HT_BED3 = st.tuples(common.HT_NONEMPTY_SEQNAME, HT_INTERVALS).map(
    lambda x: bed.Bed3(x[0], bed.Range(x[1][0], x[1][1]))
)

HT_BED4 = st.tuples(HT_BED3, common.HT_NONEMPTY_SEQNAME).map(
    lambda x: bed.Bed4(x[0].chrom, x[0].record_range, x[1])
)

HT_BED6 = st.tuples(
    HT_BED4, st.integers(min_value=0, max_value=1000), common.HT_STRANDS
).map(lambda x: bed.Bed6(x[0].chrom, x[0].record_range, x[0].name, x[1], x[2]))

HT_BED9 = st.tuples(HT_BED6, common.HT_NONEMPTY_SEQNAME).map(
    lambda x: bed.Bed9(
        x[0].chrom,
        x[0].record_range,
        x[0].name,
        x[0].score,
        x[0].strand,
        x[0].record_range,
        x[1],
    )
)


@ht.given(st.one_of(HT_BED3, HT_BED4, HT_BED6, HT_BED9).map(bed.Record))
def test_record_of_string(record: bed.Record) -> None:
    """Test Record.of_string() using correct records."""
    assert bed.Record.of_string(str(record)) == record
