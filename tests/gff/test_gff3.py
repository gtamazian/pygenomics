"""Test module gff.gff3."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.gff import gff3
from tests import common
from tests.gff.common import HT_GFF_BASES

ATTR_KEY_SYMBOLS = [
    k for k in common.PRINTABLE_NON_WHITESPACE if k not in {'"', "=", ";"}
]

HT_GFF3_ATTRIBUTES = st.lists(
    st.tuples(
        st.text(alphabet=ATTR_KEY_SYMBOLS, min_size=1, max_size=3),
        st.text(),
    ),
).map(gff3.Attributes)

HT_GFF3_RECORDS = st.tuples(HT_GFF_BASES, HT_GFF3_ATTRIBUTES).map(
    lambda x: gff3.Record(x[0], x[1])
)


@ht.given(HT_GFF3_RECORDS)
def test_of_string(record: gff3.Record) -> None:
    """Test gff3.Record.of_string() using correct records."""
    assert gff3.Record.of_string(str(record)) == record
