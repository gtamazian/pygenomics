"""Common routines for testing gff modules."""

import hypothesis.strategies as st

from pygenomics.gff import base
from tests import common

HT_SOURCES = st.one_of(
    common.HT_NONEMPTY_SEQNAME.filter(lambda x: x != "."), st.just(None)
)

HT_RANGES = st.tuples(st.integers(min_value=1), st.integers(min_value=1)).map(
    lambda x: base.Range(x[0], x[0] + x[1])
)

HT_PHASES = st.one_of(st.sampled_from([0, 1, 2]).map(base.Phase), st.just(None))

HT_GFF_BASES = st.tuples(
    common.HT_NONEMPTY_SEQNAME,
    HT_SOURCES,
    common.HT_NONEMPTY_SEQNAME,
    HT_RANGES,
    st.integers(),
    common.HT_STRANDS,
    HT_PHASES,
).map(lambda x: base.Record(x[0], x[1], x[2], x[3], float(x[4]), x[5], x[6]))
