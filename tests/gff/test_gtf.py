"""Test module gff.gtf."""

import hypothesis as ht
import hypothesis.strategies as st

from pygenomics.gff import gtf
from tests import common
from tests.gff.common import HT_GFF_BASES

ATTRIBUTE_SYMBOLS = [k for k in common.PRINTABLE_NON_WHITESPACE if k != '"']

HT_GTF_ATTRIBUTES = st.lists(
    st.tuples(
        st.text(alphabet=[k for k in ATTRIBUTE_SYMBOLS if k != ";"], min_size=1),
        st.text(alphabet=ATTRIBUTE_SYMBOLS),
    )
).map(gtf.Attributes)

HT_GTF_RECORDS = st.tuples(HT_GFF_BASES, HT_GTF_ATTRIBUTES).map(
    lambda x: gtf.Record(x[0], x[1])
)


@ht.given(HT_GTF_RECORDS)
def test_of_string(record: gtf.Record) -> None:
    """Test gtf.Record.of_string() using correct records."""
    assert gtf.Record.of_string(str(record)) == record
