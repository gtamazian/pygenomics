"""Test module pygenomics.fastq."""

import io
from typing import List

import hypothesis as ht
import hypothesis.strategies as st
import pytest

from pygenomics import fastq, nucleotide
from tests import common

RECORD_STRATEGY = (
    st.integers(min_value=0, max_value=10)
    .flatmap(
        lambda n: st.tuples(
            common.HT_SEQNAME,
            st.text(alphabet=nucleotide.LETTERS, min_size=n, max_size=n),
            st.text(alphabet=common.PRINTABLE_NON_WHITESPACE, min_size=n, max_size=n),
        )
    )
    .map(lambda x: fastq.Record(x[0], x[1], x[2]))
)


@ht.given(st.lists(RECORD_STRATEGY))
def test_read_write(records: List[fastq.Record]) -> None:
    """Test read() and write()."""
    stream = io.StringIO()
    for k in records:
        fastq.write(stream, k)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    assert all(j == k for j, k in zip(records, fastq.read(io.StringIO(contents))))


def test_read_empty() -> None:
    """Test that read() correctly processes the empty input."""
    assert not list(fastq.read(io.StringIO("")))


@ht.given(st.text(alphabet=st.characters(blacklist_characters=["@"]), min_size=1))
def test_read_incorrect_header(line: str) -> None:
    """Test that read() catches an incorrect FASTQ record header."""
    with pytest.raises(fastq.ReadingError) as fastq_error:
        list(fastq.read(io.StringIO(line)))
    assert fastq_error.value.error_type is fastq.ReadingErrorType.INCORRECT_HEADER


@ht.given(RECORD_STRATEGY, st.characters(blacklist_characters=["+"]))
def test_read_incorrect_separator(record: fastq.Record, sep: str) -> None:
    """Test that reads() detects an incorrect separator line."""
    stream = io.StringIO()
    print("@", record.name, sep="", file=stream)
    print(record.seq, file=stream)
    print(sep, file=stream)
    print(record.qual, file=stream)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    with pytest.raises(fastq.ReadingError) as fastq_error:
        list(fastq.read(io.StringIO(contents)))
    assert fastq_error.value.error_type is fastq.ReadingErrorType.INCORRECT_SEPARATOR


@ht.given(RECORD_STRATEGY, st.integers(min_value=1, max_value=3))
def test_read_missing_lines(record: fastq.Record, num_missing_lines: int) -> None:
    """Test that read() detects missing lines in a FASTQ record."""
    stream = io.StringIO()
    print("@", record.name, sep="", file=stream)
    lines = [record.seq, "+", record.qual]
    for k in range(3 - num_missing_lines):
        print(lines[k], file=stream)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    with pytest.raises(fastq.ReadingError) as fastq_error:
        list(fastq.read(io.StringIO(contents)))
    assert fastq_error.value.error_type is fastq.ReadingErrorType.MISSING_LINES


@ht.given(RECORD_STRATEGY, RECORD_STRATEGY)
def test_read_different_seq_qual_len(first: fastq.Record, second: fastq.Record) -> None:
    """Test that read() detects inconsistent sequence and quality lines."""
    ht.assume(len(first.seq) != len(second.seq))
    stream = io.StringIO()
    print("@", first.name, sep="", file=stream)
    print(first.seq, file=stream)
    print("+", file=stream)
    print(second.qual, file=stream)

    contents = io.StringIO.getvalue(stream)
    stream.close()

    with pytest.raises(fastq.ReadingError) as fastq_error:
        list(fastq.read(io.StringIO(contents)))
    assert fastq_error.value.error_type is fastq.ReadingErrorType.DIFFERENT_SEQ_QUAL_LEN
