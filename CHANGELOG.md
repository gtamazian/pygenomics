# Changelog

All notable changes to _pygenomics_ will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1] - 2023-01-22

### Added

- Use [_Poetry_](https://python-poetry.org) for dependency management and packaging
- Use [_tox_](https://tox.wiki) for running tests and static code checks
- Use [_isort_](https://pycqa.github.io/isort/) for formatting import statements
- Use [_flake8_](https://flake8.pycqa.org) for checking code style
- Add console script entry point named after the package

### Changed

- Expand the package documentation
- Change the project layout from [the "flat layout" to the "src layout"](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/)
- Move the project configuration to `pyproject.toml` and `tox.ini`
- Require command names in the CLI
- Call [_radon_](https://radon.readthedocs.io) by [_flake8_](https://flake8.pycqa.org)

### Fixed

- Fix source code formatting
- Fix some search strategies for property-based testing

## [0.1.0] - 2022-06-24

### Added

- Initial release of the package

[0.1.1]: https://gitlab.com/gtamazian/pygenomics/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/gtamazian/pygenomics/-/tags/0.1.0
